/**
 * SystemManagement.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    ram: {
      type: "string",
    },
    drives: {
      type: "string",
      isIn: ["HDD", "SSD"],
      defaultsTo: "HDD",
    },
    drives_size: {
      type: 'string'
    },
    operating_system: {
      type: "string",
      isIn: ["windows", "linux", "mac"],
      defaultsTo: "linux",
    },
    os_version: {
      type: "string",
    },
    os_type: {
      type: 'string'
    },
    processor: {
      type: 'string'
    },
    cpu_serial_number: {
      type: 'string'
    },
    cpu_code: {
      type: 'string'
    },
    screen_serial_number: {
      type: 'string'
    },
    screen_code: {
      type: 'string'
    },
    mouse_serial_number: {
      type: 'string'
    },
    mouse_code: {
      type: 'string'
    },
    keyboard_serial_number: {
      type: 'string'
    },
    keyboard_code: {
      type: 'string'
    },
    ups_serial_number: {
      type: 'string'
    },
    ups_code: {
      type: 'string'
    },
    user_name: {
      type: 'string'
    },
    system_password: {
      type: 'string'
    },
    seat_number: {
      type: 'number'
    },
    smps: {
      type: 'string'
    },
    battery: {
      type: 'string'
    },
    status: {
      type: 'string',
      isIn: ['active', 'deactive'],
      defaultsTo: 'active'
    },
    isDeleted: {
      type: "Boolean",
      defaultsTo: false,
    },
    deletedBy: {
      model: "users",
    },
    deletedAt: {
      type: "ref",
      columnType: "datetime",
    },
    updatedBy: {
      model: "users",
    },
    addedBy: {
      model: "Users",
    },
    createdAt: {
      type: "ref",
      autoCreatedAt: true,
    },
    updatedAt: {
      type: "ref",
      autoUpdatedAt: true,
    },
    ip: {
      type: 'string'
    },
    system: {
      type: 'string'
    },
    laptopName: {
      type: 'string'
    },
    isAssignable: {
      type: 'string',
      isIn: ["Y", "N"],
      defaultsTo: "N",
    },
    laptopId: {
      type: 'string'
    },
    description: {
      type: "string",
    },



  },

};

