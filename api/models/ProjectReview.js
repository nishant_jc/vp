/**
 * ProjectReview.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    employeeId: {
      model: 'users'
    },
    projectId: {
      model: 'projects'
    },
    reviewType: {
      type: 'string'
    },
    updatedBy: {
      model: "users",
    },
    updatedAt: {
      type: "ref",
      autoUpdatedAt: true,
    },
    createdAt: {
      type: "ref",
      autoCreatedAt: true,
    },
    isDeleted: {
      type: "Boolean",
      defaultsTo: false,
    },

  },

};

