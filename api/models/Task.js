/**
 * Task.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    taskName:{
      type:"string"
    },
    project:{
      model:"projects"
    },
    Actual_time:{
      type: "ref",
      columnType: "datetime",
    },
    expected_time:{
      type: "ref",
      columnType: "datetime",
    },
    expectedhours:{
      type:"string"
    },  
    expectedminutes:{
      type:"string"
    },
    Actual_hours:{
      type:"string"
    },  
    Actual_minutes:{
      type:"string"
    },
    reason: {
      type: "string",
    },
    assignDateAndTime:{
      type: "ref",
      columnType: "datetime",
    },
    submitDateAndTime:{
      type: "ref",
      columnType: "datetime",
    },
    employee:{
      model:"users"
    },
    isDeleted: {
      type: "Boolean",
      defaultsTo: false,
    },
    deletedBy: {
      model: "users",
    },
    deletedAt: {
      type: "ref",
      columnType: "datetime",
    },
    addedBy: {
      model: "Users",
    },
    submitBy: {
      model: "Users",
    },
    status: {
      type: "string",
      isIn: ["done", "inProgress","pending","blocker","hold","completed","assigned"],
      defaultsTo: "inProgress",
    },
    description: {
      type: "string",
    },
    createdAt: {
      type: "ref",
      autoCreatedAt: true,
    },
    updatedAt: {
      type: "ref",
      autoUpdatedAt: true,
    },

  },

};

