/**
 * ReviewTask.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    expectedminutes:{
      type: "ref",
    },

    expectedhours:{
      type: "ref",
    },
    taskName:{
      type:'string'
    },
    description:{
      type:'string'
    },
    project:{
      model:'projects'
    },
    employee:{
      model:'users'
    },
    reviewType:{
      type:'string',
    },
    addedBy: {
      model: "Users",
    },  
    createdAt: {
      type: "ref",
      autoCreatedAt: true,
    },
    updatedAt: {
      type: "ref",
      autoUpdatedAt: true,
    },
    isDeleted: {
      type: 'Boolean',
      defaultsTo: false
  },
  status: {
    type: 'string',
    isIn: ['active', 'deactive'],
    defaultsTo: 'active'
  },
  actualHours:{
    type:"ref"
  },
  actualminutes:{
    type: "ref",
  },
  reason:{
    type: 'string'
  }

  },

};

