/**
 * Weeklyplan.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    project_id: {
      model: "Projects",
    },
    startDate: {
      type: 'string', 
    },
    endDate: {
      type: 'string', 
    },
    documents: {
      type: "json",
      defaultsTo: [],
    },
    tasks:{
      type:'json',
      defaultsTo:[]
    },
    isDeleted: {
      type: 'Boolean',
      defaultsTo: false
    },
    deletedBy:{
      model:'users'
    },
    deletedAt:{
      type: 'ref',
      columnType: 'datetime'
    },
    addedBy: {
      model: 'Users'
    },
    status: {
      type: 'string',
      isIn: ['active', 'deactive'],
      defaultsTo: 'active'
    },
    createdAt: {
      type: 'ref',
      autoCreatedAt: true
    },
    updatedAt: {
      type: 'ref',
      autoUpdatedAt: true
    },
  }
};


