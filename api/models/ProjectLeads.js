/**
 * ProjectLeads.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  schema: true,
  attributes: {
    title:{type:'string'},
    status:{type:'string'},
    upworkurl: { type: 'string' },
    country: { type: 'string' },
    technology: { type: 'string' },
    date: { type: "ref", columnType: "datetime", },
    addedBy: { model: 'users' },
    isDeleted: { type: 'Boolean', defaultsTo: false },
    deletedBy: { model: 'users' },
    updatedBy: { model: 'users' },
    createdAt: { type: "ref", autoCreatedAt: true },
    updatedAt: { type: "ref", autoUpdatedAt: true },

  },

};

