/**
 * Interview.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    wayOfInterview:{
      type:'string'
    },
    employeeId:{
      model:'users'
    },
    startTime:{
      type:'string'
    },
    InterviewDateAndTime:{
      type:'ref'
    },
    contactNumber:{
      type:'string'
    },
    resume:{
      type:'string'
    },     
    interviewLink:{
      type:'string'
    },  
    deletedAt: {
      type: "ref",
      columnType: "datetime",
    },
    addedBy: {
      model: "Users",
    },
    createdAt: {
      type: "ref",
      autoCreatedAt: true,
    },
    updatedAt: {
      type: "ref",
      autoUpdatedAt: true,
    },
    isDeleted: {
      type: "Boolean",
      defaultsTo: false,
    },
  },

};

