/**
 * Projects.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  primaryKey: "id",
  // sd
  attributes: {
    name: {
      type: "string",
    },
    description: {
      type: "string",
    },
    image: {
      type: "json",
      defaultsTo: [],
    },
    category_id: {
      model: "Category",
    },
    totaltime: {
      type: "string",
    },
    frontendtechnology: {
      type: "string",
    },
    backendtechnology: {
      type: "string",
    },
    startDate: {
      type: "string",
    },
    endDate: {
      type: "string",
    },
    platform: {
      model: "JobPlatform",
    },
    platformId: {
      model: "Users",
    },
    teams: {
      type: "json",
      defaultsTo: [],
    },
    members: {
      type: "json",
      defaultsTo: [],
    },
    documents: {
      type: "json",
      defaultsTo: [],
    },
    assign_member: {
      model: "Users",
    },
    progress_status: {
      type: "string",
    },
    reason: {
      type: "string",
    },
    feedback: {
      type: "string",
    },
    project_type: {
      type: "string",
      isIn: ["Hourly", "Fixed"],
      defaultsTo: "Hourly",
    },
    planRisk: {
      type: "json",
      defaultsTo: [],
    },
    clientName: {
      type: "string",
    },
    clientSkypeId: {
      type: "string",
    },
    clientPhoneNo: {
      type: "number"
    },
    clientCity: {
      type: "string"
    },
    clientCountry: {
      type: "string"
    },
    status: {
      type: "string",
      isIn: ["true", "false"],
      defaultsTo: "true"
    },
    projectStatus: {
      type: "string",
      isIn: ["awarded", "rejected", "ended", "onhold","rejected","completed","inProgress","notStarted","goingon"],
      defaultsTo: "inProgress"
    },
    websiteUrl: {
      type: 'string'
    },
    git: {
      type: 'string',
      isIn: ["github", "bitbucket"],
      defaultsTo: "bitbucket"
    },
    frontend_clone_link: {
      type: 'string',
    },
    frontend_clone_password: {
      type: 'string'
    },
    admin_clone_link: {
      type: 'string',
    },
    admin_clone_password: {
      type: 'string'
    },
    backend_clone_link: {
      type: 'string',
    },
    backend_clone_password: {
      type: 'string'
    },
    thirdparty_clone_link: {
      type: 'string',
    },
    thirdparty_clone_password: {
      type: 'string'
    },
    updated_branch_api: {
      type: 'string'
    },
    updated_branch_admin: {
      type: 'string'
    },
    updated_branch_frontend: {
      type: 'string'
    },
    frontend_repository_name: {
      type: 'string'
    },
    admin_repository_name: {
      type: 'string'
    },
    api_repository_name: {
      type: 'string'
    },
    frontend_owner_workspace_id: {
      type: 'string'
    },
    api_owner_workspace_id: {
      type: 'string'
    },
    admin_owner_workspace_id: {
      type: 'string'
    },
    is_frontend_updated_branch_merged_with_master: {
      type: "string",
      isIn: ["Y", "N"],
      defaultsTo: "N",
    },
    is_admin_updated_branch_merged_with_master: {
      type: "string",
      isIn: ["Y", "N"],
      defaultsTo: "N",
    },
    is_api_updated_branch_merged_with_master: {
      type: "string",
      isIn: ["Y", "N"],
      defaultsTo: "N",
    },
    isDeleted: {
      type: "Boolean",
      defaultsTo: false,
    },
    deletedBy: {
      model: "users",
    },
    deletedAt: {
      type: "ref",
      columnType: "datetime",
    },
    updatedBy: {
      model: "users",
    },
    addedBy: {
      model: "Users",
    },
    createdAt: {
      type: "ref",
      autoCreatedAt: true,
    },
    updatedAt: {
      type: "ref",
      autoUpdatedAt: true,
    },
    figmaLink:{
      type:"string"
    },
    uploadDocuments: {
      type: "json",
      defaultsTo: [],
    },


  },
};

