/**
 * Salary.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    employeeId: {
      model: "users",
    },
    salary: {
      type: "string",
    },
    account_no: {
      type: "string",
    },
    ifsc: {
      type: "string",
    },
    bank_name: {
      type: "string",
    },
    professional_tax: {
      type: "string",
      defaultsTo: "0",
    },
    grand_total:{type:"string"},
    net_amount:{type:"string"},
    deduction: { type: "string", defaultsTo: "0" },
    working_days: { type: "string", defaultsTo: "0" },
    isDeleted: {
      type: "Boolean",
      defaultsTo: false,
    },
    deletedBy: {
      model: "users",
    },
    deletedAt: {
      type: "ref",
      columnType: "datetime",
    },
    updatedBy: {
      model: "users",
    },
    addedBy: {
      model: "Users",
    },
    createdAt: {
      type: "ref",
      columnType: "datetime",
      autoCreatedAt: true,
    },
    updatedAt: {
      type: "ref",
      columnType: "datetime",
      autoUpdatedAt: true,
    },
  },
};
