/**
 * Inventory.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    category_id: {
      model: 'category'
    },
    code: {
      type: 'string'
    },
    serial_number: {
      type: 'string'
    },
    replace_code: {
      type: 'string'
    },
    replace_serial_number: {
      type: 'string'
    },
    status: {
      type: 'string',
      isIn: ['active', 'deactive'],
      defaultsTo: 'active'
    },
    image: {
      type: "json",
      defaultsTo: [],
    },
    InventoryImage:{
      type: 'string'
    },
    isDeleted: {
      type: "Boolean",
      defaultsTo: false,
    },
    isReplaced: {
      type: "Boolean",
      defaultsTo: false,
    },
    deletedBy: {
      model: "users",
    },
    deletedAt: {
      type: "ref",
      columnType: "datetime",
    },
    updatedBy: {
      model: "users",
    },
    addedBy: {
      model: "Users",
    },
    createdAt: {
      type: "ref",
      autoCreatedAt: true,
    },
    updatedAt: {
      type: "ref",
      autoUpdatedAt: true,
    },

  },

};

