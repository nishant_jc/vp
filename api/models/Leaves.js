/**
 * Leaves.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  primaryKey: "id",
  attributes: {
    employeeName: {
      type: "string",
    },
    fromEmail: {
      type: "string",
      isEmail: true,
    },
    toEmail: {
      type: "json",
      defaultsTo: [],
    },
    subject: {
      type: "string",
    },
    description: {
      type: "string",
    },
    attachment: {
      type: "json",
      defaultsTo: [],
    },
    leave_status: {
      type: "string",
      isIn: ["Accepted", "Rejected", "Cancel", "Pending"],
      defaultsTo: "Pending",
    },
    leaveStartDate: {
      type: "ref",
      columnType: "datetime",
    },
    leaveEndDate: {
      type: "ref",
      columnType: "datetime",
    },
     leaveStarttime: {
      type: "string"
    },
    leaveEndtime: {
      type: "string",
    },
    status: {
      type: "string",
      isIn: ["active", "deactive"],
      defaultsTo: "active",
    },
    no_of_days: {
      type: "number",
      defaultsTo: 0
    },
    paid: {
      type: "number",
      defaultsTo: 0
    },
    deduction: {
      type: "number",
      defaultsTo: 0
    },
    short_leave_count: {
      type: "number",
      defaultsTo: 0
    },
    approvedBy: {
      type: "string",
    },
    isDeleted: {
      type: "Boolean",
      defaultsTo: false,
    },
    deletedBy: {
      model: "users",
    },
    deletedAt: {
      type: "ref",
      columnType: "datetime",
    },
    updatedBy: {
      model: "users",
    },
    addedBy: {
      model: "Users",
    },
    createdAt: {
      type: "ref",
      autoCreatedAt: true,
    },
    updatedAt: {
      type: "ref",
      autoUpdatedAt: true,
    },
  },
};

