/**
 * Resignation.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  primaryKey: "id",
  attributes: {
    employeeName: {
      type: "string",
    },
    fromEmail: {
      type: "string",
      isEmail: true,
    },
    toEmail: {
      type: "json",
      defaultsTo: [],
    },
    subject: {
      type: "string",
    },
    description: {
      type: "string",
    },
    resign_status: {
      type: "string",
      isIn: ["Accepted", "Rejected", "Cancelled", "Pending"],
      defaultsTo: "Pending",
    },
    resignationStartDate: {
      type: "ref",
      autoCreatedAt: true,
    },
    resignationEndDate: {
      type: "ref",
      columnType: "datetime",
    },
    status: {
      type: "string",
      isIn: ["active", "deactive"],
      defaultsTo: "active",
    },
    isDeleted: {
      type: "Boolean",
      defaultsTo: false,
    },
    deletedBy: {
      model: "users",
    },
    deletedAt: {
      type: "ref",
      columnType: "datetime",
    },
    updatedBy: {
      model: "users",
    },
    addedBy: {
      model: "users",
    },
    createdAt: {
      type: "ref",
      autoCreatedAt: true,
    },
    updatedAt: {
      type: "ref",
      autoUpdatedAt: true,
    },
  },
};

