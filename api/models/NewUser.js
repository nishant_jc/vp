/**
 * NewUser.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

 
  attributes: {

    fullName: { type: "string" },
    email: { type: "string",isEmail: true },
    designation: { type: "string" },
    status: { type: "string",isIn: ["deactive", "active"], defaultsTo: "active" },
    isDeleted: { type: "Boolean", defaultsTo: false},

  },

};

