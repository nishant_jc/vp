/**
 * Attendance.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  primaryKey: "id",
  attributes: {
    checkIn: {
      type: "ref",
      columnType: "datetime",
    },
    checkOut: {
      type: "ref",
      columnType: "datetime",
    },
    userId: {
      model: 'users'
    },
    name: {
      type: "string",
    },
    email: {
      type: "string",
    },
    totalTime: {
      type: "string",
    },
    totalseconds: {
      type: "string",
    },
    attendanceDate: {
      type: "ref",
      columnType: "datetime",
    },
    punchInDate: {
      type: "ref",
      columnType: "datetime",
    },
    reason: {
      type: "string",
    },
    status: {
      type: 'string',
      isIn: ['active', 'deactive'],
      defaultsTo: 'active'
    },

    isDeleted: {
      type: 'Boolean',
      defaultsTo: false
    },
    deletedBy: {
      model: 'users'
    },
    deletedAt: {
      type: 'ref',
      columnType: 'datetime'
    },
    updatedBy: {
      model: 'users'
    },
    addedBy: {
      model: 'Users'
    },
    createdAt: {
      type: 'ref',
      autoCreatedAt: true
    },
    updatedAt: {
      type: 'ref',
      autoUpdatedAt: true
    },
    check_in_device: {
      type: "string"
    },
    check_out_device: {
      type: "string"
    }
  },
};

