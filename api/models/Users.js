/**
 * Users.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

var promisify = require('bluebird').promisify;
var bcrypt = require('bcrypt-nodejs');
// var db = Users.getDatastore().manager;

// var db=sails.models["Users"].getDatastore().manager;



module.exports = {
  schema: true,

  // attributes: {
  //   employeeId: {
  //     type: "string",
  //   },
  //   firstName: {
  //     type: "string",
  //   },
  //   lastName: {
  //     type: "string",
  //   },
  //   fullName: {
  //     type: "string",
  //   },
  //   email: {
  //     type: "string",
  //     isEmail: true,
  //   },
  //   aadharCardno: {
  //     type: "number",
  //   },
  //   passportNo: {
  //     type: "string",
  //   },
  //   votercardNo: {
  //     type: "string",
  //   },
  //   aadharCardimages: {
  //     type: "json",
  //     defaultsTo: [],
  //   },
  //   passportImages: {
  //     type: "json",
  //     defaultsTo: [],
  //   },
  //   votercardImages: {
  //     type: "json",
  //     defaultsTo: [],
  //   },
  //   gmail: {
  //     type: "string",
  //   },
  //   secondaryEmail: {
  //     type: "string",
  //     isEmail: true,
  //   },
  //   designation: {
  //     type: "string",
  //   },
  //   mobileNo: {
  //     type: "ref",
  //     columnType: "bigint",
  //   },
  //   gender: {
  //     type: "string",
  //     isIn: ["M", "F"],
  //     defaultsTo: "M",
  //   },
  //   dob: {
  //     type: "string",
  //   },
  //   image: {
  //     type: "json",
  //     defaultsTo: [],
  //   },
  //   documents: {
  //     type: "json",
  //     defaultsTo: [],
  //   },
  //   timeSheet: {
  //     type: "json",
  //     defaultsTo: [],
  //   },
  //   password: {
  //     type: "string",
  //     columnName: "encryptedPassword",
  //     minLength: 8,
  //   },
  //   fatherName: {
  //     type: "string",
  //   },
  //   motherName: {
  //     type: "string",
  //   },
  //   primaryAddress: {
  //     type: "string",
  //   },
  //   primaryAddress1: {
  //     type: "string",
  //   },
  //   primaryAddress2: {
  //     type: "string",
  //   },
  //   primaryCity: {
  //     type: "string",
  //   },
  //   primaryState: {
  //     type: "string",
  //   },
  //   primaryZipcode: {
  //     type: "number",
  //   },
  //   primaryCountry: {
  //     type: "string",
  //   },
  //   secondaryAddress: {
  //     type: "string",
  //   },
  //   secondaryAddress1: {
  //     type: "string",
  //   },
  //   secondaryAddress2: {
  //     type: "string",
  //   },
  //   secondaryCity: {
  //     type: "string",
  //   },
  //   secondaryState: {
  //     type: "string",
  //   },
  //   secondaryZipcode: {
  //     type: "number",
  //   },
  //   secondaryCountry: {
  //     type: "string",
  //   },
  //   contactNumber: {
  //     type: "ref",
  //     columnType: "bigint",
  //   },
  //   facebookURL: {
  //     type: "string",
  //   },
  //   instagramURL: {
  //     type: "string",
  //   },
  //   linkedinURL: {
  //     type: "string",
  //   },
  //   documentImages: {
  //     type: "json",
  //     defaultsTo: [],
  //   },
  //   isVerified: {
  //     type: "string",
  //     isIn: ["Y", "N"],
  //     defaultsTo: "N",
  //   },
  //   role: {
  //     type: "string",
  //     defaultsTo: "user",
  //     isIn: ["user", "employee", "HR", "manager", "admin", "site_admin","portal_admin","bda","Employees"]
  //   },
  //   verificationCode: {
  //     type: "string",
  //   },

  //   otp: {
  //     type: "string",
  //   },
  //   checkInstatus: {
  //     type: "string",
  //     isIn: ["true", "false"],
  //     defaultsTo: "false"
  //   },
  //   status: {
  //     type: "string",
  //     isIn: ["true", "false"],
  //     defaultsTo: "true"
  //   },
  //   domain: {
  //     type: "string",
  //     isIn: ["web", "ios", "andriod"],
  //     defaultsTo: "web",
  //   },

  //   isDeleted: {
  //     type: "Boolean",
  //     defaultsTo: false,
  //   },

  //   dateOfJoining: {
  //     type: "ref",
  //     columnType: "datetime",
  //   },
  //   dateOfResignation: {
  //     type: "ref",
  //     columnType: "datetime",
  //   },
  //   dateOfRelieving: {
  //     type: "ref",
  //     columnType: "datetime",
  //   },
    // appraisalDate:{
    //   type: "ref",
    //   columnType: "datetime",
    // },
  //   addedBy: {
  //     model: "users",
  //   },
  //   deletedBy: {
  //     model: "users",
  //   },
  //   deletedAt: {
  //     type: "ref",
  //     columnType: "datetime",
  //   },
  //   updatedBy: {
  //     model: "users",
  //   },
  //   createdAt: {
  //     type: "ref",
  //     autoCreatedAt: true,
  //   },
  //   updatedAt: {
  //     type: "ref",
  //     autoUpdatedAt: true,
  //   },
  //   lastlogin:{
  //     type: "string",
  //   }
  // },
  attributes: {
    employeeId: {
      type: "string",
    },
    firstName: {
      type: "string",
    },
    lastName: {
      type: "string",
    },
    fullName: {
      type: "string",
    },
    email: {
      type: "string",
      isEmail: true,
    },
    secondaryEmail: {
      type: "string",
      isEmail: true,
    },
    designation: {
      type: "string",
    },
    mobileNo: {
      type: "ref",
      columnType: "bigint",
    },
    gmail: {
      type: "string",
    },
    gender: {
      type: "string",
      isIn: ["M", "F","O"],
      defaultsTo: "M",
    },
    dob: {
      type: "string",
    },
    image: {
      type: "json",
      defaultsTo: [],
    },
    timeSheet: {
      type: "json",
      defaultsTo: [],
    },
    password: {
      type: "string",
      columnName: "encryptedPassword",
      minLength: 8,
    },
    fatherName: {
      type: "string",
    },
    motherName: {
      type: "string",
    },
    primaryAddress: {
      type: "string",
    },
    primaryAddress1: {
      type: "string",
    },
    primaryAddress2: {
      type: "string",
    },
    primaryCity: {
      type: "string",
    },
    primaryState: {
      type: "string",
    },
    primaryZipcode: {
      type: "number",
    },
    primaryCountry: {
      type: "string",
    },
    secondaryAddress: {
      type: "string",
    },
    secondaryAddress1: {
      type: "string",
    },
    secondaryAddress2: {
      type: "string",
    },
    secondaryCity: {
      type: "string",
    },
    secondaryState: {
      type: "string",
    },
    secondaryZipcode: {
      type: "number",
    },
    secondaryCountry: {
      type: "string",
    },
    contactNumber: {
      type: "ref",
      columnType: "bigint",
    },
    facebookURL: {
      type: "string",
    },
    instagramURL: {
      type: "string",
    },
    linkedinURL: {
      type: "string",
    },
    twitterURL: {
      type: "string",
    },
    documentImages: {
      type: "json",
      defaultsTo: [],
    },
    salary:{
      type:"string",
      defaultsTo:"0"
    },
    isVerified: {
      type: "string",
      isIn: ["Y", "N"],
      defaultsTo: "N",
    },
    role: {
      type: "string",
      defaultsTo: "developer",
      isIn: ["admin", "site_admin", "subadmin", "HR", "developer", "qa", "bde", "seo", "designer"]
      // isIn: ["user", "employee", "HR", "manager", "admin", "site_admin", "portal_admin", "bda", "Employees", "bidder"]
    },
    skill: {
      type: "json",
      defaultsTo: [],
    },
    verificationCode: {
      type: "string",
    },

    otp: {
      type: "string",
    },
    aadharCardno: {
      type: "number",
    },
    status: {
      type: "string",
      isIn: ["active", "deactive"],
      defaultsTo: "deactive",
    },
    releave: {
      type: "Boolean",
      defaultsTo: false,
    },
    releaveDocument:{
      type: "json",
      defaultsTo: [],
    },
    maritalStatus: {
      type: "string",
      isIn: ["single", "married"],
      defaultsTo: "single",
    },
    spouseName: {
      type: "string",
    },
    domain: {
      type: "string",
      isIn: ["web", "ios", "andriod"],
      defaultsTo: "web",
    },

    isDeleted: {
      type: "Boolean",
      defaultsTo: false,
    },
    checkInstatus: {
      type: "string",
      isIn: ["true", "false"],
      defaultsTo: "false"
    },

    dateOfJoining: {
      type: "ref",
      columnType: "datetime",
    },
    dateOfResignation: {
      type: "ref",
      columnType: "datetime",
    },
    dateOfRelieving: {
      type: "ref",
      columnType: "datetime",
    },
    passportNo: {
      type: "string",
    },
    votercardNo: {
      type: "string",
    },
    pancardNo: {
      type: "string",
    },
    isTL: {
      type: "Boolean",
      defaultsTo: false,
    },
    tlTechnology: {
      type: "string"
    },
    addedBy: {
      model: "users",
    },
    deletedBy: {
      model: "users",
    },
    deletedAt: {
      type: "ref",
      columnType: "datetime",
    },
    updatedBy: {
      model: "users",
    },
    createdAt: {
      type: "ref",
      autoCreatedAt: true,
    },
    updatedAt: {
      type: "ref",
      autoUpdatedAt: true,
    },
    device: {
      type: 'string'
    },
    appraisalDate:{
      type: "ref",
      columnType: "datetime",
    },
    isTrainee: {
      type: "Boolean",
      defaultsTo: false,
    },
    employee_code: {
      type: 'string'
    },


  },
  beforeCreate: function (user, next) {
    if (user.firstName && user.lastName) {
      user.fullName = user.firstName + " " + user.lastName;
    }

    if (user.hasOwnProperty("password")) {
      user.password = bcrypt.hashSync(user.password, bcrypt.genSaltSync(10));
      next(false, user);
    } else {
      next(null, user);
    }
  },
  authenticate: function (email, password) {
    console.log("in auth    ");
    var query = {};
    query.email = email;
    query.$or = [{ roles: ["SA", "A"] }];

    return Users.findOne(query)
      .populate("roleId")
      .then(function (user) {
        //return API.Model(Users).findOne(query).then(function(user){
        return user && user.date_verified && user.comparePassword(password)
          ? user
          : null;
      });
  },
  customToJSON: function () {
    // Return a shallow copy of this record with the password and ssn removed.
    return _.omit(this, ["password", "verificationCode"]);
  },
};
