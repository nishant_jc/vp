/**
 * PortfolioShare.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    selected_items: {
      type: "json",
      defaultsTo: [],
    },
    start_date: {
      type: "ref",
      columnType: "datetime",
    },
    end_date: {
      type: "ref",
      columnType: "datetime",
    },
    key_send: {
      type: 'string'
    },
    addedBy: {
      model: 'Users'
    },
    deletedBy: {
      model: 'users'
    },
    deletedAt: {
      type: 'ref',
      columnType: 'datetime'
    },
    isDeleted: {
      type: "Boolean",
      defaultsTo: false,
    },
    createdAt: {
      type: 'ref',
      autoCreatedAt: true
    },
    updatedAt: {
      type: "ref",
      autoUpdatedAt: true,
    },
  },

};

