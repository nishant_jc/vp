/**
 * PortfolioNew.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    domain: {
      type: 'string',
    },
    name: {
      type: 'string'
    },
    description: {
      type: 'string',
    },
    image: {
      type: "json",
      defaultsTo: [],
    },
    start_date: {
      type: "ref",
      columnType: "datetime",
    },
    end_date: {
      type: "ref",
      columnType: "datetime",
    },
    platform: {
      type: "string",
    },
    technology: {
      type: 'string'
    },
    updatedBy: {
      model: 'Users'
    },
    addedBy: {
      model: 'Users'
    },
    deletedBy: {
      model: 'users'
    },
    deletedAt: {
      type: 'ref',
      columnType: 'datetime'
    },
    isDeleted: {
      type: "Boolean",
      defaultsTo: false,
    },
    createdAt: {
      type: 'ref',
      autoCreatedAt: true
    },
    updatedAt: {
      type: 'ref',
      autoUpdatedAt: true
    },
  },
}

