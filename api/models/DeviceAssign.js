/**
 * DeviceAssign.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    device_name:{
      type:"string",
    },
    serial_no:{
      type:"string",
    },
    device_type:{
      type:"string",
    },
    description:{
      type:"string",
    },
    status: {
      type: "string",
      isIn: ["assigned", "accepted"],
      defaultsTo: "assigned",
    },
    employeeId:{
      model:"users"
    },
    projectId:{
      model:'projects'
    },
    startTime:{
      type:'ref',
      columnType: "datetime",
    },
    endTime:{
      type:'ref',
      columnType: "datetime",
    },
    isDeleted: {
      type: "Boolean",
      defaultsTo: false,
    },
    createdAt: {
      type: "ref",
      autoCreatedAt: true,
    },
    updatedAt: {
      type: "ref",
      autoUpdatedAt: true,
    },
    deletedBy: {
      model: "users",
    },
    updatedBy: {
      model: "users",
    },

  },

};

