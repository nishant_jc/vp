/**
 * ContactUs.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    name: {
      type: "string",
    },
    email: {
      type: "string",
    },
    description: {
      type: "string",
    },
    phone: {
      type: "string",
    },
    location: {
      type: "string",
    },
    service: { type: "string" },
    company: { type: "string" },
    subject: { type: "string" },
    message: {
      type: "string",
    },
    status: {
      type: "string",
      isIn: ["active", "deactive"],
      defaultsTo: "active",
    },
    updatedBy: {
      model: "Users",
    },
    addedBy: {
      model: "Users",
    },
    deletedBy: {
      model: "users",
    },
    deletedAt: {
      type: "ref",
      columnType: "datetime",
    },
    isDeleted: {
      type: "Boolean",
      defaultsTo: false,
    },
    createdAt: {
      type: "ref",
      autoCreatedAt: true,
    },
    updatedAt: {
      type: "ref",
      autoUpdatedAt: true,
    },
  },
};
