/**
 * Appraisal.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
    primaryKey: 'id',
    attributes: {
        employeeId: {
            type: "string",
        },
        name: {
            type: 'string',
        },
        date: {
            type: "string",
        },
        email: {
            type: "string",
            isEmail: true,
        },
        previousAmt: {
            type: "string",
        },
        appraisalAmt: {
            type: "string",
        },
        currentAmt : {
            type: "string",
        },
        status: {
            type: "string",
            isIn: ["Hold", "Inprocess", "Complete", "Pending", "Cancel"],
            defaultsTo: "Pending",
        },
        isDeleted: {
            type: "Boolean",
            defaultsTo: false,
        },
        deletedBy: {
            model: "users",
        },
        deletedAt: {
            type: "ref",
            columnType: "datetime",
        },
        updatedBy: {
            model: 'users'
        },
        addedBy: {
            model: 'Users'
        },
        createdAt: {
            type: 'ref',
            autoCreatedAt: true
        },
        updatedAt: {
            type: 'ref',
            autoUpdatedAt: true
        },

    }
};