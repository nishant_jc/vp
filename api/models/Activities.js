/**
 * Activities.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  schema: true,

  attributes: {

    firstName: {
      type: "string",
    },
    lastName: {
      type: "string",
    },
    fullName: {
      type: "string",
    },
    email: {
      type: "string",
      isEmail: true,
    },
    role: {
      type: "string",
      defaultsTo: "user",
      isIn: ["admin", "site_admin", "subadmin", "HR", "developer", "qa", "bde", "seo", "designer"]
    },
    createdAt: {
      type: "ref",
      autoCreatedAt: true,
    },

  },

};

