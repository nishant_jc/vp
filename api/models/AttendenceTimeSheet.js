/**
 * AttendenceTimeSheet.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    date: {
      type: "ref",
      columnType: "datetime",
    },
    totaltime: {
      type: "string",
    },
    totalseconds: {
      type: "string",
    },
    status: {
      type: 'string',
      isIn: ['active', 'deactive'],
      defaultsTo: 'active'
    },
    isDeleted: {
      type: 'Boolean',
      defaultsTo: false
    },
    deletedBy: {
      model: 'users'
    },
    deletedAt: {
      type: 'ref',
      columnType: 'datetime'
    },
    updatedBy: {
      model: 'users'
    },
    addedBy: {
      model: 'Users'
    },
    createdAt: {
      type: 'ref',
      autoCreatedAt: true
    },
    updatedAt: {
      type: 'ref',
      autoUpdatedAt: true
    },
    check_in_device: {
      type: "string"
    },
    check_out_device: {
      type: "string"
    },
    mobile_device_time:{
      type: "string"
    },
    desktop_device_time:{
      type: "string"
    }

  }

};

