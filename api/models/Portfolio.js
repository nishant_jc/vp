/**
 * Portfolio.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    title: {
      type: 'string',
    },
    description: {
      type: 'string',
    },
    image: {
      type: "json",
      defaultsTo: [],
    },
    status: {
      type: "string",
      isIn: ["true", "false"],
      defaultsTo: "true"
    },
    updatedBy: {
      model: 'Users'
    },
    addedBy: {
      model: 'Users'
    },
    deletedBy: {
      model: 'users'
    },
    deletedAt: {
      type: 'ref',
      columnType: 'datetime'
    },
    isDeleted: {
      type: "Boolean",
      defaultsTo: false,
    },
    createdAt: {
      type: 'ref',
      autoCreatedAt: true
    },
    updatedAt: {
      type: 'ref',
      autoUpdatedAt: true
    },
  },

};

