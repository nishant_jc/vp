/**
 * @DESC:  In this class/files job platform related functions (save, update, add comment)
 * @Request param: form data values
 * @Return : Success message with required data
 * @Author: Chandra Shekhar.
 */

var constantObj = sails.config.constants;

module.exports = {
  /**
   *
   * @param {*} data
   * @param {*} context
   * @param {*} req
   * @param {*} res
   * @returns
   * @description Used to add platform
   * @createdAt 15/09/2021
   * @createdBy : Chandra shekhar
   */
  savePlatform: (data, context, req, res) => {
    if (!data.name || typeof data.name == undefined) {
      return res.status(404).json({
        success: false,
        error: { message: constantObj.jobplatform.PLATFORM_NAME_REQUIRED },
      });
    }

    let query = {};
    query.isDeleted = false;
    query.name = data.name.toLowerCase();
    data.addedBy = context.identity.id;
    JobPlatform.findOne(query).then((typeExist) => {
      if (typeExist) {
        return res.status(404).json({
          success: false,
          error: { message: constantObj.jobplatform.PLATFORM_ALREADY_EXIST },
        });
      } else {
        data.name = data.name.toLowerCase();
        JobPlatform.create(data)
          .then((savedgroup) => {
            return res.status(200).json({
              success: true,
              message: constantObj.jobplatform.PLATFORM_SAVED,
            });
          })
          .catch((err) => {
            return res
              .status(400)
              .json({ success: false, error: { message: " " + err } });
          });
      }
    });
  },
};
