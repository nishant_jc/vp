/**
 * @DESC:  In this class/files Weeklyplan related functions (save, update, add Testimonials,delete)
 * @Request param: form data values
 * @Return : Success message with required data
 * @Author: Rohit kumar.
 */

var constantObj = sails.config.constants;
const SmtpController = require("../controllers/SmtpController");

module.exports = {
    /**
     *
     * @param {*} data
     * @param {*} context
     * @param {*} req
     * @param {*} res
     * @returns
     * @description Used to add Daliytask
     * @createdAt 08/12/2021
     * @createdBy : Rohit kumar
     */
    saveDailyTask: async (data, context, req, res) => {
        try {
            data.addedBy = context.identity.id
            DailyTask.create(data).then((savedDailyTask) => {
                return res.status(200).json({
                    success: true,
                    message: constantObj.DailyTask.DAILYTASK_SAVED,
                    data:savedDailyTask
                });
            });
        } catch (error) {
            return res.status(400).json({
                success: false,
                error: { message: error },
            });
        }
    },

    /***
     * @description : Used to update the DailyTask,
     * @createdAt :  08/12/2021
     */
    updateDailyTask: (data, context, req, res) => {
        data.updatedBy = context.identity.id
        let _id = data.id;
        DailyTask.update({ id: _id }, data).then((DailyTaskdata) => {
            return res.status(200).json({
                "success": true,
                "message": constantObj.DailyTask.UPDATED_DAILYTASK,
                data:DailyTaskdata
            });
        }).catch((err) => {
            return res.status(400).json({ "success": false, "error": { "code": 400, "message": " " + err } })
        });
    },
};