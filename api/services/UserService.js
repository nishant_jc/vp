var bcrypt = require('bcrypt-nodejs');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
const SmtpController = require('../controllers/SmtpController');
const Helper = require('../controllers/HelperController')



// var commonServiceObj = require('./commonService');
var constantObj = sails.config.constants;
var transport = nodemailer.createTransport(smtpTransport({
  host: sails.config.appSMTP.host,
  port: sails.config.appSMTP.port,
  debug: sails.config.appSMTP.debug,
  auth: {
    user: sails.config.appSMTP.auth.user, //access using /congig/appSMTP.js
    pass: sails.config.appSMTP.auth.pass
  }
}));

function validateEmail(email) {
  const domain = email.slice(email.lastIndexOf('@') + 1);
  if (domain === 'jcsoftwaresolution.com') {
    return true;
  } else {
    return false;
  }
}


emailGeneratedCode = function (options) { //email generated code
  email = options.email,
    htmlMessage = options.message;
  message = '';
  style = {
    header: `
        padding:30px 15px;
        text-align:center;
        background-color:#f2f2f2;
        `,
    body: `
        padding:15px;
        height: 100px;
        `,
    hTitle: `font-family: 'Raleway', sans-serif;
        font-size: 25px;
        height:auto;
        line-height: normal;
        font-weight: bold;
        background:none;
        padding:0;
        color:#333;
        `,
    maindiv: `
        width:600px;
        margin:auto;
        font-family: Lato, sans-serif;
        font-size: 14px;
        color: #333;
        line-height: 24px;
        font-weight: 300;
        border: 1px solid #eaeaea;
        `,
    textPrimary: `color:#3e3a6e;
        `,
    h5: `font-family: Raleway, sans-serif;
        font-size: 22px;
        background:none;
        padding:0;
        color:#333;
        height:auto;
        font-weight: bold;
        line-height:normal;
        `,
    m0: `margin:0;`,
    mb3: 'margin-bottom:15px;',
    textCenter: `text-align:center;`,
    btn: `padding:10px 30px;
        font-weight:500;
        font-size:14px;
        line-height:normal;
        border:0;
        display:inline-block;
        text-decoration:none;
        `,
    btnPrimary: `
        background-color:#3e3a6e;
        color:#fff;
        `,
    footer: `
        padding:10px 15px;
        font-weight:500;
        color:#fff;
        text-align:center;
        background-color:#000;
        `
  }

  message += `<div class="container" style="` + style.maindiv + `">
    <div class="header" style="`+ style.header + `text-align:center">
        <img src="http://198.251.65.146:4019/img/logo.png" width="150" style="margin-bottom:20px;" />
        <h3 style="`+ style.hTitle + style.m0 + `">` + options.subject + `</h3>
    </div>
    <div class="body" style="`+ style.body + `">       
        <p style="`+ style.m0 + style.mb3 + style.textCenter + `margin-bottom:20px">` + htmlMessage + `</p>
    </div>
    <div class="footer" style="`+ style.footer + `">
    &copy 2023 JC Software Solution All rights reserved.
    </div>
</div>
    `


  SmtpController.sendEmail(email, options.subject, message)



};

userVerifyLink = function (options) {
  var email = options.email
  message = '';
  style = {
    header: `
        padding:30px 15px;
        text-align:center;
        background-color:#f2f2f2;
        `,
    body: `
        padding:15px;
        height: 230px;
        `,
    hTitle: `font-family: 'Raleway', sans-serif;
        font-size: 37px;
        height:auto;
        line-height: normal;
        font-weight: bold;
        background:none;
        padding:0;
        color:#333;
        `,
    maindiv: `
        width:600px;
        margin:auto;
        font-family: Lato, sans-serif;
        font-size: 14px;
        color: #333;
        line-height: 24px;
        font-weight: 300;
        border: 1px solid #eaeaea;
        `,
    textPrimary: `color:#3e3a6e;
        `,
    h5: `font-family: Raleway, sans-serif;
        font-size: 22px;
        background:none;
        padding:0;
        color:#333;
        height:auto;
        font-weight: bold;
        line-height:normal;
        `,
    m0: `margin:0;`,
    mb3: 'margin-bottom:15px;',
    textCenter: `text-align:center;`,
    btn: `padding:10px 30px;
        font-weight:500;
        font-size:14px;
        line-height:normal;
        border:0;
        display:inline-block;
        text-decoration:none;
        `,
    btnPrimary: `
        background-color:#3e3a6e;
        color:#fff;
        `,
    footer: `
        padding:10px 15px;
        font-weight:500;
        color:#fff;
        text-align:center;
        background-color:#000;
        `
  }

  message += `<div class="container" style="` + style.maindiv + `">
    <div class="header" style="`+ style.header + `text-align:center">
        <img src="http://198.251.65.146:4019/img/logo.png" width="150" style="margin-bottom:20px;" />
        <h2 style="`+ style.hTitle + style.m0 + `">Welcome to JC Software Solution</h2>
    </div>
    <div class="body" style="`+ style.body + `">
        <h5 style="`+ style.h5 + style.m0 + style.mb3 + style.textCenter + `">Hello ` + options.fullName + `</h5>
        <p style="`+ style.m0 + style.mb3 + style.textCenter + `margin-bottom:20px;font-weight: 600">You are one step away from verifying your account and joining the our community.
        Please verify your account by clicking the link below.</p>
        <div style="`+ style.textCenter + `">
            <a style="text-decoration:none" href="` + sails.config.BACK_WEB_URL + "/verifyUser?id=" + options.id + `"><span style="` + style.btn + style.btnPrimary + `">Verify Email</span></a>
        </div>
    </div>
    <div class="footer" style="`+ style.footer + `">
    &copy 2023 JC Software Solution All rights reserved.
    </div>
</div>`

  transport.sendMail(
    {
      from: "JC Software Solution <" + sails.config.appSMTP.auth.user + ">",
      to: email,
      subject: "Email Verification",
      html: message,
    },
    function (err, info) {
      console.log(err, info);
    }
  );
}


generatePassword = function () { // action are perform to generate random password for user
  var length = 8,
    charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*_-=+;:",
    retVal = "";
  for (var i = 0, n = charset.length; i < length; ++i) {
    retVal += charset.charAt(Math.floor(Math.random() * n));
  }

  return retVal;
};

module.exports = {

  /**
   * 
   * @param {*} data 
   * @param {*} context contains detail of current user
   * @param {*} req 
   * @param {*} res 
   * @returns success,code,data, message
   */
  editProfile: (data, context, req, res) => {
    try {

      var id = data.id

      if ((!data.id) || data.id == undefined) {
        return res.status(404).json({ "success": false, "error": { "code": 404, "message": "Id required" } });
      }

      if (data.firstName && data.lastName) {
        data.fullName = data.firstName + ' ' + data.lastName;

      }

      if (typeof data.primaryAddress1 !== "undefined") {
        data.primaryAddress =
          data.primaryAddress1 +
          " " +
          data.primaryAddress2 +
          ", " +
          data.primaryCity +
          ", " +
          data.primaryState +
          ", " +
          data.primaryZipcode +
          ", " +
          data.primaryCountry;
      
      }

      if (typeof req.body.secondaryAddress1 !== "undefined") {

        data.secondaryAddress =
          data.secondaryAddress1 +
          " " +
          data.secondaryAddress2 +
          ", " +
          data.secondaryCity +
          ", " +
          data.secondaryState +
          ", " +
          data.secondaryZipcode +
          ", " +
          data.secondaryCountry;
    
      }

      data.updatedBy = context.identity.id;

      Users.updateOne({ id: id }, data).then((user) => {

        return res.status(200).json({
          success: true,
          code: 200,
          data: user,
          message: constantObj.user.UPDATED_USER,
        })

      });



    } catch (err) {
      return res.status(400).json({ success: false, error: { "code": 400, "message": "" + err } });
    }
  },

  /**
   * 
   * @param {*} data 
   * @param {*} context 
   * @param {*} req 
   * @param {*} res 
   * @CreatedAt 01/09/2021
   * @description Used to change the password of
   */

  changePassword: (data, context, req, res) => {
    let newPassword = data.newPassword;
    let confirmPassword = data.confirmPassword;
    let currentPassword = data.currentPassword;

    let query = {};
    query.id = context.identity.id;

    Users.findOne(query).then((user) => {
      if (!bcrypt.compareSync(currentPassword, user.password)) {
        return res.status(404).json({ "success": false, "error": { "code": 404, "message": constantObj.user.CURRENT_PASSWORD, key: 'CURRENT_PASSWORD' } });
      } else {
        if (newPassword != confirmPassword) {
          return res.status(404).json({ "success": false, "error": { "code": 404, "message": "New password and confirm password does not match", key: 'WRONG_PASSWORD' } });
        } else {
          var encryptedPassword = bcrypt.hashSync(newPassword, bcrypt.genSaltSync(10));
          return Users.update({ id: context.identity.id }, { encryptedPassword: encryptedPassword }).then(function (user) {
            return res.status(200).json({ "success": true, "code": 200, message: constantObj.user.PASSWORD_CHANGED, Key: "PASSWORD_CHANGED" });
          });
        }
      }
    });
  },


  /**
   * 
   * @param {*} data 
   * @param {*} context 
   * @returns 
   */
  webForgotPassword: async (data, context, req, res) => {
    var user = await Users.findOne({
      email: data.email,
      isDeleted: false,
    });
    if (!user || user === undefined) {
      return res.status(404).json({
        "success": false,
        "error": {
          "code": 404,
          "message": "No such user exist"
        }
      })
    } else {

      var length = 8,
        charset =
          "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*_-=+;:",
        retVal = "";
      for (var i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
      }
      var verificationCode = retVal;
      var updatedUser = await Users.updateOne({ id: user.id }, { verificationCode: verificationCode })
      if (updatedUser) {
        emailGeneratedCodeToUser({
          email: data.email,
          name: user.fullName,
          resetCode: verificationCode,
        });

        await res.status(200).json({
          "success": true,
          "code": 200,
          "message": "Verification code has been sent on email."
        });

      }




    }

  },
  /**
   * 
   * @param {*} data 
   * @param {*} context 
   * @param {*} req 
   * @param {*} res 
   * @returns 
   * @description Used to reset the password using verification code sent on email
   * @createdAt : 06/07/2021
   */
  resetPassword: async (data, context, req, res) => {
    try {
      var code = data.code
      var newPassword = data.newPassword
      var confirmPassword = data.confirmPassword
      if (newPassword != confirmPassword) {
        return res.status(400).json({
          success: false,
          message: "Confirm Password Not Matched"
        })
      }
      let user = await Users.findOne({ verificationCode: code })

      if (!user || user.verificationCode !== code) { //check for case senstive
        return res.status(404).json({
          error: {
            success: false,
            code: 404,
            message: "Verification code wrong"
          }
        })
      } else {
        const encryptedPassword = bcrypt.hashSync(newPassword, bcrypt.genSaltSync(10));
     
        Users.updateOne({ id: user.id }, { password: encryptedPassword }).then((updatedUser) => {
         
          return res.status(200).json({
            success: true,
            code: 200,
            message: "Password reset successfully."
          })
        })

      }
    } catch (err) {
      return res.status(400).json({ "success": true, "error": { "code": 400, "message": "" + err } });
    }
  },

  /**
   * 
   * @param {*} data 
   * @param {*} context 
   * @param {*} req 
   * @param {*} res 
   * @returns 
   * @description : Api use to add sub admin
   * @createdAt : 12/07/2021
   */

  addUser: async (data, context, req, res) => {
    let rolestatus = '';
    if ((!req.body.email) || typeof req.body.email == undefined) {
      return res.status(404).json({ "success": false, "error": { "code": 404, "message": constantObj.user.EMAIL_REQUIRED } });
    }

    if (!validateEmail(req.body.email)) {
      return res.status(404).json({
        success: false,
        error: { code: 404, message: constantObj.user.INVALID_EMAIL },
      });
    }

    if (!req.body.password || typeof req.body.password == undefined) {
      return res.status(404).json({
        success: false,
        error: { code: 404, message: constantObj.user.PASSWORD_REQUIRED },
      });
    }

    var date = new Date();
    try {
      var user = await Users.findOne({ email: req.body.email.toLowerCase(), isDeleted: false });

      if (user) {
        return res.status(404).json({ "success": false, "error": { "code": 400, "message": constantObj.user.EMAIL_EXIST } });
      } else {
        data = req.body;
        data.email = data.email.toLowerCase();
        data.status = "active";
        data.isVerified = "Y";

        if (!req.body.role || typeof req.body.role == undefined) {
          rolestatus = "user";
        } else {
          rolestatus = data.role;
        }
        data.role = rolestatus;
        if (typeof req.body.firstName !== "undefined") {
          data.fullName = req.body.firstName + " " + req.body.lastName;
        } else {
          data.fullName = data.email;
        }


        if (typeof req.body.primaryAddress1 !== "undefined") {
          data.primaryAddress =
            req.body.primaryAddress1 +
            " " +
            req.body.primaryState +
            ", " +
            req.body.primaryZipcode;
        }

        if (typeof req.body.secondaryAddress1 !== "undefined") {
          data.secondaryAddress =
            req.body.secondaryAddress1;
        }

        var newUser = await Users.create(data).fetch()
        var d = new Date(newUser.dateOfJoining);
        var year = d.getFullYear();
        var month = d.getMonth();
        var day = d.getDate();
        var appraisalDate = new Date(year + 1, month, day);
        await Users.updateOne({ email: req.body.email, isDeleted: false }, { appraisalDate: appraisalDate })
        if (newUser) {
          addUserEmail({
            email: data.email,
            name: newUser.fullName,
            password: data.password,
          });
          res.status(200).json({
            success: true,
            code: 200,
            message: constantObj.user.USER_SUCCESSFULLY_REGISTERED,
            data:newUser
          });

        }
      }
    } catch (err) {
      return res.status(400).json({ "success": true, "error": { "code": 400, "error": "" + err } });
    }

  },


  changePassword: (data, context, req, res) => {

    let newPassword = data.newPassword;
    let confirmPassword = data.confirmPassword;
    let currentPassword = data.currentPassword;

    let query = {};

    query.id = context.identity.id;

    Users.findOne(query).then((user) => {
      if (!bcrypt.compareSync(currentPassword, user.password)) {
        return res.status(404).json({ "success": false, "error": { "code": 404, "message": constantObj.user.CURRENT_PASSWORD } });
      } else {
        if (newPassword != confirmPassword) {
          return res.status(403).json({ "success": false, "error": { "code": 403, "message": constantObj.user.CONFIRM_PASSWORD_NOTMATCH } });
        } else {
          var encryptedPasswordNew = bcrypt.hashSync(newPassword, bcrypt.genSaltSync(10));


          Users.update({ id: context.identity.id }, { password: encryptedPasswordNew, encryptedPassword: encryptedPasswordNew, }).then(() => {
            res.status(200).json({
              "success": true,
              "code": 200,
              "message": constantObj.user.PASSWORD_CHANGED
            });
          });

        }
      }
    });

  },

  /**
  * 
  * @param {*} data consist frontend payload
  * @param {*} context 
  * @param {*} req 
  * @param {*} res 
  * @description: autoLogin the user using uers's id and detail of user with token in response
  * @createdAt: 26/07/2021
  */
  autoLogin: (data, context, req, res) => {
    var id = data.id
    if ((!id) || typeof id == undefined) {
      return res.status(404).json({ "success": false, "error": { "code": 404, "message": constantObj.user.ID_REQUIRED } });
    }


    Users.findOne({ id: id }).then(user => {
      if (user) {
        if (user.status != 'active') {
          return res.status(400).json({ "success": false, "error": { "code": 400, "message": "Account Deactivated. Please Contact To Admin" } });
        } else {
          var token = jwt.sign({ user_id: user.id, firstName: user.firstName },
            { issuer: 'Jcsoftware', subject: user.email, audience: "Vanmodsy" })

          user.access_token = token;

          return res.status(200).json({
            "success": true,
            "code": 200,
            "message": constantObj.user.SUCCESSFULLY_LOGGEDIN,
            "data": user
          });

        }
      }
    })
  }

};




/** Email to  user after generated code */
var emailGeneratedCodeToUser = function (options) {

  var email = options.email;
  var userName = options.name;
  var verificationcode = options.resetCode;
  firstName = Helper.toTitleCase(userName);

  if (firstName) {
    firstName = userName;
  }

  message = "";
  style = {
    header: `
        padding:30px 15px;
        text-align:center;
        background-color:#f2f2f2;
        `,
    body: `
        padding:15px;
        height: 140px;
        `,
    hTitle: `font-family: 'Raleway', sans-serif;
        font-size: 37px;
        height:auto;
        line-height: normal;
        font-weight: bold;
        background:none;
        padding:0;
        color:#333;
        `,
    maindiv: `
        width:600px;
        margin:auto;
        font-family: Lato, sans-serif;
        font-size: 14px;
        color: #333;
        line-height: 24px;
        font-weight: 300;
        border: 1px solid #eaeaea;
        `,
    textPrimary: `color:#3e3a6e;
        `,
    h5: `font-family: Raleway, sans-serif;
        font-size: 22px;
        background:none;
        padding:0;
        color:#333;
        height:auto;
        font-weight: bold;
        line-height:normal;
        `,
    m0: `margin:0;`,
    mb3: "margin-bottom:15px;",
    textCenter: `text-align:center;`,
    btn: `padding:10px 30px;
        font-weight:500;
        font-size:14px;
        line-height:normal;
        border:0;
        display:inline-block;
        text-decoration:none;
        `,
    btnPrimary: `
        background-color:#3e3a6e;
        color:#fff;
        `,
    footer: `
        padding:10px 15px;
        font-weight:500;
        color:#fff;
        text-align:center;
        background-color:#000;`,
  };

  message +=
    `<div class="container" style="` +
    style.maindiv +
    `">
    <div class="header" style="` +
    style.header +
    `text-align:center">
        <img src="https://bestdataprovider.com/wp-content/uploads/Jayachetna-Software-Solution-JC.png" width="150" style="margin-bottom:20px;" />
        <h2 style="` +
    style.hTitle +
    style.m0 +
    `">Reset Verification Code</h2>
    </div>
    <div class="body" style="` +
    style.body +
    `">
        <h5 style="` +
    style.h5 +
    style.m0 +
    style.mb3 +
    style.textCenter +
    `">Hello ` +
    firstName +
    `</h5>
        <p style="` +
    style.m0 +
    style.mb3 +
    style.textCenter +
    `margin-bottom:20px">Your forgot password request is generated. Here is you new verification code for resetting your password below.<br> 
        Verification Code :  ` +
    verificationcode +
    ` <br></p>
  
        </div>
        <div class="footer" style="` +
    style.footer +
    `">
        &copy 2023 JC Software Solution All rights reserved.
        </div>
    </div>`;

  SmtpController.sendEmail(email, "Forgot password", message);
};





/** Email to  user after adding user from admin */
var addUserEmail = function (options) {
  var email = options.email,
    password = options.password;
  firstName = Helper.toTitleCase(options.name)

  if (firstName === "" || firstName === undefined) {
    firstName = email;
  }

  message = '';
  style = {
    header: `
        padding:30px 15px;
        text-align:center;
        background-color:#f2f2f2;
        `,
    body: `
        padding:15px;
        height: 200px;
        `,
    hTitle: `font-family: 'Raleway', sans-serif;
        font-size: 37px;
        height:auto;
        line-height: normal;
        font-weight: bold;
        background:none;
        padding:0;
        color:#333;
        `,
    maindiv: `
        width:600px;
        margin:auto;
        font-family: Lato, sans-serif;
        font-size: 14px;
        color: #333;
        line-height: 24px;
        font-weight: 300;
        border: 1px solid #eaeaea;
        `,
    textPrimary: `color:#3e3a6e;
        `,
    h5: `font-family: Raleway, sans-serif;
        font-size: 22px;
        background:none;
        padding:0;
        color:#333;
        height:auto;
        font-weight: bold;
        line-height:normal;
        `,
    m0: `margin:0;`,
    mb3: "margin-bottom:15px;",
    textCenter: `text-align:center;`,
    marginBottom: "margin-bottom:30px;text-align:center;",
    btn: `padding:10px 30px;
        font-weight:500;
        font-size:14px;
        line-height:normal;
        border:0;
        display:inline-block;
        text-decoration:none;
        `,
    btnPrimary: `
        background-color:#3e3a6e;
        color:#fff;
        `,
    footer: `
        padding:10px 15px;
        font-weight:500;
        color:#fff;
        text-align:center;
        background-color:#000;`,
  };

  message +=
    `<div class="container" style="` +
    style.maindiv +
    `">
    <div class="header" style="` +
    style.header +
    `text-align:center">
    <img src="https://mng.jcsoftwaresolution.in/assets/imgpsh_fullsize_anim.png" style="width: 107px;" />
        <h2 style="` +
    style.hTitle +
    style.m0 +
    `">Welcome to JC Software Solution</h2>
    </div>
    <div class="body" style="` +
    style.body +
    `">
        <h5 style="` +
    style.h5 +
    style.m0 +
    style.mb3 +
    style.textCenter +
    `">Hello ` +
    firstName +
    `</h5>
        <p style="` +
    style.m0 +
    style.mb3 +
    style.textCenter +
    `margin-bottom:20px">Your account has been created successfully.<br>
      </p>
 
      </div>
        <div class="footer" style="` +
    style.footer +
    `">
        &copy 2023 JC Software Solution All rights reserved.
        </div>
    </div>`;

  SmtpController.sendEmail(email, 'Registration', message)

};
