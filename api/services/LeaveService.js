var constantObj = sails.config.constants;
const SmtpController = require("../controllers/SmtpController");
var constant = require("../../config/local");
var moment = require("moment");

exports.calculateLeaveMatrics = async (employeeEmail, currentDate) => {
  console.log(moment(currentDate).format("YYYY"));
  let year = moment(currentDate).format("YYYY");
  let day = moment(currentDate).format("DD");
  let month = moment(currentDate).format("MM");

  let leavesTakenTillNow = 0;
  let paidLeavesTillNow = 0;
  let paidLeavesLeftTillNow = 0;

  var query = {};

  query.and = [
    {
      leaveStartDate: { ">=": `${year}` + "-01-01" },
      leaveEndDate: { "<": `${year}` + "-" + `${month}` + `-${day}` },
    },
  ];

  query.fromEmail = employeeEmail;
  query.leave_status = "Accepted";
  query.isDeleted = false;
  leaves = await Leaves.find(query);

  for await (let leave of leaves) {
    if(leave && leave.subject === "Full Day Leave" )
    leavesTakenTillNow += leave.no_of_days;
    if(leave && leave.subject === "Short Leave" )
        leavesTakenTillNow += 0.25;
    else
        leavesTakenTillNow += 0.50;
  }
  
  paidLeavesTillNow = Number(month);

  paidLeavesLeftTillNow = (Number(month) - leavesTakenTillNow) < 0? 0: (Number(month) - leavesTakenTillNow);

  unpaidLeaves = (leavesTakenTillNow - Number(month))<0?0:(leavesTakenTillNow - Number(month));
  

  return { paidLeavesTillNow, paidLeavesLeftTillNow, leavesTakenTillNow ,unpaidLeaves};
};

// calculateLeaveMatrics();
