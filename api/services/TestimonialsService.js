/**
 * @DESC:  In this class/files Testimonials related functions (save, update, add Testimonials,delete)
 * @Request param: form data values
 * @Return : Success message with required data
 * @Author: Rohit kumar.
 */

var constantObj = sails.config.constants;

module.exports = {
    /**
     *
     * @param {*} data
     * @param {*} context
     * @param {*} req
     * @param {*} res
     * @returns
     * @description Used to add Testimonial to  Testimonials
     * @createdAt 19/10/2021
     * @createdBy : Rohit kumar
     */
    saveTestimonial: async (data, context, req, res) => {
        try {
            data.addedBy = context.identity.id
            Testimonials.create(data).then((savedtestimonial) => {
                return res.status(200).json({
                    success: true,
                    message: constantObj.Testimonials.CONTENT_SAVED,
                });
            });
        } catch (error) {
            return res.status(400).json({
                success: false,
                error: { message: error },
            });
        }
    },

    /***
     * @description : Used to update the role
     * @createdAt : 19/10/2021
     */
    updateTestimonial: (data, context, req, res) => {
        data.updatedBy = context.identity.id
        let _id = data.id;
        Testimonials.update({ id: _id }, data).then((testimonial) => {
            return res.status(200).json({
                "success": true,
                "message": constantObj.Testimonials.UPDATED_CONTENT,
            });
        }).catch((err) => {
            return res.status(400).json({ "success": false, "error": { "code": 400, "message": " " + err } })
        });
    },
};