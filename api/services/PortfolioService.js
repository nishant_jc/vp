/**
 * @DESC:  In this class/files Portfolio related functions (save, update, add Testimonials,delete)
 * @Request param: form data values
 * @Return : Success message with required data
 * @Author: Rohit kumar.
 */

 var constantObj = sails.config.constants;

 module.exports = {
     /**
      *
      * @param {*} data
      * @param {*} context
      * @param {*} req
      * @param {*} res
      * @returns
      * @description Used to add Portfolio
      * @createdAt 25/10/2021
      * @createdBy : Rohit kumar
      */
     savePortfolio: async (data, context, req, res) => {
         try {
             data.addedBy = context.identity.id
             Portfolio.create(data).then((savedPortfolio) => {
                 return res.status(200).json({
                     success: true,
                     message: constantObj.Portfolio.PORTFOLIO_SAVED,
                 });
             });
         } catch (error) {
             return res.status(400).json({
                 success: false,
                 error: { message: error },
             });
         }
     },
 
     /***
      * @description : Used to update the Portfolio,
      * @createdAt : 25/10/2021
      */
     updatePortfolio: (data, context, req, res) => {
         data.updatedBy = context.identity.id
         let _id = data.id;
         Portfolio.update({ id: _id }, data).then((Portfolio) => {
             return res.status(200).json({
                 "success": true,
                 "message": constantObj.Portfolio.UPDATED_PORTFOLIO,
             });
         }).catch((err) => {
             return res.status(400).json({ "success": false, "error": { "code": 400, "message": " " + err } })
         });
     },
 };