/**
 * @DESC:  In this class/files  Appraisal related functions (save, update, add comment)
 * @Request param: form data values
 * @Return : Success message with required data
 * @Author: Rohit kumar.
 */

var bcrypt = require("bcrypt-nodejs");
var nodemailer = require("nodemailer");
var smtpTransport = require("nodemailer-smtp-transport");
const SmtpController = require("../controllers/SmtpController");
const Helper = require("../controllers/HelperController");

var constantObj = sails.config.constants;

module.exports = {
  /**
*
* @param {*} data
* @param {*} context
* @param {*} req
* @param {*} res
* @returns
* @description Used to add appraisal to appraisal management
* @createdAt 07/10/2021
* @createdBy : Rohit kumar
*/
  saveAppraisal: async (data, context, req, res) => {
    try {

      let requiredFieldError = [];
      let requiredFileds = [
        "employeeId",
        "name",
        "email",
        "date",
      ];

      let formData = req.body;
      console.log(formData.date,"===============date")
      requiredFileds.forEach(function (value) {
        if (!formData.hasOwnProperty(value)) {
          //REQ_NAME = value.charAt(0).toUpperCase() + value.slice(1);
          requiredFieldError.push(value + " is required");
        }
      });

      if (requiredFieldError.length != 0) {
        return res.status(404).json({
          success: false,
          error: { code: 404, message: requiredFieldError },
        });
      }

      let toEmails = data.email;
      let toEmail = toEmails.split(",");
      var employeeId = data.employeeId;
      var fullName = data.fullName;
      var designation = data.designation;
      var email = data.email;
      var fromEmail = context.identity.email;

      data.addedBy = context.identity.id;

      var appraisal = await Appraisal.create(data).fetch();
      if (appraisal) {
        var d = new Date(appraisal.date);
        var year = d.getFullYear();
        var month = d.getMonth();
        var day = d.getDate();
        var appraisalDate = new Date(year + 1, month, day);
        await Users.updateOne({ email: data.email }, { appraisalDate: appraisalDate })


        var appraisal_date = new Date(formData.date);
        var effective_date = appraisal_date.toLocaleDateString();

        sendAppraisalEmail({
          fromEmail: fromEmail,
          toemail: formData.email,
          appraisalAmt: formData.appraisalAmt,
          fullNname: formData.name,
          appraisalDate:effective_date
          // description: description,
        });

        res.status(200).json({
          success: true,
          message: constantObj.Appraisal.APPRAISAL_SAVED,
        });
      }

    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },

  /**
   *
   * @param {*} data
   * @param {*} context
   * @param {*} req
   * @param {*} res
   * @returns
   * @description Used to update appraisal
   * @createdAt 07/10/2021
   * @createdBy : Rohit kumar
   */
  updateAppraisal: (data, context, req, res) => {
    data.updatedBy = context.identity.id
    let _id = data.id;
    Appraisal.update({ id: _id }, data).then((appraisal) => {
      return res.status(200).json({
        "success": true,
        "message": constantObj.Appraisal.UPDATED_APPRAISAL,
      });
    }).catch((err) => {
      return res.status(400).json({ "success": false, "error": { "code": 400, "message": " " + err } })
    });
  },
};


var sendAppraisalEmail = function (options) {
  let toemail = options.toemail;
  let name = options.fullNname;
  let status = options.status
  let appraisalAmt = options.appraisalAmt;
  let appraisalDate = options.appraisalDate;


  message = "";
  style = {
    header: `
      padding:30px 15px;
      text-align:center;
      background-color:#f2f2f2;
      `,
    body: `
      padding:15px;
      height: 95px;
      `,
    hTitle: `font-family: 'Raleway', sans-serif;
      font-size: 30px;
      height:auto;
      line-height: normal;
      font-weight: bold;
      background:none;
      padding:0;
      color:#333;
      `,
    maindiv: `
      width:600px;
      margin:auto;
      font-family: Lato, sans-serif;
      font-size: 14px;
      color: #333;
      line-height: 24px;
      font-weight: 300;
      border: 1px solid #eaeaea;
      `,
    textPrimary: `color:#3e3a6e;
      `,
    h5: `font-family: Raleway, sans-serif;
      font-size: 22px;
      background:none;
      padding:0;
      color:#333;
      height:auto;
      font-weight: bold;
      line-height:normal;
      `,
    m0: `margin:0;`,
    mb3: "margin-bottom:15px;",
    textCenter: `text-align:center;`,
    textleft: `text-align:left;`,
    btn: `padding:10px 30px;
      font-weight:500;
      font-size:14px;
      line-height:normal;
      border:0;
      display:inline-block;
      text-decoration:none;
      `,
    btnPrimary: `
      background-color:#3e3a6e;
      color:#fff;
      `,
    footer: `
      padding:10px 15px;
      font-weight:500;
      color:#fff;
      text-align:center;
      background-color:#000;
      margin-top:70px`,
  };

  message +=
    `<div class="container" style="` +
    style.maindiv +
    `">
  <div class="header" style="` +
    style.header +
    `text-align:center">
    <img src="https://mng.jcsoftwaresolution.in/assets/imgpsh_fullsize_anim.png" style="width: 107px;" />
      <h6 style="` +
    style.hTitle +
    style.m0 +
    `">Dear ` + name + `</h6>
  </div>
  <div class="body" style="` +
    style.body +
    `">
      <p style="` +
    style.m0 +
    style.mb3 +
    style.textCenter +
    `margin:35px 0px 20px">I am writing to share with you the results of your performance evaluation.I want to start by thanking you for your hard work and dedication over the past year.Your contributions to our organization have been invaluable, and we appreciate all that you do.In recognition of this, we have decided to offer you an increased remuneration of ` + appraisalAmt + `.It will be effective from ` + appraisalDate + `.<b></p>
      </div>
      <div class="footer" style="` +
    style.footer +
    `">
      &copy 2023 JC Software Solution All rights reserved.
      </div>
  </div>`;

  //var to = ["chander@yopmail.com", "amit@yopmail.com"];
  SmtpController.sendLeaveEmailOnChange(
    toemail,
    "Congratulations",
    message
  );
};
