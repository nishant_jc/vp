/**
 * @DESC:  In this class/files Services related functions (save, update, add Testimonials,delete)
 * @Request param: form data values
 * @Return : Success message with required data
 * @Author: Rohit kumar.
 */

 var constantObj = sails.config.constants;

 module.exports = {
     /**
      *
      * @param {*} data
      * @param {*} context
      * @param {*} req
      * @param {*} res
      * @returns
      * @description Used to add Service
      * @createdAt 22/10/2021
      * @createdBy : Rohit kumar
      */
     saveService: async (data, context, req, res) => {
         try {
             data.addedBy = context.identity.id
             Services.create(data).then((savedservice) => {
                 return res.status(200).json({
                     success: true,
                     message: constantObj.Service.SERVICE_SAVED,
                 });
             });
         } catch (error) {
             return res.status(400).json({
                 success: false,
                 error: { message: error },
             });
         }
     },
 
     /***
      * @description : Used to update the SERVICE,
      * @createdAt : 22/10/2021
      */
     updateService: (data, context, req, res) => {
         data.updatedBy = context.identity.id
         let _id = data.id;
         Services.update({ id: _id }, data).then((Service) => {
             return res.status(200).json({
                 "success": true,
                 "message": constantObj.Service.UPDATED_SERVICE,
             });
         }).catch((err) => {
             return res.status(400).json({ "success": false, "error": { "code": 400, "message": " " + err } })
         });
     },
 };