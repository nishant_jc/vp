/**
 * @DESC:  In this class/files Roles related functions (save, update, add comment)
 * @Request param: Authorization and form data values
 * @Return : Success message with required data
 * @Author: JCsoftware Solution Pvt. Ltd.
*/

var constantObj = sails.config.constants;

module.exports = {

	/***
	 * @description : Used to update the role
	 * @createdAt : 01/09/2021
	 */
	updateRoles: (data, context, req, res) => {
		data.updatedBy = context.identity.id
		let _id = data.id;
		Roles.update({ id: _id }, data).then((role) => {
			return res.status(200).json({
				"success": true,
				"message": constantObj.Roles.UPDATED_ROLE,
			});
		}).catch((err) => {
			return res.status(400).json({ "success": false, "error": { "code": 400, "message": " " + err } })
		});
	},



};
