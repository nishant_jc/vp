/**
 * @DESC:  In this class/files technology related functions (save, update, add Testimonials,delete)
 * @Request param: form data values
 * @Return : Success message with required data
 * @Author: Rohit kumar.
 */

 var constantObj = sails.config.constants;

 module.exports = {
     /**
      *
      * @param {*} data
      * @param {*} context
      * @param {*} req
      * @param {*} res
      * @returns
      * @description Used to add technology
      * @createdAt 25/10/2021
      * @createdBy : Rohit kumar
      */
     saveTechnology: async (data, context, req, res) => {
         try {
             data.addedBy = context.identity.id
             Technology.create(data).then((savedTechnology) => {
                 return res.status(200).json({
                     success: true,
                     message: constantObj.Technology.TECHNOLOGY_SAVED,
                 });
             });
         } catch (error) {
             return res.status(400).json({
                 success: false,
                 error: { message: error },
             });
         }
     },
 
     /***
      * @description : Used to update the Technology,
      * @createdAt : 25/10/2021
      */
     updateTechnology: (data, context, req, res) => {
         data.updatedBy = context.identity.id
         let _id = data.id;
         Technology.update({ id: _id }, data).then((Technology) => {
             return res.status(200).json({
                 "success": true,
                 "message": constantObj.Technology.UPDATED_TECHNOLOGY,
             });
         }).catch((err) => {
             return res.status(400).json({ "success": false, "error": { "code": 400, "message": " " + err } })
         });
     },
 };