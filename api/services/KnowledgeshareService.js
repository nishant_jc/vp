/**
 * @DESC:  In this class/files Knowledge share related functions (save, update, add comment)
 * @Request param: form data values
 * @Return : Success message with required data
 * @Author: Chandra Shekhar.
 */

var constantObj = sails.config.constants;
const SmtpController = require("../controllers/SmtpController");
var constant = require('../../config/local');

module.exports = {
  /**
   *
   * @param {*} data
   * @param {*} context
   * @param {*} req
   * @param {*} res
   * @returns
   * @description Used to add post to knowledge share management
   * @createdAt 24/09/2021
   * @createdBy : Chandra shekhar
   */
  savePost: (data, context, req, res) => {
    try {
      let requiredFieldError = [];
      let requiredFileds = [
        "name",
        "description",
      ];

      let formData = req.body;
      requiredFileds.forEach(function (value) {
        if (!formData.hasOwnProperty(value)) {
          REQ_NAME = value.charAt(0).toUpperCase() + value.slice(1);
          requiredFieldError.push(REQ_NAME + " is required");
        }
      });

      if (requiredFieldError.length != 0) {
        return res.status(404).json({
          success: false,
          error: { code: 404, message: requiredFieldError },
        });
      }

      let query = {};
      query.isDeleted = false;
      query.name = data.name.toLowerCase();
      data.addedBy = context.identity.id;
      data.postByUser = context.identity.id;
      Knowledgeshare.findOne(query).then((typeExist) => {
        if (typeExist) {
          return res.status(404).json({
            success: false,
            error: {
              code: 404,
              message: constantObj.knowledgeshare.POST_ALREADY_EXIST,
            },
          });
        } else {
          data.name = data.name.toLowerCase();
          Knowledgeshare.create(data).fetch().then(async (savedpost) => {

            if (savedpost) {
              var users = await Users.find({role :"developer",status: "active", isDeleted: false });
              var email_array = [];

              if (users && users.length > 0) {
                //console.log(users, "==============users");
                for await (let emails of users) {
                  var user_email = emails.email;
                  email_array.push(user_email);
                  //console.log(email_array, "==========email in array")
                }
              }

              const chunkSize = 25;
              const chunks = [];

              for (let i = 0; i < email_array.length; i += chunkSize) {
                const chunk = email_array.slice(i, i + chunkSize);
                chunks.push(chunk);
              }

              console.log(chunks.length,"==============chunks_length");

              for (let i = 0; i < chunks.length; i += 1) {

                // SendEmailToAllEmployees({
                //   toemail: chunks[i]
                // });

                  console.log(chunks[i], "=============chuncked_email")

                }

              return res.status(200).json({
                success: true,
                message: constantObj.knowledgeshare.POST_SAVED,
              });
            }
          });
        }
      });
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },
};


SendEmailToAllEmployees = function (options) {
  var toemail = options.toemail;
  //var attachment = options.attachment;
  (message = "");
  message += ` <div class="maindiv" style="align-items: center; box-shadow: 0px 0px 8px 0px #8080808a; height: auto; background-color: #fff; 
  width:600px;
  margin:auto;">

      <div style="border: 1px solid #ff8f26cc ;    margin:2rem auto; max-width: 600px; height: auto; background-color: #ff8f26cc ;  text-align: center;">

          <h3 style="font-size: 25px; color: #fff;">Hello Employee</h3>

      </div>
      <div style="text-align: center;">

          <div style="margin-bottom: 14px;">
              <img src="${constant.LOGO}/assets/img/logoImg.png" style="width: 107px;" />
          </div>
          <div>
              <p style="font-size: 15px;"><b>New Knowledge Sharing Post</b></p>
              <p style="font-size: 15px;">Someone added new Knowledge Sharing Post.Please Check your knowledge sharing section in the JC Portal as some knowledge is being shared with you.Thank You</p>

          </div >
          <div style=" margin-top:50px">

  <p style="font-size: 15px; color: gray; padding-bottom: 15px; ">© 2023 JC Software Solution  All rights reserved.</p>
  </div>

      </div >
  </div > `

  //var to = ["chander@yopmail.com", "amit@yopmail.com"];
  SmtpController.sendPostUploadEmail(
    toemail,
    "New Knowledge Sharing Post",
    message
  );
};
