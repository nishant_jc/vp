/**
 * @DESC:  In this class/files Weeklyplan related functions (save, update, add Testimonials,delete)
 * @Request param: form data values
 * @Return : Success message with required data
 * @Author: Rohit kumar.
 */

 var constantObj = sails.config.constants;

 module.exports = {
     /**
      *
      * @param {*} data
      * @param {*} context
      * @param {*} req
      * @param {*} res
      * @returns
      * @description Used to add Weeklyplan
      * @createdAt 25/10/2021
      * @createdBy : Rohit kumar
      */
     saveWeeklyplan: async (data, context, req, res) => {
         try {
             data.addedBy = context.identity.id
             Weeklyplan.create(data).then((savedWeeklyplan) => {
                 return res.status(200).json({
                     success: true,
                     message: constantObj.Weeklyplan.WEEKLYPLAN_SAVED,
                 });
             });
         } catch (error) {
             return res.status(400).json({
                 success: false,
                 error: { message: error },
             });
         }
     },
 
     /***
      * @description : Used to update the Weeklyplan,
      * @createdAt : 25/10/2021
      */
     updateWeeklyplan: (data, context, req, res) => {
         data.updatedBy = context.identity.id
         let _id = data.id;
         Weeklyplan.update({ id: _id }, data).then((Weeklyplan) => {
             return res.status(200).json({
                 "success": true,
                 "message": constantObj.Weeklyplan.UPDATED_WEEKLYPLAN,
             });
         }).catch((err) => {
             return res.status(400).json({ "success": false, "error": { "code": 400, "message": " " + err } })
         });
     },
 };