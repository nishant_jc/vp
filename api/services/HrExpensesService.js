/**
 * @DESC:  In this class/files HrExpenses related functions (save, update, add HrExpenses,delete)
 * @Request param: form data values
 * @Return : Success message with required data
 * @Author: Rohit kumar.
 */

var constantObj = sails.config.constants;
const SmtpController = require("../controllers/SmtpController");

module.exports = {
    /**
     *
     * @param {*} data
     * @param {*} context
     * @param {*} req
     * @param {*} res
     * @returns
     * @description Used to add HrExpenses
     * @createdAt 13/12/2021
     * @createdBy : Rohit kumar
     */
    saveHrExpenses: async (data, context, req, res) => {
        try {
            var date = new Date();
            var current_date = date.toISOString().substring(0, 10);
            var current_year = date.toISOString().substring(0, 4);
            var current_month = date.toISOString().substring(5, 7);

            var starting_date = `${current_year}` + "-" + `${current_month}` + "-01";
            var ending_date = current_date;


            let total = 0;
            let total_expenses = 0;
            let query = {};


            query.date = {
                '>=': starting_date,
                '<=': ending_date
            };

            query.isDeleted = "false";

            const find_wallet = await Hrwallet.find(query);

            for (items of find_wallet) {
                var amount = items.amount;
                total += amount;
            }

            const find_expenses = await HrExpenses.find(query);

            for (itm of find_expenses) {
                var expenses = itm.amount;
                total_expenses += Number(expenses);
            }


            var left_amount = total - total_expenses;

            if (left_amount >= data.amount) {
                data.payment_status = "paid";
            } else {
                data.payment_status = "pending";
            }

            data.addedBy = context.identity.id;

            HrExpenses.create(data).then((HrExpensesTask) => {
                return res.status(200).json({
                    success: true,
                    message: constantObj.HrExpenses.EXPENSES_SAVED,
                    data: HrExpensesTask
                });
            });
        } catch (error) {
            return res.status(400).json({
                success: false,
                error: { message: error },
            });
        }
    },

    /***
     * @description : Used to update the HrExpenses,
     * @createdAt :  13/12/2021
     */
    updateHrExpenses: (data, context, req, res) => {
        data.updatedBy = context.identity.id
        let _id = data.id;
        HrExpenses.update({ id: _id }, data).then((HrExpensesdata) => {
            return res.status(200).json({
                "success": true,
                "message": constantObj.HrExpenses.UPDATED_EXPENSES,
                data: HrExpensesdata
            });
        }).catch((err) => {
            return res.status(400).json({ "success": false, "error": { "code": 400, "message": " " + err } })
        });
    },
};