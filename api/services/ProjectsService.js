/**
 * @DESC:  In this class/files Projects related functions (save, update, add comment)
 * @Request param: form data values
 * @Return : Success message with required data
 * @Author: Chandra Shekhar.
 */

var constantObj = sails.config.constants;
var async = require("async");
const SmtpController = require("../controllers/SmtpController");

module.exports = {
  /**
   *
   * @param {*} data
   * @param {*} context
   * @param {*} req
   * @param {*} res
   * @returns
   * @description Used to add projects to project management
   * @createdAt 15/09/2021
   * @createdBy : Chandra shekhar
   */
  saveProject: (data, context, req, res) => {
    try {
      let requiredFieldError = [];
      let requiredFileds = [
        "name",
      ];
      // var admin=sails.config.ADMIN_EMAIL;

      // return;
      let formData = req.body;
      // let data=req.body.text;   
      // let senddata=formData
      // SmtpController.send(senddata);
      requiredFileds.forEach(function (value) {
        if (!formData.hasOwnProperty(value)) {
          REQ_NAME = value.charAt(0).toUpperCase() + value.slice(1);
          requiredFieldError.push(REQ_NAME + " is required");
        }
      });

      if (requiredFieldError.length != 0) {
        return res.status(404).json({
          success: false,
          error: { code: 404, message: requiredFieldError },
        });

      }

      let query = {};
      query.isDeleted = false;
      var memberdata = data.members;
      var userEmailArray = [];
      var teamsdata = data.teams;
      if (teamsdata != undefined) {
        let allteams = teamsdata.split(",");
        data.teams = allteams;
      }
      query.name = data.name.toLowerCase();
      data.addedBy = context.identity.id;
      Projects.findOne(query).then((typeExist) => {
        if (typeExist) {
          return res.status(404).json({
            success: false,
            error: { code: 404, message: constantObj.projects.PROJECT_ALREADY_EXIST },
          });
        } else {
          if (memberdata != undefined) {
            async.each(
              memberdata,
              async function (element, callback) {
                var userData = await Users.findOne({ id: element });
                useremail = userData.email;
                userEmailArray.push(useremail);
                callback();

              },
              function (err) {
                setTimeout(() => {
                  sendAssignEmail({
                    toemail: userEmailArray,
                    name: data.name,
                  });
                }, 3000);
              }
            );
          }
          data.name = data.name.toLowerCase();
          Projects.create(data).then((savedproject) => {
            // htmlcode();
            if (req.identity.role == 'bidder') {
              sendProjectEmail({
                toemail: sails.config.ADMIN_EMAIL,
                name: data.name,
                addedByname: req.identity.fullName,
              });
            }

            return res.status(200).json({
              success: true,
              message: constantObj.projects.PROJECT_SAVED,
            });
          });
        }
      });

    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },
};
// var sendChangeStatus=function(){
//   (message = "");
//   style = {
//     header: `
//         padding:30px 15px;
//         text-align:center;
//         background-color:#f2f2f2;
//         `,
//     body: `
//         padding:15px;
//         height: 140px;
//         `,
//     hTitle: `font-family: 'Raleway', sans-serif;
//         font-size: 37px;
//         height:auto;
//         line-height: normal;
//         font-weight: bold;
//         background:none;
//         padding:0;
//         color:#333;
//         `,
//     maindiv: `
//         width:600px;
//         margin:auto;
//         font-family: Lato, sans-serif;
//         font-size: 14px;
//         color: #333;
//         line-height: 24px;
//         font-weight: 300;
//         border: 1px solid #eaeaea;
//         `,
//     textPrimary: `color:#3e3a6e;
//         `,
//     h5: `font-family: Raleway, sans-serif;
//         font-size: 22px;
//         background:none;
//         padding:0;
//         color:#333;
//         height:auto;
//         font-weight: bold;
//         line-height:normal;
//         `,
//     m0: `margin:0;`,
//     mb3: "margin-bottom:15px;",
//     textCenter: `text-align:center;`,
//     textleft: `text-align:left;`,
//     btn: `padding:10px 30px;
//         font-weight:500;
//         font-size:14px;
//         line-height:normal;
//         border:0;
//         display:inline-block;
//         text-decoration:none;
//         `,
//     btnPrimary: `
//         background-color:#3e3a6e;
//         color:#fff;
//         `,
//     footer: `
//         padding:10px 15px;
//         font-weight:500;
//         color:#fff;
//         text-align:center;
//         background-color:#000;`,
//   };
//   message +=
//   `<div class="container" style="` +
//   style.maindiv +
//   `">
//   <div class="header" style="` +
//   style.header +
//   `text-align:center">
//       <img src="http://198.251.65.146:4091/img/logo.png" width="150" style="margin-bottom:20px;" />
//       <h2 style="` +
//   style.hTitle +
//   style.m0 +
//   `">Congratulations Member</h2>
//   </div>
//   <div class="body" style="` +
//   style.body +
//   `">
//       <p style="` +
//   style.m0 +
//   style.mb3 +
//   style.textCenter +
//   `margin-bottom:20px">You are seleted for the <b>new </b>project of <b>'` +

//   `'<b></p>
//       </div>
//       <div class="footer" style="` +
//   style.footer +
//   `">
//       &copy 2023 JC Software Solution All rights reserved.
//       </div>
//   </div>`;

//   SmtpController.sendChangeStatusEmail(
//     "this is new project",
//     message
//   );
// };

/** Email to  user after generated code */
var sendAssignEmail = function (options) {
  var toemail = options.toemail;
  var projectname = options.name;
  (message = "");
  style = {
    header: `
        padding:30px 15px;
        text-align:center;
        background-color:#f2f2f2;
        `,
    body: `
        padding:15px;
        height: 140px;
        `,
    hTitle: `font-family: 'Raleway', sans-serif;
        font-size: 37px;
        height:auto;
        line-height: normal;
        font-weight: bold;
        background:none;
        padding:0;
        color:#333;
        `,
    maindiv: `
        width:600px;
        margin:auto;
        font-family: Lato, sans-serif;
        font-size: 14px;
        color: #333;
        line-height: 24px;
        font-weight: 300;
        border: 1px solid #eaeaea;
        `,
    textPrimary: `color:#3e3a6e;
        `,
    h5: `font-family: Raleway, sans-serif;
        font-size: 22px;
        background:none;
        padding:0;
        color:#333;
        height:auto;
        font-weight: bold;
        line-height:normal;
        `,
    m0: `margin:0;`,
    mb3: "margin-bottom:15px;",
    textCenter: `text-align:center;`,
    textleft: `text-align:left;`,
    btn: `padding:10px 30px;
        font-weight:500;
        font-size:14px;
        line-height:normal;
        border:0;
        display:inline-block;
        text-decoration:none;
        `,
    btnPrimary: `
        background-color:#3e3a6e;
        color:#fff;
        `,
    footer: `
        padding:10px 15px;
        font-weight:500;
        color:#fff;
        text-align:center;
        background-color:#000;`,
  };

  message +=
    `<div class="container" style="` +
    style.maindiv +
    `">
    <div class="header" style="` +
    style.header +
    `text-align:center">
    <img src="https://mng.jcsoftwaresolution.in/assets/imgpsh_fullsize_anim.png" style="width: 107px;" />
        <h2 style="` +
    style.hTitle +
    style.m0 +
    `">Congratulations Member</h2>
    </div>
    <div class="body" style="` +
    style.body +
    `">
        <p style="` +
    style.m0 +
    style.mb3 +
    style.textCenter +
    `margin-bottom:20px">You are seleted for the <b>new </b>project of <b>'` +
    projectname +
    `'<b></p>
        </div>
        <div class="footer" style="` +
    style.footer +
    `">
        &copy 2023 JC Software Solution All rights reserved.
        </div>
    </div>`;

  //var to = ["chander@yopmail.com", "amit@yopmail.com"];
  SmtpController.sendProjectAssignEmail(
    toemail,
    "You are selected for the new project",
    message
  );
};


/** Email to  user after generated code */
var sendProjectEmail = function (options) {
  var toemail = options.toemail;
  var projectname = options.name;
  var addedByname = options.addedByname;

  (message = "");
  style = {
    header: `
        padding:30px 15px;
        text-align:center;
        background-color:#f2f2f2;
        `,
    body: `
        padding:15px;
        height: 140px;
        `,
    hTitle: `font-family: 'Raleway', sans-serif;
        font-size: 37px;
        height:auto;
        line-height: normal;
        font-weight: bold;
        background:none;
        padding:0;
        color:#333;
        `,
    maindiv: `
        width:600px;
        margin:auto;
        font-family: Lato, sans-serif;
        font-size: 14px;
        color: #333;
        line-height: 24px;
        font-weight: 300;
        border: 1px solid #eaeaea;
        `,
    textPrimary: `color:#3e3a6e;
        `,
    h5: `font-family: Raleway, sans-serif;
        font-size: 22px;
        background:none;
        padding:0;
        color:#333;
        height:auto;
        font-weight: bold;
        line-height:normal;
        `,
    m0: `margin:0;`,
    mb3: "margin-bottom:15px;",
    textCenter: `text-align:center;`,
    textleft: `text-align:left;`,
    btn: `padding:10px 30px;
        font-weight:500;
        font-size:14px;
        line-height:normal;
        border:0;
        display:inline-block;
        text-decoration:none;
        `,
    btnPrimary: `
        background-color:#3e3a6e;
        color:#fff;
        `,
    footer: `
        padding:10px 15px;
        font-weight:500;
        color:#fff;
        text-align:center;
        background-color:#000;`,
  };

  message +=
    `<div class="container" style="` +
    style.maindiv +
    `">
    <div class="header" style="` +
    style.header +
    `text-align:center">
        <img src="http://198.251.65.146:4091/img/logo.png" width="150" style="margin-bottom:20px;" />
        <h2 style="` +
    style.hTitle +
    style.m0 +
    `">Congratulations Admin</h2>
    </div>
    <div class="body" style="` +
    style.body +
    `">
        <p style="` +
    style.m0 +
    style.mb3 +
    style.textCenter +
    `margin-bottom:20px">New project is added : <b>` +
    projectname +
    `<b> by <b>` +
    addedByname +
    `<b></p>
        </div>
        <div class="footer" style="` +
    style.footer +
    `">
        &copy 2023 JC Software Solution All rights reserved.
        </div>
    </div>`;

  //var to = ["chander@yopmail.com", "amit@yopmail.com"];
  SmtpController.sendProjectAssignEmail(
    toemail,
    "New project created successfully.",
    message
  );
};