/**
 * @DESC:  In this class/files Serviceandtechnology related functions (save, update, add Testimonials,delete)
 * @Request param: form data values
 * @Return : Success message with required data
 * @Author: Rohit kumar.
 */

 var constantObj = sails.config.constants;

 module.exports = {
     /**
      *
      * @param {*} data
      * @param {*} context
      * @param {*} req
      * @param {*} res
      * @returns
      * @description Used to add Serviceandtechnology
      * @createdAt 22/10/2021
      * @createdBy : Rohit kumar
      */
     saveServiceandtechnology: async (data, context, req, res) => {
         try {
             data.addedBy = context.identity.id
             Serviceandtechnology.create(data).then((savedtestimonial) => {
                 return res.status(200).json({
                     success: true,
                     message: constantObj.Serviceandtechnology.SERVICE_SAVED,
                 });
             });
         } catch (error) {
             return res.status(400).json({
                 success: false,
                 error: { message: error },
             });
         }
     },
 
     /***
      * @description : Used to update the SERVICE,
      * @createdAt : 22/10/2021
      */
     updateServiceandtechnology: (data, context, req, res) => {
         data.updatedBy = context.identity.id
         let _id = data.id;
         Serviceandtechnology.update({ id: _id }, data).then((Serviceandtechnology) => {
             return res.status(200).json({
                 "success": true,
                 "message": constantObj.Serviceandtechnology.UPDATED_SERVICE,
             });
         }).catch((err) => {
             return res.status(400).json({ "success": false, "error": { "code": 400, "message": " " + err } })
         });
     },
 };