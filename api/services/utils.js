exports.addOffsetTime = (date, offset) => {
    if (offset < 0) {
        let newOffset = Math.abs(offset)
        date.setMinutes(newOffset)
    } else {
        date.setMinutes(offset)
    }
    return date;
}
exports.getTimeOffSet = async () => {
    let currentDate = new Date();
    const offset = currentDate.getTimezoneOffset();
    return offset;
}
exports.getStartDate = async (stringDate) => {  // stringDate formate must be in yyyy:mm:dd, Example : 2022-3-4
    let date = new Date(stringDate);
    const offset = await this.getTimeOffSet();
    date = await this.addOffsetTime(date, offset);
    date.setUTCHours(0, 0, 0, 0)
    return date;
}

exports.getEndDate = async (stringDate) => {  // stringDate formate must be in yyyy:mm:dd, Example : 2022-3-4
    let date = new Date(stringDate);
    const offset = await this.getTimeOffSet();
    date = await this.addOffsetTime(date, offset);
    date.setUTCHours(23, 59, 59, 999)
    return date;
}