/**
 * @DESC:  In this class/files Leaves related functions (save, update, add comment)
 * @Request param: form data values
 * @Return : Success message with required data
 * @Author: Chandra Shekhar.
 */

var bcrypt = require("bcrypt-nodejs");
var nodemailer = require("nodemailer");
var smtpTransport = require("nodemailer-smtp-transport");
const SmtpController = require("../controllers/SmtpController");
const Helper = require("../controllers/HelperController");
var constantObj = sails.config.constants;


module.exports = {
  /**
   *
   * @param {*} data
   * @param {*} context
   * @param {*} req
   * @param {*} res
   * @returns
   * @description Used to add resign to Resignation management
   * @createdAt 23/09/2021
   * @createdBy : Chandra shekhar
   */
  saveResign: async (data, context, req, res) => {
    try {
      let requiredFieldError = [];
      let requiredFileds = [
        "toEmail",
        "subject",
        "description",
      ];

      let formData = req.body;
      requiredFileds.forEach(function (value) {
        if (!formData.hasOwnProperty(value)) {
          requiredFieldError.push(value + " is required");
        }
      });

      if (requiredFieldError.length != 0) {
        return res.status(404).json({
          success: false,
          error: { code: 404, message: requiredFieldError },
        });
      }

      const toEmails = data.toEmail; 
      let toEmail = toEmails;
      var subject = data.subject;
      var description = data.description;
      var fromEmail = context.identity.email;
      var designation=context.identity.designation
      var PhoneNumber=context.identity.mobileNo
      data.addedBy = context.identity.id;
      data.employeeName = context.identity.fullName;
      data.fromEmail = context.identity.email;
      let sortBy = "updatedAt desc"
      let userDetail = await Resignation.find({ fromEmail: fromEmail }).sort(sortBy).limit(1);
      if (userDetail.length > 0) {
        if (userDetail[0].resign_status == "Pending") {
           res.status(400).json({
            success: false,
            message: constantObj.resign.CHECK_RESIGN_STATUS,
          });
        }
        else {
          sendResignEmail({
            fromEmail: fromEmail,
            toemail: toEmail,
            subject: subject,
            fullNname: data.employeeName,
            description: description,
            designation:designation,
            PhoneNumber:PhoneNumber
          });

          Resignation.create(data).then((savedresign) => {
             res.status(200).json({
              success: true,
              message: constantObj.resign.RESIGN_SAVED,
            });
          });
        }
      }
        else {
          sendResignEmail({
            fromEmail: fromEmail,
            toemail: toEmail,
            subject: subject,
            fullNname: data.employeeName,
            description: description,
            designation:designation,
            PhoneNumber:PhoneNumber
          });

          Resignation.create(data).then((savedresign) => {
             res.status(200).json({
              success: true,
              message: constantObj.resign.RESIGN_SAVED,
            });
          });
        }

    } catch (error) {
      console.log(error,"===============error")
       res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },
};

/** Send Email for resign.*/
var sendResignEmail = function (options) {
    
  var fromEmail = options.fromEmail;
  var description = options.description;
  var subject = options.subject;
  var toemail = options.toemail;
  var userName = options.fullNname;
  var PhoneNumber= options.PhoneNumber
  var designation= options.designation
   firstName = Helper.toTitleCase(userName);

  if (typeof firstName == undefined) {
      firstName = email;
  }

  message = "";
  style = {
    header: `
        padding:30px 15px;
        text-align:center;
        background-color:#f2f2f2;
        `,
    body: `
        padding:15px;
        height: 140px;
        `,
    hTitle: `font-family: 'Raleway', sans-serif;
        font-size: 37px;
        height:auto;
        line-height: normal;
        font-weight: bold;
        background:none;
        padding:0;
        color:#333;
        `,
    maindiv: `
        width:600px;
        margin:auto;
        font-family: Lato, sans-serif;
        font-size: 14px;
        color: #333;
        line-height: 24px;
        font-weight: 300;
        border: 1px solid #eaeaea;
        `,
    textPrimary: `color:#3e3a6e;
        `,
    h5: `font-family: Raleway, sans-serif;
        font-size: 22px;
        background:none;
        padding:0;
        color:#333;
        height:auto;
        font-weight: bold;
        line-height:normal;
        `,
    m0: `margin:0;`,
    mb3: "margin-bottom:15px;",
    textCenter: `text-align:center;`,
    textleft: `text-align:left;`,
    btn: `padding:10px 30px;
        font-weight:500;
        font-size:14px;
        line-height:normal;
        border:0;
        display:inline-block;
        text-decoration:none;
        `,
    btnPrimary: `
        background-color:#3e3a6e;
        color:#fff;
        `,
    footer: `
        padding:10px 15px;
        font-weight:500;
        color:#fff;
        text-align:center;
        background-color:#000;
        margin-top:80px`,
  };

  message +=
    `<div class="container" style="` +
    style.maindiv +
    `">
    <div class="header" style="` +
    style.header +
    `text-align:center">
    <img src="https://mng.jcsoftwaresolution.in/assets/imgpsh_fullsize_anim.png" style="width: 107px;" />
        <h2 style="` +
    style.hTitle +
    style.m0 +
    `">Resignation Application</h2>
    </div>
    <div class="body" style="` +
    style.body +
    `">
        <p style="` +
    style.m0 +
    style.mb3 +
    style.textleft +
    `margin-bottom:20px">Hi Sir/Madam </p>
        <p style="` +
    style.m0 +
    style.mb3 +
    style.textleft +
    `margin-bottom:20px">  ` +
    description +
    `</p>
    <p style="` +
    style.m0 +
    style.mb3 +
    style.textleft +
    `margin-bottom:20px">Thankyou<br> Name:` + userName +`<br>Designation: `+ designation + 
  ` <br>Phone Number: `+ PhoneNumber +
    `<br>Company: JC software Solution
    </p>

        </div>
        <div class="footer" style="` +
    style.footer +
    `">
        &copy 2023 JC Software Solution All rights reserved.
        </div>
    </div>`;

//var to = ["chander@yopmail.com", "amit@yopmail.com"];
SmtpController.sendResignationEmail(fromEmail, toemail, subject, message, userName);
};
