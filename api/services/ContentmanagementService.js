/**
 * @DESC:  In this class/files Contentmanagement related functions (save, update, add comment)
 * @Request param: form data values
 * @Return : Success message with required data
 * @Author: Rohit kumar.
 */

 var constantObj = sails.config.constants;
 
 module.exports = {
   /**
    *
    * @param {*} data
    * @param {*} context
    * @param {*} req
    * @param {*} res
    * @returns
    * @description Used to add Content to Contentmanagement management
    * @createdAt 19/10/2021
    * @createdBy : Rohit kumar
    */
    saveContent: async (data, context, req, res) => {
     try {
       let requiredFieldError = [];
       let requiredFileds = [
         "title",
         "description",
       ];
 
       let formData = req.body;
       requiredFileds.forEach(function (value) {
         if (!formData.hasOwnProperty(value)) {
           requiredFieldError.push(value + " is required");
         }
       });
 
       if (requiredFieldError.length != 0) {
         return res.status(404).json({
           success: false,
           error: { code: 404, message: requiredFieldError },
         });
       }
 
       var title = data.title;
       data.addedBy = context.identity.id;
       let contentDetail = await Contentmanagement.find({ title: title });
         if (contentDetail.length >0) {
             await res.status(400).json({
               success: false,
               message: constantObj.Contentmanagement.CHECK_CONTENT_STATUS,
             });
         }
         else {

             Contentmanagement.create(data).then((savedcontent) => {
               return res.status(200).json({
                 success: true,
                 message: constantObj.Contentmanagement.CONTENT_SAVED,
               });
             });
         }
     } catch (error) {
       return res.status(400).json({
         success: false,
         error: { message: error },
       });
     }
    },

    /***
	 * @description : Used to update the role
	 * @createdAt : 19/10/2021
	 */
    updateContent: (data, context, req, res)=> {
		data.updatedBy = context.identity.id
		let _id = data.id;
        Contentmanagement.update({ id: _id }, data).then( (content)=> {
			return res.status(200).json({
				"success": true,				
				"message": constantObj.Contentmanagement.UPDATED_CONTENT,
			});
		}).catch((err)=>{
			return res.status(400).json({ "success": false,  "error":{"code": 400,"message": " "+err } })
		});
	},
 };