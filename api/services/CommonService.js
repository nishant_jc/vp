/**
 * CommonService
 *
 * @description To handle service to do any method call from here
 * @author  Vibhay
 * @Date 03/09/2021
 */

module.exports = {
  statusUpdate: async (data, context, req, res) => {
    if ((!data.status) || typeof data.status == undefined) {
      res.status(404).json({ "success": false, "error": { "code": 404, "message": "Status required" } });
    } else if ((!data.id) || typeof data.id == undefined) {
      res.status(404).json({ "success": false, "error": { "code": 404, "message": "Id required" } });
    }
    else {
      const Model = sails.models[data.model] || false;      
      if (Model) {
        try {
          const response = await Model.updateOne({ id: data.id },{ status: data.status});
          if(response)res.status(200).json({ "success": true, "message": "Status updated successfully." });
          else res.status(404).json({ "success": false, "error": { "code": 404, "message": "Not found " } });
          
        } catch (error) {
          res.status(404).json({ "success": false, "error": { "code": 404, "message": error.toString() } });
        }

      } else {
        res.status(404).json({ "success": false, "error": { "code": 404, "message": "Model name not correct." } });
      }
    }
  },
  delete: async (data, context, req, res) => {
    if ((!data.id) || typeof data.id == undefined) {
      res.status(404).json({ "success": false, "error": { "code": 404, "message": "Id required" } });
    }
    else {
      const Model = sails.models[data.model] || false;      
      if (Model) {
        try {
          const response = await Model.updateOne({ id: data.id },{ isDeleted: true});
          if(response)res.status(200).json({ "success": true, "message": "Record deleted successfully." });
          else res.status(404).json({ "success": false, "error": { "code": 404, "message": "Not found " } });
          
        } catch (error) {
          res.status(404).json({ "success": false, "error": { "code": 404, "message": error.toString() } });
        }

      } else {
        res.status(404).json({ "success": false, "error": { "code": 404, "message": "Model name not correct." } });
      }
    }
  }
};