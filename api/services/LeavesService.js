/**
 * @DESC:  In this class/files Leaves related functions (save, update, add comment)
 * @Request param: form data values
 * @Return : Success message with required data
 * @Author: Chandra Shekhar.
 */

var bcrypt = require("bcrypt-nodejs");
var nodemailer = require("nodemailer");
var smtpTransport = require("nodemailer-smtp-transport");
const SmtpController = require("../controllers/SmtpController");
const Helper = require("../controllers/HelperController");

var constantObj = sails.config.constants;

module.exports = {
  /**
   *
   * @param {*} data
   * @param {*} context
   * @param {*} req
   * @param {*} res
   * @returns
   * @description Used to add leaves to Leave management
   * @createdAt 16/09/2021
   * @createdBy : Chandra shekhar
   */
  saveLeaves: (data, context, req, res) => {
    try {

      let requiredFieldError = [];
      let requiredFileds = [
        "toEmail",
        "subject",
        "description",
        "leaveStartDate",
        "leaveEndDate",
      ];

      let formData = req.body;
      requiredFileds.forEach(function (value) {
        if (!formData.hasOwnProperty(value)) {
          //REQ_NAME = value.charAt(0).toUpperCase() + value.slice(1);
          requiredFieldError.push(value + " is required");
        }
      });

      if (requiredFieldError.length != 0) {
        return res.status(404).json({
          success: false,
          error: { code: 404, message: requiredFieldError },
        });
      }

      let toEmail = data.toEmail;
      var subject = data.subject;
      var description = data.description;
      var attachment = data.attachment;
      var fromEmail = context.identity.email;
      var designation = context.identity.designation
      var PhoneNumber = context.identity.contactNumber
      var Start_date = data.leaveStartDate
      var End_date = data.leaveEndDate
      data.addedBy = context.identity.id;
      data.employeeName = context.identity.fullName;
      data.fromEmail = context.identity.email;
      sendLeaveEmail({
        fromEmail: fromEmail,
        toemail: toEmail,
        subject: subject,
        fullNname: data.employeeName,
        description: description,
        attachment: attachment,
        designation: designation,
        PhoneNumber: PhoneNumber,
        Start_date: Start_date,
        End_date: End_date
      });

      Leaves.create(data).then((savedleaves) => {
        return res.status(200).json({
          success: true,
          message: constantObj.leaves.LEAVES_SAVED,
        });
      });

    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },
};


/** Send Email to admin for application leave.*/
var sendLeaveEmail = function (options) {

  var fromEmail = options.fromEmail;
  var description = options.description;
  var attachment = options.attachment;
  var subject = options.subject;
  var toemail = options.toemail;
  var userName = options.fullNname;
  var PhoneNumber = options.PhoneNumber;
  var designation = options.designation;
  var Start_date = options.Start_date;
  var End_date = options.End_date;
  firstName = Helper.toTitleCase(userName);

  if (typeof firstName == undefined) {
    firstName = fromEmail;
  }

  message = "";
  style = {
    header: `
        padding:30px 15px;
        text-align:center;
        background-color:#f2f2f2;
        `,
    body: `
        padding:15px;
        height: 140px;
        `,
    hTitle: `font-family: 'Raleway', sans-serif;
        font-size: 37px;
        height:auto;
        line-height: normal;
        font-weight: bold;
        background:none;
        padding:0;
        color:#333;
        `,
    maindiv: `
        width:600px;
        margin:auto;
        font-family: Lato, sans-serif;
        font-size: 14px;
        color: #333;
        line-height: 24px;
        font-weight: 300;
        border: 1px solid #eaeaea;
        `,
    textPrimary: `color:#3e3a6e;
        `,
    h5: `font-family: Raleway, sans-serif;
        font-size: 22px;
        background:none;
        padding:0;
        color:#333;
        height:auto;
        font-weight: bold;
        line-height:normal;
        `,
    m0: `margin:0;`,
    mb3: "margin-bottom:15px;",
    textCenter: `text-align:center;`,
    textleft: `text-align:left;`,
    btn: `padding:10px 30px;
        font-weight:500;
        font-size:14px;
        line-height:normal;
        border:0;
        display:inline-block;
        text-decoration:none;
        `,
    btnPrimary: `
        background-color:#3e3a6e;
        color:#fff;
        `,
    footer: `
        padding:10px 15px;
        font-weight:500;
        color:#fff;
        text-align:center;
        background-color:#000;
        margin-top: 300px;`
    ,
  };

  message +=
    `<div class="container" style="` +
    style.maindiv +
    `">
    <div class="header" style="` +
    style.header +
    `text-align:center">
    <img src="https://mng.jcsoftwaresolution.in/assets/imgpsh_fullsize_anim.png" style="width: 107px;" />
        <h2 style="` +
    style.hTitle +
    style.m0 +
    `">Leave Application</h2>
    </div>
    <div class="body" style="` +
    style.body +
    `">
        <p style="` +
    style.m0 +
    style.mb3 +
    style.textleft +
    `margin-bottom:20px">Hi Sir/Madam </p>
        <p style="` +
    style.m0 +
    style.mb3 +
    style.textleft +
    `margin-bottom:20px">  ` + "Start date: " +
    Start_date + " End date: " + End_date +
    description +
    `</p>
    <p style="` +
    style.m0 +
    style.mb3 +
    style.textleft +
    `margin-bottom:20px">Thankyou<br> Name:` + userName + `<br>Designation: ` + designation +
    ` <br>Phone Number: ` + PhoneNumber +
    `<br>Company: JC software Solution
    </p>

        </div>
        <div class="footer" style="` + style.footer + `">
        &copy 2023 JC Software Solution All rights reserved.
        </div>
    </div>`;

  //var to = ["chander@yopmail.com", "amit@yopmail.com"];
  SmtpController.sendLeaveEmail(fromEmail, toemail, subject, message, userName, attachment);
};
