/**
 * @DESC:  In this class/files ContactUs related functions (save, update, add ContactUs)
 * @Request param: form data values
 * @Return : Success message with required data
 * @Author: Rohit kumar.
 */

 var constantObj = sails.config.constants;
 
 module.exports = {
   /**
    *
    * @param {*} data
    * @param {*} context
    * @param {*} req
    * @param {*} res
    * @returns
    * @description Used to add ContactUs
    * @createdAt 09/11/2021
    * @createdBy : Rohit kumar
    */
    saveContactUs: async (data, context, req, res) => {
    try {
       let requiredFieldError = [];
       let requiredFileds = [
         "name",
         "email",
         "description",
       ];
 
       let formData = req.body;
       requiredFileds.forEach(function (value) {
         if (!formData.hasOwnProperty(value)) {
           requiredFieldError.push(value + " is required");
         }
       });
 
       if (requiredFieldError.length != 0) {
         return res.status(404).json({
           success: false,
           error: { code: 404, message: requiredFieldError },
         });
       }
 
       var name = data.name;
        ContactUs.create(data).then((savedcontent) => {
            return res.status(200).json({
                success: true,
                message: constantObj.ContactUs.CONTACTUS_SAVED,
            });
        });
     } catch (error) {
       return res.status(400).json({
         success: false,
         error: { message: error },
       });
     }
    },

    /***
	 * @description : Used to update the ContactUs
	 * @createdAt : 09/12/2021
	 */
    updateContactUs: (data, context, req, res)=> {
		let _id = data.id;
        ContactUs.update({ id: _id }, data).then( (content)=> {
			return res.status(200).json({
				"success": true,				
				"message": constantObj.ContactUs.UPDATED_CONTACTUS,
			});
		}).catch((err)=>{
			return res.status(400).json({ "success": false,  "error":{"code": 400,"message": " "+err } })
		});
	},
 };