/**
 * UpcomingEventsController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

var ObjectId = require("mongodb").ObjectID;
const db = sails.getDatastore().manager


module.exports = {

    /**
            *
            * @description: Api Used to get work Anniversaries of employees
            * @createdAt : 06/10/2023
            * @createdBy :Kawalpreet Kaur
            */
    //     workAnniversary: async (req, res) => {

    //         try {
    //             let obj = {};
    //             var array =[];

    //             const employees = await Users.find({ "isDeleted": false, "status": "active" });

    //             if (employees && employees.length > 0) {
    //                 for await (let employee of employees) {

    //                     var employee_name = employee.fullName;
    //                     var doj_date = new Date(employee.dateOfJoining);
    //                     var employee_doj = employee.dateOfJoining;
    //                     console.log(employee_doj, "===============employee_doj");
    //                     var doj_year = employee_doj.substring(0, 4);
    //                     var doj_month = employee_doj.substring(5, 7);
    //                     var doj_day = employee_doj.substring(8, 10);
    //                     var anni_year;

    //                     var date = new Date();
    //                     const current_date = date.toISOString().substring(0, 10);
    //                     var current_year = current_date.substring(0, 4);

    //                     if(doj_year == current_year){
    //                         anni_year = Number(current_year)+1;
    //                     }else{
    //                         anni_year = current_year;
    //                     }

    //                     var anniversary_date = `${anni_year}` + "-" + `${doj_month}` + "-" + `${doj_day}`;
    //                     var anni_date = new Date(anniversary_date);


    //                         const result = anni_date - date;
    //                         //console.log(result, "=========result");
    //                         var days = result / (1000 * 60 * 60 * 24);
    //                         //console.log(days, "=========days");


    //                     // var appraisal_year = Number(doj_year) + 1;
    //                     // var appraisal_date = `${appraisal_year}` + "-" + `${doj_month}` + "-" + `${doj_day}`;
    //                     // var user_appraisal_date = new Date(appraisal_date);
    //                     var anniversary_year = Number(doj_year) + 1;

    //                     // const result = date - doj_date;
    //                     // //console.log(result, "=========result");
    //                     // var days = result / (1000 * 60 * 60 * 24);
    //                     // //console.log(days, "=========days");
    //                     obj.emp_name = employee_name;
    //                     obj.doj = employee_doj;
    //                     obj.anniversary = anniversary_date;
    //                     obj.days_left = days;
    // console.log(obj,"=====================obj")

    //                      array.push(obj);


    //                     console.log(employee_name, "===============employee_name");
    //                     console.log(employee_doj, "===============employee_doj");
    //                     console.log(anniversary_date, "===============anniversary_date");
    //                     console.log(employee.dob, "===============dob");
    //                     console.log(days, "=========days");
    //                     console.log("________________________________")
    //                 }
    //             }

    // if(employees){
    //     return res.status(200).json({
    //         success: true,
    //         code: 200,
    //         data: array,
    //       });
    // }






    //         } catch (err) {
    //             console.log(err);
    //             return res.status(400).json({
    //                 success: false,
    //                 error: { code: 400, message: "" + err }
    //             })

    //         }

    //     }
    workAnniversary: async (req, res) => {

        try {
            var search = req.param('search');
            //var isDeleted = req.param('isDeleted');
            var page = req.param('page');
            var count = parseInt(req.param('count'));
            let sortBy = req.param("sortBy");
            let addedBy = req.param('addedBy');

            var query = {};
            if (search) {
                query.$or = [
                    { fullName: { $regex: search, '$options': 'i' } },
                ]
            }
            //let sortquery = {};
            let sortquery = { days: -1 };
            // if (sortBy) {
            //     let typeArr = [];
            //     typeArr = sortBy.split(" ");
            //     let sortType = typeArr[1];
            //     let field = typeArr[0];
            //     sortquery[field ? field : 'updatedAt'] = sortType ? (sortType == 'desc' ? -1 : 1) : -1;
            // } else {
            //     sortquery = { days: 1 }
            // }
            query.role = { "$nin": ["admin", "subadmin"] };
            query.status = "active";
            query.isDeleted = false;

            // if (isDeleted) {
            //     if (isDeleted === 'true') {
            //         isDeleted = true;
            //     } else {
            //         isDeleted = false;
            //     }
            //     query.isDeleted = isDeleted;
            // } else {
            //     query.isDeleted = false;
            // }
            if (addedBy) {
                query.addedBy_id = ObjectId(addedBy);
            }

            const pipeline = [
                {
                    $project: {
                        // days: "$days",
                        fullName: "$fullName",
                        dateOfJoining: "$dateOfJoining",
                        role: "$role",
                        status: "$status",
                        isDeleted: "$isDeleted",
                        addedBy: "$addedBy",
                        updatedBy: "$updatedBy",
                        createdAt: "$createdAt",
                        updatedAt: "$updatedAt",
                    }
                },
                {
                    $match: query
                }
            ]
            db.collection('users').aggregate([...pipeline]).toArray((err, totalResult) => {
                if (page && count) {
                    var skipNo = (page - 1) * count;
                    // pipeline.push(
                    //     {
                    //         $skip: Number(skipNo)
                    //     },
                    //     {
                    //         $limit: Number(count)
                    //     })
                }
                db.collection('users').aggregate([...pipeline]).toArray(async (err, result) => {

                    // if (result && result.length > 0) {
                    for await (let employee of result) {

                        var employee_name = employee.fullName;
                        var doj_date = new Date(employee.dateOfJoining);
                        var employee_doj = employee.dateOfJoining;
                        var doj_year = employee_doj.substring(0, 4);
                        var doj_month = employee_doj.substring(5, 7);
                        var doj_day = employee_doj.substring(8, 10);
                        var anni_year;
                        var days;

                        var date = new Date();
                        const current_date = date.toISOString().substring(0, 10);
                        var current_year = current_date.substring(0, 4);

                        if (doj_year == current_year) {
                            anni_year = Number(current_year) + 1;
                        } else {
                            anni_year = current_year;
                        }
                        var anniversary_date;
                        var anni_date

                        anniversary_date = `${anni_year}` + "-" + `${doj_month}` + "-" + `${doj_day}`;
                        anni_date = new Date(anniversary_date);

                        if (anniversary_date >= current_date) {
                            const result = anni_date - date;
                            days = result / (1000 * 60 * 60 * 24);
                        } else {
                            anni_year = Number(current_year) + 1;
                            anniversary_date = `${anni_year}` + "-" + `${doj_month}` + "-" + `${doj_day}`;
                            anni_date = new Date(anniversary_date);

                            const result2 = anni_date - date;
                            days = result2 / (1000 * 60 * 60 * 24);
                        }

                        var years = anni_year - doj_year;
                        employee.anniversary_date = anniversary_date;
                        employee.days = days;
                        employee.years = years;
                    }
                    var data = result.sort((a, b) => a.days - b.days).slice(skipNo, page * count);
                    //result.sort((a, b) => a.days - b.days);

                    return res.status(200).json({
                        "success": true,
                        "data": data,
                        "total": totalResult.length,
                    });
                })
            })
        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { code: 400, message: "" + err }
            })

        }
    },
    birthdays: async (req, res) => {
        try {
            var search = req.param('search');
            //var isDeleted = req.param('isDeleted');
            var page = req.param('page');
            var count = parseInt(req.param('count'));
            let sortBy = req.param("sortBy");
            let addedBy = req.param('addedBy');

            var query = {};
            if (search) {
                query.$or = [
                    { fullName: { $regex: search, '$options': 'i' } },
                ]
            }
            //let sortquery = {};
            let sortquery = { days: -1 };
            // if (sortBy) {
            //     let typeArr = [];
            //     typeArr = sortBy.split(" ");
            //     let sortType = typeArr[1];
            //     let field = typeArr[0];
            //     sortquery[field ? field : 'updatedAt'] = sortType ? (sortType == 'desc' ? -1 : 1) : -1;
            // } else {
            //     sortquery = { days: 1 }
            // }
            query.role = { "$nin": ["admin", "subadmin"] };
            query.status = "active";
            query.isDeleted = false;

            // if (isDeleted) {
            //     if (isDeleted === 'true') {
            //         isDeleted = true;
            //     } else {
            //         isDeleted = false;
            //     }
            //     query.isDeleted = isDeleted;
            // } else {
            //     query.isDeleted = false;
            // }
            if (addedBy) {
                query.addedBy_id = ObjectId(addedBy);
            }

            const pipeline = [
                {
                    $project: {
                        days: "$days",
                        fullName: "$fullName",
                        dob: "$dob",
                        role: "$role",
                        status: "$status",
                        isDeleted: "$isDeleted",
                        addedBy: "$addedBy",
                        updatedBy: "$updatedBy",
                        createdAt: "$createdAt",
                        updatedAt: "$updatedAt",
                    }
                },
                {
                    $match: query
                },
                // {
                //     $sort: sortquery
                // },
            ]
            db.collection('users').aggregate([...pipeline]).toArray((err, totalResult) => {
                if (page && count) {
                    var skipNo = (page - 1) * count;
                    // pipeline.push(
                    //     {
                    //         $skip: Number(skipNo)
                    //     },
                    //     {
                    //         $limit: Number(count)
                    //     })
                }
                db.collection('users').aggregate([...pipeline]).toArray(async (err, result) => {

                    // if (result && result.length > 0) {
                    for await (let employee of result) {

                        var employee_dob = employee.dob;
                        var dob_year = employee_dob.substring(0, 4);
                        var dob_month = employee_dob.substring(5, 7);
                        var dob_day = employee_dob.substring(8, 10);
                        var bith_year;
                        var days;

                        var date = new Date();
                        const current_date = date.toISOString().substring(0, 10);
                        var current_year = current_date.substring(0, 4);

                        // if (dob_year == current_year) {
                        //     anni_year = Number(current_year) + 1;
                        // } else {
                        //     anni_year = current_year;
                        // }
                        var birthday_date;
                        var birth_date;

                        birthday_date = `${current_year}` + "-" + `${dob_month}` + "-" + `${dob_day}`;
                        birth_date = new Date(birthday_date);

                        if (birthday_date >= current_date) {
                            const result = birth_date - date;
                            days = result / (1000 * 60 * 60 * 24);
                        } else {
                            bith_year = Number(current_year) + 1;
                            birthday_date = `${bith_year}` + "-" + `${dob_month}` + "-" + `${dob_day}`;
                            birth_date = new Date(birthday_date);

                            const result2 = birth_date - date;
                            days = result2 / (1000 * 60 * 60 * 24);
                        }
                        var years = bith_year - dob_year;

                        employee.birthday_date = birthday_date;
                        employee.days = days;
                        employee.years = years;
                    }
                    //result.sort((a, b) => a.days - b.days);
                    var data = result.sort((a, b) => a.days - b.days).slice(skipNo, page * count);

                    return res.status(200).json({
                        "success": true,
                        "data": data,
                        "total": totalResult.length,
                    });
                })
            })
        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { code: 400, message: "" + err }
            })

        }
    },

    upcomingAppraisals: async (req, res) => {
        try {
            var search = req.param('search');
            var page = req.param('page') || 1;
            var count = parseInt(req.param('count')) || 10;
            let sortBy = req.param("sortBy");
            let addedBy = req.param('addedBy');

            var query = {};

            if (search) {
                query.$or = [
                    { name: { $regex: search, '$options': 'i' } },
                ]
            }

            query.status = "Pending";
            query.isDeleted = false;

            //let sortquery = { updatedAt: -1 };


            const pipeline = [
                {
                    $sort: {
                        "date": 1
                    }
                },
                {
                    $group: {
                        "_id": "$employeeId",
                        "email": { "$first": "$email" },
                        "employeeId": { "$last": '$employeeId' },
                        "name": { "$last": '$name' },
                        "previousAmt": { "$last": '$previousAmt' },
                        "appraisalAmt": { "$last": '$appraisalAmt' },
                        "date": { "$last": '$date' },
                        "currentAmt": { "$last": '$currentAmt' },
                        "status": { "$last": '$status' },
                        "isDeleted": { "$last": '$isDeleted' },
                        "addedBy": { "$last": '$addedBy' },
                        "updatedBy": { "$last": '$updatedBy' },
                        "createdAt": { "$last": '$createdAt' },
                        "updatedAt": { "$last": '$updatedAt' },
                        "days": { "$last": '$days' },
                    }
                },
                {
                    $project: {
                        employeeId: "$_id",
                        email: "$email",
                        name: "$name",
                        previousAmt: "$previousAmt",
                        appraisalAmt: "$appraisalAmt",
                        date: "$date",
                        currentAmt: "$currentAmt",
                        employeeId: "$employeeId",
                        status: "$status",
                        isDeleted: "$isDeleted",
                        addedBy: "$addedBy",
                        updatedBy: "$updatedBy",
                        createdAt: "$createdAt",
                        updatedAt: "$updatedAt",
                    }
                },
                {
                    $match: query
                },
            ]
            db.collection('appraisal').aggregate([...pipeline]).toArray(async (err, totalResult) => {
                if (err) {
                    return res.status(400).json({
                        success: false,
                        error: { message: "" + err },
                    });
                } else {

                    if (page && count) {
                        var skipNo = (page - 1) * count;
                        // pipeline.push(
                        //     {
                        //         $skip: Number(skipNo)
                        //     },
                        //     {
                        //         $limit: Number(count)
                        //     })
                    }
                }
                db.collection('appraisal').aggregate([...pipeline]).toArray(async (err, result) => {
                    if (err) {
                        return res.status(400).json({
                            success: false,
                            error: { message: "" + err },
                        });
                    }
                    else {
                        if (result && result.length > 0) {
                            for await (var employees of result) {

                                var emp_id = String(employees.employeeId);
                                var query = {};
                                query.id = emp_id;
                                query.status = "active";
                                query.isDeleted = false;

                                const employee_details = await Users.find(query);

                                var last_appraisal = employees.date;
                                var appraisal_year = last_appraisal.substring(0, 4);
                                var appraisal_month = last_appraisal.substring(5, 7);
                                var appraisal_day = last_appraisal.substring(8, 10);
                                var next_appraisal_year = Number(appraisal_year) + 1;
                                var days;

                                var date = new Date();
                                const current_date = date.toISOString().substring(0, 10);
                                var current_year = current_date.substring(0, 4);

                                var appraisal_date;
                                var appr_date;

                                appraisal_date = `${next_appraisal_year}` + "-" + `${appraisal_month}` + "-" + `${appraisal_day}`;
                                appr_date = new Date(appraisal_date);


                                if (appraisal_date > current_date) {
                                    var date_result = appr_date - date;
                                    var days = date_result / (1000 * 60 * 60 * 24);

                                }
                                employees.appraisal_date = appraisal_date;
                                employees.days = days;
                            }
                        }
                        var data = result.sort((a, b) => a.days - b.days).slice(skipNo, page * count);

                        return res.status(200).json({
                            "success": true,
                            "data": data,
                            "total": totalResult.length,
                        });
                    }
                })
            })
        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { code: 400, message: "" + err }
            })

        }
    },
};

