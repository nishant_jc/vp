/**
 * WeeklyplanController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */


 var constantObj = sails.config.constants;
 const reader = require('xlsx');


module.exports = {
  /**
   *
   * @param {*} req
   * @param {*} res
   * @description: Used to add Weeklyplan
   * @createdAt 25/10/2021
   * @createdBy : Rohit kumar
   */

   addWeeklyplan: (req, res) => {
    API(WeeklyplanService.saveWeeklyplan, req, res);
  },

  /**
   *
   * @param {*} req
   * @param {*} res
   * @description: Used to edit Service
   * @createdAt 25/10/2021
   * @createdBy : Rohit kumar
   */

  edit: (req, res) => {
    API(WeeklyplanService.updateWeeklyplan, req, res);
  },


  /**
   *
   * @param {*} res
   * @returns
   * @description: Api Used to get Services
   * @createdAt : 25/10/2021
   * @createdBy Rohit kumar
   */

   getAllWeeklyplan: async (req, res, next)=> {
    var search = req.param("search");
    var sortBy = req.param("sortBy");
    var page = req.param("page");
    if (page == undefined) {
      page = 1;
    }
    var count = req.param("count");
    if (count == undefined) {
      count = 100;
    }
    var skipNo = (page - 1) * count;

    var query = {};
    if (sortBy) {
      sortBy = sortBy.toString();
    } else {
      sortBy = "createdAt desc";
    }

    query.isDeleted = false;
    await Weeklyplan.count(query)
      .exec( (err, total)=> {

        if (err) {
          return res.status(400).json({
            success: false,
            error: err,
          });
        } else {
            Weeklyplan.find(query)
            .sort(sortBy)
            .exec( (err, Weeklyplan)=> {
              if (err) {
                return res.status(400).json({ success: false, error: { code: 400, message: err } });
              } else {
                return res.json({
                  success: true,
                  code: 200,
                  data: Weeklyplan,
                  total: total,
                });
              }
            });
        }
      });
  },


  /**
   *
   * @param {id} req
   * @param {*} res
   * @returns
   * @description: Api Used to get Weeklyplan by projectID
   * @createdAt : 25/10/2021
   * @createdBy Rohit kumar
   */

   getWeeklyplanbyuserID: async (req, res) => {
    try {
      let projectID = req.param("id");
      if (projectID == null) {
        return res.status(404).json({
          success: false,
          message: constantObj.Weeklyplan.ID_REQUIRED,
        });
      } else {
        var WeeklyplanData = await Weeklyplan.find({ project_id: projectID,isDeleted:false }).sort('updatedAt DESC');;
        if (WeeklyplanData) {
          return res.status(200).json({
            success: true,
            message: constantObj.Weeklyplan.GET_DATA,
            data: WeeklyplanData,
          });
        } else {
          return res.status(404).json({
            success: false,
            message: constantObj.Weeklyplan.NO_RESULT,
          });
        }
      }
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },


  /**
   *
   * @param {id} req
   * @param {*} res
   * @returns
   * @description: Api Used to get single Service by ID
   * @createdAt : 25/10/2021
   * @createdBy Rohit kumar
   */

   getSingleWeeklyplan: async (req, res) => {
    try {
      let WeeklyplanID = req.param("id");
      if (WeeklyplanID == null) {
        return res.status(404).json({
          success: false,
          message: constantObj.Weeklyplan.ID_REQUIRED,
        });
      } else {
        var WeeklyplanData = await Weeklyplan.findOne({ id: WeeklyplanID,isDeleted:false });
        if (WeeklyplanData) {
          return res.status(200).json({
            success: true,
            message: constantObj.Weeklyplan.GET_DATA,
            data: WeeklyplanData,
          });
        } else {
          return res.status(404).json({
            success: false,
            message: constantObj.Weeklyplan.NO_RESULT,
          });
        }
      }
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },
  

  /**
   *
   * @param {id} req
   * @param {*} res
   * @returns
   * @description: Api Used to delete Service by ID
   * @createdAt : 25/10/2021
   * @createdBy Rohit kumar
   */

   WeeklyplanDeletedByID: async (req, res) => {
    try {
       let deletedID = req.param("id");
       if (deletedID == undefined) {
        return res.status(404).json({
          success: false,
          message: constantObj.Weeklyplan.ID_REQUIRED,
        });
      }
      var deletedByUser = req.identity.id;
      var deletedData = await Weeklyplan.updateOne({
        id: deletedID,
      }).set({ isDeleted: true, deletedBy: deletedByUser });
        
        if (deletedData) {
          return res.status(200).json({
            success: true,
            message: constantObj.Weeklyplan.DELETED_WEEKLYPLAN,
          });
        }
   
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },
  
  /**
   *
   * @param {id} req
   * @param {*} res
   * @returns
   * @description: Api Used to get single weeklyplan in json by ID
   * @createdAt : 17/11/2021
   * @createdBy Rohit kumar
   */

   getSingleWeeklyplanjson: async (req, res) => {
    try {
      let WeeklyplanID = req.param("id");
      if (WeeklyplanID == null) {
        return res.status(404).json({
          success: false,
          message: constantObj.Weeklyplan.ID_REQUIRED,
        });
      } else {
        var WeeklyplanData = await Weeklyplan.findOne({ id: WeeklyplanID,isDeleted:false });
        if (WeeklyplanData) {
          var filename = WeeklyplanData.documents;
          const file = await reader.readFile('assets/'+ filename)
  
          let data = []
            
          const sheets = file.SheetNames
            
          for(let i = 0; i < sheets.length; i++)
          {
            const temp = reader.utils.sheet_to_json(
                  file.Sheets[file.SheetNames[i]])
            temp.forEach((res) => {
                data.push(res)
            })
          }
          return res.status(200).json({
            success: true,
            message: constantObj.Weeklyplan.GET_DATA,
            data: data,
          });
        } else {
          return res.status(404).json({
            success: false,
            message: constantObj.Weeklyplan.NO_RESULT,
          });
        }
      }
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },
  

};

