/**
 * NewUserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const db = sails.getDatastore().manager;
var ObjectId = require("mongodb").ObjectID;

module.exports = {

    newUser : async(req,res)=>{
        try {
           
        var search = req.query.search;
        let page = req.param('page') || 1;
        let count = req.param('count') || 10;
        var status = req.param("status");
        var isDeleted = req.param("isDeleted");


        var query = {};

        if (search) {
            query.$or = [
                { fullName: { $regex: search, '$options': 'i' } },

            ];
        };

        query.status = "active"
        query.isDeleted = false;

        const pipeline = [
            {
                $project: {
                    _id: "$_id",
                    fullName: "$fullName",
                    email: "$email",
                    designation: "$designation",
                    isDeleted: "$isDeleted",
                    status: "$status"
                   
                }
            },
            {
                $match: query,
            }
        ];
        let totalResult = await db.collection('users').aggregate(pipeline).toArray()
        if (page && count) {
            var skipNo = (page - 1) * count;
            pipeline.push(
                {
                    $skip: Number(skipNo)
                },
                {
                    $limit: Number(count)
                })
        }
       
        let result = await db.collection('users').aggregate(pipeline).toArray()

        let resData = {
            total: totalResult ? totalResult.length : 0,
            data: result ? result : []
        }
        if (!req.param('page') && !req.param('count')) {
            resData.data = totalResult ? totalResult : []
        }
        return res.status(200).json({
            success: true,
            total: totalResult.length,
            data: result,
        });
    }
    catch (err) { 
        return res.status(500).json({
            success: false,
            error: { code: 500, message: '' + err },
        });
    }
}
       
               

};
