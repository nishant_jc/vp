/**
 * DailyTaskController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
 var constantObj = sails.config.constants;
 var async = require("async");
 module.exports = {
    /**
     *
     * @param {*} req
     * @param {*} res
     * @description: Used to add DailyTask
     * @createdAt 08/12/2021
     * @createdBy : Rohit kumar
     */
  
     addDailyTask: (req, res) => {
      API(DailyTaskService.saveDailyTask, req, res);
    },
  
    /**
     *
     * @param {*} req
     * @param {*} res
     * @description: Used to edit DailyTask
     * @createdAt 08/12/2021
     * @createdBy : Rohit kumar
     */
  
    edit: (req, res) => {
      API(DailyTaskService.updateDailyTask, req, res);
    },
  
  
    /**
     *
     * @param {*} res
     * @returns
     * @description: Api Used to get DailyTaskbyuserid
     * @createdAt : 08/12/2021
     * @createdBy Rohit kumar
     */
  
     getDailyTaskbyuserID: async (req, res, next)=> {
      var search = req.param("search");
      var sortBy = req.param("sortBy");
      var page = req.param("page");
      if (page == undefined) {
        page = 1;
      }
      var count = req.param("count");
      if (count == undefined) {
        count = 100;
      }
      var skipNo = (page - 1) * count;
  
      var query = {};
      if (sortBy) {
        sortBy = sortBy.toString();
      } else {
        sortBy = "createdAt desc";
      }
  
      query.isDeleted = false;
      await DailyTask.count(query)
        .exec( (err, total)=> {
  
          if (err) {
            return res.status(400).json({
              success: false,
              error: err,
            });
          } else {
            DailyTask.find(query)
              .sort(sortBy)
              .skip(skipNo)
              .limit(count)
             .exec( (err, DailyTask)=> {
                if (err) {
                  return res.status(400).json({ success: false, error: { code: 400, message: err } });
                } else {
                  return res.json({
                    success: true,
                    code: 200,
                    data: DailyTask,
                    total: total,
                  });
                }
              });
          }
        });
    },
  
  
    /**
   *
   * @param {*} req
   * @param {*} res
   * @description: Used to get DailyTask by date
   * @createdAt 08/12/2021
   * @createdBy : Rohit kumar
   */
    getAllDailyTask: async (req, res) => {
       try{ 
        var date = req.param("date");
        var utc = new Date().toJSON().slice(0,10).replace(/-/g,'-');
        var currentDate = "";
        if(date){
        currentDate = date;
        }else{
        currentDate = utc;
        }
        let DailyTaskdata = [];
        let query = {};

        DailyTask.find(query).populate("addedBy").exec(async (err, totalResult) => {
            async.each(totalResult, async function (userdata, callback) {
                var tasksdate=userdata.createdAt.toJSON().slice(0,10).replace(/-/g,'-');
                if (tasksdate == currentDate) {
                    DailyTaskdata.push(userdata);
                }
                else{
                  DailyTaskdata.push(userdata);
                }
            });
            if (DailyTaskdata && !DailyTaskdata.length) {
            return res.status(200).json({
                success: true,
                message: constantObj.DailyTask.NO_RESULT,
            });
            } else {
            return res.status(200).json({
                success: true,
                message: constantObj.DailyTask.GET_DATA,
                data: DailyTaskdata,
            });
            }

        })
      }catch(err){
        console.log(err)
      }
    },
  
  
    /**
     *
     * @param {id} req
     * @param {*} res
     * @returns
     * @description: Api Used to get single DailyTask by ID
     * @createdAt : 08/12/2021
     * @createdBy Rohit kumar
     */
  
     getSingleDailyTask: async (req, res) => {
      try {
        let DailyTaskID = req.param("id");
        if (DailyTaskID == null) {
          return res.status(404).json({
            success: false,
            message: constantObj.DailyTask.ID_REQUIRED,
          });
        } else {
          var DailyTaskData = await DailyTask.find({ id: DailyTaskID,isDeleted:false }).limit(1);
          if (DailyTaskData) {
            return res.status(200).json({
              success: true,
              message: constantObj.DailyTask.GET_DATA,
              data: DailyTaskData,
            });
          } else {
            return res.status(404).json({
              success: false,
              message: constantObj.DailyTask.NO_RESULT,
            });
          }
        }
      } catch (error) {
        return res.status(400).json({
          success: false,
          error: { message: error },
        });
      }
    },
    
  
    /**
     *
     * @param {id} req
     * @param {*} res
     * @returns
     * @description: Api Used to delete DailyTask by ID
     * @createdAt : 08/12/2021
     * @createdBy Rohit kumar
     */
  
     DailyTaskDeletedByID: async (req, res) => {
      try {
         let deletedID = req.param("id");
         if (deletedID == undefined) {
          return res.status(404).json({
            success: false,
            message: constantObj.DailyTask.ID_REQUIRED,
          });
        }
        var deletedByUser = req.identity.id;
        var deletedData = await DailyTask.updateOne({
          id: deletedID,
        }).set({ isDeleted: true, deletedBy: deletedByUser });
          
          if (deletedData) {
            return res.status(200).json({
              success: true,
              message: constantObj.DailyTask.DELETED_DAILYTASK,
            });
          }
     
      } catch (error) {
        return res.status(400).json({
          success: false,
          error: { message: error },
        });
      }
    },
    
  };

