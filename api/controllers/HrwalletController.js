/**
 * HrwalletController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
var constantObj = sails.config.constants;

module.exports = {

    addMoneyToHrWallet: async (req, res) => {
        try {
            let data = req.body;
            // const date = new Date();
            // const new_date = date.toISOString().substring(0, 10);

            // data.date = new_date,
            data.updatedBy = req.identity.id;
            data.addedBy = req.identity.id;


            const new_data = await Hrwallet.create(data).fetch();

            if (new_data) {
                return res.status(200).json({
                    success: true,
                    message: constantObj.HrWallet.ADD_MONEY_SUCCESS
                });
            }

        } catch (error) {
            return res.status(400).json({
                success: false,
                error: { message: error },
            });
        }

    },

    getHrWalletDetails: async (req, res) => {

        try {
            var date = new Date();
            var current_date = date.toISOString().substring(0, 10);
            var current_year = date.toISOString().substring(0, 4);
            var current_month = date.toISOString().substring(5, 7);
        
            var starting_date = `${current_year}` + "-" + `${current_month}` + "-01";
            var ending_date = current_date;

            var start_date = req.param('start_date') ? req.param('start_date') : starting_date;
            var end_date = req.param('end_date') ? req.param('end_date') : ending_date;


            let total = 0;
            let total_expenses = 0;
            let query = {};

            
                query.date = {
                    '>=': start_date,
                    '<=': end_date
                };

                query.isDeleted = "false";

            const find_wallet = await Hrwallet.find(query);

            for (items of find_wallet) {
                var amount = items.amount;
                total += amount;
            }

            const find_expenses = await HrExpenses.find(query).sort("date DESC");

            for (itm of find_expenses) {
                var expenses = itm.amount;
                total_expenses += Number(expenses);
            }


            var left_amount = total - total_expenses;


                return res.status(200).json({
                    success: true,
                    message: constantObj.HrWallet.DETAILS_FOUND,
                    total_advance: total,
                    total_expenses: total_expenses,
                    left_amount: left_amount,
                    total_records:find_expenses.length,
                    expenses_data: find_expenses
                });


        } catch (error) {
            return res.status(400).json({
                success: false,
                error: { message: error },
            });
        }

    },
    getAdvanceMoneyDetails: async (req, res) => {

        try{

            var date = new Date();
            var current_date = date.toISOString().substring(0, 10);
            var current_year = date.toISOString().substring(0, 4);
            var current_month = date.toISOString().substring(5, 7);
        
            var starting_date = `${current_year}` + "-" + `${current_month}` + "-01";
            var ending_date = current_date;

            var start_date = req.param('start_date') ? req.param('start_date') : starting_date;
            var end_date = req.param('end_date') ? req.param('end_date') : ending_date;
    
    
            let total = 0;
            let total_expenses = 0;
            let query = {};
    
            
                query.date = {
                    '>=': start_date,
                    '<=': end_date
                };
    
                query.isDeleted = "false";
    
    
        const advance_money_details = await Hrwallet.find(query).sort("createdAt DESC");

        for (items of advance_money_details) {
            var amount = items.amount;
            total += amount;
        }
    
    
        return res.status(200).json({
            success: true,
            message: constantObj.HrWallet.DETAILS_FOUND,
            total_amount:total,
            total_records: advance_money_details.length,
            add_advance_data: advance_money_details
        });
    
    
        } catch (error) {
            console.log(error)
            return res.status(400).json({
                success: false,
                error: { message: error },
            });
        }
    }

};

