/**
 * ReviewTaskController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const constants = require('../../config/constants').constants;
const db = sails.getDatastore().manager;
var constantObj = sails.config.constants;
module.exports = {


    /**
    *
    * @description:
    * @createdAt : 19/06/2023
    * @createdBy :Vishal Tyagi
    */

    addTaskReview: async (req, res) => {
        const data = req.body;

        try {

            if (data.taskProject && data.taskProject.length > 0) {

                for await (let item of data.taskProject) {
                    let addReviewTask = await ReviewTask.create(item).fetch();
                }
                        return res.status(200).json({
                            success: true,
                            code: 200,
                            //data: addReviewTask,
                            message: constantObj.reviewTask.ADD,
                        });
            }


        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { "code": 400, "message": "" + err }
            });
        }

    },

    taskReviewList: async (req, res) => {

        try {
            var search = req.param("search");
            var sortBy = req.param("sortBy");
            var page = req.param("page");
            var count = req.param("count");
            if (page == undefined) {
                page = 1;
            }
            if (count == undefined) {
                count = 1000;
            }
            var skipNo = (page - 1) * count;
            var query = {};

            if (sortBy) {
                sortBy = sortBy.toString();
            } else {
                sortBy = 1;
            }

            if (search) {
                query.$or = [{ name: { $regex: search, $options: "i" } }];
            }

            query.isDeleted = false;
            db.collection("reviewtask")
                .aggregate([
                    {
                        $lookup: {
                            from: "users",
                            localField: "employee",
                            foreignField: "_id",
                            as: "employee",
                        },
                    },
                    {
                        $unwind: {
                            path: "$employee",
                            preserveNullAndEmptyArrays: true,
                        },
                    },
                    {
                        $lookup: {
                            from: "projects",
                            localField: "project",
                            foreignField: "_id",
                            as: "project",
                        },
                    },
                    {
                        $unwind: {
                            path: "$project",
                            preserveNullAndEmptyArrays: true,
                        },
                    },

                    {
                        $project: {
                            id: "$_id",
                            expectedminutes: "$expectedminutes",
                            expectedhours: "$expectedhours",
                            employee: "$employee",
                            taskName: "$taskName",
                            project: "$project",
                            reviewType: "$reviewType",
                            addedBy: "$addedBy",
                            isDeleted: "$isDeleted",
                            createdAt: "$createdAt",
                        },
                    },
                    {
                        $match: query,
                    },
                ])
                .toArray((err, totalResult) => {
                    db.collection("reviewtask")
                        .aggregate([
                            {
                                $lookup: {
                                    from: "users",
                                    localField: "employee",
                                    foreignField: "_id",
                                    as: "employee",
                                },
                            },
                            {
                                $unwind: {
                                    path: "$employee",
                                    preserveNullAndEmptyArrays: true,
                                },
                            },
                            {
                                $lookup: {
                                    from: "projects",
                                    localField: "project",
                                    foreignField: "_id",
                                    as: "project",
                                },
                            },
                            {
                                $unwind: {
                                    path: "$project",
                                    preserveNullAndEmptyArrays: true,
                                },
                            },
                            {
                                $project: {
                                    id: "$_id",
                                    expectedminutes: "$expectedminutes",
                                    expectedhours: "$expectedhours",
                                    employee: "$employee",
                                    taskName: "$taskName",
                                    project: "$project",
                                    reviewType: "$reviewType",
                                    addedBy: "$addedBy",
                                    isDeleted: "$isDeleted",
                                    createdAt: "$createdAt",
                                },
                            },
                            {
                                $match: query,
                            },
                            { $sort: { createdAt: sortBy } },
                            {
                                $skip: skipNo,
                            },
                            {
                                $limit: Number(count),
                            },
                        ])
                        .toArray((err, result) => {
                            if (err) {
                                return res.status(400).json({
                                    success: false,
                                    error: { message: "" + err },
                                });
                            } else {
                                return res.status(200).json({
                                    success: true,
                                    code: 200,
                                    data: result,
                                    total: totalResult.length,
                                });
                            }
                        });
                });
        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { "code": 400, "message": "" + err }
            });
        }
    },

    getTaskReviewById: async (req, res) => {
        try {
            const id = req.param("id");
            if (!id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.reviewTask.ID_MISSING },
                });
            } else {
                let find = await ReviewTask.findOne({ id: id, });

                if (!find) {
                    return res.status(404).json({
                        success: false,
                        error: { code: 404, message: constantObj.reviewTask.NOT_FOUND },
                    });
                } else {
                    return res.status(200).json({
                        success: true,
                        code: 200,
                        data:find,
                    });
                }
            }

        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { "code": 400, "message": "" + err }
            });
        }
    },

    editTaskReview: async (req, res) => {
        try {
            var data = req.body;
            const id = data.id;

            if (!id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.reviewTask.ID_MISSING },
                });
            } else {
                data.updatedBy = req.identity.id
                var find = await ReviewTask.findOne({ id: id, isDeleted: false });

                if (!find) {
                    return res.status(404).json({
                        success: false,
                        error: { code: 404, message: constantObj.reviewTask.NOT_FOUND },
                    });
                } else {
                    delete data.id;
                    let updated = await ReviewTask.updateOne({ id: id }, data);

                    return res.status(200).json({
                        success: true,
                        code: 200,
                        message: constantObj.reviewTask.UPDATED,
                    });
                }
            }

        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { "code": 400, "message": "" + err }
            });
        }
    },

    removeTaskReview: async (req, res) => {
        try {
            const id = req.param("id");
            if (!id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.reviewTask.ID_MISSING },
                });
            } else {
                var find = await ReviewTask.findOne({ id: id });
                if (!find) {
                    return res.status(404).json({
                        success: false,
                        error: { code: 404, message: constantObj.reviewTask.NOT_FOUND },
                    });
                } else {
                    let removed = await ReviewTask.updateOne({ id: id }, { isDeleted: true });
                    return res.status(200).json({
                        success: true,
                        code: 200,
                        message: constantObj.reviewTask.DELETE,
                    });
                }
            }

        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { "code": 400, "message": "" + err }
            });
        }
    },

};

