/**
 * DeviceController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

var ObjectId = require("mongodb").ObjectID;
var constantObj = sails.config.constants;
const db = sails.getDatastore().manager;

module.exports = {

    addDevice: async (req, res) => {

        try {
            let data = req.body;

            if (!data.device_name) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Device.DEVICE_NAME_REQUIRED }
                });
            }

            if (!data.device_type) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Device.DEVICE_TYPE_REQUIRED }
                });
            }

            if (!data.os_type) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Device.OS_TYPE_REQUIRED }
                });
            }

            if (!data.serial_no) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Device.SERIAL_NO_REQUIRED }
                });
            }

            const exist_data = await Device.findOne({ device_name: data.device_name, device_type: data.device_type, os_type: data.os_type, serial_no: data.serial_no, isDeleted: false });

            if (exist_data) {
                return res.status(400).json({
                    success: false,
                    error: { code: 400, message: constantObj.Device.ALREADY_EXIST }
                });
            }

            data.addedBy = req.identity.id;

            const add_device = await Device.create(data).fetch();

            if (add_device) {
                return res.status(200).json({
                    success: true,
                    code: 200,
                    message: constantObj.Device.ADD_SUCCESS,
                    device_details: add_device
                });
            } else {
                return res.status(200).json({
                    success: false,
                    code: 400,
                    message: constantObj.Device.ADD_FAILED
                });
            }

        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { "code": 400, "message": "" + err }
            });
        }

    },
    getSingleDevice: async (req, res) => {

        try {
            const id = req.param("id");

            if (!id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Device.ID_REQUIRED },
                });

            }

            const device_details = await Device.findOne({ id: id, isDeleted: false });

            if (device_details) {
                return res.status(200).json({
                    success: true,
                    message: constantObj.Device.FOUND,
                    data: device_details,
                });
            } else {
                return res.status(400).json({
                    success: false,
                    message: constantObj.Device.FAILED
                });
            }


        } catch (error) {
            return res.status(400).json({
                success: false,
                error: { code: 400, message: error },
            });
        }

    },

    deviceList: async (req, res) => {

        try {
            var search = req.param('search');
            var device_type = req.param('device_type');
            var isDeleted = req.param('isDeleted');
            var page = req.param('page');
            var count = parseInt(req.param('count'));
            let sortBy = req.param("sortBy");
            let addedBy = req.param('addedBy');

            var date = new Date();
            var current_date = date.toISOString().substring(0, 10);

            var query = {};

            //query.start_date = { '$gte': `${current_date}` };

            // query.$and = [
            //     {
            //     start_date: { '$gte': `${current_date}` },
            //     end_date: { '$lte': "2023-10-31" }
            //     }]

            if (device_type) {
                query.$or = [
                    { device_type: { $regex: device_type, '$options': 'i' } }
                ]
            }
            if (search) {
                query.$or = [
                    { device_name: { $regex: search, '$options': 'i' } }
                ]
            }
            let sortquery = {};
            // if (sortBy) {
            //     let typeArr = [];
            //     typeArr = sortBy.split(" ");
            //     let sortType = typeArr[1];
            //     let field = typeArr[0];
            //     sortquery[field ? field : 'updatedAt'] = sortType ? (sortType == 'desc' ? -1 : 1) : -1;
            // } else {
            sortquery = { updatedAt: -1 }
            //}

            // if (isDeleted) {
            //     if (isDeleted === 'true') {
            //         isDeleted = true;
            //     } else {
            //         isDeleted = false;
            //     }
            query.isDeleted = false;
            // } else {
            //     query.isDeleted = false;
            // }
            if (addedBy) {
                query.addedBy_id = ObjectId(addedBy);
            }

            const pipeline = [
                //     {
                //         $lookup: {
                //         from: 'deviceassign',
                //         localField: 'serial_no',
                //         foreignField: 'serial_no',
                //         as: 'assigned_device_details'
                //        },
                // },
                {
                    $project: {
                        device_name: "$device_name",
                        device_type: "$device_type",
                        os_type: "$os_type",
                        serial_no: "$serial_no",
                        isDeleted: "$isDeleted",
                        addedBy: "$addedBy",
                        updatedBy: "$updatedBy",
                        createdAt: "$createdAt",
                        updatedAt: "$updatedAt",
                        assigned_device_details: "$assigned_device_details"
                    }
                },
                {
                    $match: query
                },
                {
                    $sort: sortquery
                },
            ]
            db.collection('device').aggregate([...pipeline]).toArray((err, totalResult) => {

                if (err) {
                    console.log(err)
                    return res.status(400).json({
                        success: false,
                        error: { code: 400, message: "" + err }
                    })
                }
                if (page && count) {
                    var skipNo = (page - 1) * count;
                    pipeline.push(
                        {
                            $skip: Number(skipNo)
                        },
                        {
                            $limit: Number(count)
                        })
                }
                db.collection('device').aggregate([...pipeline]).toArray(async (err, result) => {
                    if (err) {
                        console.log(err)
                        return res.status(400).json({
                            success: false,
                            error: { code: 400, message: "" + err }
                        })
                    }
                    if (result && result.length > 0) {
                        for await (var device_details of result) {
                            var device_serial_no = device_details.serial_no;

                            const assign_device_details = await DeviceAssign.findOne({ serial_no: device_serial_no, status: "assigned", isDeleted: false });
                            let assigned_data;
                            if (assign_device_details) {
                                let assigned_device_array = [assign_device_details];
                                assigned_data = assigned_device_array;
                            }else{
                                assigned_data = [];
                            }
                            device_details.assigned_device_details = assigned_data;
                        }
                    }
                    return res.status(200).json({
                        "success": true,
                        "data": result,
                        "total": totalResult.length,
                    });
                })
            })
        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { code: 400, message: "" + err }
            })
        }
    },
    updateDevice: async (req, res) => {

        try {
            const data = req.body;

            if (!data.id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Device.ID_REQUIRED },
                });

            }

            const find_device = await Device.findOne({ id: data.id, isDeleted: false });
            if (!find_device) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Device.ID_NOT_FOUND },
                });
            }

            data.updatedBy = req.identity.id;

            const updated_data = await Device.updateOne({ id: data.id, isDeleted: false }).set(data);

            if (updated_data) {
                return res.status(200).json({
                    success: true,
                    message: constantObj.Device.UPDATE_SUCCESS,
                    data: updated_data,
                });

            } else {
                return res.status(400).json({
                    success: false,
                    message: constantObj.Device.UPDATE_FAILED
                });
            }


        } catch (error) {
            return res.status(400).json({
                success: false,
                error: { code: 400, message: error },
            });
        }

    },
    deleteDevice: async (req, res) => {

        try {
            const id = req.param("id");
            var deleted_by = req.identity.id;

            if (!id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Device.ID_REQUIRED },
                });
            }

            const device_data = await Device.findOne({ id: id, isDeleted: false });

            if (!device_data) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Device.ID_NOT_FOUND },
                });
            }

            var deleted_at = new Date();

            const delete_device = await Device.updateOne({ id: id, isDeleted: false }).set({ isDeleted: true, deletedBy: deleted_by, deletedAt: deleted_at });
            if (delete_device) {
                return res.status(200).json({
                    success: true,
                    message: constantObj.Device.DELETE_SUCCESS
                });
            } else {
                return res.status(400).json({
                    success: false,
                    message: constantObj.Device.DELETE_FAILED
                });
            }


        } catch (error) {
            return res.status(400).json({
                success: false,
                error: { code: 400, message: error },
            });
        }
    }










};

