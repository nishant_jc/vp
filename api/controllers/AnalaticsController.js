/**
 * AnalaticsController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const db = sails.getDatastore().manager;
var constantObj = sails.config.constants;
module.exports = {


    getAnalatic: async (req, res) => {

        if (req.param('type') == 'yearly') {
            var condition = {
                "date_year": {
                    // "$year": "$createdAt"
                    "$year": { $toDate: "$createdAt" }
                }
            }
        } else if (req.param('type') == 'monthly') {
            var condition = {
                "date_year": {
                    // "$year": "$createdAt"
                    "$year": { $toDate: "$createdAt" }
                },
                "date_month": {
                    // "$month": "$createdAt"
                    "$month": { $toDate: "$createdAt" }
                }
            }
        } else if (req.param('type') == 'weekly') {
            var condition = {
                "date_year": {
                    // "$year": "$createdAt"
                    "$year": { $toDate: "$createdAt" }
                },
                "date_month": {
                    // "$month": "$createdAt"
                    "$month": { $toDate: "$createdAt" }

                },
                "day": {
                    // "$dayOfWeek": "$createdAt"
                    "$dayOfWeek": { $toDate: "$createdAt" }
                }
            }
        } else if (req.param('type') == 'daily') {
            var condition = {
                "date_year": {
                    // "$year": "$createdAt"
                    "$year": { $toDate: "$createdAt" }
                },
                "date_month": {
                    // "$month": "$createdAt"
                    "$month": { $toDate: "$createdAt" }
                },
                "day": {
                    // "$dayOfWeek": "$createdAt"
                    "$dayOfMonth": { $toDate: "$createdAt" }
                }
            }
        } else {
            var condition = {
                "date_year": {
                    // "$year": "$createdAt"
                    "$year": { $toDate: "$createdAt" }
//  change on $year : createdAt to "$year": { $toDate: "$createdAt" } 
                }
            }
        }
        
        
       
        var inProgress = "inProgress"
        var assigned = "assigned"
        var pending = "pending"
        var blocker = "blocker"
        var done = "done"
        var hold = "hold" 
        var completed = "completed"
        var rejected = "rejected"
        var awarded = "awarded"
        var onhold = "onhold"
        var notStarted = "notStarted"
        var ended = "ended"
        var goingon = "goingon"
        var employee = "employee"
        
        totalUsersCounts(condition, employee, req, async (totalEmployeeCounts) => {
        totalTaskCounts(condition,inProgress, req,async(totalinProgressTask)=>{  
            totalTaskCounts(condition, assigned, req, async (totalAssignedTaskCounts) => {
            totalTaskCounts(condition,pending,req,async(totalpendingTaskCount)=>{
            totalTaskCounts(condition,blocker, req, async(totalBlockerTaskCount)=>{    
            totalTaskCounts(condition,done,req,async(totalDoneTaskCount)=>{
            totalTaskCounts(condition,hold,req,async(totalholdTaskCount)=>{ 
            totalTaskCounts(condition, completed, req, async (totalCompletedTaskCounts) => {
                    projectCounts(condition, inProgress, req, async (totalInProgressProjectsCount) => {
                    projectCounts(condition, rejected, req, async (totalRejectedProjectsCount) => {
                    projectCounts(condition,awarded,req, async(totalAwardedProjectCount)=>{
                    projectCounts(condition,completed,req, async(totalCompletedProjectCount)=>{
                    projectCounts(condition, onhold, req, async (totalOnholdProjectsCount) => {
                    projectCounts(condition,notStarted,req,async(totalNotStartedProjectCount)=>{ 
                    projectCounts(condition, ended, req, async (totalEndedProjectsCount) => {
                    projectCounts(condition,goingon,req,async(totalGoingonProjectCount)=>{
                        return res.status(200).json({
                        success: true,
                        code: 200,
                        totalAssignedTaskCounts: totalAssignedTaskCounts,
                        totalCompletedTaskCounts: totalCompletedTaskCounts,
                        totalinProgressTask:totalinProgressTask,
                        totalpendingTaskCount:totalpendingTaskCount,
                        totalDoneTaskCount:totalDoneTaskCount,
                        totalBlockerTaskCount:totalBlockerTaskCount,
                        totalholdTaskCount:totalholdTaskCount,
                        totalOnholdProjectsCount: totalOnholdProjectsCount,
                        totalInProgressProjectsCount: totalInProgressProjectsCount,
                        totalEndedProjectsCount: totalEndedProjectsCount,
                        totalEmployeeCounts: totalEmployeeCounts,
                        totalCompletedProjectCount:totalCompletedProjectCount,
                        totalRejectedProjectsCount:totalRejectedProjectsCount,
                        totalAwardedProjectCount:totalAwardedProjectCount,
                        totalNotStartedProjectCount:totalNotStartedProjectCount,
                        totalGoingonProjectCount:totalGoingonProjectCount
                    });
                  })
                });
              });
            });
           })
         })
        })
       })
      });
     });
    })
   })
   });
  })
 })
})
},
                                    
                                                                        
                                        
       
       
     getTotalEmployeeCount: async (req, res) => {
         try {
             query = {},
                 query.role = ["developer", "qa", "bde", "seo", "HR","designer"]
             query.isDeleted = false
             var count = await Users.count(query);
 
             if (!count) {
                 return res.status(200).json({
                     success: true,
                     code: 200,
                     "totalEmployeeCounts": [],
                 })
             } else {
                 return res.status(200).json({
                     success: true,
                     code: 200,
                     "totalEmployeeCounts": count,
 
                 });
             }
         } catch (err) {
             return res.status(400).json({ success: true, code: 400, error: err });
         }
     },
 
     getTotalTask: async (req, res) => {
         try {
 
             const status = req.param("status");
 
             query = {},
             query.isDeleted = false
             query.status = status
 
             var count = await Task.count(query);
             if (!count) {
                 return res.status(200).json({
                     success: true,
                     code: 200,
                     "totalTaskCount": [],
                 })
             } else {
                 return res.status(200).json({
                     success: true,
                     code: 200,
                     "totalTaskCount": count,
 
                 });
             }
         } catch (err) {
             return res.status(400).json({ success: true, code: 400, error: err });
         }
     },
 
     getNewEmployee: async (req, res) => {
         try {
             // var role = "employee";
             var role = ["developer", "qa", "bde", "seo", "HR","designer"]
             var d = new Date();
             lastMonthDate = new Date(d.setMonth(d.getMonth() - 3)); //3 month ago
             query = {}
             query.role = role
             query.isDeleted = false
             query.createdAt = { '>': lastMonthDate }
 
             var data = await Users.count(query);
             return res.status(200).json({
                 success: true,
                 code: 200,
                 newEmployee: data,
 
             });
 
         } catch (err) {
             return res.status(400).json({ success: true, code: 400, error: err });
         }
 
     },
 
     getTotalProjects: async (req, res) => {
         try {
 
             // const progress_status = req.param("status");
 
             query = {},
                 query.isDeleted = false
             // query.progress_status = progress_status
 
             var count = await Projects.count(query);
             if (!count) {
                 return res.status(200).json({
                     success: true,
                     code: 200,
                     "TotalProjects": []
                 })
             } else {
                 return res.status(200).json({
                     success: true,
                     code: 200,
                     "TotalProjects": count,
 
                 });
             }
         } catch (err) {
             return res.status(400).json({ success: true, code: 400, error: err });
         }
     },
 
     progressStatusProjectsCount: async (req, res) => {
         try {
 
             const progress_status = req.param("status");
 
             query = {},
                 query.isDeleted = false
             query.progress_status = progress_status
 
             var count = await Projects.count(query);
             if (!count) {
                 return res.status(200).json({
                     success: true,
                     code: 200,
                     "TotalProjects": [],
                 })
             } else {
                 return res.status(200).json({
                     success: true,
                     code: 200,
                     "TotalProjects": count,
 
                 });
             }
         } catch (err) {
             return res.status(400).json({ success: true, code: 400, error: err });
         }
     },
 
 };
 
 var totalUsersCounts = async function (condition, role, req, cb) {
 
     var query = {};
 
     if (req.param('type') == 'yearly') {
         sortBy = { year: 1 }
     } else if (req.param('type') == 'monthly') {
         sortBy = { month: 1 }
     } else if (req.param('type') == 'weekly') {
         sortBy = { day: 1 }
     } else if (req.param('type') == 'daily') {
         sortBy = { day: 1 }
     } else {
         sortBy = { year: 1 }
     }
 
     // var student = "student";
 
     if (role) {
         query.role = role
     }
 
     query.isDeleted = false
     //(query, "-----------query")
     db.collection('users').aggregate([
         {
             $match: query
         },
         {
             "$group": {
                 "_id": condition,
                 "count": {
                     "$sum": 0
                 },
                 "sum": {
                     "$sum": 1
                 }
             }
         },
         {
             $project: {
                 id: "$_id",
                 sum: 1.0,
                 _id: 0.0,
                 role: "$role",
                 createdAt: "$createdAt",
                 year: "$_id.date_year",
                 month: "$_id.date_month",
                 day: "$_id.day",
             }
         },
         { $sort: sortBy }
     ]).toArray((counterr, totalresults) => {
         //(totalresults, "------------------totalresults");
         if (counterr) {
             return cb({
                 success: false,
                 error: counterr
             });
         } else {
 
             if (req.param('type') == 'yearly') {
                 response = []
                 years = []
                 totalresults.forEach(element => {
                     years.push(element.year)
                 });
                 var allYears = years.filter(function (item, i, ar) {
                     return ar.indexOf(item) === i;
                 });
                 allYears.forEach(element => {
                     let obj = {}
                     obj.year = element
                     const results = totalresults.filter(entry => entry.year === element)
                     if (results.length > 0) {
                         obj.totalEmployeeCounts = Math.round(results[0].sum)
                     } else {
                         obj.totalEmployeeCounts = 0
                     }
 
                     response.push(year = obj)
 
                 });
                 response.sort((a, b) => {
                     return a.year - b.year;
                 });
                 return cb({
                     response
                 });
             } else if (req.param('type') == 'monthly') {
                 response = []
                 months = []
                 totalresults.forEach(element => {
                     months.push({ month: element.month, year: element.year })
                 });
 
                 allYears = months.filter((thing, index) => {
                     const _thing = JSON.stringify(thing);
                     return index === months.findIndex(obj => {
                         return JSON.stringify(obj) === _thing;
                     });
                 })
 
                 allYears.forEach(element => {
                     let obj = {}
                     obj.month = element.month
                     obj.year = element.year
                     const results = totalresults.filter(entry => entry.month === element.month && entry.year === element.year)
                     if (results.length > 0) {
                         obj.totalEmployeeCounts = Math.round(results[0].sum)
                     } else {
                         obj.totalEmployeeCounts = 0
                     }
                     response.push(year = obj)
 
                 });
                 response.sort((a, b) => (a.year > b.year) ? 1 : (a.year === b.year) ? ((a.month > b.month) ? 1 : -1) : -1)
                 return cb({
                     response
                 });
             } else if (req.param('type') == 'weekly' || req.param('type') == 'daily') {
                 response = []
                 months = []
                 totalresults.forEach(element => {
                     months.push({ day: element.day, month: element.month, year: element.year })
                 });
 
                 allYears = months.filter((thing, index) => {
                     const _thing = JSON.stringify(thing);
                     return index === months.findIndex(obj => {
                         return JSON.stringify(obj) === _thing;
                     });
                 })
 
                 allYears.forEach(element => {
                     let obj = {}
                     obj.day = element.day
                     obj.month = element.month
                     obj.year = element.year
                     const results = totalresults.filter(entry => entry.day === element.day && entry.month === element.month && entry.year === element.year)
                     if (results.length > 0) {
                         obj.totalEmployeeCounts = Math.round(results[0].sum)
                     } else {
                         obj.totalEmployeeCounts = 0
                     }
 
                     response.push(year = obj)
 
                 });
                 let sortBy = [{
                     prop: 'year',
                     direction: 1
                 }, {
                     prop: 'month',
                     direction: 1
                 }, {
                     prop: 'day',
                     direction: 1
                 }];
                 response.sort(function (a, b) {
                     let i = 0, result = 0;
                     while (i < sortBy.length && result === 0) {
                         result = sortBy[i].direction * (a[sortBy[i].prop].toString() < b[sortBy[i].prop].toString() ? -1 : (a[sortBy[i].prop].toString() > b[sortBy[i].prop].toString() ? 1 : 0));
                         i++;
                     }
                     return result;
                 })
                 return cb({
                     response
                 });
             }
         }
     });
 
 };
 
 var totalTaskCounts = function (condition, status, req, cb) {
 
     var query = {};
 
     if (req.param('type') == 'yearly') {
         sortBy = { year: 1 }
     } else if (req.param('type') == 'monthly') {
         sortBy = { month: 1 }
     } else if (req.param('type') == 'weekly') {
         sortBy = { day: 1 }
     } else if (req.param('type') == 'daily') {
         sortBy = { day: 1 }
     } else {
         sortBy = { year: 1 }
     }
 
     if (status) {
         query.status = status
     }
 
     query.isDeleted = false
 
     db.collection('task').aggregate([
         {
             $match: query
         },
         {
             "$group": {
                 "_id": condition,
                 "count": {
                     "$sum": 0
                 },
                 "sum": {
                     "$sum": 1
                 }
             }
         },
         {
             $project: {
                 id: "$_id",
                 sum: 1.0,
                 _id: 0.0,
                 category: "$category",
                 createdAt: "$createdAt",
                 year: "$_id.date_year",
                 month: "$_id.date_month",
                 day: "$_id.day",
             }
         },
         { $sort: sortBy }
     ]).toArray((counterr, totalresults) => {
 
         if (counterr) {
             return cb({
                 success: false,
                 error: counterr
             });
         } else {
 
             if (req.param('type') == 'yearly') {
                 response = []
                 years = []
                 totalresults.forEach(element => {
                     years.push(element.year)
                 });
                 var allYears = years.filter(function (item, i, ar) {
                     return ar.indexOf(item) === i;
                 });
                 allYears.forEach(element => {
                     let obj = {}
                     obj.year = element
                     const results = totalresults.filter(entry => entry.year === element)
                     if (results.length > 0) {
                         obj.totalTasks = Math.round(results[0].sum)
                     } else {
                         obj.totaltasks = 0
                     }
 
                     response.push(year = obj)
 
                 });
                 response.sort((a, b) => {
                     return a.year - b.year;
                 });
                 return cb({
                     response
                 });
             } else if (req.param('type') == 'monthly') {
                 response = []
                 months = []
                 totalresults.forEach(element => {
                     months.push({ month: element.month, year: element.year })
                 });
 
                 allYears = months.filter((thing, index) => {
                     const _thing = JSON.stringify(thing);
                     return index === months.findIndex(obj => {
                         return JSON.stringify(obj) === _thing;
                     });
                 })
 
                 allYears.forEach(element => {
                     let obj = {}
                     obj.month = element.month
                     obj.year = element.year
                     const results = totalresults.filter(entry => entry.month === element.month && entry.year === element.year)
                     if (results.length > 0) {
                         obj.totalTasks = Math.round(results[0].sum)
                     } else {
                         obj.totalTasks = 0
                     }
                     response.push(year = obj)
 
                 });
                 response.sort((a, b) => (a.year > b.year) ? 1 : (a.year === b.year) ? ((a.month > b.month) ? 1 : -1) : -1)
                 return cb({
                     response
                 });
             } else if (req.param('type') == 'weekly' || req.param('type') == 'daily') {
                 response = []
                 months = []
                 totalresults.forEach(element => {
                     months.push({ day: element.day, month: element.month, year: element.year })
                 });
 
                 allYears = months.filter((thing, index) => {
                     const _thing = JSON.stringify(thing);
                     return index === months.findIndex(obj => {
                         return JSON.stringify(obj) === _thing;
                     });
                 })
 
                 allYears.forEach(element => {
                     let obj = {}
                     obj.day = element.day
                     obj.month = element.month
                     obj.year = element.year
                     const results = totalresults.filter(entry => entry.day === element.day && entry.month === element.month && entry.year === element.year)
                     if (results.length > 0) {
                         obj.totalTasks = Math.round(results[0].sum)
                     } else {
                         obj.totalTasks = 0
                     }
 
                     response.push(year = obj)
 
                 });
                 let sortBy = [{
                     prop: 'year',
                     direction: 1
                 }, {
                     prop: 'month',
                     direction: 1
                 }, {
                     prop: 'day',
                     direction: 1
                 }];
                 response.sort(function (a, b) {
                     let i = 0, result = 0;
                     while (i < sortBy.length && result === 0) {
                         result = sortBy[i].direction * (a[sortBy[i].prop].toString() < b[sortBy[i].prop].toString() ? -1 : (a[sortBy[i].prop].toString() > b[sortBy[i].prop].toString() ? 1 : 0));
                         i++;
                     }
                     return result;
                 })
                 return cb({
                     response
                 });
             }
         }
     });
 
 };
 var projectCounts = async function (condition, projectStatus, req, cb) {
 
     var query = {};
 
     if (req.param('type') == 'yearly') {
         sortBy = { year: 1 }
     } else if (req.param('type') == 'monthly') {
         sortBy = { month: 1 }
     } else if (req.param('type') == 'weekly') {
         sortBy = { day: 1 }
     } else if (req.param('type') == 'daily') {
         sortBy = { day: 1 }
     } else {
         sortBy = { year: 1 }
     }
 
     // var student = "student";
 
     if (projectStatus) {
         query.projectStatus = projectStatus
     }
 
     query.isDeleted = false
     //(query, "-----------query")
 
 
     db.collection('projects').aggregate([
         {
             $match: query
         },
         {
             "$group": {
                 "_id": condition,
                 "count": {
                     "$sum": 0
                 },
                 "sum": {
                     "$sum": 1
                 },
                 createdAt: { $first: "$createdAt" }
             }
         },
         {
             $project: {
                 id: "$_id",
                 sum: 1.0,
                 _id: 0.0,
                 progress_status: "$progress_status",
                 createdAt: "$createdAt",
                 year: "$_id.date_year",
                 month: "$_id.date_month",
                 day: "$_id.day",
             }
         },
         { $sort: sortBy }
     ]).toArray((counterr, totalresults) => {
         if (counterr) {
             return cb({
                 success: false,
                 error: counterr
             });
         } else {
 
             if (req.param('type') == 'yearly') {
                 response = []
                 years = []
                 totalresults.forEach(element => {
                     years.push(element.year)
                 });
                 var allYears = years.filter(function (item, i, ar) {
                     return ar.indexOf(item) === i;
                 });
                 allYears.forEach(element => {
                     let obj = {}
                     obj.year = element
                     const results = totalresults.filter(entry => entry.year === element)
                     if (results.length > 0) {
                         obj.totalEmployeeCounts = Math.round(results[0].sum)
                     } else {
                         obj.totalEmployeeCounts = 0
                     }
 
                     response.push(year = obj)
 
                 });
                 response.sort((a, b) => {
                     return a.year - b.year;
                 });
                 return cb({
                     response
                 });
             } else if (req.param('type') == 'monthly') {
                 response = []
                 months = []
                 totalresults.forEach(element => {
                     months.push({ month: element.month, year: element.year })
                 });
 
                 allYears = months.filter((thing, index) => {
                     const _thing = JSON.stringify(thing);
                     return index === months.findIndex(obj => {
                         return JSON.stringify(obj) === _thing;
                     });
                 })
 
                 allYears.forEach(element => {
                     let obj = {}
                     obj.month = element.month
                     obj.year = element.year
                     const results = totalresults.filter(entry => entry.month === element.month && entry.year === element.year)
                     if (results.length > 0) {
                         obj.totalEmployeeCounts = Math.round(results[0].sum)
                     } else {
                         obj.totalEmployeeCounts = 0
                     }
                     response.push(year = obj)
 
                 });
                 response.sort((a, b) => (a.year > b.year) ? 1 : (a.year === b.year) ? ((a.month > b.month) ? 1 : -1) : -1)
                 return cb({
                     response
                 });
             } else if (req.param('type') == 'weekly' || req.param('type') == 'daily') {
                 response = []
                 months = []
                 totalresults.forEach(element => {
                     months.push({ day: element.day, month: element.month, year: element.year })
                 });
 
                 allYears = months.filter((thing, index) => {
                     const _thing = JSON.stringify(thing);
                     return index === months.findIndex(obj => {
                         return JSON.stringify(obj) === _thing;
                     });
                 })
 
                 allYears.forEach(element => {
                     let obj = {}
                     obj.day = element.day
                     obj.month = element.month
                     obj.year = element.year
                     const results = totalresults.filter(entry => entry.day === element.day && entry.month === element.month && entry.year === element.year)
                     if (results.length > 0) {
                         obj.totalEmployeeCounts = Math.round(results[0].sum)
                     } else {
                         obj.totalEmployeeCounts = 0
                     }
 
                     response.push(year = obj)
 
                 });
                 let sortBy = [{
                     prop: 'year',
                     direction: 1
                 }, {
                     prop: 'month',
                     direction: 1
                 }, {
                     prop: 'day',
                     direction: 1
                 }];
                 response.sort(function (a, b) {
                     let i = 0, result = 0;
                     while (i < sortBy.length && result === 0) {
                         result = sortBy[i].direction * (a[sortBy[i].prop].toString() < b[sortBy[i].prop].toString() ? -1 : (a[sortBy[i].prop].toString() > b[sortBy[i].prop].toString() ? 1 : 0));
                         i++;
                     }
                     return result;
                 })
                 return cb({
                     response
                 });
             }
         }
     });
 
 };
       
        
       
       





















