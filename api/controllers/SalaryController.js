/**
 * SalaryController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

var ObjectId = require("mongodb").ObjectID;
var constantObj = sails.config.constants;
const db = sails.getDatastore().manager;
const SmtpController = require("../controllers/SmtpController");
var constant = require("../../config/local");
const excel = require("exceljs");

module.exports = {
  addSalary: async (req, res) => {
    try {
      const data = req.body;

      if (!data.employeeId) {
        return res.status(404).json({
          success: false,
          error: {
            code: 404,
            message: constantObj.Salary.EMPLOYEE_ID_REQUIRED,
          },
        });
      }

      if (!data.salary) {
        return res.status(404).json({
          success: false,
          error: { code: 404, message: constantObj.Salary.SALARY_REQUIRED },
        });
      }

      const find_salary = await Salary.findOne({
        employeeId: data.employeeId,
        isDeleted: false,
      });

      if (find_salary) {
        data.addedBy = req.identity.id;

        if (data.salary >= "25000") {
          data.professional_tax = "200";
        }

        const salary_data = await Salary.updateOne(
          { employeeId: data.employeeId, isDeleted: false },
          {}
        );
        return res.status(200).json({
          success: true,
          message: constantObj.Salary.UPDATE_SUCCESS,
        });
      }

      data.addedBy = req.identity.id;

      if (data.salary >= "25000") {
        data.professional_tax = "200";
      }

      const salary_data = await Salary.create(data).fetch();

      if (salary_data) {
        return res.status(200).json({
          success: true,
          message: constantObj.Salary.ADD_SUCCESS,
          data: salary_data,
        });
      } else {
        return res.status(400).json({
          success: false,
          message: constantObj.Salary.ADD_FAILED,
        });
      }
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { code: 400, message: error },
      });
    }
  },
  getSingleSalary: async (req, res) => {
    try {
      const id = req.param("id");

      if (!id) {
        return res.status(404).json({
          success: false,
          error: { code: 404, message: constantObj.Salary.ID_REQUIRED },
        });
      }

      const salary_details = await Salary.findOne({ id: id, isDeleted: false }).populate("employeeId");

      if (salary_details) {
        return res.status(200).json({
          success: true,
          message: constantObj.Salary.FOUND,
          data: salary_details,
        });
      } else {
        return res.status(400).json({
          success: false,
          message: constantObj.Salary.FAILED,
        });
      }
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { code: 400, message: error },
      });
    }
  },

  Salarylist: async (req, res) => {
    try {
      var search = req.param("search");
      var isDeleted = req.param("isDeleted");
      var page = req.param("page");
      var count = parseInt(req.param("count"));
      let sortBy = req.param("sortBy");
      let addedBy = req.param("addedBy");

      var query = {};

      if (search) {
        query.$or = [{ employee_name: { $regex: search, $options: "i" } }];
      }
      let sortquery = {};
      if (sortBy) {
        let typeArr = [];
        typeArr = sortBy.split(" ");
        let sortType = typeArr[1];
        let field = typeArr[0];
        sortquery[field ? field : "updatedAt"] = sortType
          ? sortType == "desc"
            ? -1
            : 1
          : -1;
      } else {
        sortquery = { updatedAt: 1 };
      }

      if (isDeleted) {
        if (isDeleted === "true") {
          isDeleted = true;
        } else {
          isDeleted = false;
        }
      } else {
        query.isDeleted = false;
      }
      if (addedBy) {
        query.addedBy_id = ObjectId(addedBy);
      }

      const pipeline = [
        {
          $lookup: {
            from: "users",
            localField: "employeeId",
            foreignField: "_id",
            as: "employee_details",
          },
        },
        {
          $unwind: {
            path: "$employee_details",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $project: {
            employeeId: "$employeeId",
            employee_name: "$employee_details.fullName",
            salary: "$salary",
            account_no: "$account_no",
            ifsc: "$ifsc",
            deduction: "$deduction",
            grand_total: "$grand_total",
            net_amount: "$net_amount",
            working_days: "$working_days",
            bank_name: "$bank_name",
            professional_tax: "$professional_tax",
            employee_details: "$employee_details",
            isDeleted: "$isDeleted",
            addedBy: "$addedBy",
            updatedBy: "$updatedBy",
            createdAt: "$createdAt",
            updatedAt: "$updatedAt",
          },
        },
        {
          $match: query,
        },
        {
          $sort: sortquery,
        },
      ];
      db.collection("salary")
        .aggregate([...pipeline])
        .toArray((err, totalResult) => {
          if (page && count) {
            var skipNo = (page - 1) * count;
            pipeline.push(
              {
                $skip: Number(skipNo),
              },
              {
                $limit: Number(count),
              }
            );
          }
          db.collection("salary")
            .aggregate([...pipeline])
            .toArray((err, result) => {
              return res.status(200).json({
                success: true,
                data: result,
                total: totalResult.length,
              });
            });
        });
    } catch (err) {
      return res.status(400).json({
        success: false,
        error: { code: 400, message: "" + err },
      });
    }
  },
  updateSalary: async (req, res) => {
    try {
      const data = req.body;

      if (!data.id) {
        return res.status(404).json({
          success: false,
          error: { code: 404, message: constantObj.Salary.ID_REQUIRED },
        });
      }

      const find_salary = await Salary.findOne({
        id: data.id,
        isDeleted: false,
      });
      if (!find_salary) {
        return res.status(404).json({
          success: false,
          error: { code: 404, message: constantObj.Salary.ID_NOT_FOUND },
        });
      }

      if (data.employeeId) {
        return res.status(400).json({
          success: false,
          error: { code: 400, message: constantObj.Salary.CANNOT_UPDATE_ID },
        });
      }

      data.updatedBy = req.identity.id;

      const updated_data = await Salary.updateOne({
        id: data.id,
        isDeleted: false,
      }).set(data);

      if (updated_data) {
        return res.status(200).json({
          success: true,
          message: constantObj.Salary.UPDATE_SUCCESS,
          data: updated_data,
        });
      } else {
        return res.status(400).json({
          success: false,
          message: constantObj.Salary.UPDATE_FAILED,
        });
      }
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { code: 400, message: error },
      });
    }
  },
  deleteSalary: async (req, res) => {
    try {
      const id = req.param("id");
      var deleted_by = req.identity.id;

      if (!id) {
        return res.status(404).json({
          success: false,
          error: { code: 404, message: constantObj.Salary.ID_REQUIRED },
        });
      }

      const salary_data = await Salary.findOne({ id: id, isDeleted: false });

      if (!salary_data) {
        return res.status(404).json({
          success: false,
          error: { code: 404, message: constantObj.Salary.ID_NOT_FOUND },
        });
      }

      var deleted_at = new Date();

      const delete_salary = await Salary.updateOne({
        id: id,
        isDeleted: false,
      }).set({ isDeleted: true, deletedBy: deleted_by, deletedAt: deleted_at });
      if (delete_salary) {
        return res.status(200).json({
          success: true,
          message: constantObj.Salary.DELETE_SUCCESS,
        });
      } else {
        return res.status(400).json({
          success: false,
          message: constantObj.Salary.DELETE_FAILED,
        });
      }
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { code: 400, message: error },
      });
    }
  },
  //     payableSalaryList: async (req, res) => {
  //         try {
  //             var search = req.param('search');
  //             //var isDeleted = req.param('isDeleted');
  //             var page = req.param('page');
  //             var count = parseInt(req.param('count'));
  //             let sortBy = req.param("sortBy");
  //             let addedBy = req.param('addedBy');

  //             var query = {};
  //             if (search) {
  //                 query.$or = [
  //                     { fullName: { $regex: search, '$options': 'i' } },
  //                 ]
  //             }
  //             //let sortquery = {};
  //             let sortquery = { days: -1 };
  //             // if (sortBy) {
  //             //     let typeArr = [];
  //             //     typeArr = sortBy.split(" ");
  //             //     let sortType = typeArr[1];
  //             //     let field = typeArr[0];
  //             //     sortquery[field ? field : 'updatedAt'] = sortType ? (sortType == 'desc' ? -1 : 1) : -1;
  //             // } else {
  //             //     sortquery = { days: 1 }
  //             // }
  //             query.role = { "$nin": ["admin", "subadmin"] };
  //             query.status = "active";
  //             query.isDeleted = false;

  //             // if (isDeleted) {
  //             //     if (isDeleted === 'true') {
  //             //         isDeleted = true;
  //             //     } else {
  //             //         isDeleted = false;
  //             //     }
  //             //     query.isDeleted = isDeleted;
  //             // } else {
  //             //     query.isDeleted = false;
  //             // }
  //             if (addedBy) {
  //                 query.addedBy_id = ObjectId(addedBy);
  //             }

  //             const pipeline = [
  //                 {
  //                     $project: {
  //                         // days: "$days",
  //                         id:"$_id",
  //                         fullName: "$fullName",
  //                         email: "$email",
  //                         status: "$status",
  //                         role: "$role",
  //                         dateOfJoining: "$dateOfJoining",
  //                         isDeleted: "$isDeleted",
  //                         addedBy: "$addedBy",
  //                         updatedBy: "$updatedBy",
  //                         createdAt: "$createdAt",
  //                         updatedAt: "$updatedAt",
  //                     }
  //                 },
  //                 {
  //                     $match: query
  //                 },
  //                 // {
  //                 //     $sort: sortquery
  //                 // },
  //             ]
  //             db.collection('users').aggregate([...pipeline]).toArray((err, totalResult) => {
  //                 if (page && count) {
  //                     var skipNo = (page - 1) * count;
  //                     // pipeline.push(
  //                     //     {
  //                     //         $skip: Number(skipNo)
  //                     //     },
  //                     //     {
  //                     //         $limit: Number(count)
  //                     //     })
  //                 }
  //                 db.collection('users').aggregate([...pipeline]).toArray(async (err, result) => {

  //                     console.log(result,"===================result")
  //                     if (result && result.length > 0) {
  //                     for await (let employee of result) {

  //                         //console.log(employee,"=========employee")

  //                         var employee_id = employee.id;
  //                         console.log(employee_id,"============employee_id")

  // const salary_data = await Salary.find({employeeId:employee_id,isDeleted:false});

  // if(salary_data && salary_data.length>0){
  //     for await(let salary_details of salary_data){
  //         const salary = salary_details.amount;
  //         console.log(salary,"================salary")
  //     }

  // }
  //                     }

  //                      }
  //                     //result.sort((a, b) => a.days - b.days);
  //                     var data = result.sort((a, b) => a.days - b.days).slice(skipNo, page * count);

  // return res.status(200).json({
  //     "success": true,
  //     "data": data,
  //     "total": totalResult.length,
  // });
  //                 })
  //             })
  //         } catch (err) {
  //             return res.status(400).json({
  //                 success: false,
  //                 error: { code: 400, message: "" + err }
  //             })

  //         }
  //     },

  // payableSalaryList: async (req, res) => {

  // try {
  //     var search = req.param('search');
  //     var isDeleted = req.param('isDeleted');
  //     var page = req.param('page');
  //     var count = parseInt(req.param('count'));
  //     let sortBy = req.param("sortBy");
  //     let addedBy = req.param('addedBy');

  //     var query = {};

  //     if (search) {
  //         query.$or = [
  //             { employee_name: { $regex: search, '$options': 'i' } },
  //         ]
  //     }
  //     let sortquery = {};
  //     if (sortBy) {
  //         let typeArr = [];
  //         typeArr = sortBy.split(" ");
  //         let sortType = typeArr[1];
  //         let field = typeArr[0];
  //         sortquery[field ? field : 'updatedAt'] = sortType ? (sortType == 'desc' ? -1 : 1) : -1;
  //     } else {
  //         sortquery = { updatedAt: 1 }
  //     }

  //     if (isDeleted) {
  //         if (isDeleted === 'true') {
  //             isDeleted = true;
  //         } else {
  //             isDeleted = false;
  //         }
  //     } else {
  //         query.isDeleted = false;
  //     }
  //     if (addedBy) {
  //         query.addedBy_id = ObjectId(addedBy);
  //     }

  //     const pipeline = [
  //         {
  //             $lookup: {
  //                 from: "users",
  //                 localField: "employeeId",
  //                 foreignField: "_id",
  //                 as: "employee_details",
  //             },
  //         },
  //         {
  //             $unwind: {
  //                 path: "$employee_details",
  //                 preserveNullAndEmptyArrays: true,
  //             },
  //         },
  //         {
  //             $project: {
  //                 employeeId: "$employeeId",
  //                 employee_name: "$employee_details.fullName",
  //                 salary: "$salary",
  //                 account_no: "$account_no",
  //                 ifsc: "$ifsc",
  //                 bank_name: "$bank_name",
  //                 professional_tax: "$professional_tax",
  //                 //employee_details: "$employee_details",
  //                 isDeleted: "$isDeleted",
  //                 addedBy: "$addedBy",
  //                 updatedBy: "$updatedBy",
  //                 createdAt: "$createdAt",
  //                 updatedAt: "$updatedAt",
  //             }
  //         },
  //         {
  //             $match: query
  //         },
  //         {
  //             $sort: sortquery
  //         },
  //     ]
  //     db.collection('salary').aggregate([...pipeline]).toArray((err, totalResult) => {
  //         if (page && count) {
  //             var skipNo = (page - 1) * count;
  //             pipeline.push(
  //                 {
  //                     $skip: Number(skipNo)
  //                 },
  //                 {
  //                     $limit: Number(count)
  //                 })
  //         }
  //         db.collection('salary').aggregate([...pipeline]).toArray(async (err, result) => {

  //             //console.log(result, "===================result")

  //             // let salary;
  //             // let leave = 0;
  //             // let deduction = 0;
  //             // let paid = 0;
  //             // let per_day_pay;
  //             // let deduction_amount;
  //             // let total_salary;
  //             if (result && result.length > 0) {

  //                 let salary;
  //                 let employee_id;
  //                 let professional_tax;
  //                 // let leave = 0;
  //                 // let deduction = 0;
  //                 // let paid = 0;
  //                 let per_day_pay;
  //                 let deduction_amount;
  //                 let total_salary;

  //                 for await (let employee of result) {

  //                     employee_id = employee.employeeId;
  //                     salary = employee.salary;
  //                     professional_tax = employee.professional_tax;
  //                     // console.log(employee_id, "============employee_id")
  //                     // console.log(salary, "============salary")

  //                     per_day_pay = salary / 30;
  //                     //console.log(per_day_pay, "============per_day_pay")

  //                     let leaves_query = {};

  //                     leaves_query.and = [
  //                         {
  //                             leaveStartDate: { '>=': "2024-04-01" },
  //                             leaveEndDate: { '<': "2024-04-30" }
  //                         }]

  //                     leaves_query.addedBy = String(employee_id);
  //                     leaves_query.leave_status = "Accepted";
  //                     leaves_query.isDeleted = false;

  //                     let leave = 0;
  //                     let deduction = 0;
  //                     let paid = 0;

  //                     const leaves_data = await Leaves.find(leaves_query);
  //                     if (leaves_data) {
  //                         //console.log(leaves_data, "==================leaves_data")

  //                         for await (let leave_details of leaves_data) {
  //                             leave = leave + leave_details.no_of_days;
  //                             deduction = deduction + leave_details.deduction;
  //                             paid = paid + leave_details.paid;
  //                         }

  //                     }
  //                     deduction_amount = (per_day_pay * deduction) + Number(professional_tax);
  //                     total_salary = salary - deduction_amount;

  //                     // console.log(employee_id, "============employee_id")
  //                     // console.log(salary, "============salary")
  //                     // console.log(leave, "=========leave")
  //                     // console.log(deduction, "=========deduction")
  //                     // console.log(paid, "=========paid")
  //                     // console.log(deduction_amount, "=========deduction_amount")
  //                     // console.log(total_salary, "=========total_salary")

  //                     employee.leave = leave;
  //                     employee.deduction = deduction;
  //                     employee.paid = paid;
  //                     employee.deduction_amount = deduction_amount;
  //                     employee.total_salary = total_salary;

  //                 }
  //                 // console.log(leave, "=========leave")
  //                 // console.log(deduction, "=========deduction")
  //                 // console.log(paid, "=========paid")
  //                 // console.log(deduction_amount, "=========deduction_amount")
  //                 // console.log(total_salary, "=========total_salary")
  //                 //console.log(employee,"=========employee")

  //                 // var employee_id = employee.id;
  //                 // console.log(employee_id, "============employee_id")

  //                 //const salary_data = await Salary.find({ employeeId: employee_id, isDeleted: false });
  //             }

  //             return res.status(200).json({
  //                 "success": true,
  //                 "data": result,
  //                 "total": totalResult.length,
  //             });
  //         })
  //     })
  // } catch (err) {
  //     return res.status(400).json({
  //         success: false,
  //         error: { code: 400, message: "" + err }
  //     })
  // }
  // },

  // const generatePayableSalaryExcel = async (result) => {
  //     const workbook = new excel.Workbook();
  //     const worksheet = workbook.addWorksheet('Payable Salary List');

  //     // Define column headers
  //     worksheet.columns = [
  //         { header: 'Employee Name', key: 'employee_name', width: 20 },
  //         { header: 'Salary', key: 'salary', width: 15 },
  //         { header: 'Account No', key: 'account_no', width: 15 },
  //         { header: 'IFSC', key: 'ifsc', width: 15 },
  //         { header: 'Bank Name', key: 'bank_name', width: 20 },
  //         { header: 'Professional Tax', key: 'professional_tax', width: 15 },
  //         { header: 'Leave', key: 'leave', width: 15 },
  //         { header: 'Deduction', key: 'deduction', width: 15 },
  //         { header: 'Total Salary', key: 'total_salary', width: 15 },
  //     ];

  //     // Add data rows
  //     result.forEach(employee => {
  //         worksheet.addRow({
  //             employee_name: employee.employee_name,
  //             salary: employee.salary,
  //             account_no: employee.account_no,
  //             ifsc: employee.ifsc,
  //             bank_name: employee.bank_name,
  //             professional_tax: employee.professional_tax,
  //             leave: employee.leave,
  //             deduction: employee.deduction,
  //             total_salary: employee.total_salary
  //         });
  //     });

  //     // Save the Excel file
  //     const filePath = 'payable_salary_list.xlsx';
  //     await workbook.xlsx.writeFile(filePath);
  //     return filePath;
  // };

  payableSalaryList: async (req, res) => {
    try {
      console.log("Here there");
      var search = req.param("search");
      var isDeleted = req.param("isDeleted");
      var page = req.param("page");
      var count = parseInt(req.param("count"));
      let sortBy = req.param("sortBy");
      let addedBy = req.param("addedBy");

      var date = new Date();
      const current_date = date.toISOString().substring(0, 10);
      var current_year = date.toISOString().substring(0, 4);
      var current_month = date.toISOString().substring(5, 7);

      var query = {};

      if (search) {
        query.$or = [{ employee_name: { $regex: search, $options: "i" } }];
      }
      let sortquery = {};
      if (sortBy) {
        let typeArr = [];
        typeArr = sortBy.split(" ");
        let sortType = typeArr[1];
        let field = typeArr[0];
        sortquery[field ? field : "updatedAt"] = sortType
          ? sortType == "desc"
            ? -1
            : 1
          : -1;
      } else {
        sortquery = { updatedAt: 1 };
      }

      if (isDeleted) {
        if (isDeleted === "true") {
          isDeleted = true;
        } else {
          isDeleted = false;
        }
      } else {
        query.isDeleted = false;
      }
      if (addedBy) {
        query.addedBy_id = ObjectId(addedBy);
      }

      const pipeline = [
        {
          $lookup: {
            from: "users",
            localField: "employeeId",
            foreignField: "_id",
            as: "employee_details",
          },
        },
        {
          $unwind: {
            path: "$employee_details",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $project: {
            employeeId: "$employeeId",
            employee_name: "$employee_details.fullName",
            salary: "$salary",
            account_no: "$account_no",
            ifsc: "$ifsc",
            bank_name: "$bank_name",
            professional_tax: "$professional_tax",
            net_amount: "$net_amount",
            grand_total: "$grand_total",
            //employee_details: "$employee_details",
            isDeleted: "$isDeleted",
            addedBy: "$addedBy",
            updatedBy: "$updatedBy",
            createdAt: "$createdAt",
            updatedAt: "$updatedAt",
          },
        },
        {
          $match: query,
        },
        {
          $sort: sortquery,
        },
      ];
      db.collection("salary")
        .aggregate([...pipeline])
        .toArray((err, totalResult) => {
          if (page && count) {
            var skipNo = (page - 1) * count;
            pipeline.push(
              {
                $skip: Number(skipNo),
              },
              {
                $limit: Number(count),
              }
            );
          }
          db.collection("salary")
            .aggregate([...pipeline])
            .toArray(async (err, result) => {
              if (result && result.length > 0) {
                let salary;
                let employee_id;
                let professional_tax;
                let per_day_pay;
                let deduction_amount;
                let total_salary;

                for await (let employee of result) {
                  employee_id = employee.employeeId;
                  salary = employee.salary;
                  professional_tax = employee.professional_tax;

                  per_day_pay = salary / 30;

                  let leaves_query = {};

                  leaves_query.and = [
                    {
                      leaveStartDate: {
                        ">=":
                          `${current_year}` + "-" + `${current_month}` + "-01",
                      },
                      leaveEndDate: {
                        "<":
                          `${current_year}` + "-" + `${current_month}` + "-30",
                      },
                    },
                  ];

                  leaves_query.addedBy = String(employee_id);
                  leaves_query.leave_status = "Accepted";
                  leaves_query.isDeleted = false;

                  let leave = 0;
                  let deduction = 0;
                  let paid = 0;
                  let working_days;

                  const leaves_data = await Leaves.find(leaves_query);
                  if (leaves_data) {
                    for await (let leave_details of leaves_data) {
                      leave = leave + leave_details.no_of_days;
                      deduction = deduction + leave_details.deduction;
                      paid = paid + leave_details.paid;
                    }
                  }
                  working_days = 30 - deduction;
                  deduction_amount =
                    per_day_pay * deduction + Number(professional_tax);
                  total_salary = salary - deduction_amount;
                  // console.log(deduction_amount,"deduction_amount------------>")
                  employee.leave = leave;
                  employee.deduction = deduction;
                  employee.paid = paid;
                  employee.deduction_amount = deduction_amount;
                  employee.total_salary = total_salary;
                  employee.working_days = working_days;
                }
              }
              return res.status(200).json({
                success: true,
                data: result,
                total: totalResult.length,
              });
            });
        });
    } catch (err) {
      return res.status(400).json({
        success: false,
        error: { code: 400, message: "" + err },
      });
    }
  },
};
