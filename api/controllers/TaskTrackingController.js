/**
 * TaskTrackingController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    taskTracking: async (req, res) => {

        try {

            let page = req.param('page') || 1;
            let count = req.param('count') || 10;

            var query = {};

            query.isDeleted = false;

            const pipeline = [
                {
                    $match: query,
                },

                {
                    $lookup: {
                        from: "users",
                        localField: "employee",
                        foreignField: "_id",
                        as: "employee_details"
                    }
                },
                {
                    $unwind: {
                        path: "$employee_details",
                        preserveNullAndEmptyArrays: true
                    }
                },

                {
                    $project: {
                        _id: "$_id",
                        employee_id: "$employee",
                        employee_name: "$employee_details.firstName",
                        status: "$status",
                        isDeleted: "$isDeleted",

                    }
                },
                {
                    $group: {
                        _id: "$employee_id",
                        employee_name: { $first: "$employee_name" },
                        total_tasks: { $sum: 1 }, 
                        done_tasks: {
                            $sum: { $cond: [{ $eq: ["$status", "done"] }, 1, 0] } 
                        },
                        pending_tasks: {
                            $sum: { $cond: [{ $eq: ["$status", "pending"] }, 1, 0] } 
                        }
                    }
                },
                

            ];

            let totalResult = await db.collection('task').aggregate(pipeline).toArray()
            if (page && count) {
                var skipNo = (page - 1) * count;
                pipeline.push(
                    {
                        $skip: Number(skipNo)
                    },
                    {
                        $limit: Number(count)
                    })
            }

            let result = await db.collection('task').aggregate(pipeline).toArray()
            for(let employee of result){
                employee.done_percentage = done_tasks * 100 / total_tasks;
                employee.pending_percentage = pending_tasks * 100 / total_tasks;

            }


            let resData = {
                total: totalResult ? totalResult.length : 0,
                data: result ? result : []
            }
            if (!req.param('page') && !req.param('count')) {
                resData.data = totalResult ? totalResult : []
            }
            return res.status(200).json({
                success: true,
                total: totalResult.length,
                data: result,
            });
        }
        catch (err) {
            return res.status(500).json({
                success: false,
                error: { code: 500, message: '' + err },
            });
        }
    }
};


