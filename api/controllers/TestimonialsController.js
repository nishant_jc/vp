/**
 * TestimonialsController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */


var constantObj = sails.config.constants;
module.exports = {
  /**
   *
   * @param {*} req
   * @param {*} res
   * @description: Used to add tesTestimonial
   * @createdAt 21/10/2021
   * @createdBy : Rohit kumar
   */

   addTestimonial: (req, res) => {
    API(TestimonialsService.saveTestimonial, req, res);
  },

  /**
   *
   * @param {*} req
   * @param {*} res
   * @description: Used to add Content
   * @createdAt 19/10/2021
   * @createdBy : Rohit kumar
   */

  edit: (req, res) => {
    API(TestimonialsService.updateTestimonial, req, res);
  },


  /**
   *
   * @param {*} res
   * @returns
   * @description: Api Used to get Content
   * @createdAt : 19/10/2021
   * @createdBy Rohit kumar
   */

   getAllTestimonials: async (req, res, next)=> {
    var search = req.param("search");
    var sortBy = req.param("sortBy");
    var page = req.param("page");
    if (page == undefined) {
      page = 1;
    }
    var count = req.param("count");
    if (count == undefined) {
      count = 100;
    }
    var skipNo = (page - 1) * count;

    var query = {};
    if (sortBy) {
      sortBy = sortBy.toString();
    } else {
      sortBy = "createdAt desc";
    }

    query.isDeleted = false;
    await Testimonials.count(query)
      .exec( (err, total)=> {

        if (err) {
          return res.status(400).json({
            success: false,
            error: err,
          });
        } else {
            Testimonials.find(query)
            .sort(sortBy)
            .exec( (err, Testimonials)=> {
              if (err) {
                return res.status(400).json({ success: false, error: { code: 400, message: err } });
              } else {
                return res.json({
                  success: true,
                  code: 200,
                  data: Testimonials,
                  total: total,
                });
              }
            });
        }
      });
  },


  /**
   *
   * @param {id} req
   * @param {*} res
   * @returns
   * @description: Api Used to get single project by ID
   * @createdAt : 20/10/2021
   * @createdBy Rohit kumar
   */

   getSingleTestimonial: async (req, res) => {
    try {
      let TestimonialID = req.param("id");
      if (TestimonialID == null) {
        return res.status(404).json({
          success: false,
          message: constantObj.Testimonials.ID_REQUIRED,
        });
      } else {
        var TestimonialData = await Testimonials.findOne({ id: TestimonialID });
        if (TestimonialData) {
          return res.status(200).json({
            success: true,
            message: constantObj.Testimonials.GET_DATA,
            data: TestimonialData,
          });
        } else {
          return res.status(404).json({
            success: false,
            message: constantObj.Testimonials.NO_RESULT,
          });
        }
      }
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },
  

  /**
   *
   * @param {id} req
   * @param {*} res
   * @returns
   * @description: Api Used to delete Testimonial by ID
   * @createdAt : 21/10/2021
   * @createdBy Rohit kumar
   */

  testimonialDeletedByID: async (req, res) => {
    try {
       let deletedID = req.param("id");
       if (deletedID == undefined) {
        return res.status(404).json({
          success: false,
          message: constantObj.Testimonials.ID_REQUIRED,
        });
      }
      var deletedByUser = req.identity.id;
      var deletedData = await Testimonials.updateOne({
        id: deletedID,
      }).set({ isDeleted: true, deletedBy: deletedByUser });
        
        if (deletedData) {
          return res.status(200).json({
            success: true,
            message: constantObj.Testimonials.DELETED_TESTIMONIALS,
          });
        }
   
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },

};

