/**
 * DeviceAssignController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

var ObjectId = require("mongodb").ObjectID;
var constantObj = sails.config.constants;
const db = sails.getDatastore().manager;
var constant = require('../../config/local.js');
const SmtpController = require('../controllers/SmtpController');

module.exports = {
    /**
        *
        * @description: Api Used to Assign device
        * @createdAt : 25/10/2023
        * @createdBy :Kawalpreet Kaur
        */

    assignDevice: async (req, res) => {
        const data = req.body;
        if (!data.device_name || typeof data.device_name == undefined) {
            return res.status(404).json({
                success: false,
                error: { code: 404, message: constantObj.assignDevice.DEVICE_NAME_REQUIRED },
            });
        }

        if (!data.serial_no || typeof data.serial_no == undefined) {
            return res.status(404).json({
                success: false,
                error: { code: 404, message: constantObj.assignDevice.SERIAL_NO_REQUIRED },
            });
        }

        if (!data.device_type || typeof data.device_type == undefined) {
            return res.status(404).json({
                success: false,
                error: { code: 404, message: constantObj.assignDevice.DEVICE_TYPE_REQUIRED },
            });
        }


        const already_assigned = await DeviceAssign.findOne({ device_name: data.device_name, isDeleted: false, status: "assigned" });


        const employee = await Users.findOne({ id: data.employeeId, isDeleted: false, status: "active" });
        var employee_name = employee.fullName;
        var employee_email = employee.email;

        const project = await Projects.findOne({ id: data.projectId, isDeleted: false, status: true });
        var project_name = project.name;

        if (already_assigned) {
            const user_id = already_assigned.employeeId;

            const user = await Users.findOne({ id: user_id, isDeleted: false });
            const user_name = user.fullName;

            return res.status(400).json({
                success: false,
                error: { code: 400, message: "This laptop is already assigned to " + `${user_name}` }
            });
        }
        try {
            const query = {};
            query.employeeId = data.employeeId;
            query.projectId = data.projectId;
            query.device_name = data.device_name;
            query.device_name = data.device_name;
            query.isDeleted = false;

            var existed = await DeviceAssign.findOne(query);
            if (existed) {
                return res.status(400).json({
                    success: false,
                    error: { code: 400, message: constantObj.assignDevice.EXIST },
                });
            } else {
                data.startTime = new Date(data.startTime);
                data.endTime = new Date(data.endTime);
                //console.log(data.endTime,"============endTime")

                let assignDevice = await DeviceAssign.create(data).fetch();
                var ending_date = assignDevice.endTime;
                var endDate = ending_date.toLocaleString('en-GB')
                var end_date = endDate.substring(0, 10);
                if (assignDevice) {
                    deviceAssigned({
                        email: employee_email,
                        fullname: employee_name,
                        projectname: project_name,
                        enddate: end_date,
                        device_name: data.device_name
                    });
                    return res.status(200).json({
                        success: true,
                        code: 200,
                        data: assignDevice,
                        message: constantObj.assignDevice.ASSIGN,
                    });
                }
            }


        } catch (err) {
            return res.status(400).json({
                success: false,
                code: 400,
                error: err
            });
        }

    },

    /**
     *
     * @description: Api Used to show assigned device List
     * @createdAt : 25/10/2023
     * @createdBy :Kawalpreet Kaur
     */

    assignDeviceList: async (req, res) => {

        try {
            var search = req.param("search");
            var sortBy = req.param("sortBy");
            var page = req.param("page");
            var count = req.param("count");
            var start = req.param('start')
            var end = req.param('end')
            if (page == undefined) {
                page = 1;
            }
            if (count == undefined) {
                count = 1000;
            }
            var skipNo = (page - 1) * count;
            var query = {};

            if (sortBy) {
                sortBy = sortBy.toString();
            } else {
                sortBy = -1;
            }

            if (search) {
                query.$or = [{ name: { $regex: search, $options: "i" } }];
            }
            if (start && end) {
                startDate = new Date(start);
                startDate.setUTCHours(0, 0, 0, 0)
                endDate = new Date(end);
                endDate.setUTCHours(23, 59, 0, 0)
                query.startTime = { $gte: startDate, $lte: endDate }

            }

            query.isDeleted = false;

            db.collection("deviceassign")
                .aggregate([
                    {
                        $lookup: {
                            from: "users",
                            localField: "employeeId",
                            foreignField: "_id",
                            as: "employeeId",
                        },
                    },
                    {
                        $unwind: {
                            path: "$employeeId",
                            preserveNullAndEmptyArrays: true,
                        },
                    },
                    {
                        $lookup: {
                            from: "projects",
                            localField: "projectId",
                            foreignField: "_id",
                            as: "projectId",
                        },
                    },
                    {
                        $unwind: {
                            path: "$projectId",
                            preserveNullAndEmptyArrays: true,
                        },
                    },

                    {
                        $project: {
                            id: "$_id",
                            device_name: "$device_name",
                            serial_no: "$serial_no",
                            device_type: "$device_type",
                            description: "$description",
                            status: "$status",
                            projectId: "$projectId",
                            employeeId: "$employeeId",
                            startTime: "$startTime",
                            endTime: "$endTime",
                            deletedBy: "$deletedBy",
                            addedBy: "$addedBy",
                            updatedBy: "$updatedBy",
                            isDeleted: "$isDeleted",
                            updatedAt: "$updatedAt",
                        },
                    },
                    {
                        $match: query,
                    },
                ])
                .toArray((err, totalResult) => {
                    db.collection("deviceassign")
                        .aggregate([
                            {
                                $lookup: {
                                    from: "users",
                                    localField: "employeeId",
                                    foreignField: "_id",
                                    as: "employeeId",
                                },
                            },
                            {
                                $unwind: {
                                    path: "$employeeId",
                                    preserveNullAndEmptyArrays: true,
                                },
                            },
                            {
                                $lookup: {
                                    from: "projects",
                                    localField: "projectId",
                                    foreignField: "_id",
                                    as: "projectId",
                                },
                            },
                            {
                                $unwind: {
                                    path: "$projectId",
                                    preserveNullAndEmptyArrays: true,
                                },
                            },
                            {
                                $project: {
                                    id: "$_id",
                                    device_name: "$device_name",
                                    serial_no: "$serial_no",
                                    device_type: "$device_type",
                                    description: "$description",
                                    status: "$status",
                                    projectId: "$projectId",
                                    employeeId: "$employeeId",
                                    startTime: "$startTime",
                                    endTime: "$endTime",
                                    deletedBy: "$deletedBy",
                                    addedBy: "$addedBy",
                                    updatedBy: "$updatedBy",
                                    isDeleted: "$isDeleted",
                                    updatedAt: "$updatedAt",
                                },
                            },
                            {
                                $match: query,
                            },
                            { $sort: { updatedAt: sortBy } },
                            {
                                $skip: skipNo,
                            },
                            {
                                $limit: Number(count),
                            },
                        ])
                        .toArray((err, result) => {
                            if (err) {
                                return res.status(400).json({
                                    success: false,
                                    error: { message: "" + err },
                                });
                            } else {
                                return res.status(200).json({
                                    success: true,
                                    code: 200,
                                    data: result,
                                    total: totalResult.length,
                                });
                            }
                        });
                });
        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { "code": 400, "message": "" + err }
            });
        }
    },

    /**
      *
      * @description: Api Used to Edit Assign device
      * @createdAt : 25/10/2023
      * @createdBy :Kawalpreet Kaur
      */

    editAssignDevice: async (req, res) => {
        try {
            var data = req.body;
            const id = data.id;
            data.updatedBy = req.identity.id

            var date = new Date();

            if (!id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.assignDevice.ID_MISSING },
                });
            } else {
                data.updatedBy = req.identity.id
                var find_Assign_device = await DeviceAssign.findOne({ id: id, isDeleted: false });

                var emp_id = find_Assign_device.employeeId;
                var project_id = find_Assign_device.projectId;

                const employee = await Users.findOne({ id: emp_id, isDeleted: false, status: "active" });
                var employee_name = employee.fullName;
                var employee_email = employee.email;

                const project = await Projects.findOne({ id: project_id, isDeleted: false, status: true });
                var project_name = project.name;



                if (!find_Assign_device) {
                    return res.status(404).json({
                        success: false,
                        error: { code: 404, message: constantObj.assignDevice.NOT_FOUND },
                    });
                } else {
                    if (data.status == "accepted") {
                        data.endTime = date;
                    } else if (data.endTime && !(data.status == "accepted")) {

                        var endDate = data.endTime;
                var ending_date = new Date(endDate);
                var endDate = ending_date.toLocaleString('en-GB')
                var end_date = endDate.substring(0, 10);
                        updateAssignedDevice({
                            email: employee_email,
                            fullname: employee_name,
                            projectname: project_name,
                            enddate: end_date,
                            device_name : find_Assign_device.device_name
                        });
                    }

                    delete data.id;
                    let update_assign_device = await DeviceAssign.updateOne({ id: id }, data);

                    return res.status(200).json({
                        success: true,
                        code: 200,
                        message: constantObj.assignDevice.UPDATED,
                    });
                }
            }

        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { "code": 400, "message": "" + err }
            });
        }
    },

    /**
        *
        * @description: Api Used to find details of Assigned device
        * @createdAt : 25/10/2023
        * @createdBy :Kawalpreet Kaur
        */

    detailAssignDevice: async (req, res) => {
        try {
            const id = req.param("id");

            if (!id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.assignDevice.ID_MISSING },
                });
            } else {
                var find_Assign_device = await DeviceAssign.findOne({ id: id, isDeleted: false });
                if (!find_Assign_device) {
                    return res.status(404).json({
                        success: false,
                        error: { code: 404, message: constantObj.assignDevice.NOT_FOUND },
                    });
                } else {
                    return res.status(200).json({
                        success: true,
                        code: 200,
                        data: find_Assign_device,
                        message: constantObj.assignDevice.DETAIL
                    });
                }
            }

        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { "code": 400, "message": "" + err }
            });
        }
    },

    /**
        *
        * @description: Api Used to remove Assigned device
        * @createdAt : 25/10/2023
        * @createdBy :Kawalpreet Kaur
        */
    removeAssignDevice: async (req, res) => {
        try {
            const id = req.param("id");
            if (!id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.assignDevice.ID_MISSING },
                });
            } else {
                var find_Assign_device = await DeviceAssign.findOne({ id: id, isDeleted: false });
                if (!find_Assign_device) {
                    return res.status(404).json({
                        success: false,
                        error: { code: 404, message: constantObj.assignDevice.NOT_FOUND },
                    });
                } else {

                    let removed = await DeviceAssign.updateOne({ id: id }, { isDeleted: true, deletedBy: req.identity.id });
                    return res.status(200).json({
                        success: true,
                        code: 200,
                        message: constantObj.assignDevice.DELETED
                    });
                }
            }

        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { "code": 400, "message": "" + err }
            });
        }
    },


};

deviceAssigned = function (options) {
    var email = options.email;

    message = '';
    style = {
        header: `
    padding:30px 15px;
    text-align:center;
    background-color:#f2f2f2;
    `,
        body: `
    padding:15px;
    height: 230px;
    `,
        hTitle: `font-family: 'Raleway', sans-serif;
    font-size: 37px;
    height:auto;
    line-height: normal;
    font-weight: bold;
    background:none;
    padding:0;
    color:#333;
    `,
        maindiv: `
    width:600px;
    margin:auto;
    font-family: Lato, sans-serif;
    font-size: 14px;
    color: #333;
    line-height: 24px;
    font-weight: 300;
    border: 1px solid #eaeaea;
    `,
        textPrimary: `color:#3e3a6e;
    `,
        h5: `font-family: Raleway, sans-serif;
    font-size: 22px;
    background:none;
    padding:0;
    color:#333;
    height:auto;
    font-weight: bold;
    line-height:normal;
    `,
        m0: `margin:0;`,
        mb3: 'margin-bottom:15px;',
        textCenter: `text-align:center;`,
        btn: `padding:10px 30px;
    font-weight:500;
    font-size:14px;
    line-height:normal;
    border:0;
    display:inline-block;
    text-decoration:none;
    `,
        btnPrimary: `
    background-color:#3e3a6e;
    color:#fff;
    `,
        footer: `
    padding:10px 15px;
    font-weight:500;
    color:#fff;
    text-align:center;
    background-color:#000;
    `
    }

    message += `<div class="container" style="` + style.maindiv + `">
<div class="header" style="`+ style.header + `text-align:center">
    <img src="${constant.LOGO}/assets/img/logoImg.png" width="150" style="margin-bottom:20px;" />
</div>
<div class="body" style="`+ style.body + `">
    <h5 style="`+ style.h5 + style.m0 + style.mb3 + style.textCenter + `">Hello ` + options.fullname + `</h5>
    <p style="`+ style.m0 + style.mb3 + `margin-bottom:20px;font-weight:200 ">We would like to inform you that the management has decided to assign you with a ` + options.device_name + ` device for the completion of ` + options.projectname + ` project till <b>` + options.enddate + `</b>.<br>
    Also,we would like to share with you that you will be responsible for taking care of the device.In case of loss or damage,you will be responsible for the replacement.After completetion of the assigned task you will be required to return the assigned device to the HR Department.</p>
</div>
<div class="footer" style="`+ style.footer + `">
&copy 2023 JC Software Solution All rights reserved.
</div>
</div>`



    SmtpController.sendEmail(email, 'Device Assigned', message)

};

updateAssignedDevice = function (options) {
    var email = options.email;

    message = '';
    style = {
        header: `
    padding:30px 15px;
    text-align:center;
    background-color:#f2f2f2;
    `,
        body: `
    padding:15px;
    height: 230px;
    `,
        hTitle: `font-family: 'Raleway', sans-serif;
    font-size: 37px;
    height:auto;
    line-height: normal;
    font-weight: bold;
    background:none;
    padding:0;
    color:#333;
    `,
        maindiv: `
    width:600px;
    margin:auto;
    font-family: Lato, sans-serif;
    font-size: 14px;
    color: #333;
    line-height: 24px;
    font-weight: 300;
    border: 1px solid #eaeaea;
    `,
        textPrimary: `color:#3e3a6e;
    `,
        h5: `font-family: Raleway, sans-serif;
    font-size: 22px;
    background:none;
    padding:0;
    color:#333;
    height:auto;
    font-weight: bold;
    line-height:normal;
    `,
        m0: `margin:0;`,
        mb3: 'margin-bottom:15px;',
        textCenter: `text-align:center;`,
        btn: `padding:10px 30px;
    font-weight:500;
    font-size:14px;
    line-height:normal;
    border:0;
    display:inline-block;
    text-decoration:none;
    `,
        btnPrimary: `
    background-color:#3e3a6e;
    color:#fff;
    `,
        footer: `
    padding:10px 15px;
    font-weight:500;
    color:#fff;
    text-align:center;
    background-color:#000;
    `
    }

    message += `<div class="container" style="` + style.maindiv + `">
<div class="header" style="`+ style.header + `text-align:center">
    <img src="${constant.LOGO}/assets/img/logoImg.png" width="150" style="margin-bottom:20px;" />
</div>
<div class="body" style="`+ style.body + `">
    <h5 style="`+ style.h5 + style.m0 + style.mb3 + style.textCenter + `">Hello ` + options.fullname + `</h5>
    <p style="`+ style.m0 + style.mb3 + `margin-bottom:20px;font-weight:200 ">We would like to inform you that the management has extended the due date of returning the device assigned to you.Now you can use ` + options.device_name + ` device assigned for ` + options.projectname + ` project till <b>` + options.enddate + `</b>.<br>
    Also,we would like to share with you that you will be responsible for taking care of the device.In case of loss or damage,you will be responsible for the replacement.After completetion of the assigned task you will be required to return the assigned device to the HR Department.</p>
</div>
<div class="footer" style="`+ style.footer + `">
&copy 2023 JC Software Solution All rights reserved.
</div>
</div>`



    SmtpController.sendEmail(email, 'Important: Device Date Updated', message)

};
