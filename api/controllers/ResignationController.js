/**
 * ResignationController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */


var _request = require("request");
var ObjectId = require("mongodb").ObjectID;
var constantObj = sails.config.constants;
const db = sails.getDatastore().manager;
const SmtpController = require("../controllers/SmtpController");



module.exports = {
  /**
   *
   * @param {*} req
   * @param {*} res
   * @description: Used to add resignation
   * @createdAt 23/09/2021
   * @createdBy : Chandra Shekhar
   */

  addResign: (req, res) => {
    API(ResignationService.saveResign, req, res);
  },

  /**
   *
   * @param {*} res
   * @returns
   * @description: Api Used to get resignation
   * @createdAt : 23/09/2021
   * @createdBy Chandra Shekhar
   */

  getResign: (req, res) => {
    try {
      var search = req.param("search");
      var listsortBy = req.param("sortBy");
      var page = req.param("page");
      var count = req.param("count");

      if (page == undefined) {
        page = 1;
      }
      if (count == undefined) {
        count = 10;
      }
      var skipNo = (page - 1) * count;
      var query = {};
      var sorting = "";
      if (listsortBy) {
        sorting = listsortBy.toString();
        if (sorting == "asc") {
          sortBy = -1;
        } else {
          sortBy = 1;
        }
      } else {
        sortBy = -1;
      }

      if (search) {
        query.$or = [{ fromEmail: { $regex: search, $options: "i" } }];
      }

      query.isDeleted = false;
      db.collection("resignation")
        .aggregate([
          {
            $project: {
              id: "$_id",
              employeeName: "$employeeName",
              fromEmail: "$fromEmail",
              toEmail: "$toEmail",
              subject: "$subject",
              description: "$description",
              resignationStartDate: "$resignationStartDate",
              resignationEndDate: "$resignationEndDate",
              resign_status: "$resign_status",
              status: "$status",
              createdAt: "$createdAt",
              deletedBy: "$deletedBy",
              addedBy: "$addedBy",
              updatedBy: "$updatedBy",
              isDeleted: "$isDeleted",
              deletedAt: "$deletedAt",
              updatedAt: "$updatedAt",
            },
          },
          {
            $match: query,
          },
        ])
        .toArray((err, totalResult) => {
          db.collection("resignation")
            .aggregate([
              {
                $project: {
                  id: "$_id",
                  employeeName: "$employeeName",
                  fromEmail: "$fromEmail",
                  toEmail: "$toEmail",
                  subject: "$subject",
                  description: "$description",
                  resignationStartDate: "$resignationStartDate",
                  resignationEndDate: "$resignationEndDate",
                  resign_status: "$resign_status",
                  status: "$status",
                  createdAt: "$createdAt",
                  deletedBy: "$deletedBy",
                  addedBy: "$addedBy",
                  updatedBy: "$updatedBy",
                  isDeleted: "$isDeleted",
                  deletedAt: "$deletedAt",
                  updatedAt: "$updatedAt",
                },
              },
              {
                $match: query,
              },
              { $sort: { updatedAt: sortBy } },
              {
                $skip: skipNo,
              },
              {
                $limit: Number(count),
              },
            ])
            .toArray((err, result) => {
              if (err) {
                return res.status(400).json({
                  success: false,
                  error: { message: "" + err },
                });
              } else {
                return res.status(200).json({
                  success: true,
                  message: constantObj.resign.GET_DATA,
                  data: result,
                  total: totalResult.length,
                });
              }
            });
        });
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },

  /**
   *
   * @param {*} req
   * @param {*} res
   * @returns
   * @description: Api Used to update status of the resignation
   * @createdAt : 23/09/2021
   * @createdBy Chandra Shekhar
   */
  updatestatus: async (req, res) => {
    try {
      let resignId = req.param("id");
      let resignStatus = req.param("resignStatus");
      let fromemail = req.param("fromemail");
      let toemployeeemail = req.param("toemployeeemail");
      let tomanagementemail = req.param("tomanagementemail");

      let employee = await Users.findOne({ email: toemployeeemail, isDeleted: false })
      let employeeName = employee.fullName;

      if (resignId == undefined) {
        return res.status(404).json({
          success: false,
          message: constantObj.resign.ID_REQUIRED,
        });
      }
      var updatedresignData = await Resignation.updateOne({
        id: resignId,
      }).set({ resign_status: resignStatus });

      let toEmailArray = tomanagementemail.split(",");

      sendResignationEmailToManagementForAdminResponse({
        toemail: toEmailArray,
        name: employeeName,
        status: resignStatus,
      });

      sendResignationEmailToEmployee({
        toemail: toemployeeemail,
        name: employeeName,
        status: resignStatus,
      });
      return res.status(200).json({
        success: true,
        message: constantObj.resign.UPDATED_RESIGN_STATUS,
        data: updatedresignData,
      });
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },

  /**
   *
   * @param {*} req
   * @param {*} res
   * @returns
   * @description: Api Used to cancel resignation by user
   * @createdAt : 23/09/2021
   * @createdBy Chandra Shekhar
   */
  cancelstatus: async (req, res) => {
    try {
      let resignId = req.param("id");
      let fromemail = req.param("fromemail");
      let toemail = req.param("toemail");
      let resignStatus = "Cancelled";
      let status = "deactive";
      if (resignId == undefined) {
        return res.status(404).json({
          success: false,
          message: constantObj.resign.ID_REQUIRED,
        });
      }

      let employee = await Users.findOne({ email: fromemail, isDeleted: false })
      let employeeName = employee.fullName;
      var updatedresignData = await Resignation.updateOne({
        id: resignId,
      }).set({ status: status, resign_status: resignStatus });
      let toEmailArray = toemail.split(",");
      sendResignationEmailToManagement({
        toemail: toEmailArray,
        name: employeeName,
        status: resignStatus,
      });
      return res.status(200).json({
        success: true,
        message: constantObj.resign.CANCEL_RESIGN_STATUS,
        data: updatedresignData,
      });
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },

  /**
 *
 * @param {id} req
 * @param {*} res
 * @returns
 * @description: Api Used to get single resign by ID
 * @createdAt : 19/11/2021
 * @createdBy Rohit kumar
 */

  getSingleResignation: async (req, res) => {
    try {
      let resignationID = req.param("id");
      if (resignationID == null) {
        return res.status(404).json({
          success: false,
          message: constantObj.resign.ID_REQUIRED,
        });
      } else {
        var resignationData = await Resignation.findOne({ id: resignationID }).populate(
          "addedBy"
        );
        if (resignationData) {
          return res.status(200).json({
            success: true,
            message: constantObj.resign.GET_DATA,
            data: resignationData,
          });
        } else {
          return res.status(404).json({
            success: false,
            message: constantObj.resign.NO_RESULT,
          });
        }
      }
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },
};

/** Email to  user after generated code */
var sendResignationEmailToManagement = function (options) {
  let toemail = options.toemail;
  let name = options.name;
  let status = options.status

  message = "";
  style = {
    header: `
      padding:30px 15px;
      text-align:center;
      background-color:#f2f2f2;
      `,
    body: `
      padding:15px;
      height: 140px;
      `,
    hTitle: `font-family: 'Raleway', sans-serif;
      font-size: 35px;
      height:auto;
      line-height: normal;
      font-weight: bold;
      background:none;
      padding:0;
      color:#333;
      `,
    maindiv: `
      width:600px;
      margin:auto;
      font-family: Lato, sans-serif;
      font-size: 14px;
      color: #333;
      line-height: 24px;
      font-weight: 300;
      border: 1px solid #eaeaea;
      `,
    textPrimary: `color:#3e3a6e;
      `,
    h5: `font-family: Raleway, sans-serif;
      font-size: 22px;
      background:none;
      padding:0;
      color:#333;
      height:auto;
      font-weight: bold;
      line-height:normal;
      `,
    m0: `margin:0;`,
    mb3: "margin-bottom:15px;",
    textCenter: `text-align:center;`,
    textleft: `text-align:left;`,
    btn: `padding:10px 30px;
      font-weight:500;
      font-size:14px;
      line-height:normal;
      border:0;
      display:inline-block;
      text-decoration:none;
      `,
    btnPrimary: `
      background-color:#3e3a6e;
      color:#fff;
      `,
    footer: `
      padding:10px 15px;
      font-weight:500;
      color:#fff;
      text-align:center;
      background-color:#000;`,
  };

  message +=
    `<div class="container" style="` +
    style.maindiv +
    `">
  <div class="header" style="` +
    style.header +
    `text-align:center">
    <img src="https://mng.jcsoftwaresolution.in/assets/imgpsh_fullsize_anim.png" style="width: 107px;" />
      <h3 style="` +
    style.hTitle +
    style.m0 +
    `">Hi Admin</h3>
  </div>
  <div class="body" style="` +
    style.body +
    `">
      <p style="` +
    style.m0 +
    style.mb3 +
    style.textCenter +
    `margin-bottom:20px">I would like to inform you that I would like to cancel my resignation which I had applied.
  </p>
      </div>
      <div class="footer" style="` +
    style.footer +
    `">
      &copy 2023 JC Software Solution All rights reserved.
      </div>
  </div>`;
  //var to = ["chander@yopmail.com", "amit@yopmail.com"];
  SmtpController.sendResignationEmailOnChangeemployee(
    toemail,
    "Cancel resignation",
    message
  );
};



/** Email to  user after generated code */
var sendResignationEmailToEmployee = function (options) {
  let toemail = options.toemail;
  let name = options.name;
  let status = options.status


  message = "";
  style = {
    header: `
      padding:30px 15px;
      text-align:center;
      background-color:#f2f2f2;
      `,
    body: `
      padding:15px;
      height: 140px;
      `,
    hTitle: `font-family: 'Raleway', sans-serif;
      font-size: 35px;
      height:auto;
      line-height: normal;
      font-weight: bold;
      background:none;
      padding:0;
      color:#333;
      `,
    maindiv: `
      width:600px;
      margin:auto;
      font-family: Lato, sans-serif;
      font-size: 14px;
      color: #333;
      line-height: 24px;
      font-weight: 300;
      border: 1px solid #eaeaea;
      `,
    textPrimary: `color:#3e3a6e;
      `,
    h5: `font-family: Raleway, sans-serif;
      font-size: 22px;
      background:none;
      padding:0;
      color:#333;
      height:auto;
      font-weight: bold;
      line-height:normal;
      `,
    m0: `margin:0;`,
    mb3: "margin-bottom:15px;",
    textCenter: `text-align:center;`,
    textleft: `text-align:left;`,
    btn: `padding:10px 30px;
      font-weight:500;
      font-size:14px;
      line-height:normal;
      border:0;
      display:inline-block;
      text-decoration:none;
      `,
    btnPrimary: `
      background-color:#3e3a6e;
      color:#fff;
      `,
    footer: `
      padding:10px 15px;
      font-weight:500;
      color:#fff;
      text-align:center;
      background-color:#000;`,
  };

  message +=
    `<div class="container" style="` +
    style.maindiv +
    `">
  <div class="header" style="` +
    style.header +
    `text-align:center">
    <img src="https://mng.jcsoftwaresolution.in/assets/imgpsh_fullsize_anim.png" style="width: 107px;" />
      <h6 style="` +
    style.hTitle +
    style.m0 +
    `">Dear` + ' ' + name + `</h6>
  </div>
  <div class="body" style="` +
    style.body +
    `">
      <p style="` +
    style.m0 +
    style.mb3 +
    style.textCenter +
    `margin-bottom:20px">Your resignation is <b>` +
    status +
    `</b></p>
      </div>
      <div class="footer" style="` +
    style.footer +
    `">
      &copy 2023 JC Software Solution All rights reserved.
      </div>
  </div>`;

  //var to = ["chander@yopmail.com", "amit@yopmail.com"];
  SmtpController.sendResignationEmailOnChange(
    toemail,
    "Response of resignation letter",
    message
  );
};

var sendResignationEmailToManagementForAdminResponse = function (options) {
  let toemail = options.toemail;
  let name = options.name;
  let status = options.status

  message = "";
  style = {
    header: `
      padding:30px 15px;
      text-align:center;
      background-color:#f2f2f2;
      `,
    body: `
      padding:15px;
      height: 140px;
      `,
    hTitle: `font-family: 'Raleway', sans-serif;
      font-size: 35px;
      height:auto;
      line-height: normal;
      font-weight: bold;
      background:none;
      padding:0;
      color:#333;
      `,
    maindiv: `
      width:600px;
      margin:auto;
      font-family: Lato, sans-serif;
      font-size: 14px;
      color: #333;
      line-height: 24px;
      font-weight: 300;
      border: 1px solid #eaeaea;
      `,
    textPrimary: `color:#3e3a6e;
      `,
    h5: `font-family: Raleway, sans-serif;
      font-size: 22px;
      background:none;
      padding:0;
      color:#333;
      height:auto;
      font-weight: bold;
      line-height:normal;
      `,
    m0: `margin:0;`,
    mb3: "margin-bottom:15px;",
    textCenter: `text-align:center;`,
    textleft: `text-align:left;`,
    btn: `padding:10px 30px;
      font-weight:500;
      font-size:14px;
      line-height:normal;
      border:0;
      display:inline-block;
      text-decoration:none;
      `,
    btnPrimary: `
      background-color:#3e3a6e;
      color:#fff;
      `,
    footer: `
      padding:10px 15px;
      font-weight:500;
      color:#fff;
      text-align:center;
      background-color:#000;`,
  };

  message +=
    `<div class="container" style="` +
    style.maindiv +
    `">
  <div class="header" style="` +
    style.header +
    `text-align:center">
    <img src="https://mng.jcsoftwaresolution.in/assets/imgpsh_fullsize_anim.png" style="width: 107px;" />
      <h6 style="` +
    style.hTitle +
    style.m0 +
    `">Hi Management</h6>
  </div>
  <div class="body" style="` +
    style.body +
    `">
      <p style="` +
    style.m0 +
    style.mb3 +
    style.textCenter +
    `margin-bottom:20px">Resignation of <b>` + name + `</b> is <b>` + status + `</b>
  </p>
      </div>
      <div class="footer" style="` +
    style.footer +
    `">
      &copy 2023 JC Software Solution All rights reserved.
      </div>
  </div>`;
  //var to = ["chander@yopmail.com", "amit@yopmail.com"];
  SmtpController.sendResignationEmailOnChangeemployee(
    toemail,
    "Resignation response",
    message
  );
};