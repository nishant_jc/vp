/**
 * ProjectsController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
var _request = require("request");
var ObjectId = require("mongodb").ObjectID;
var constantObj = sails.config.constants;
const { constants } = require("../../config/constants");
const db = sails.getDatastore().manager;
const sharp = require("sharp");
const fs = require("fs");

var bcrypt = require("bcrypt-nodejs");
var nodemailer = require("nodemailer");
var smtpTransport = require("nodemailer-smtp-transport");
const SmtpController = require("../controllers/SmtpController");
const Helper = require("../controllers/HelperController");

const AdmZip = require('adm-zip');

function convertDate(dateString){
  var p = dateString.split(/\D/g)
  return [p[2],p[1],p[0] ].join("-")
}

/**
   *
   * @description: Used to generate random file name for every file
   * @createdAt 16/09/2021
   * @createdBy : Chandra Shekhar
    */

generateFileName = function () {
  var uuid = require("uuid");
  var randomStr = uuid.v4();
  var date = new Date();
  var currentDate = date.valueOf();
  retVal = randomStr + currentDate;
  return retVal;
};


getdata = async function (option) {
  var membersArray = [];
  var userEmailArray = [];
  var async = require("async");
  async.forEachOf(option, async function (element, key, callback) {
    let checkteamsmember = await Team.findOne({
      id: element,
    });
    var allteams = checkteamsmember.members;
    allteams.forEach(async (element) => {
      var userData = await Users.findOne({ id: element });
      useremail = userData.email;
      userName = userData.fullName;
      // emailwithName = useremail + "/" + userName;
      userEmailArray.push(useremail);
    });
  });
  setTimeout(() => {
    return userEmailArray;
  }, 3000);
};

module.exports = {
  /**
   *
   * @param {*} req
   * @param {*} res
   * @description: Used to add post in groups
   * @createdAt 15/09/2021
   * @createdBy : Chandra Shekhar
   */

  addproject: (req, res) => {
    API(ProjectsService.saveProject, req, res);
  },

  /**
   *
   * @param {*} res
   * @returns
   * @description: Api Used to get projects
   * @createdAt : 16/09/2021
   * @createdBy Chandra Shekhar
   */

  getProjects: (req, res) => {
    try {
      var search = req.param("search");
      var listsortBy = req.param("sortBy");
      var page = req.param("page");
      var count = req.param("count");
      var id = req.param("id");
      var status = req.param("status");
      var projectStatus = req.param("projectstatus");


      if (page == undefined) {
        page = 1;
      }
      if (count == undefined) {
        count = 10;
      }
      var skipNo = (page - 1) * count;
      var query = {};
      var sorting = "";
      var sortBy = "";
      if (listsortBy) {
        sorting = listsortBy.toString();
        if (sorting == "asc") {
          sortBy = -1;
        } else {
          sortBy = 1;
        }
      } else {
        sortBy = -1;
      }

      if (search) {
        query.$or = [
          { name: { $regex: search, $options: "i" } },
          { tags: { $regex: search, $options: "i" } },
        ];
      }
      if (status) {
        query.status = status;
      }

      if (projectStatus) {
        query.projectStatus = projectStatus;
      }
      query.isDeleted = false;
      if (id) {
        query.members = { $in: [(id)] }
      }
      db.collection("projects")
        .aggregate([
          {
            $lookup: {
              from: "category",
              localField: "category_id",
              foreignField: "_id",
              as: "category_id",
            },
          },
          {
            $unwind: {
              path: "$category_id",
              preserveNullAndEmptyArrays: true,
            },
          },
          {
            $lookup: {
              from: "jobplatform",
              localField: "platform",
              foreignField: "_id",
              as: "platform",
            },
          },
          {
            $unwind: {
              path: "$platform",
              preserveNullAndEmptyArrays: true,
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "platformId",
              foreignField: "_id",
              as: "platformId",
            },
          },
          {
            $unwind: {
              path: "$platformId",
              preserveNullAndEmptyArrays: true,
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "assign_member",
              foreignField: "_id",
              as: "assign_member",
            },
          },
          {
            $unwind: {
              path: "$assign_member",
              preserveNullAndEmptyArrays: true,
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "addedBy",
              foreignField: "_id",
              as: "addedBy",
            },
          },
          {
            $unwind: {
              path: "$addedBy",
              preserveNullAndEmptyArrays: true,
            },
          },
          {
            $project: {
              id: "$_id",
              name: "$name",
              description: "$description",
              image: "$image",
              category_id: "$category_id",
              frontendtechnology: "$frontendtechnology",
              backendtechnology: "$backendtechnology",
              totaltime: "$totaltime",
              startDate: "$startDate",
              endDate: "$endDate",
              platform: "$platform",
              platformId: "$platformId",
              documents: "$documents",
              teams: "$teams",
              members: "$members",
              assign_member: "$assign_member",
              progress_status: "$progress_status",
              feedback: "$feedback",
              project_type: "$project_type",
              clientName: "$clientName",
              clientSkypeId: "$clientSkypeId",
              clientPhoneNo: "$clientPhoneNo",
              clientCity: "$clientCity",
              clientCountry: "$clientCountry",
              status: "$status",
              projectStatus: "$projectStatus",
              websiteUrl: "$websiteUrl",
              git: "$git",
              backend_clone_link: "$backend_clone_link",
              backend_clone_password: "$backend_clone_password",
              frontend_clone_link: "$frontend_clone_link",
              frontend_clone_password: "$frontend_clone_password",
              admin_clone_link: "$admin_clone_link",
              admin_clone_password: "$admin_clone_password",
              thirdparty_clone_link: "$thirdparty_clone_link",
              thirdparty_clone_password: "$thirdparty_clone_password",
              updated_branch_api: "$updated_branch_api",
              updated_branch_admin: "$updated_branch_admin",
              updated_branch_frontend: "$updated_branch_frontend",
              frontend_repository_name: "$frontend_repository_name",
              admin_repository_name: "$admin_repository_name",
              api_repository_name: "$api_repository_name",
              owner_workspace_id: "$owner_workspace_id",
              is_frontend_updated_branch_merged_with_master: "$is_frontend_updated_branch_merged_with_master",
              is_admin_updated_branch_merged_with_master: "$is_admin_updated_branch_merged_with_master",
              is_api_updated_branch_merged_with_master: "$is_api_updated_branch_merged_with_master",
              frontend_owner_workspace_id: "$frontend_owner_workspace_id",
              api_owner_workspace_id: "$api_owner_workspace_id",
              admin_owner_workspace_id: "$admin_owner_workspace_id",
              createdAt: "$createdAt",
              deletedBy: "$deletedBy",
              addedBy: "$addedBy",
              figmaLink: "$figmaLink",
              uploadDocuments: "$uploadDocuments",
              weekly_plan: "$weekly_plan",
              updatedBy: "$updatedBy",
              isDeleted: "$isDeleted",
              deletedAt: "$deletedAt",
              updatedAt: "$updatedAt",
            },
          },
          {
            $match: query,
          },
        ])
        .toArray((err, totalResult) => {
          db.collection("projects")
            .aggregate([
              {
                $lookup: {
                  from: "category",
                  localField: "category_id",
                  foreignField: "_id",
                  as: "category_id",
                },
              },
              {
                $unwind: {
                  path: "$category_id",
                  preserveNullAndEmptyArrays: true,
                },
              },
              {
                $lookup: {
                  from: "jobplatform",
                  localField: "platform",
                  foreignField: "_id",
                  as: "platform",
                },
              },
              {
                $unwind: {
                  path: "$platform",
                  preserveNullAndEmptyArrays: true,
                },
              },
              {
                $lookup: {
                  from: "users",
                  localField: "platformId",
                  foreignField: "_id",
                  as: "platformId",
                },
              },
              {
                $unwind: {
                  path: "$platformId",
                  preserveNullAndEmptyArrays: true,
                },
              },
              {
                $lookup: {
                  from: "users",
                  localField: "assign_member",
                  foreignField: "_id",
                  as: "assign_member",
                },
              },
              {
                $unwind: {
                  path: "$assign_member",
                  preserveNullAndEmptyArrays: true,
                },
              },
              {
                $lookup: {
                  from: "users",
                  localField: "addedBy",
                  foreignField: "_id",
                  as: "addedBy",
                },
              },
              {
                $unwind: {
                  path: "$addedBy",
                  preserveNullAndEmptyArrays: true,
                },
              },

              {
                $project: {
                  id: "$_id",
                  name: "$name",
                  description: "$description",
                  image: "$image",
                  category_id: "$category_id",
                  frontendtechnology: "$frontendtechnology",
                  backendtechnology: "$backendtechnology",
                  totaltime: "$totaltime",
                  startDate: "$startDate",
                  endDate: "$endDate",
                  platform: "$platform",
                  platformId: "$platformId",
                  documents: "$documents",
                  teams: "$teams",
                  members: "$members",
                  assign_member: "$assign_member",
                  progress_status: "$progress_status",
                  feedback: "$feedback",
                  project_type: "$project_type",
                  clientName: "$clientName",
                  clientSkypeId: "$clientSkypeId",
                  clientPhoneNo: "$clientPhoneNo",
                  clientCity: "$clientCity",
                  clientCountry: "$clientCountry",
                  status: "$status",
                  projectStatus: "$projectStatus",
                  websiteUrl: "$websiteUrl",
                  git: "$git",
                  backend_clone_link: "$backend_clone_link",
                  backend_clone_password: "$backend_clone_password",
                  frontend_clone_link: "$frontend_clone_link",
                  frontend_clone_password: "$frontend_clone_password",
                  admin_clone_link: "$admin_clone_link",
                  admin_clone_password: "$admin_clone_password",
                  thirdparty_clone_link: "$thirdparty_clone_link",
                  thirdparty_clone_password: "$thirdparty_clone_password",
                  updated_branch_api: "$updated_branch_api",
                  updated_branch_admin: "$updated_branch_admin",
                  updated_branch_frontend: "$updated_branch_frontend",
                  frontend_repository_name: "$frontend_repository_name",
                  admin_repository_name: "$admin_repository_name",
                  api_repository_name: "$api_repository_name",
                  owner_workspace_id: "$owner_workspace_id",
                  frontend_owner_workspace_id: "$frontend_owner_workspace_id",
                  api_owner_workspace_id: "$api_owner_workspace_id",
                  admin_owner_workspace_id: "$admin_owner_workspace_id",
                  is_frontend_updated_branch_merged_with_master: "$is_frontend_updated_branch_merged_with_master",
                  is_admin_updated_branch_merged_with_master: "$is_admin_updated_branch_merged_with_master",
                  is_api_updated_branch_merged_with_master: "$is_api_updated_branch_merged_with_master",
                  createdAt: "$createdAt",
                  deletedBy: "$deletedBy",
                  addedBy: "$addedBy",
                  figmaLink: "$figmaLink",
                  uploadDocuments: "$uploadDocuments",
                  weekly_plan: "$weekly_plan",
                  updatedBy: "$updatedBy",
                  isDeleted: "$isDeleted",
                  deletedAt: "$deletedAt",
                  updatedAt: "$updatedAt",
                },
              },
              {
                $match: query,
              },
              {
                $sort: {
                  createdAt: -1
                }
              },
              {
                $skip: skipNo,
              },
              {
                $limit: Number(count),
              },
            ])
            .toArray(async (err, result) => {
              if (err) {
                return res.status(400).json({
                  success: false,
                  error: { message: "" + err },
                });
              } else {

                function* getpost(id) {
                  yield Weeklyplan.find({ project_id: id + "", isDeleted: false });


                }
                await (async function () {
                  for await (const singleResult of result) {
                    var id = JSON.stringify(singleResult.id);
                    var projectId = id.replace(/['"]+/g, "");
                    if (id != undefined) {
                      for await (const isProject of getpost(projectId)) {
                        if (isProject !== undefined) {
                          if (Object.entries(isProject).length > 0) {
                            singleResult["weeklyPlan"] = isProject;
                          }
                        }
                      }
                    }
                  }
                })();

                return res.status(200).json({
                  success: true,
                  message: constantObj.projects.GET_DATA,
                  data: result,
                  total: totalResult.length,
                });
              }
            });
        });
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },

  /**
   *
   * @param {id} req
   * @param {*} res
   * @returns
   * @description: Api Used to get single project by ID
   * @createdAt : 16/09/2021
   * @createdBy Chandra Shekhar
   */

  getSingleProject: async (req, res) => {
    try {
      let projectID = req.param("id");
      if (projectID == null) {
        return res.status(404).json({
          success: false,
          message: constantObj.projects.ID_REQUIRED,
        });
      } else {
        var weekly_plan = await Weeklyplan.find({ project_id: projectID });
        // console.log(weekly_plan,"============weekly_plan");
        var projectData = await Projects.findOne({ id: projectID }).populate('addedBy')
          .populate("category_id")
          .populate("platform")
          .populate("platformId")

        // console.log(projectData,"============projectData");

        let member_arr = [];
        if (projectData && projectData.members && projectData.members.length > 0) {
          for await (id of projectData.members) {
            let user = await Users.findOne({ id: id });
            if (user) {
              member_arr.push(user);
            }
          }
          projectData.members = member_arr;
        }

        if (projectData) {
          projectData.weekly_plan = weekly_plan;
        }

        if (projectData) {
          return res.status(200).json({
            success: true,
            message: constantObj.projects.GET_DATA,
            data: projectData,
          });
        } else {
          return res.status(404).json({
            success: false,
            message: constantObj.projects.NO_RESULT,
          });
        }
      }
    } catch (error) {
      // console.log(error,"error");
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },

  /**
   *
   * @param {id} req
   * @param {*} res
   * @returns
   * @description: Api Used to update project data by ID
   * @createdAt : 16/09/2021
   * @createdBy Chandra Shekhar
   */

  updateSingleProjects: async (req, res) => {
    try {
      let projectId = req.param("id");
      if (projectId == undefined) {
        return res.status(404).json({
          success: false,
          message: constantObj.projects.ID_REQUIRED,
        });
      }
      let checkmembers = await Projects.findOne({
        id: projectId,
      });

      let formprojectData = req.body;
      delete formprojectData.createdAt;
      delete formprojectData.updatedAt;

      // let memberString = req.body.members;
      // if (memberString != "") {
      //   let allMembers = memberString.split(",");
      //   allMembers.forEach((element) => {
      //     if (membersArray.indexOf(element) === -1) {
      //       membersArray.push(element);
      //     }
      //   });
      //   req.body.members = membersArray;
      // }                                                                    change on 10/may/2023 by bhagaya laxmi


      // code change on 10/may/2023
      // if (memberString != "") {
      //   if (memberString && memberString.length > 0) {
      //     memberString.forEach((items) => {
      //       if (membersArray.indexOf(items) === -1) {
      //         membersArray.push(items);
      //       }
      //     });
      //   }
      //   req.body.members = membersArray;

      // }
      // code change on 10/may/2023

      var updatedprojectData = await Projects.updateOne({
        id: projectId
      }, formprojectData)
      // console.log(updatedprojectData,'updatedprojectData');
      // console.log(req.identity.role);
      // if(req.identity.role=='employee' && req.body.projectStatus=='awarded'){
      //   // console.log('lllll');
      //   sendProjectEmail({
      //     toemail : sails.config.ADMIN_EMAIL,
      //     name : updatedprojectData.name,
      //     addedByname : req.identity.fullName,
      //    });
      // }
      // console.log(updatedprojectData)
      return res.status(200).json({
        success: true,
        message: constantObj.projects.UPDATED_PROJECT,
        data: updatedprojectData,
      });
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },

  /**
   *
   * @param {id} req
   * @param {*} res
   * @returns
   * @description: Api Used to remove member from project
   * @createdAt : 16/09/2021
   * @createdBy Chandra Shekhar
   */

  removeMemberFromProject: async (req, res) => {
    try {
      let projectId = req.param("id");
      let memberID = req.param("members");

      if (!projectId || typeof projectId == undefined) {
        return res.status(404).json({
          success: false,
          error: { code: 404, message: constantObj.projects.ID_REQUIRED },
        });
      }
      if (!memberID || typeof memberID == undefined) {
        return res.status(404).json({
          success: false,
          error: { code: 404, message: constantObj.projects.USER_ID_REQUIRED },
        });
      }

      let checkMembers = await Projects.findOne({
        id: projectId,
      });
      let usersArray = checkMembers.members;
      let newUsersArray = "";
      let allMembers = memberID.split(",");
      allMembers.forEach((element) => {
        if (usersArray.indexOf(element) !== -1)
          newUsersArray = usersArray.filter(function (obj) {
            return obj !== element;
          });
      });
      if (newUsersArray == "") {
        return res.status(400).json({
          success: false,
          error: { message: constantObj.projects.MEMBER_NOT_EXIST },
        });
      } else {
        var usersUpdated = await Projects.updateOne({
          id: projectId,
        }).set({ members: newUsersArray });

        return res.status(200).json({
          success: true,
          message: constantObj.projects.REMOVED_MEMBER_SUCCESS,
          data: usersUpdated,
        });
      }
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },

  /**
  *
  * @param {id} req
  * @param {*} res
  * @returns
  * @description: Api Used to delete project by projectID
  * @createdAt : 16/09/2021
  * @createdBy Chandra Shekhar
  */

  projectDeletedByID: async (req, res) => {
    try {
      let deletedID = req.param("id");
      if (deletedID == undefined) {
        return res.status(404).json({
          success: false,
          message: constantObj.projects.ID_REQUIRED,
        });
      }
      var deletedByUser = req.identity.id;
      var deletedData = await Projects.updateOne({
        id: deletedID,
      }).set({ isDeleted: true, deletedBy: deletedByUser });
      if (deletedData) {
        return res.status(200).json({
          success: true,
          message: constantObj.projects.DELETED_PROJECT,
        });
      } else {
        return res.status(404).json({
          success: false,
          message: constantObj.projects.NO_RESULT,
        });
      }
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },

  /**
   *
   * @param {*} req
   * @param {*} res
   * @returns
   * @description: Api Used to upload image
   * @createdAt : 16/09/2021
   * @createdBy Chandra Shekhar
   */

  uploadImage: async (req, res) => {
    var modelName = req.param("modelName");

    // define folders path
    var rootpath = process.cwd();
    var basefolderpath = rootpath + "/assets/images";
    var fullpath = rootpath + "/assets/images/" + modelName;
    var fullpaththumbnail =
      rootpath + "/assets/images/" + modelName + "/thumbnail";
    var fullpath200 =
      rootpath + "/assets/images/" + modelName + "/thumbnail/200";
    var fullpath300 =
      rootpath + "/assets/images/" + modelName + "/thumbnail/300";
    var fullpath500 =
      rootpath + "/assets/images/" + modelName + "/thumbnail/500";

    //Check image upload folder is exists or not. If not create all folders
    if (fs.existsSync(basefolderpath) == false) {
      fs.mkdirSync(basefolderpath);
      fs.mkdirSync(fullpath);
      fs.mkdirSync(fullpaththumbnail);
      fs.mkdirSync(fullpath200);
      fs.mkdirSync(fullpath300);
      fs.mkdirSync(fullpath500);
    } else {
      if (fullpath == false) {
        fs.mkdirSync(fullpath);
        fs.mkdirSync(fullpaththumbnail);
        fs.mkdirSync(fullpath200);
        fs.mkdirSync(fullpath300);
        fs.mkdirSync(fullpath500);
      }
    }

    try {
      req
        .file("file")
        .upload({ dirname: "../../assets/images" }, async (err, file) => {
          var responseData = {};
          file.forEach(async (element, index) => {
            var name = generateFileName();
            typeArr = element.type.split("/");
            fileExt = typeArr[1];

            fs.readFile(file[index].fd, async (err, data) => {
              if (err) {
                return res.status(403).json({
                  success: false,
                  error: {
                    code: 403,
                    message: err,
                  },
                });
              } else {
                if (data) {
                  var path = file[index].fd;
                  fs.writeFile(
                    "assets/images/" + modelName + "/" + name + "." + fileExt,
                    data,
                    function (err, image) {
                      if (err) {
                        return res.status(400).json({
                          success: false,
                          error: {
                            code: 400,
                            message: err,
                          },
                        });
                      }
                    }
                  );

                  // fullpath.push( name+'.'+fileExt)
                  // resImagePath.push('assets/images/'+modelName+'/'  + name+'.'+fileExt)
                  responseData.fullpath = name + "." + fileExt;
                  responseData.imagePath =
                    "images/" + modelName + "/" + name + "." + fileExt;
                  var thumbpath =
                    "assets/images/" +
                    modelName +
                    "/thumbnail/200/" +
                    name +
                    "." +
                    fileExt;
                  sharp(path)
                    .resize({ height: 200, width: 200 })
                    .toFile(thumbpath)
                    .then(function (newFileInfo) { })
                    .catch(function (err) {
                    });
                  var thumbpath1 =
                    "assets/images/" +
                    modelName +
                    "/thumbnail/300/" +
                    name +
                    "." +
                    fileExt;
                  var thumbpath2 =
                    "assets/images/" +
                    modelName +
                    "/thumbnail/500/" +
                    name +
                    "." +
                    fileExt;
                  sharp(path)
                    .resize({ height: 300, width: 300 })
                    .toFile(thumbpath1)
                    .then(function (newFileInfo) { })
                    .catch(function (err) {
                    });
                  sharp(path)
                    .resize({ height: 500, width: 500 })
                    .toFile(thumbpath2)
                    .then(function (newFileInfo) { })
                    .catch(function (err) {
                    });

                  if (index == file.length - 1) {
                    await new Promise((resolve) => setTimeout(resolve, 2000)); //Because file take times to write in .tmp folder
                    return res.json({
                      success: true,
                      code: 200,
                      data: responseData,
                    });
                  }
                }
              }
            }); //end of loop
          });
        });
    } catch (err) {
      return res
        .status(500)
        .json({ success: false, error: { code: 500, message: "" + err } });
    }
  },

  /**
   *
   * @param {*} req
   * @param {*} res
   * @returns
   * @description: Api Used to upload document
   * @createdAt : 16/09/2021
   * @createdBy Chandra Shekhar
   */

  uploadDocument: async (req, res) => {
    var modelName = req.param("modelName");
    if (!modelName) {
      throw constants.projects.MODEL_NAME_REQUIRED;
    }

    // define folders path
    var rootpath = process.cwd();
    var folder = rootpath + "/assets/documents";
    var fullpath = rootpath + "/assets/documents/" + modelName;

    //Check image upload folder is exists or not. If not create all folders
    if (fs.existsSync(folder) == false) {
      fs.mkdirSync(folder);
      fs.mkdirSync(fullpath);
    }
    if (fs.existsSync(fullpath) == false) {
      fs.mkdirSync(fullpath)
    }

    try {
      req
        .file("file")
        .upload({ dirname: "../../assets/documents" }, async (err, file) => {
          var multipleimageArray = [];

          file.forEach(async (element, index) => {
            var responseData = {};
            var name = generateName();
            typeArr = element.type.split("/");
            let fileExt = element.fd.split('.').pop();

            fs.readFile(file[index].fd, async (err, data) => {
              if (err) {
                return res.status(403).json({
                  success: false,
                  error: {
                    code: 403,
                    message: err,
                  },
                });
              } else {
                if (data) {
                  var path = file[index].fd;

                  fs.writeFile(
                    "assets/documents/" + modelName + "/" + name + "." + fileExt,
                    data,
                    function (err, image) {
                      if (err) {
                        return res.status(400).json({
                          success: false,
                          error: {
                            code: 400,
                            message: err,
                          },
                        });
                      }
                    }
                  );

                  // fullpath.push( name+'.'+fileExt)
                  // resImagePath.push('assets/images/'+modelName+'/'  + name+'.'+fileExt)

                  responseData.fullpath = name + "." + fileExt;
                  responseData.imagePath =
                    "documents/" + modelName + "/" + name + "." + fileExt;

                  multipleimageArray.push(responseData);



                  if (index == file.length - 1) {
                    await new Promise((resolve) => setTimeout(resolve, 2000)); //Because file take times to write in .tmp folder
                    return res.json({
                      success: true,
                      code: 200,
                      data: multipleimageArray,
                    });
                  }
                }
              }
            }); //end of loop
          });
        });
    } catch (err) {
      return res
        .status(500)
        .json({ success: false, error: { code: 500, message: "" + err } });
    }
  },

  /**
   *
   * @param {id,teamId} req
   * @param {*} res
   * @returns
   * @description: Api Used to remove team from project
   * @createdAt : 21/09/2021
   * @createdBy Chandra Shekhar
   */

  removeTeamFromProject: async (req, res) => {
    try {
      let projectId = req.param("id");
      let teamId = req.param("teamId");

      if (!projectId || typeof projectId == undefined) {
        return res.status(404).json({
          success: false,
          error: { code: 404, message: constantObj.projects.ID_REQUIRED },
        });
      }
      if (!teamId || typeof teamId == undefined) {
        return res.status(404).json({
          success: false,
          error: { code: 404, message: constantObj.projects.USER_ID_REQUIRED },
        });
      }

      let checkteams = await Projects.findOne({
        id: projectId,
      });
      let teamsArray = checkteams.teams;
      let newTeamsArray = "";
      let allteams = teamId.split(",");
      allteams.forEach((element) => {
        if (teamsArray.indexOf(element) !== -1)
          newTeamsArray = teamsArray.filter(function (obj) {
            return obj !== element;
          });
      });
      if (newTeamsArray == "") {
        return res.status(400).json({
          success: false,
          error: { message: constantObj.team.TEAM_NOT_EXIST },
        });
      } else {
        var teamsUpdated = await Projects.updateOne({
          id: projectId,
        }).set({ teams: newTeamsArray });

        return res.status(200).json({
          success: true,
          message: constantObj.team.REMOVED_TEAM_SUCCESS,
          data: teamsUpdated,
        });
      }
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },
  /**
   *
   * @param {id,teamId} req
   * @param {*} res
   * @returns
   * @description: Api Used to add team to project
   * @createdAt : 21/09/2021
   * @createdBy Chandra Shekhar
   */

  addTeamToProject: async (req, res) => {
    try {
      let projectId = req.param("id");
      let teamId = req.param("teamId");

      if (!projectId || typeof projectId == undefined) {
        return res.status(404).json({
          success: false,
          error: { code: 404, message: constantObj.projects.ID_REQUIRED },
        });
      }
      if (!teamId || typeof teamId == undefined) {
        return res.status(404).json({
          success: false,
          error: { code: 404, message: constantObj.projects.USER_ID_REQUIRED },
        });
      }

      let checkteams = await Projects.findOne({
        id: projectId,
      });

      let teamsArray = checkteams.teams;
      let projectname = checkteams.name;
      let allteams = teamId.split(",");
      let errorMessage = [];
      let successMessage = [];

      allteams.forEach((element) => {
        if (teamsArray.indexOf(element) === -1) {
          teamsArray.push(element);
          successMessage.push(
            element + " team is added in project successfully"
          );
        } else {
          errorMessage.push(element + " is already exist in project");
        }
      });
      var teamsUpdated = await Projects.updateOne({
        id: projectId,
      }).set({ teams: teamsArray });

      let userEmailArray = [];
      var async = require("async");
      async.forEachOf(teamsArray, async function (element, key, callback) {
        let checkteamsmember = await Team.findOne({
          id: element,
        });
        var allteams = checkteamsmember.members;
        allteams.forEach(async (element) => {
          var userData = await Users.findOne({ id: element });
          useremail = userData.email;
          userName = userData.fullName;
          // emailwithName = useremail + "/" + userName;
          userEmailArray.push(useremail);
        });
      });
      setTimeout(() => {
        sendAssignEmail({
          toemail: userEmailArray,
          name: projectname,
        });
        return res.status(200).json({
          success: true,
          message: constantObj.projects.ASSIGN_MEMBER_PROJECT,
        });
      }, 3000);
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },

  /**
   *
   * @param {id,teamId} req
   * @param {*} res
   * @returns
   * @description: Api Used to add member to project
   * @createdAt : 21/09/2021
   * @createdBy Chandra Shekhar
   */

  addmemberToProject: async (req, res) => {
    try {
      let projectId = req.param("id");
      let memberId = req.param("memberId");

      if (!projectId || typeof projectId == undefined) {
        return res.status(404).json({
          success: false,
          error: { code: 404, message: constantObj.projects.ID_REQUIRED },
        });
      }
      if (!memberId || typeof memberId == undefined) {
        return res.status(404).json({
          success: false,
          error: { code: 404, message: constantObj.projects.USER_ID_REQUIRED },
        });
      }

      let checkmembers = await Projects.findOne({
        id: projectId,
      });
      let userEmailArray = []
      // if(checkmembers && checkmembers.members && checkmembers.members.length > 0){

      let memberIdArray = checkmembers.members.length > 0 ? checkmembers.members : [memberId];
      let projectname = checkmembers.name;
      let description = checkmembers.description;
      let startDate = checkmembers.startDate;
      let endDate = checkmembers.endDate;
      let documents = checkmembers.documents
      let allmembersID = memberId.split(",");

      allmembersID.forEach(async (element) => {
        if (memberIdArray.indexOf(element) === -1) {
          memberIdArray.push(element);
        }
        var userData = await Users.findOne({ id: element });
        var useremail = userData.email;
        userEmailArray.push(useremail);
      });
      var memberUpdated = await Projects.updateOne({
        id: projectId,
      }).set({ members: memberIdArray });

      sendAssignEmail({
        toemail: userEmailArray,
        name: projectname,
        description: description,
        startDate: startDate,
        endDate: endDate,
        documents: documents,
      });

      return res.status(200).json({
        success: true,
        message: constantObj.projects.ASSIGN_MEMBER_PROJECT,
      });
    } catch (error) {
      // console.log(error);
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },

  /**
   *
   * @param {*} res
   * @returns
   * @description: Api Used to get projects by userID
   * @createdAt : 06/10/2021
   * @createdBy Rohit kumar
   */

  getProjectsByUserID: (req, res) => {
    try {
      var search = req.param("search");
      var listsortBy = req.param("sortBy");
      var page = req.param("page");
      var count = req.param("count");

      var members = req.param("id");


      if (page == undefined) {
        page = 1;
      }
      if (count == undefined) {
        count = 10;
      }
      var skipNo = (page - 1) * count;
      var query = {};
      var sortBy = "";
      var sorting = "";
      // if (listsortBy) {
      //   sorting = listsortBy.toString();
      //   if (sorting == "asc") {
      //     sortBy = -1;
      //   } else {
      //     sortBy = 1;
      //   }
      // } else {
      //   sortBy = -1;
      // }

      if (search) {
        query.$or = [
          { name: { $regex: search, $options: "i" } },
        ];
      }
      if (members) {
        var membersArr = members.split(",");
        query.members = { $in: membersArr };
      }
      query.isDeleted = false;
      db.collection("projects")
        .aggregate([
          {
            $lookup: {
              from: "category",
              localField: "category_id",
              foreignField: "_id",
              as: "category_id",
            },
          },
          {
            $unwind: {
              path: "$category_id",
              preserveNullAndEmptyArrays: true,
            },
          },
          {
            $lookup: {
              from: "jobplatform",
              localField: "platform",
              foreignField: "_id",
              as: "platform",
            },
          },
          {
            $unwind: {
              path: "$platform",
              preserveNullAndEmptyArrays: true,
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "platformId",
              foreignField: "_id",
              as: "platformId",
            },
          },
          {
            $unwind: {
              path: "$platformId",
              preserveNullAndEmptyArrays: true,
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "assign_member",
              foreignField: "_id",
              as: "assign_member",
            },
          },
          {
            $unwind: {
              path: "$assign_member",
              preserveNullAndEmptyArrays: true,
            },
          },
          {
            $project: {
              id: "$_id",
              name: "$name",
              description: "$description",
              image: "$image",
              category_id: "$category_id",
              frontendtechnology: "$frontendtechnology",
              backendtechnology: "$backendtechnology",
              totaltime: "$totaltime",
              startDate: "$startDate",
              endDate: "$endDate",
              platform: "$platform",
              platformId: "$platformId",
              documents: "$documents",
              teams: "$teams",
              members: "$members",
              assign_member: "$assign_member",
              progress_status: "$progress_status",
              websiteUrl: "$websiteUrl",
              feedback: "$feedback",
              project_type: "$project_type",
              clientName: "$clientName",
              clientSkypeId: "$clientSkypeId",
              clientPhoneNo: "$clientPhoneNo",
              clientCity: "$clientCity",
              clientCountry: "$clientCountry",
              status: "$status",
              createdAt: "$createdAt",
              deletedBy: "$deletedBy",
              addedBy: "$addedBy",
              updatedBy: "$updatedBy",
              isDeleted: "$isDeleted",
              deletedAt: "$deletedAt",
              updatedAt: "$updatedAt",
            },
          },
          {
            $match: query,
          },
        ])
        .toArray((err, totalResult) => {
          db.collection("projects")
            .aggregate([
              {
                $lookup: {
                  from: "category",
                  localField: "category_id",
                  foreignField: "_id",
                  as: "category_id",
                },
              },
              {
                $unwind: {
                  path: "$category_id",
                  preserveNullAndEmptyArrays: true,
                },
              },
              {
                $lookup: {
                  from: "jobplatform",
                  localField: "platform",
                  foreignField: "_id",
                  as: "platform",
                },
              },
              {
                $unwind: {
                  path: "$platform",
                  preserveNullAndEmptyArrays: true,
                },
              },
              {
                $lookup: {
                  from: "users",
                  localField: "platformId",
                  foreignField: "_id",
                  as: "platformId",
                },
              },
              {
                $unwind: {
                  path: "$platformId",
                  preserveNullAndEmptyArrays: true,
                },
              },
              {
                $lookup: {
                  from: "users",
                  localField: "assign_member",
                  foreignField: "_id",
                  as: "assign_member",
                },
              },
              {
                $unwind: {
                  path: "$assign_member",
                  preserveNullAndEmptyArrays: true,
                },
              },
              {
                $project: {
                  id: "$_id",
                  name: "$name",
                  description: "$description",
                  image: "$image",
                  category_id: "$category_id",
                  frontendtechnology: "$frontendtechnology",
                  backendtechnology: "$backendtechnology",
                  totaltime: "$totaltime",
                  startDate: "$startDate",
                  endDate: "$endDate",
                  platform: "$platform",
                  platformId: "$platformId",
                  documents: "$documents",
                  teams: "$teams",
                  members: "$members",
                  assign_member: "$assign_member",
                  progress_status: "$progress_status",
                  feedback: "$feedback",
                  project_type: "$project_type",
                  clientName: "$clientName",
                  clientSkypeId: "$clientSkypeId",
                  clientPhoneNo: "$clientPhoneNo",
                  clientCity: "$clientCity",
                  clientCountry: "$clientCountry",
                  status: "$status",
                  createdAt: "$createdAt",
                  deletedBy: "$deletedBy",
                  addedBy: "$addedBy",
                  updatedBy: "$updatedBy",
                  isDeleted: "$isDeleted",
                  deletedAt: "$deletedAt",
                  updatedAt: "$updatedAt",
                },
              },
              {
                $match: query,
              },
              // { $sort: { updatedAt: sortBy } },
              {
                $skip: skipNo,
              },
              {
                $limit: Number(count),
              },
            ])
            .toArray((err, result) => {
              if (err) {
                return res.status(400).json({
                  success: false,
                  error: { message: "" + err },
                });
              } else {
                return res.status(200).json({
                  success: true,
                  message: constantObj.projects.GET_DATA,
                  data: result,
                  total: totalResult.length,
                });
              }
            });
        });
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },

  /**
  *
  * @param {*} res
  * @returns
  * @description: Api Used to get projects status count by userID
  * @createdAt : 18/10/2021
  * @createdBy Rohit kumar
  */

  getProjectsstatuscountByUserID: async (req, res) => {
    try {
      var userID = req.identity.id;
      let projects = await Projects.find({
        members: { in: [userID] },
      });
      let InProgress = 0;
      let Failed = 0;
      let Success = 0;


      projects.forEach((element) => {
        switch (element.progress_status) {
          case 'InProgress': InProgress++;
            break;

          case 'Failed': Failed++;
            break;

          case 'Success': Success++;
            break;
        }
      });
      let result = {
        InProgress: InProgress,
        Failed: Failed,
        Success: Success
      }
      return res.status(200).json({
        success: true,
        message: constantObj.projects.GET_DATA,
        data: result,
        total: projects.length,
      });


    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { code: 400, message: error },
      });
    }
  },

  /**
  *
  * @param {*} res
  * @returns
  * @description: Api Used to get projects status count by userID
  * @createdAt : 19/10/2021
  * @createdBy Rohit kumar
  */

  getProjectsstatuscount: async (req, res) => {
    try {
      let projects = await Projects.find({});
      let InProgress = 0;
      let Failed = 0;
      let Success = 0;


      projects.forEach((element) => {
        switch (element.progress_status) {
          case 'InProgress': InProgress++;
            break;

          case 'Failed': Failed++;
            break;

          case 'Success': Success++;
            break;
        }
      });
      let result = {
        InProgress: InProgress,
        Failed: Failed,
        Success: Success
      }
      return res.status(200).json({
        success: true,
        message: constantObj.projects.GET_DATA,
        data: result,
        total: projects.length,
      });


    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { code: 400, message: error },
      });
    }
  },


  /**
*
* @param {*} res
* @returns
* @description: Api Used to get project document zip  by projectId 
* @createdAt : 15/11/2021
* @createdBy Rohit kumar
*/

  // getprojectdocumentzip: async (req, res) => {

  //   try {
  //     var projectId = req.param("id");
  //     let projects = await Projects.findOne({ id: projectId });
  //     console.log(projects,"================projects");

  //     if (projects.documents.length > 0) {
  //       const zip = new AdmZip();
  //       for (var i = 0; i < projects.documents.length; i++) {
  //         zip.addLocalFile('./assets/documents/users/' + projects.documents[i].fullpath);
  //       }

  //       // Define zip file name
  //       const downloadName = `${Date.now()}.zip`;

  //       const bufferdata = zip.toBuffer();

  //       // save file zip in root directory
  //       zip.writeZip('./assets/documents/users/' + downloadName);

  //       // code to download zip file

  //       res.set('Content-Type', 'application/octet-stream');
  //       res.set('Content-Disposition', `attachment; filename=${downloadName}`);
  //       res.set('Content-Length', bufferdata.length);
  //       res.send(bufferdata);

  //     } else {
  //       return res.status(404).json({
  //         success: false,
  //         message: "No documents found."
  //       })
  //     }
  //   } catch (err) {
  //     console.log(err)
  //   }

  // },



  getprojectdocumentzip: async (req, res) => {

    try {
      var projectId = req.param("id");
      let weekly_plans_document = await Weeklyplan.find({ project_id: projectId });


      const zip = new AdmZip();

      var rootpath = process.cwd();

      var downloadFileName = 'AllWeeklyPlans.zip';
      var folder = rootpath + "/assets/documents/documents/" + downloadFileName;

      for await (var items of weekly_plans_document) {
        zip.addLocalFile(rootpath + "/assets/" + items.documents);
      }

      //-------------------LOCAL--------------------
      // var weekly_plans_document = [{
      //   "documents": "documents/documents/b7be451e-f6bb-4c42-8d72-58d06c34e67a1687960821041.pdf"
      // }, {
      //   "documents": "documents/documents/sample.pdf"
      // }];

      // for await (var items of weekly_plans_document) {
      //   console.log(items.documents, "===========items.documents");
      //   console.log('in loop');
      //   await zip.addLocalFile(rootpath + "/assets/" + items.documents);
      //   // zip.addLocalFolder(rootpath + "/assets/" + items.documents);
      //   console.log(rootpath + "/assets/" + items.documents, "==============data");
      // }
      //---------------------------LOCAL-----------------------


      const outputFile = folder;

      const bufferdata = zip.toBuffer();

      zip.writeZip(outputFile);
      res.set('Content-Type', 'application/octet-stream');
      res.set('Content-Disposition', `attachment; filename=${downloadFileName}`);
      res.set('Content-Length', bufferdata.length);
      res.send(bufferdata);
    } catch (e) {
      console.log(`Something went wrong. ${e}`);
    }
  }
};

/** Email to  user after generated code */
var sendAssignEmail = function (options) {
  var toemail = options.toemail;
  var projectname = options.name;
  var description = options.description;
  var startDate = options.startDate;
  var endDate = options.endDate;
  var documents = options.documents

  
  
  startDate= convertDate(startDate)
  endDate= convertDate(endDate)

  message = "";
  style = {
    header: `
        padding:30px 15px;
        text-align:center;
        background-color:#f2f2f2;
        `,
    body: `
        padding:15px;
        height: 140px;
        `,
    hTitle: `font-family: 'Raleway', sans-serif;
        font-size: 37px;
        height:auto;
        line-height: normal;
        font-weight: bold;
        background:none;
        padding:0;
        color:#333;
        `,
    maindiv: `
        width:600px;
        margin:auto;
        font-family: Lato, sans-serif;
        font-size: 14px;
        color: #333;
        line-height: 24px;
        font-weight: 300;
        border: 1px solid #eaeaea;
        `,
    textPrimary: `color:#3e3a6e;
        `,
    h5: `font-family: Raleway, sans-serif;
        font-size: 22px;
        background:none;
        padding:0;
        color:#333;
        height:auto;
        font-weight: bold;
        line-height:normal;
        `,
    m0: `margin:0;`,
    mb3: "margin-bottom:15px;",
    textCenter: `text-align:center;`,
    textleft: `text-align:left;`,
    btn: `padding:10px 30px;
        font-weight:500;
        font-size:14px;
        line-height:normal;
        border:0;
        display:inline-block;
        text-decoration:none;
        `,
    btnPrimary: `
        background-color:#3e3a6e;
        color:#fff;
        `,
    footer: `
        padding:10px 15px;
        font-weight:500;
        color:#fff;
        text-align:center;
        background-color:#000;`,
  };

  message +=
    `<div class="container" style="` +
    style.maindiv +
    `">
    <div class="header" style="` +
    style.header +
    `text-align:center">
    <img src="https://mng.jcsoftwaresolution.in/assets/imgpsh_fullsize_anim.png" style="width: 107px;" />
        <h2 style="` +
    style.hTitle +
    style.m0 +
    `">Congratulations Member</h2>
    </div>
    <div class="body" style="` +
    style.body +
    `">
        <p style="` +
    style.m0 +
    style.mb3 +
    style.textCenter +
    `margin-bottom:20px">You are seleted for the <b>new </b>project of <b><b style="text-transform: capitalize;">` +
    projectname +
    `</b><b></p>
    <p style="` +
    style.m0 +
    style.mb3 +
    style.textCenter +
    `margin-bottom:20px">` +
    description +
    `</p>
    <p style="` +
    style.m0 +
    style.mb3 +
    style.textCenter +
    `margin-bottom:20px">Start Date : ` +
    startDate +
    `</p>
    <p style="` +
    style.m0 +
    style.mb3 +
    style.textCenter +
    `margin-bottom:20px">End Date : ` +
    endDate +
    `</p>
    <p style="` +
    style.m0 +
    style.mb3 +
    style.textCenter +
    `margin-bottom:20px">` +
    documents +
    `</p>
        </div>
        <div class="footer" style="` +
    style.footer +
    `">
        &copy 2023 JC Software Solution All rights reserved.
        </div>
    </div>`;

  //var to = ["chander@yopmail.com", "amit@yopmail.com"];
  SmtpController.sendProjectAssignEmail(
    toemail,
    "You are selected for the new project",
    message
  );
};

var sendProjectEmail = function (options) {
  var toemail = options.toemail;
  var projectname = options.name;
  var addedByname = options.addedByname;

  (message = "");
  style = {
    header: `
        padding:30px 15px;
        text-align:center;
        background-color:#f2f2f2;
        `,
    body: `
        padding:15px;
        height: 140px;
        `,
    hTitle: `font-family: 'Raleway', sans-serif;
        font-size: 37px;
        height:auto;
        line-height: normal;
        font-weight: bold;
        background:none;
        padding:0;
        color:#333;
        `,
    maindiv: `
        width:600px;
        margin:auto;
        font-family: Lato, sans-serif;
        font-size: 14px;
        color: #333;
        line-height: 24px;
        font-weight: 300;
        border: 1px solid #eaeaea;
        `,
    textPrimary: `color:#3e3a6e;
        `,
    h5: `font-family: Raleway, sans-serif;
        font-size: 22px;
        background:none;
        padding:0;
        color:#333;
        height:auto;
        font-weight: bold;
        line-height:normal;
        `,
    m0: `margin:0;`,
    mb3: "margin-bottom:15px;",
    textCenter: `text-align:center;`,
    textleft: `text-align:left;`,
    btn: `padding:10px 30px;
        font-weight:500;
        font-size:14px;
        line-height:normal;
        border:0;
        display:inline-block;
        text-decoration:none;
        `,
    btnPrimary: `
        background-color:#3e3a6e;
        color:#fff;
        `,
    footer: `
        padding:10px 15px;
        font-weight:500;
        color:#fff;
        text-align:center;
        background-color:#000;`,
  };

  message +=
    `<div class="container" style="` +
    style.maindiv +
    `">
    <div class="header" style="` +
    style.header +
    `text-align:center">
        <img src="http://198.251.65.146:4091/img/logo.png" width="150" style="margin-bottom:20px;" />
        <h2 style="` +
    style.hTitle +
    style.m0 +
    `">Congratulations Admin</h2>
    </div>
    <div class="body" style="` +
    style.body +
    `">
        <p style="` +
    style.m0 +
    style.mb3 +
    style.textCenter +
    `margin-bottom:20px">New project is awarded : <b>` +
    projectname +
    `<b> by <b>` +
    addedByname +
    `<b></p>
        </div>
        <div class="footer" style="` +
    style.footer +
    `">
        &copy 2021 JC Software Solution All rights reserved.
        </div>
    </div>`;

  //var to = ["chander@yopmail.com", "amit@yopmail.com"];
  SmtpController.sendProjectAssignEmail(
    toemail,
    "New project created successfully.",
    message
  );
};
