/**
 * ContentmanagementController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */


 var constantObj = sails.config.constants;
module.exports = {
  /**
   *
   * @param {*} req
   * @param {*} res
   * @description: Used to add Content
   * @createdAt 19/10/2021
   * @createdBy : Rohit kumar
   */

   addContent: (req, res) => {
    API(ContentmanagementService.saveContent, req, res);
  },

  /**
   *
   * @param {*} req
   * @param {*} res
   * @description: Used to add Content
   * @createdAt 19/10/2021
   * @createdBy : Rohit kumar
   */

  edit: (req, res) => {
    API(ContentmanagementService.updateContent, req, res);
  },


  /**
   *
   * @param {*} res
   * @returns
   * @description: Api Used to get Content
   * @createdAt : 19/10/2021
   * @createdBy Rohit kumar
   */

   getAllcontents: function (req, res, next) {
    var search = req.param("search");
    var sortBy = req.param("sortBy");
    var page = req.param("page");
    if (page == undefined) {
      page = 1;
    }
    var count = req.param("count");
    if (count == undefined) {
      count = 100;
    }
    var skipNo = (page - 1) * count;

    var query = {};
    if (sortBy) {
      sortBy = sortBy.toString();
    } else {
      sortBy = "createdAt desc";
    }

    var searchQuery = {};
    if (search) {
      query.slug = {
        like: "%" + search.toLowerCase() + "%",
      };
      searchQuery.title = {
        like: "%" + search.toLowerCase() + "%",
      };
    }

  

    var isDeleted_name = req.param("isDeleted");
    if (isDeleted_name) {
      query.isDeleted = req.param("isDeleted");
      searchQuery.isDeleted = req.param("isDeleted");
    }

    Contentmanagement.count(query)
      .where(searchQuery)
      .exec(function (err, total) {

        if (err) {
          return res.status(400).json({
            success: false,
            error: err,
          });
        } else {
            Contentmanagement.find(query)
            .where(searchQuery)
            .populate("deletedBy")
            .sort(sortBy)
            .exec(function (err, contents) {
              if (err) {
                return res.status(400).json({ success: false, error: { code: 400, message: err } });
              } else {
                return res.json({
                  success: true,
                  code: 200,
                  data: contents,
                  total: total,
                });
              }
            });
        }
      });
  },


  /**
   *
   * @param {id} req
   * @param {*} res
   * @returns
   * @description: Api Used to get single project by ID
   * @createdAt : 20/10/2021
   * @createdBy Rohit kumar
   */

   getSingleContent: async (req, res) => {
    try {
      let ContentID = req.param("id");
      if (ContentID == null) {
        return res.status(404).json({
          success: false,
          message: constantObj.Contentmanagement.ID_REQUIRED,
        });
      } else {
        var ContentData = await Contentmanagement.findOne({ id: ContentID });
        if (ContentData) {
          return res.status(200).json({
            success: true,
            message: constantObj.Contentmanagement.GET_DATA,
            data: ContentData,
          });
        } else {
          return res.status(404).json({
            success: false,
            message: constantObj.Contentmanagement.NO_RESULT,
          });
        }
      }
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },

};

