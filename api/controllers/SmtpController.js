/**
 * SmtpController
 *
 * @description :: Server-side logic for managing Smtp
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
const credentials = require('../../config/local');


module.exports = {
  /**
   *
   * @param {*} req
   * @param {*} res
   * @description Used to get smtp detail
   * @createdAt 01/09/2021
   * @createdBy : Amit Kumar
   */
  smtp: (req, res) => {
    Smtp.find({}).then((smtp) => {
      if (smtp.length > 0) {
        return res.status(200).json({
          success: true,
          code: 200,
          data: smtp[0],
        });
      } else {
        return res.status(200).json({
          success: true,
          code: 200,
          data: {},
        });
      }
    });
  },
  /**
   *
   * @param {*} req {id:""}
   * @param {*} res
   * @description Used to update the smtp details
   * @createdAt 01/09/2021
   * @returns {success:"",code:"",data:""}
   * @createdBy : Amit Kumar
   */
  edit: (req, res) => {
    var id = req.param("id");

    data = req.body;
    Smtp.updateOne({ id: id }, data).then((updatedSmtp) => {
      return res.status(200).json({
        success: true,
        code: 200,
        data: updatedSmtp,
        message: "SMTP updated successfully.",
      });
    });
  },
  /**
   *
   * @param {*} req
   * @param {*} res
   * @createdAt 01/09/2021
   * @createdBy : Amit Kumar
   */
  testSMTP: (req, res) => {
    data = req.body;
    transport = nodemailer.createTransport(
      smtpTransport({
        host: data.host,
        port: data.port,
        debug: true,
        sendmail: true,
        requiresAuth: true,
        auth: {
          user: data.user,
          pass: data.pass,
        },
        tls: {
          rejectUnauthorized: false,
        },
      })
    );

    var myVar;

    myVar = setTimeout(() => {
      return res.status(400).json({
        success: false,
        error: { code: 400, message: "SMTP credentials are not valid." },
      });
    }, 10000);

    transport.sendMail(
      {
        from: "EmployeeTimeSheet  <" + data.user + ">",
        to: "test@yopmail.com",
        subject: "SMTP TESTING",
        html: "This is a test messege for SMTP check.SMTP credentials working fine.",
      },
      function (err, info) {
        clearTimeout(myVar);

        if (err) {
          return res.status(400).json({
            success: false,
            error: { code: 400, message: "" + err },
          });
        } else {
          return res.status(200).json({
            success: true,
            code: 200,
            message: "SMTP working successfully.",
          });
        }
      }
    );
  },

  /**
   * @description : Used to send Email
   * @createdAt 01/09/2021
   * @createdBy : Amit Kumar
   */
  sendEmail: (to, subject, message, next) => {
    Smtp.find({}).then((smtp) => {
      if (smtp.length > 0) {
        transport = nodemailer.createTransport(
          smtpTransport({
            host: smtp[0].host,
            port: smtp[0].port,
            debug: true,
            sendmail: true,
            requiresAuth: true,
            auth: {
              user: smtp[0].user,
              pass: smtp[0].pass,
            },
            tls: {
              rejectUnauthorized: false,
            },
          })
        );
        transport.sendMail(
          {
            from: "JC Software Solution <" + smtp[0].user + ">",
            to: to,
            subject: subject,
            html: message
          },
          function (err, info) {
            console.log("err", err, info);
          }
        );
      }
    });
  },

  /**
  * @description : To use send checkin & checkout mail
  * @createdAt 29/09/2021
  * @createdBy : Vibhay
  */
  sendActivityMail: (subject, message) => {
    const toMailList = ['amit@yopmail.com', 'vibhayadmin@yopmail.com'];
    const ccEmail = 'vibhayfront@yopmail.com';
    const transport = nodemailer.createTransport(smtpTransport({
      host: sails.config.appSMTP.host,
      port: sails.config.appSMTP.port,
      debug: sails.config.appSMTP.debug,
      auth: {
        user: sails.config.appSMTP.auth.user, //access using /congig/appSMTP.js
        pass: sails.config.appSMTP.auth.pass
      }
    }));
    transport.sendMail(
      {
        from: "Activity <" + sails.config.appSMTP.auth.user + ">",
        subject: subject,
        html: message,
        cc: ccEmail,
        to: toMailList
      },
      function (err, info) {
        console.log("err", err, info);
      }
    );
  },

  /**
   * @description : Used to send Leave Email to management
   * @createdAt 20/09/2021
   * @createdBy : chandra Shekhar
   */
  sendLeaveEmail: (fromEmail, to, subject, message, userName, attachment, next) => {
    Smtp.find({}).then((smtp) => {
      if (smtp.length > 0) {
        transport = nodemailer.createTransport(
          smtpTransport({
            host: smtp[0].host,
            port: smtp[0].port,
            debug: true,
            sendmail: true,
            requiresAuth: true,
            auth: {
              user: smtp[0].user,
              pass: smtp[0].pass,
            },
            tls: {
              rejectUnauthorized: false,
            },
          })
        );
        if (attachment) {
          const attachmentArray = attachment.map((attachments) => {
            console.log(`${credentials.BACK_WEB_URL}` + "/images/users/" + `${attachments}`, "================path")
            return {
              filename: `${attachments}`,
              path: `${credentials.BACK_WEB_URL}` + "/images/users/" + `${attachments}`,
              cid: 'uniq-' + `${attachments}`
            };
          });
          transport.sendMail(
            {
              from: "Employee " + userName + " <" + fromEmail + ">",
              cc: to,
              subject: subject,
              html: message,
              attachments: attachmentArray,
            },
            function (err, info) {
              console.log("err", err, info);
            }
          );
        } else {
          transport.sendMail(
            {
              from: "Employee " + userName + " <" + fromEmail + ">",
              cc: to,
              subject: subject,
              html: message,
            },
            function (err, info) {
              console.log("err", err, info);
            }
          );
        }
      }
    });









    // transport.sendMail(
    //   {
    //     from: "Employee " + userName + " <" + fromEmail + ">",
    //     cc: to,
    //     subject: subject,
    //     html: message,
    //     attachments: [
    //       {
    //         filename: `${attachment}`,
    //         path: `${credentials.BACK_WEB_URL}`+ "/images/" + `${attachment}`,
    //         cid: 'uniq-' + `${attachment}`
    //       }
    //   ]
    //   //   attachments: [
    //   //     {
    //   //       filename: 'e0f55bb5-7508-4ff2-874e-3d8fecf0b74a.jpeg',
    //   //       path: '/home/jc23/Desktop/In House/vp/assets/images/e0f55bb5-7508-4ff2-874e-3d8fecf0b74a.jpeg',
    //   //       cid: 'uniq-e0f55bb5-7508-4ff2-874e-3d8fecf0b74a.jpeg'
    //   //     }
    //   // ]
    //   },
    //   function (err, info) {
    //     console.log("err", err, info);
    //   }
    // );
  },
  //}
  //   );
  // },

  /**
   * @description : Used to send Resignation Email to management
   * @createdAt 01/11/2021
   * @createdBy : Rohit kumar
   */
  sendResignationEmail: (fromEmail, to, subject, message, userName, next) => {
    Smtp.find({}).then((smtp) => {
      if (smtp.length > 0) {
        transport = nodemailer.createTransport(
          smtpTransport({
            host: smtp[0].host,
            port: smtp[0].port,
            debug: true,
            sendmail: true,
            requiresAuth: true,
            auth: {
              user: smtp[0].user,
              pass: smtp[0].pass,
            },
            tls: {
              rejectUnauthorized: false,
            },
          })
        );

        transport.sendMail(
          {
            from: "Employee " + userName + " <" + fromEmail + ">",
            cc: to,
            subject: subject,
            html: message,
          },
          function (err, info) {
            console.log("err", err, info);
          }
        );
      }
    });
  },

  /**
   * @description : Used to send Leave Email to management
   * @createdAt 20/09/2021
   * @createdBy : chandra Shekhar
   */
  sendProjectAssignEmail: (to, subject, message, next) => {
    Smtp.find({}).then((smtp) => {
      if (smtp.length > 0) {
        transport = nodemailer.createTransport(
          smtpTransport({
            host: smtp[0].host,
            port: smtp[0].port,
            debug: true,
            sendmail: true,
            requiresAuth: true,
            auth: {
              user: smtp[0].user,
              pass: smtp[0].pass,
            },
            tls: {
              rejectUnauthorized: false,
            },
          })
        );

        transport.sendMail(
          {
            from: "Team Members  <" + smtp[0].user + ">",
            cc: to,
            subject: subject,
            html: message,
          },
          function (err, info) {
            console.log("err", err, info);
          }
        );
      }
    });
  },
  /**
   * @description : Used to send Leave Email to management and Employee
   * @createdAt 05/10/2021
   * @createdBy : Rohit kumar
   */
  sendLeaveEmailOnChange: (to, subject, message, next) => {
    Smtp.find({}).then((smtp) => {
      if (smtp.length > 0) {
        transport = nodemailer.createTransport(
          smtpTransport({
            host: smtp[0].host,
            port: smtp[0].port,
            debug: true,
            sendmail: true,
            requiresAuth: true,
            auth: {
              user: smtp[0].user,
              pass: smtp[0].pass,
            },
            tls: {
              rejectUnauthorized: false,
            },
          })
        );

        transport.sendMail(
          {
            from: "Admin  <" + smtp[0].user + ">",
            cc: to,
            subject: subject,
            html: message,
          },
          function (err, info) {
            console.log("err", err, info);
          }
        );
      }
    });
  },
  /**
   * @description : Used to send Leave Email to management and Employee
   * @createdAt 01/11/2021
   * @createdBy : Rohit kumar
   */
  sendResignationEmailOnChange: (to, subject, message, next) => {
    Smtp.find({}).then((smtp) => {
      if (smtp.length > 0) {
        transport = nodemailer.createTransport(
          smtpTransport({
            host: smtp[0].host,
            port: smtp[0].port,
            debug: true,
            sendmail: true,
            requiresAuth: true,
            auth: {
              user: smtp[0].user,
              pass: smtp[0].pass,
            },
            tls: {
              rejectUnauthorized: false,
            },
          })
        );

        transport.sendMail(
          {
            from: "Admin  <" + smtp[0].user + ">",
            cc: to,
            subject: subject,
            html: message,
          },
          function (err, info) {
            console.log("err", err, info);
          }
        );
      }
    });
  },

  /**
   * @description : Used to send Leave Email to management and Employee
   * @createdAt 01/11/2021
   * @createdBy : Rohit kumar
   */
  sendResignationEmailOnChangeemployee: (to, subject, message, next) => {
    Smtp.find({}).then((smtp) => {
      if (smtp.length > 0) {
        transport = nodemailer.createTransport(
          smtpTransport({
            host: smtp[0].host,
            port: smtp[0].port,
            debug: true,
            sendmail: true,
            requiresAuth: true,
            auth: {
              user: smtp[0].user,
              pass: smtp[0].pass,
            },
            tls: {
              rejectUnauthorized: false,
            },
          })
        );

        transport.sendMail(
          {
            from: "Employee  <" + smtp[0].user + ">",
            cc: to,
            subject: subject,
            html: message,
          },
          function (err, info) {
            console.log("err", err, info);
          }
        );
      }
    });
  },

  /**
   * @description : Used to send weekly Email to employee
   * @createdAt 26/10/2021
   * @createdBy : Rohit kumar
   */
  sendWeeklyPlanEmail: (to, subject, message, next) => {
    Smtp.find({}).then((smtp) => {
      if (smtp.length > 0) {
        transport = nodemailer.createTransport(
          smtpTransport({
            host: smtp[0].host,
            port: smtp[0].port,
            debug: true,
            sendmail: true,
            requiresAuth: true,
            auth: {
              user: smtp[0].user,
              pass: smtp[0].pass,
            },
            tls: {
              rejectUnauthorized: false,
            },
          })
        );

        transport.sendMail(
          {
            from: "Admin   <" + smtp[0].user + ">",
            cc: to,
            subject: subject,
            html: message,
          },
          function (err, info) {
            console.log("err", err, info);
          }
        );
      }
    });
  },

  sendChangeStatusEmail: async (senddata, message) => {
    var transporter = nodemailer.createTransport({
      host: "smtp.gmail.com",
      port: 587,
      secure: false,
      requireTLS: true,
      service: 'gmail',
      auth: {
        user: 'amitkjcsoftwaresolution@gmail.com',
        pass: 'fdvjrupmierdlzbt'
      }
    });

    var mailOptions = {
      from: 'amitkjcsoftwaresolution@gmail.com',
      to: 'shivani@yopmail.com',
      subject: 'abc',
      text: senddata.text,
      html: message
    };

    transporter.sendMail(mailOptions, function (err, info) {
      if (err) {
      } else {
        console.log('Email sent: ' + info.response);
      }
    });

    // return res.status(200).json({
    //   sucess: true,
    // });
  },

  /**
 * @description : Used to send Email to admin for System Complaint
 * @createdAt 26/06/2023
 * @createdBy : Shivani Chauhan
 */
  sendComplaintEmail: (to, subject, message, next) => {
    Smtp.find({}).then((smtp) => {
      if (smtp.length > 0) {
        transport = nodemailer.createTransport(
          smtpTransport({
            host: smtp[0].host,
            port: smtp[0].port,
            debug: true,
            sendmail: true,
            requiresAuth: true,
            auth: {
              user: smtp[0].user,
              pass: smtp[0].pass,
            },
            tls: {
              rejectUnauthorized: false,
            },
          })
        );

        transport.sendMail(
          {
            from: "Employee  <" + smtp[0].user + ">",
            cc: to,
            subject: subject,
            html: message,
          },
          function (err, info) {
            console.log("err", err, info);
          }
        );
      }
    });
  },


  /**
* @description : Used to send Email to employee by admin when system problem is solved 
* @createdAt 26/06/2023
* @createdBy : Shivani Chauhan
*/

  sendComplaintEmailResponse: (to, subject, message, next) => {
    Smtp.find({}).then((smtp) => {
      if (smtp.length > 0) {
        transport = nodemailer.createTransport(
          smtpTransport({
            host: smtp[0].host,
            port: smtp[0].port,
            debug: true,
            sendmail: true,
            requiresAuth: true,
            auth: {
              user: smtp[0].user,
              pass: smtp[0].pass,
            },
            tls: {
              rejectUnauthorized: false,
            },
          })
        );

        transport.sendMail(
          {
            from: "Admin  <" + smtp[0].user + ">",
            cc: to,
            subject: subject,
            html: message,
          },
          function (err, info) {
            console.log("err", err, info);
          }
        );
      }
    });
  },

  /**
* @description : Used to send Email to TL for taking interview with resume attachment.
* @createdAt 28/06/2023
* @createdBy : Shivani Chauhan
*/
  sendInvitationEmail: (to, subject, message, resume, next) => {
    Smtp.find({}).then((smtp) => {
      if (smtp.length > 0) {
        transport = nodemailer.createTransport(
          smtpTransport({
            host: smtp[0].host,
            port: smtp[0].port,
            debug: true,
            sendmail: true,
            requiresAuth: true,
            auth: {
              user: smtp[0].user,
              pass: smtp[0].pass,
            },
            tls: {
              rejectUnauthorized: false,
            },
          })
        );
        transport.sendMail(
          {
            from: "JC Software Solution <" + smtp[0].user + ">",
            to: to,
            subject: subject,
            html: message,
            attachments: [{
              path: resume
            },]
          },
          function (err, info) {
            console.log("err", err, info);
          }
        );
      }
    });
  },

  sendPostUploadEmail: (to, subject, message, resume, next) => {
    Smtp.find({}).then((smtp) => {
      if (smtp.length > 0) {
        transport = nodemailer.createTransport(
          smtpTransport({
            host: smtp[0].host,
            port: smtp[0].port,
            debug: true,
            sendmail: true,
            requiresAuth: true,
            auth: {
              user: smtp[0].user,
              pass: smtp[0].pass,
            },
            tls: {
              rejectUnauthorized: false,
            },
          })
        );
        transport.sendMail(
          {
            from: "JC Software Solution <" + smtp[0].user + ">",
            to: to,
            subject: subject,
            html: message
          },
          function (err, info) {
            console.log("err", err, info);
          }
        );
      }
    });
  },
};
