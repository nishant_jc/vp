/**
 * HolidaysController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

var ObjectId = require("mongodb").ObjectID;
var constantObj = sails.config.constants;
const db = sails.getDatastore().manager;

module.exports = {

    addHoliday: async (req, res) => {

        try {

            const data = req.body;

            if (!data.event) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Holidays.EVENT_REQUIRED },
                });
            }

            if (!data.start_date) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Holidays.START_DATE_REQUIRED },
                });
            }

            if (!data.end_date) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Holidays.END_DATE_REQUIRED },
                });
            }

            data.addedBy = req.identity.id;

            const holiday_data = await Holidays.create(data).fetch();

            if (holiday_data) {
                return res.status(200).json({
                    success: true,
                    message: constantObj.Holidays.ADD_SUCCESS,
                    data: holiday_data,
                });
            } else {
                return res.status(400).json({
                    success: false,
                    message: constantObj.Holidays.ADD_FAILED
                });
            }

        } catch (error) {
            return res.status(400).json({
                success: false,
                error: { code: 400, message: error },
            });
        }

    },
    getSingleHoliday: async (req, res) => {

        try {
            const id = req.param("id");

            if (!id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Holidays.ID_REQUIRED },
                });

            }

            const holiday_details = await Holidays.findOne({ id: id, isDeleted:false});

            if (holiday_details) {
                return res.status(200).json({
                    success: true,
                    message: constantObj.Holidays.FOUND,
                    data: holiday_details,
                });
            } else {
                return res.status(400).json({
                    success: false,
                    message: constantObj.Holidays.FAILED
                });
            }


        } catch (error) {
            return res.status(400).json({
                success: false,
                error: { code: 400, message: error },
            });
        }

    },

    Holidaylist: async (req, res) => {

        try {
            var search = req.param('search');
            var isDeleted = req.param('isDeleted');
            var page = req.param('page');
            var count = parseInt(req.param('count'));
            let sortBy = req.param("sortBy");
            let addedBy = req.param('addedBy');

            var date = new Date();
            var current_date = date.toISOString().substring(0, 10);

            var query = {};

            query.start_date = { '$gte': `${current_date}` };

            // query.$and = [
            //     {
            //     start_date: { '$gte': `${current_date}` },
            //     end_date: { '$lte': "2023-10-31" }
            //     }]
            if (search) {
                query.$or = [
                    { event: { $regex: search, '$options': 'i' } },
                ]
            }
            let sortquery = {};
            // if (sortBy) {
            //     let typeArr = [];
            //     typeArr = sortBy.split(" ");
            //     let sortType = typeArr[1];
            //     let field = typeArr[0];
            //     sortquery[field ? field : 'updatedAt'] = sortType ? (sortType == 'desc' ? -1 : 1) : -1;
            // } else {
            sortquery = { start_date: 1 }
            //}

            // if (isDeleted) {
            //     if (isDeleted === 'true') {
            //         isDeleted = true;
            //     } else {
            //         isDeleted = false;
            //     }
            query.isDeleted = false;
            // } else {
            //     query.isDeleted = false;
            // }
            if (addedBy) {
                query.addedBy_id = ObjectId(addedBy);
            }

            const pipeline = [
                {
                    $project: {
                        event: "$event",
                        start_date: "$start_date",
                        end_date: "$end_date",
                        description: "$description",
                        isDeleted: "$isDeleted",
                        addedBy: "$addedBy",
                        updatedBy: "$updatedBy",
                        createdAt: "$createdAt",
                        updatedAt: "$updatedAt",
                    }
                },
                {
                    $match: query
                },
                {
                    $sort: sortquery
                },
            ]
            db.collection('holidays').aggregate([...pipeline]).toArray((err, totalResult) => {
                if (page && count) {
                    var skipNo = (page - 1) * count;
                    pipeline.push(
                        {
                            $skip: Number(skipNo)
                        },
                        {
                            $limit: Number(count)
                        })
                }
                db.collection('holidays').aggregate([...pipeline]).toArray((err, result) => {
                    return res.status(200).json({
                        "success": true,
                        "data": result,
                        "total": totalResult.length,
                    });
                })
            })
        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { code: 400, message: "" + err }
            })
        }
    },
    updateHoliday: async (req, res) => {

        try {
            const data = req.body;

            if (!data.id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Holidays.ID_REQUIRED },
                });

            }

            const find_holiday = await Holidays.findOne({ id: data.id, isDeleted: false });
            if (!find_holiday) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Holidays.ID_NOT_FOUND },
                });
            }

            data.updatedBy = req.identity.id;

            const updated_data = await Holidays.updateOne({ id: data.id, isDeleted: false }).set(data);

            if (updated_data) {
                return res.status(200).json({
                    success: true,
                    message: constantObj.Holidays.UPDATE_SUCCESS,
                    data: updated_data,
                });

            } else {
                return res.status(400).json({
                    success: false,
                    message: constantObj.Holidays.UPDATE_FAILED
                });
            }


        } catch (error) {
            return res.status(400).json({
                success: false,
                error: { code: 400, message: error },
            });
        }

    },
    deleteHoliday: async (req, res) => {

        try {
            const id = req.param("id");
            var deleted_by = req.identity.id;

            if (!id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Holidays.ID_REQUIRED },
                });
            }

            const holiday_data = await Holidays.findOne({ id: id, isDeleted: false });

            if (!holiday_data) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Holidays.ID_NOT_FOUND },
                });
            }

            var deleted_at = new Date();

            const delete_holiday = await Holidays.updateOne({ id: id, isDeleted: false }).set({ isDeleted: true,deletedBy : deleted_by,deletedAt : deleted_at });
            if (delete_holiday) {
                return res.status(200).json({
                    success: true,
                    message: constantObj.Holidays.DELETE_SUCCESS
                });
            } else {
                return res.status(400).json({
                    success: false,
                    message: constantObj.Holidays.DELETE_FAILED
                });
            }


        } catch (error) {
            return res.status(400).json({
                success: false,
                error: { code: 400, message: error },
            });
        }
    }


};

