/**
 * RolesController
 *
 * @description :: Server-side logic for managing Roles
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 const db = sails.getDatastore().manager;
 var constantObj = sails.config.constants;
module.exports = {

  /**
   *
   * @param {*} data
   * @param {*} context
   * @param {*} req
   * @param {*} res
   * @returns
   * @description : Api use to add Roles
   * @createdAt : 13/09/2021
   * @createdBy : Chandra Shekhar
   */

  addRoles: async (req, res) => {
    if (!req.body.name || typeof req.body.name == undefined) {
      return res.status(404).json({
        success: false,
        error: { code: 404, message: constantObj.Roles.ROLE_REQUIRED },
      });
    }

    var date = new Date();
    try {
      var roleName = req.body.name.toLowerCase();
       var roleCheck = await Roles.findOne({
         name: roleName,
         isDeleted: false,
       });
      if (roleCheck) {
        return res.status(404).json({
          success: false,
          error: {
            code: 400,
            message: constantObj.Roles.ROLE_ALREADY_EXIST,
          },
        });
      } else {
        data = req.body;

        var newRole = await Roles.create(data).fetch();
        if (newRole) {
          res.status(200).json({
            success: true,
            code: 200,
            message: constantObj.Roles.ROLES_SUCCESSFULLY_CREATED,
          });
        }
      }
    } catch (err) {
      return res
        .status(400)
        .json({ success: true, error: { code: 400, error: "" + err } });
    }
  },

  /**
   *
   * @param {*} req
   * @param {*} res
   * @returns
   * @description : Api use to edit Role
   * @createdAt : 13/09/2021
   * @createdBy : Chandra Shekhar
   */

  edit: (req, res) => {
    API(RolesService.updateRoles, req, res);
  },
  /**
   *
   * @param {*} req
   * @param {*} res
   * @description : Listing of roles
   * @createdAt :01/09/2021
   * @createdBy : Amit Kumar
   */
  getAllRoles: (req, res) => {
    var search = req.param("search");
    var sortBy = req.param("sortBy");
    var page = req.param("page");
    var count = req.param("count");

    if (page == undefined) {
      page = 1;
    }
    if (count == undefined) {
      count = 10;
    }
    var skipNo = (page - 1) * count;
    var query = {};

    if (sortBy) {
      sortBy = sortBy.toString();
    } else {
      sortBy = "createdAt desc";
    }

    if (search) {
      query.$or = [{ name: { $regex: search, $options: "i" } }];
    }

    query.isDeleted = false;
    db.collection("roles")
      .aggregate([
        {
          $project: {
            id: "$_id",
            name: "$name",
            isEditable: "$isEditable",
            permission: "$permission",
            status: "$status",
            personal_details: "$personal_details",
            deletedBy: "$deletedBy",
            contact_information: "$contact_information",
            addedBy: "$addedBy",
            updatedBy: "$updatedBy",
            isDeleted: "$isDeleted",
            deletedAt: "$deletedAt",
            updatedAt: "$updatedAt",
          },
        },
        {
          $match: query,
        },
      ])
      .toArray((err, totalResult) => {
        db.collection("roles")
          .aggregate([
            {
              $project: {
                id: "$_id",
                name: "$name",
                isEditable: "$isEditable",
                permission: "$permission",
                status: "$status",
                personal_details: "$personal_details",
                deletedBy: "$deletedBy",
                contact_information: "$contact_information",
                addedBy: "$addedBy",
                updatedBy: "$updatedBy",
                isDeleted: "$isDeleted",
                deletedAt: "$deletedAt",
                updatedAt: "$updatedAt",
              },
            },
            {
              $match: query,
            },

            {
              $skip: skipNo,
            },
            {
              $limit: Number(count),
            },
          ])
          .toArray((err, result) => {
            if (err) {
              return res.status(400).json({
                success: false,
                error: { message: "" + err },
              });
            } else {
              return res.status(200).json({
                success: true,
                code: 200,
                data: result,
                total: totalResult.length,
              });
            }
          });
      });
  },

  /**
   *
   * @param {*} req {id:""}
   * @param {*} res
   *
   */
  getRole: (req, res) => {
    var id = req.param("id");
    if (!id || typeof id == undefined) {
      return res.status(404).json({
        success: false,
        error: { code: 404, message: constantObj.Roles.ID_REQUIRED },
      });
    }
    Roles.findOne({ id: id })
      .then((role) => {
        return res.status(200).json({
          success: true,
          code: 200,
          data: role,
        });
      })
      .catch((err) => {
        return res
          .status(400)
          .json({ success: false, error: { code: 400, message: " " + err } });
      });
  },

  /**
   *
   * @param {*} req {"role":""}
   * @param {*} res
   * @description Used to get the permissions of particular role
   */
  permissionOfRole: (req, res) => {
    var role = req.param("role");

    Roles.findOne({ name: role }).then((roleDetail) => {
      if (!roleDetail) {
        return res.status(404).json({
          success: false,
          error: { code: 404, message: constantObj.Roles.NOT_FOUND },
        });
      } else {
        if (roleDetail && roleDetail.permission.length > 0) {
          var permissions = {};
          roleDetail.permission.forEach((element) => {
            var name = element.name;
            permissions[name] = element.isChecked;
          });

          return res.status(200).json({
            success: true,
            code: 200,
            data: permissions,
          });
        }
      }
    });
  },
};
