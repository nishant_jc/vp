/**
 *  
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
var ObjectId = require("mongodb").ObjectID;
var constantObj = sails.config.constants;
const db = sails.getDatastore().manager;
var constant = require('../../config/local.js');
const SmtpController = require('../controllers/SmtpController');

module.exports = {

    /**
      *
      * @description: Api Used to Assign laptop
      * @createdAt : 24/05/2023
      * @createdBy :Vishal Tyagi
      */

    assignLaptop: async (req, res) => {
        const data = req.body;
        if (!data.laptop || typeof data.laptop == undefined) {
            return res.status(404).json({
                success: false,
                error: { code: 404, message: constantObj.assignLaptop.LAPTOP_REQUIRED },
            });
        }


        const already_assigned = await LaptopAssign.findOne({ laptop: data.laptop, isDeleted: false, status: "assigned" });


        const employee = await Users.findOne({ id: data.employeeId, isDeleted: false, status: "active" });
        var employee_name = employee.fullName;
        var employee_email = employee.email;

        const project = await Projects.findOne({ id: data.projectId, isDeleted: false, status: true });
        var project_name = project.name;

        if (already_assigned) {
            const user_id = already_assigned.employeeId;

            const user = await Users.findOne({ id: user_id, isDeleted: false });
            const user_name = user.fullName;
            
            return res.status(400).json({
                success: false,
                error: { code: 400, message: "This laptop is already assigned to " + `${user_name}` }
            });
        }
        try {
            const query = {};
            query.employeeId = data.employeeId;
            query.projectId = data.projectId;
            query.laptop = data.laptop;

            var existed = await LaptopAssign.findOne(query);
            if (existed) {
                return res.status(400).json({
                    success: false,
                    error: { code: 400, message: constantObj.assignLaptop.EXIST },
                });
            } else {
                data.startTime = new Date(data.startTime);
                data.endTime = new Date(data.endTime);
                //console.log(data.endTime,"============endTime")

                let assignLaptop = await LaptopAssign.create(data).fetch();
                var ending_date = assignLaptop.endTime;
                var endDate = ending_date.toLocaleString('en-GB')
                var end_date = endDate.substring(0, 10);
                if (assignLaptop) {
                    laptopAssigned({
                        email: employee_email,
                        fullname: employee_name,
                        projectname: project_name,
                        enddate: end_date
                        //laptop : data.laptop
                    });
                    return res.status(200).json({
                        success: true,
                        code: 200,
                        data: assignLaptop,
                        message: constantObj.assignLaptop.ASSIGN,
                    });
                }
            }


        } catch (err) {
            return res.status(400).json({
                success: false,
                code: 400,
                error: err
            });
        }

    },

    /**
     *
     * @description: Api Used to Assign laptop Listing
     * @createdAt : 24/05/2023
     * @createdBy :Vishal Tyagi
     */

    assignLaptopList: async (req, res) => {

        try {
            var search = req.param("search");
            var sortBy = req.param("sortBy");
            var page = req.param("page");
            var count = req.param("count");
            var start = req.param('start')
            var end = req.param('end')
            if (page == undefined) {
                page = 1;
            }
            if (count == undefined) {
                count = 1000;
            }
            var skipNo = (page - 1) * count;
            var query = {};

            if (sortBy) {
                sortBy = sortBy.toString();
            } else {
                sortBy = 1;
            }

            if (search) {
                query.$or = [{ name: { $regex: search, $options: "i" } }];
            }
            if (start && end) {
                startDate = new Date(start);
                startDate.setUTCHours(0, 0, 0, 0)
                endDate = new Date(end);
                endDate.setUTCHours(23, 59, 0, 0)
                query.startTime = { $gte: startDate, $lte: endDate }

            }

            query.isDeleted = false;

            db.collection("laptopassign")
                .aggregate([
                    {
                        $lookup: {
                            from: "users",
                            localField: "employeeId",
                            foreignField: "_id",
                            as: "employeeId",
                        },
                    },
                    {
                        $unwind: {
                            path: "$employeeId",
                            preserveNullAndEmptyArrays: true,
                        },
                    },
                    {
                        $lookup: {
                            from: "projects",
                            localField: "projectId",
                            foreignField: "_id",
                            as: "projectId",
                        },
                    },
                    {
                        $unwind: {
                            path: "$projectId",
                            preserveNullAndEmptyArrays: true,
                        },
                    },

                    {
                        $project: {
                            id: "$_id",
                            laptop: "$laptop",
                            description: "$description",
                            status: "$status",
                            projectId: "$projectId",
                            employeeId: "$employeeId",
                            startTime: "$startTime",
                            endTime: "$endTime",
                            deletedBy: "$deletedBy",
                            addedBy: "$addedBy",
                            updatedBy: "$updatedBy",
                            isDeleted: "$isDeleted",
                            updatedAt: "$updatedAt",
                        },
                    },
                    {
                        $match: query,
                    },
                ])
                .toArray((err, totalResult) => {
                    db.collection("laptopassign")
                        .aggregate([
                            {
                                $lookup: {
                                    from: "users",
                                    localField: "employeeId",
                                    foreignField: "_id",
                                    as: "employeeId",
                                },
                            },
                            {
                                $unwind: {
                                    path: "$employeeId",
                                    preserveNullAndEmptyArrays: true,
                                },
                            },
                            {
                                $lookup: {
                                    from: "projects",
                                    localField: "projectId",
                                    foreignField: "_id",
                                    as: "projectId",
                                },
                            },
                            {
                                $unwind: {
                                    path: "$projectId",
                                    preserveNullAndEmptyArrays: true,
                                },
                            },
                            {
                                $project: {
                                    id: "$_id",
                                    laptop: "$laptop",
                                    description: "$description",
                                    status: "$status",
                                    projectId: "$projectId",
                                    employeeId: "$employeeId",
                                    startTime: "$startTime",
                                    endTime: "$endTime",
                                    deletedBy: "$deletedBy",
                                    addedBy: "$addedBy",
                                    updatedBy: "$updatedBy",
                                    isDeleted: "$isDeleted",
                                    updatedAt: "$updatedAt",
                                },
                            },
                            {
                                $match: query,
                            },
                            { $sort: { laptop: sortBy } },
                            {
                                $skip: skipNo,
                            },
                            {
                                $limit: Number(count),
                            },
                        ])
                        .toArray((err, result) => {
                            if (err) {
                                return res.status(400).json({
                                    success: false,
                                    error: { message: "" + err },
                                });
                            } else {
                                return res.status(200).json({
                                    success: true,
                                    code: 200,
                                    data: result,
                                    total: totalResult.length,
                                });
                            }
                        });
                });
        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { "code": 400, "message": "" + err }
            });
        }
    },

    /**
      *
      * @description: Api Used to Edit Assignlaptop
      * @createdAt : 24/05/2023
      * @createdBy :Vishal Tyagi
      */

    editAssignLaptop: async (req, res) => {
        try {
            var data = req.body;
            const id = data.id;
            data.updatedBy = req.identity.id

            var date = new Date();

            if (!id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.assignLaptop.ID_MISSING },
                });
            } else {
                data.updatedBy = req.identity.id
                var find_Assign_laptop = await LaptopAssign.findOne({ id: id, isDeleted: false });

                if (!find_Assign_laptop) {
                    return res.status(404).json({
                        success: false,
                        error: { code: 404, message: constantObj.assignLaptop.NOT_FOUND },
                    });
                } else {
                    if(data.status == "accepted"){
                        data.endTime = date;
                    }
                    
                    delete data.id;
                    let update_assign_laptop = await LaptopAssign.updateOne({ id: id }, data);

                    return res.status(200).json({
                        success: true,
                        code: 200,
                        message: constantObj.assignLaptop.UPDATED,
                    });
                }
            }

        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { "code": 400, "message": "" + err }
            });
        }
    },

    /**
        *
        * @description: Api Used to Detail Assignlaptop
        * @createdAt : 24/05/2023
        * @createdBy :Vishal Tyagi
        */

    detailAssignLaptop: async (req, res) => {
        try {
            const id = req.param("id");

            if (!id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.assignLaptop.ID_MISSING },
                });
            } else {
                var find_Assign_laptop = await LaptopAssign.findOne({ id: id, isDeleted: false });
                if (!find_Assign_laptop) {
                    return res.status(404).json({
                        success: false,
                        error: { code: 404, message: constantObj.assignLaptop.NOT_FOUND },
                    });
                } else {
                    return res.status(200).json({
                        success: true,
                        code: 200,
                        data: find_Assign_laptop,
                        message: constantObj.assignLaptop.DETAIL
                    });
                }
            }

        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { "code": 400, "message": "" + err }
            });
        }
    },

    /**
        *
        * @description: Api Used to remove Assignlaptop
        * @createdAt : 24/05/2023
        * @createdBy :Vishal Tyagi
        */
    removeAssignLaptop: async (req, res) => {
        try {
            const id = req.param("id");
            if (!id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.assignLaptop.ID_MISSING },
                });
            } else {
                var find_Assign_laptop = await LaptopAssign.findOne({ id: id, isDeleted: false });
                if (!find_Assign_laptop) {
                    return res.status(404).json({
                        success: false,
                        error: { code: 404, message: constantObj.assignLaptop.NOT_FOUND },
                    });
                } else {

                    let removed = await LaptopAssign.updateOne({ id: id }, { isDeleted: true, deletedBy: req.identity.id });
                    return res.status(200).json({
                        success: true,
                        code: 200,
                        message: constantObj.assignLaptop.DELETED
                    });
                }
            }

        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { "code": 400, "message": "" + err }
            });
        }
    },


};

laptopAssigned = function (options) {
    var email = options.email;

    message = '';
    style = {
        header: `
        padding:30px 15px;
        text-align:center;
        background-color:#f2f2f2;
        `,
        body: `
        padding:15px;
        height: 230px;
        `,
        hTitle: `font-family: 'Raleway', sans-serif;
        font-size: 37px;
        height:auto;
        line-height: normal;
        font-weight: bold;
        background:none;
        padding:0;
        color:#333;
        `,
        maindiv: `
        width:600px;
        margin:auto;
        font-family: Lato, sans-serif;
        font-size: 14px;
        color: #333;
        line-height: 24px;
        font-weight: 300;
        border: 1px solid #eaeaea;
        `,
        textPrimary: `color:#3e3a6e;
        `,
        h5: `font-family: Raleway, sans-serif;
        font-size: 22px;
        background:none;
        padding:0;
        color:#333;
        height:auto;
        font-weight: bold;
        line-height:normal;
        `,
        m0: `margin:0;`,
        mb3: 'margin-bottom:15px;',
        textCenter: `text-align:center;`,
        btn: `padding:10px 30px;
        font-weight:500;
        font-size:14px;
        line-height:normal;
        border:0;
        display:inline-block;
        text-decoration:none;
        `,
        btnPrimary: `
        background-color:#3e3a6e;
        color:#fff;
        `,
        footer: `
        padding:10px 15px;
        font-weight:500;
        color:#fff;
        text-align:center;
        background-color:#000;
        `
    }

    message += `<div class="container" style="` + style.maindiv + `">
    <div class="header" style="`+ style.header + `text-align:center">
        <img src="${constant.LOGO}/assets/img/logoImg.png" width="150" style="margin-bottom:20px;" />
    </div>
    <div class="body" style="`+ style.body + `">
        <h5 style="`+ style.h5 + style.m0 + style.mb3 + style.textCenter + `">Hello ` + options.fullname + `</h5>
        <p style="`+ style.m0 + style.mb3 + `margin-bottom:20px;font-weight:200 ">We would like to inform you that the management has decided to assign you with a laptop for the completion of ` + options.projectname + ` project till <b>` + options.enddate + `</b>.<br>
        Also,we would like to share with you that you will be responsible for taking care of the laptop.In case of loss or damage,you will be responsible for the replacement.After completetion of the assigned task you will be required to return the assigned laptop to the HR Department.</p>
    </div>
    <div class="footer" style="`+ style.footer + `">
    &copy 2023 JC Software Solution All rights reserved.
    </div>
  </div>`



    SmtpController.sendEmail(email, 'Laptop Assigned', message)
};
