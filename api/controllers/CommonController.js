/**
 * CommonController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 * @description To common opertion define under this file
 * @author  Vibhay
 * @Date 03/09/2021
 */
var fs = require('fs');

module.exports = {
  statusUpdate: (req, res) => {
    API(CommonService.statusUpdate, req, res)
  },
  toDelete: (req, res) => {
    API(CommonService.delete, req, res)
  },



  /**
  *
  * @param {*} res
  * @returns
  * @description: Api Used to get admin cart counts
  * @createdAt : 19/10/2021
  * @createdBy Rohit kumar
  */

  getgraphcount: async (req, res) => {
    try {
      let projects = await Projects.count({ "isDeleted": false });
      let employees = await Users.count({ role: ["developer", "qa", "bde", "seo", "HR", "designer"], isDeleted: false });
      let resignation = await Resignation.count({ "isDeleted": false });
      let appraisal = await Appraisal.count({ status: 'Pending', isDeleted: false });

      let result = {
        projects: projects,
        employees: employees,
        resignation: resignation,
        appraisal: appraisal
      }
      return res.status(200).json({
        success: true,
        "message": "data fetch successfully.",
        data: result,
      });
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { code: 400, message: error },
      });
    }
  },


  multipleDocument: async (req, res) => {
    try {
      //    console.log("In Upload multipleImages");
      // var modelName = 'users';
      var modelName = req.param("modelName");
      if (!modelName || typeof modelName == undefined) {
        return res.status(404).json({
          success: false,
          error: { code: 404, message: "Please Add Model Name" },
        });
      }
      // define folders path
      var rootpath = process.cwd();
      var fullpath = rootpath + "/assets/images/" + modelName;
      var fullpaththumbnail =
        rootpath + "/assets/images/" + modelName + "/thumbnail";
      var fullpath200 =
        rootpath + "/assets/images/" + modelName + "/thumbnail/200";
      var fullpath300 =
        rootpath + "/assets/images/" + modelName + "/thumbnail/300";
      var fullpath500 =
        rootpath + "/assets/images/" + modelName + "/thumbnail/500";

      //Check image upload folder is exists or not. If not create all folders
      if (fs.existsSync(fullpath) == false) {
        fs.mkdirSync(fullpath);
        fs.mkdirSync(fullpaththumbnail);
        fs.mkdirSync(fullpath200);
        fs.mkdirSync(fullpath300);
        fs.mkdirSync(fullpath500);
      }

      
      req
        .file("data")
        .upload(
          { maxBytes: 1048576000, dirname: "../../assets/images" },
          async (err, file) => {
            if (err) {
              if (err.code == "E_EXCEEDS_UPLOAD_LIMIT") {
                return res.status(404).json({
                  success: false,
                  error: {
                    code: 404,
                    message: "Please Select document Below 100Mb",
                  },
                });
              }
            }

            if (file.length > 20) {
              return res.status(404).json({
                success: false,
                error: {
                  code: 404,
                  message: "Please upload maximum 20 document.",
                },
              });
            }

            let fullpath = [];
            let resImagePath = [];
            file.forEach(async (element, index) => {
              var name = generateName();
              // console.log(element.fd)
              typeArr = element.type.split("/");
              fileExt = typeArr[1];

              fs.readFile(file[index].fd, async (err, data) => {
                if (err) {
                  return res.status(403).json({
                    success: false,
                    error: { code: 403, message: err },
                  });
                } else {
                  if (data) {
                    var path = file[index].fd;
                    fs.writeFile(
                      ".tmp/public/images/" +
                      modelName +
                      "/" +
                      name +
                      "." +
                      fileExt,
                      data,
                      function (err, image) {
                        if (err) {
                          // console.log(err,"=============errrr");
                          //  res.status(400).json({
                          //   success: false,
                          //   error: { code: 400, message: err },
                          // });
                        }
                      }
                    );
                    fs.writeFile(
                      "assets/images/" + modelName + "/" + name + "." + fileExt,
                      data,
                      function (err, image) {
                        if (err) {
                          console.log(err);
                          return res.status(400).json({
                            success: false,
                            error: { code: 400, message: err },
                          });
                        }
                      }
                    );

                    fullpath.push(name + "." + fileExt);
                    resImagePath.push(
                      "assets/images/" + modelName + "/" + name + "." + fileExt
                    );

                    await new Promise((resolve) => setTimeout(resolve, 3000));

                    if (index == file.length - 1) {
                      await new Promise((resolve) => setTimeout(resolve, 2000));
                      return res.json({
                        success: true,
                        code: 200,
                        data: {
                          fullPath: resImagePath,
                          imagePath: fullpath,
                        },
                      });
                    }
                  }
                }
              }); //end of loop

            });
          }
        );
    } catch (err) {
      console.log(err);
      return res
        .status(500)
        .json({ success: false, error: { code: 500, message: "" + err } });
    }
  },
};

