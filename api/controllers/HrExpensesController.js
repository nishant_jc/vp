/**
 * HrExpensesController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

var constantObj = sails.config.constants;
var async = require("async");
module.exports = {
  /**
   *
   * @param {*} req
   * @param {*} res
   * @description: Used to add HrExpenses
   * @createdAt 13/12/2021
   * @createdBy : Rohit kumar
   */

  addHrExpenses: (req, res) => {
    API(HrExpensesService.saveHrExpenses, req, res);
  },

  /**
   *
   * @param {*} req
   * @param {*} res
   * @description: Used to edit HrExpenses
   * @createdAt 13/12/2021
   * @createdBy : Rohit kumar
   */

  edit: (req, res) => {
    API(HrExpensesService.updateHrExpenses, req, res);
  },


  /**
 *
 * @param {*} req
 * @param {*} res
 * @description: Used to get HrExpenses by date
 * @createdAt 13/12/2021
 * @createdBy : Rohit kumar
 */
  getAllHrExpenses: async (req, res) => {

    var date = new Date();
    var current_date = date.toISOString().substring(0, 10);
    var current_year = date.toISOString().substring(0, 4);
    var current_month = date.toISOString().substring(5, 7);

    var starting_date = `${current_year}` + "-" + `${current_month}` + "-01";
    var ending_date = current_date;

    var start_date = req.param('start_date') ? req.param('start_date') : starting_date;
    var end_date = req.param('end_date') ? req.param('end_date') : ending_date;
    // var date = req.param("date");
    // var utc = new Date().toJSON().slice(0,10).replace(/-/g,'-');
    // var currentDate = "";
    // if(date){
    // currentDate = date;
    // }else{
    // currentDate = utc;
    // }
    // let HrExpensesdata = [];
    let query = {};
    query.isDeleted = false;

    if (start_date != "" && end_date != "") {
      query.date = {
        '>=': start_date,
        '<=': end_date
      };

    }

    HrExpenses.find(query).sort("date DESC").exec(async (err, totalResult) => {
      // console.log(totalResult, "=================totalResult")
      // async.each(totalResult, async function (HrExpenses, callback) {

      //   console.log(HrExpenses,"===================HrExpenses");
      //     var expensesdate=HrExpenses.date;
      //     console.log(expensesdate,"=====================expensesdate");
      //     if (expensesdate == currentDate) {
      //         HrExpensesdata.push(HrExpenses);
      //     }
      //     // else{
      //     //   HrExpensesdata.push(HrExpenses);
      //     // }

      // });
      // console.log(HrExpensesdata, "-----------------------------HrExpensesdata");
      // if (HrExpensesdata && !HrExpensesdata.length) {
      // return res.status(200).json({
      //     success: true,
      //     message: constantObj.HrExpenses.NO_RESULT,
      // });
      // } else {
      return res.status(200).json({
        success: true,
        message: constantObj.HrExpenses.GET_DATA,
        total: totalResult.length,
        data: totalResult,

      });
      // }

    })
  },


  /**
   *
   * @param {id} req
   * @param {*} res
   * @returns
   * @description: Api Used to get single HrExpenses by ID
   * @createdAt : 13/12/2021
   * @createdBy Rohit kumar
   */

  getSingleHrExpenses: async (req, res) => {
    try {
      let HrExpensesID = req.param("id");
      if (HrExpensesID == null) {
        return res.status(404).json({
          success: false,
          message: constantObj.HrExpenses.ID_REQUIRED,
        });
      } else {
        var HrExpensesData = await HrExpenses.find({ id: HrExpensesID, isDeleted: false }).populate('employee_id').limit(1);
        if (HrExpensesData) {
          return res.status(200).json({
            success: true,
            message: constantObj.HrExpenses.GET_DATA,
            data: HrExpensesData,
          });
        } else {
          return res.status(404).json({
            success: false,
            message: constantObj.HrExpenses.NO_RESULT,
          });
        }
      }
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },


  /**
   *
   * @param {id} req
   * @param {*} res
   * @returns
   * @description: Api Used to delete HrExpenses by ID
   * @createdAt : 13/12/2021
   * @createdBy Rohit kumar
   */

  HrExpensesDeletedByID: async (req, res) => {
    try {
      let deletedID = req.param("id");
      if (deletedID == undefined) {
        return res.status(404).json({
          success: false,
          message: constantObj.DailyTask.ID_REQUIRED,
        });
      }
      var deletedByUser = req.identity.id;
      var deletedData = await HrExpenses.updateOne({
        id: deletedID,
      }).set({ isDeleted: true, deletedBy: deletedByUser });

      if (deletedData) {
        return res.status(200).json({
          success: true,
          message: constantObj.HrExpenses.DELETED_EXPENSES,
        });
      }

    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  }
};

