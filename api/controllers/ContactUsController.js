/**
 * ContactUsController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

var constantObj = sails.config.constants;
module.exports = {
  /**
   *
   * @param {*} req
   * @param {*} res
   * @description: Used to add Content
   * @createdAt 09/11/2021
   * @createdBy : Rohit kumar
   */

  addContactUs: (req, res) => {
    API(ContactUsService.saveContactUs, req, res);
  },

  /**
   *
   * @param {*} req
   * @param {*} res
   * @description: Used to add Content
   * @createdAt 09/11/2021
   * @createdBy : Rohit kumar
   */

  edit: (req, res) => {
    API(ContactUsService.updateContactUs, req, res);
  },

  /**
   *
   * @param {*} res
   * @returns
   * @description: Api Used to get Content
   * @createdAt : 09/11/2021
   * @createdBy Rohit kumar
   */

  getAllContactUs: function (req, res, next) {
    var search = req.param("search");
    var sortBy = req.param("sortBy");
    var page = req.param("page");
    if (page == undefined) {
      page = 1;
    }
    var count = req.param("count");
    if (count == undefined) {
      count = 100;
    }
    var skipNo = (page - 1) * count;

    var query = {};
    if (sortBy) {
      sortBy = sortBy.toString();
    } else {
      sortBy = "createdAt desc";
    }

    var searchQuery = {};
    if (search) {
      query.slug = {
        like: "%" + search.toLowerCase() + "%",
      };
      searchQuery.title = {
        like: "%" + search.toLowerCase() + "%",
      };
    }

    var isDeleted_name = req.param("isDeleted");
    if (isDeleted_name) {
      query.isDeleted = req.param("isDeleted");
      searchQuery.isDeleted = req.param("isDeleted");
    }

    ContactUs.count(query)
      .where(searchQuery)
      .exec(function (err, total) {
        if (err) {
          return res.status(400).json({
            success: false,
            error: err,
          });
        } else {
          ContactUs.find(query)
            .where(searchQuery)
            .populate("deletedBy")
            .sort(sortBy)
            .exec(function (err, ContactUs) {
              if (err) {
                return res
                  .status(400)
                  .json({ success: false, error: { code: 400, message: err } });
              } else {
                return res.json({
                  success: true,
                  code: 200,
                  data: ContactUs,
                  total: total,
                });
              }
            });
        }
      });
  },

  getContactUs: async function (req, res) {
    try {
      let id = req.query.id;
      if(!id){
        constantObj.CONTACT.ID_REQUIRED;
      }
      let contact = await ContactUs.findOne({id:id,isDeleted:false});
      if(!contact){
        return res
        .status(400)
        .json({ success: false, error: { code: 400, message: constantObj.CONTACT.NO_RESULT } });
      }else{
        return res.json({
          success: true,
          code: 200,
          data: contact,
        });
      }
    } catch (error) {
      return res
        .status(400)
        .json({ success: false, error: { code: 400, message: err } });
    }
  },
};
