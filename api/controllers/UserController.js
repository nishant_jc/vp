/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const bcrypt = require("bcrypt-nodejs");
var constantObj = sails.config.constants;
var constant = require("../../config/local.js");
var ObjectId = require("mongodb").ObjectID;
var async = require("async");
var services = require("../services/LeaveService.js")
const SmtpController = require("../controllers/SmtpController");
const db = sails.getDatastore().manager;
const sharp = require("sharp");
const fs = require("fs");

function validateEmail(email) {
  const domain = email.slice(email.lastIndexOf("@") + 1);
  if (domain === "jcsoftwaresolution.com") {
    return true;
  } else {
    return false;
  }
}

generatePassword = function (charLength) {
  // action are perform to generate random password for user
  var length = charLength,
    charset = "0123456789",
    retVal = "";

  for (var i = 0, n = charset.length; i < length; ++i) {
    retVal += charset.charAt(Math.floor(Math.random() * n));
  }
  return retVal;
};
/**
 *
 * @description: Used to generate random file name for every file
 * @createdAt 16/09/2021
 * @createdBy : Chandra Shekhar
 */

generateName = function () {
  var uuid = require("uuid");
  var randomStr = uuid.v4();
  var date = new Date();
  var currentDate = date.valueOf();
  retVal = randomStr + currentDate;
  return retVal;
};
module.exports = {
  /**
   *
   * @param {*} req
   * @param {*} res
   * @returns
   * @description Used to register User with role
   * @createdAt 01/09/2021
   */
  register: async (req, res) => {
    if (!req.body.email || typeof req.body.email == undefined) {
      return res.status(404).json({
        success: false,
        error: { code: 404, message: constantObj.user.EMAIL_REQUIRED },
      });
    }
    if (!req.body.password || typeof req.body.password == undefined) {
      return res.status(404).json({
        success: false,
        error: { code: 404, message: constantObj.user.PASSWORD_REQUIRED },
      });
    }

    var date = new Date();
    try {
      var user = await Users.findOne({ email: req.body.email.toLowerCase() });
      if (user) {
        return res.status(400).json({
          success: false,
          error: { code: 400, message: constantObj.user.EMAIL_EXIST },
        });
      } else {
        req.body["date_registered"] = date;
        req.body["date_verified"] = date;
        req.body["status"] = "active";
        req.body["role"] = req.body.role;

        var newUser = await Users.create(req.body).fetch();
        if (newUser) {
          userVerifyLink({
            email: newUser.email,
            fullName: newUser.fullName,
            id: newUser.id,
          });

          return res.status(200).json({
            success: true,
            code: 200,
            data: newUser,
            message: constantObj.user.SUCCESSFULLY_REGISTERED,
          });
        }
      }
    } catch (err) {
      console.error(err);
      return res.status(400).json({ success: true, code: 400, error: err });
    }
  },

  /**
   *
   * @reqBody  : {email,password}
   * @param {*} res
   * @returns
   * @description: Api Used to admin Login
   * @createdAt : 01/09/2021
   * @createdBy Amit kumar
   */
  // adminSignin: async (req, res) => {
  //   if (!req.body.email || typeof req.body.email == undefined) {
  //     return res.status(404).json({
  //       success: false,
  //       error: { code: 404, message: constantObj.user.EMAIL_REQUIRED },
  //     });
  //   }

  //   if (!validateEmail(req.body.email)) {
  //     return res.status(404).json({
  //       success: false,
  //       error: { code: 404, message: constantObj.user.INVALID_EMAIL },
  //     });
  //   }

  //   var user = await Users.findOne({
  //     email: req.body.email.toLowerCase(),
  //     isDeleted: false,
  //     // role: { in: ["admin", "subadmin", "HR", "manager"] },
  //     role: { in: ["admin", "subadmin"] }, //addedBy:-shivani on 28 april 2023

  //   });

  //   console.log(user);

  //   if (!user) {
  //     return res.status(404).json({
  //       success: false,
  //       error: { code: 404, message: constantObj.user.WRONG_EMAIL },
  //     });
  //   }

  //   // if (user && user.status == "active") {
  //   //   return res.status(404).json({
  //   //     success: false,
  //   //     error: { code: 404, message: constantObj.user.USERNAME_INACTIVE },
  //   //   });
  //   // }

  //   if (user && user.status != "active" && user.isVerified != "Y") {
  //     return res.status(404).json({
  //       success: false,
  //       error: { code: 404, message: constantObj.user.USERNAME_INACTIVE },
  //     });
  //   }

  //   const otp = generatePassword(4);
  //   var updatedUser = await Users.update({ id: user.id }, { otp: otp }).fetch();
  //   if (updatedUser) {
  //     await otpInEmail({
  //       email: req.body.email,
  //       fullName: user.fullName,
  //       otp: otp,
  //     });
  //     return res.status(200).json({
  //       success: true,
  //       message: constantObj.user.OTP_SENT,
  //     });
  //   } else {
  //     return res.status(400).json({
  //       success: false,
  //       error: { message: constantObj.user.ERROR_OCCUR },
  //     });
  //   }
  // },

  /**
   *
   * @param {*} {"email":"","otp":""}
   * @param {*} res
   * @returns detail of use and acess token
   * @description : Used to  verifiy otp of user
   * @createdAt : 01/09/2021
   * @createdBy : Amit Kumar
   */
  verifiyOtp: (req, res) => {
    try {
      var otp = req.param("otp");
      var email = req.param("email");

      if (!otp || typeof otp == undefined) {
        return res.status(404).json({
          success: false,
          error: { code: 404, message: constantObj.user.OTP_REQUIRED },
        });
      }
      if (!email || typeof email == undefined) {
        return res.status(404).json({
          success: false,
          error: { code: 404, message: constantObj.user.EMAIL_REQUIRED },
        });
      }
      var query = {};
      query.otp = otp;
      query.email = email;
      Users.findOne(query).then((user) => {
        if (user) {
          var token = jwt.sign(
            { user_id: user.id, firstName: user.firstName },
            { issuer: "Jcsoftware", subject: user.email, audience: "Vanmodsy" }
          );

          user.access_token = token;

          return res.status(200).json({
            success: true,
            data: user,
          });
        } else {
          return res.status(400).json({
            success: false,
            error: { message: constantObj.user.INVALID_OTP },
          });
        }
      });
    } catch (err) {
      return res
        .status(400)
        .json({ success: false, error: { code: 400, message: "" + err } });
    }
  },

  /*
   *changePassword
   */
  changePassword: async function (req, res) {
    if (!req.body.newPassword || typeof req.body.newPassword == undefined) {
      return res.status(404).json({
        success: false,
        error: { code: 404, message: constantObj.user.PASSWORD_REQUIRED },
      });
    }

    if (
      !req.body.confirmPassword ||
      typeof req.body.confirmPassword == undefined
    ) {
      return res.status(404).json({
        success: false,
        error: { code: 404, message: constantObj.user.CONPASSWORD_REQUIRED },
      });
    }

    if (
      !req.body.currentPassword ||
      typeof req.body.currentPassword == undefined
    ) {
      return res.status(404).json({
        success: false,
        error: {
          code: 404,
          message: constantObj.user.CURRENTPASSWORD_REQUIRED,
        },
      });
    }

    API(UserService.changePassword, req, res);
  },

  /**
   *
   * @param {*} req.body {email:"",password:""}
   * @param {*} res
   * @returns detail of the user
   * @description: Used to signup for company, manager , employee
   */
  // userSignin: async (req, res) => {
  //   if (!req.body.email || typeof req.body.email == undefined) {
  //     return res.status(404).json({
  //       success: false,
  //       error: { code: 404, message: constantObj.user.EMAIL_REQUIRED },
  //     });
  //   }

  //   if (!req.body.password || typeof req.body.password == undefined) {
  //     return res.status(404).json({
  //       success: false,
  //       error: { code: 404, message: constantObj.user.PASSWORD_REQUIRED },
  //     });
  //   }
  //   // , select: ['email', 'role', 'status', 'isVerified', 'password', 'firstName', 'lastName', 'fullName', 'image']
  //   var userDetails = await Users.find({
  //     where: {
  //       email: req.body.email.toLowerCase(),
  //       isDeleted: false,
  //       role: {
  //         in: ["user", "employee", "HR", "manager", "admin", "site_admin", "portal_admin", "bda", "Employees"],
  //       },
  //     },
  //   });
  //   var user = userDetails[0];
  //   // var user = await Users.findOne({ email: req.body.email });
  //   // console.log(user,'user====');
  //   if (!user) {
  //     return res.status(404).json({
  //       success: false,
  //       error: { code: 404, message: constantObj.user.INVALID_CRED },
  //     });
  //   }

  //   if (user && user.status != "true") {
  //     return res.status(404).json({
  //       success: false,
  //       error: { code: 404, message: constantObj.user.USERNAME_INACTIVE },
  //     });
  //   }
  //   if (user.isVerified == "N") {
  //     return res.status(404).json({
  //       success: false,
  //       error: { code: 404, message: constantObj.user.USERNAME_VERIFIED },
  //     });
  //   }
  //   if (!bcrypt.compareSync(req.body.password, user.password)) {
  //     return res.status(404).json({
  //       success: false,
  //       error: { code: 404, message: constantObj.user.INVALID_CRED },
  //     });
  //   } else {
  //     /**Genreating access token for the user */
  //     var lastLogin = new Date();
  //     let updateddata = await Users.updateOne({
  //       id: user.id,
  //     }).set({
  //       lastlogin:lastLogin,
  //     });
  //     var token = jwt.sign(
  //       { user_id: user.id, firstName: user.firstName, lastlogin:lastLogin},
  //       { issuer: "Jcsoftware", subject: user.email, audience: "L3Time" }
  //     );
  //     const refreshToken = jwt.sign(
  //       { user_id: user.id },
  //       { issuer: "refresh", subject: "user", audience: "L3Time" }
  //     );

  //     user.access_token = token;
  //     user.refresh_token = refreshToken;
  //     query = {}
  //     query.firstName = user.firstName
  //     query.lastName = user.lastName
  //     query.fullName = user.fullName
  //     query.email = user.email
  //     query.role = user.role
  //     query.login = new Date();
  //     console.log(query, "----------------query")
  //     if (user) {
  //       var activitie = await Activities.create(query);
  //     }

  //     return res.status(200).json({
  //       success: true,
  //       code: 200,
  //       message: constantObj.user.SUCCESSFULLY_LOGGEDIN,
  //       data: user,
  //     });
  //   }
  // },
  userSignin: async (req, res) => {
    console.log("reached here ");
    if (!req.body.email || typeof req.body.email == undefined) {
      return res.status(404).json({
        success: false,
        error: { code: 404, message: constantObj.user.EMAIL_REQUIRED },
      });
    }
    if (!validateEmail(req.body.email)) {
      return res.status(404).json({
        success: false,
        error: { code: 404, message: constantObj.user.INVALID_EMAIL },
      });
    }

    if (!req.body.password || typeof req.body.password == undefined) {
      return res.status(404).json({
        success: false,
        error: { code: 404, message: constantObj.user.PASSWORD_REQUIRED },
      });
    }
    // , select: ['email', 'role', 'status', 'isVerified', 'password', 'firstName', 'lastName', 'fullName', 'image']

    // role: {
    //   in: ["user", "employee", "HR", "manager", "admin", "site_admin", "portal_admin", "bda", "bidder"],
    // },

    var userDetails = await Users.find({
      where: {
        email: req.body.email.toLowerCase(),
        isDeleted: false,
        role: {
          in: ["HR", "developer", "qa", "bde", "seo", "designer"],
        },
      },
    });
    var user = userDetails[0];
    // var user = await Users.findOne({ email: req.body.email });

    if (!user) {
      return res.status(404).json({
        success: false,
        error: { code: 404, message: constantObj.user.INVALID_CRED },
      });
    }

    if (user && user.status != "active") {
      return res.status(404).json({
        success: false,
        error: { code: 404, message: constantObj.user.USERNAME_INACTIVE },
      });
    }
    if (user.isVerified == "N") {
      return res.status(404).json({
        success: false,
        error: { code: 404, message: constantObj.user.USERNAME_VERIFIED },
      });
    }
    if (!bcrypt.compareSync(req.body.password, user.password)) {
      return res.status(404).json({
        success: false,
        error: { code: 404, message: constantObj.user.INVALID_CRED },
      });
    } else {
      /**Genreating access token for the user */
      var token = jwt.sign(
        { user_id: user.id, firstName: user.firstName },
        { issuer: "Jcsoftware", subject: user.email, audience: "L3Time" }
      );
      const refreshToken = jwt.sign(
        { user_id: user.id },
        { issuer: "refresh", subject: "user", audience: "L3Time" }
      );

      user.access_token = token;
      user.refresh_token = refreshToken;
      query = {};
      query.firstName = user.firstName;
      query.lastName = user.lastName;
      query.fullName = user.fullName;
      query.email = user.email;
      query.role = user.role;
      query.login = new Date();

      if (user) {
        var activitie = await Activities.create(query);
      }
      return res.status(200).json({
        success: true,
        code: 200,
        message: constantObj.user.SUCCESSFULLY_LOGGEDIN,
        data: user,
      });
    }
  },

  admin_Signin: async (req, res) => {
    if (!req.body.email || typeof req.body.email == undefined) {
      return res.status(404).json({
        success: false,
        error: { code: 404, message: constantObj.user.EMAIL_REQUIRED },
      });
    }

    if (!validateEmail(req.body.email)) {
      return res.status(404).json({
        success: false,
        error: { code: 404, message: constantObj.user.INVALID_EMAIL },
      });
    }

    if (!req.body.password || typeof req.body.password == undefined) {
      return res.status(404).json({
        success: false,
        error: { code: 404, message: constantObj.user.PASSWORD_REQUIRED },
      });
    }
    // , select: ['email', 'role', 'status', 'isVerified', 'password', 'firstName', 'lastName', 'fullName', 'image']

    // role: {
    //   in: ["user", "employee", "HR", "manager", "admin", "site_admin", "portal_admin", "bda", "bidder"],
    // },

    var userDetails = await Users.find({
      where: {
        email: req.body.email.toLowerCase(),
        isDeleted: false,
        role: {
          in: ["admin", "subadmin"],
        },
      },
    });
    var user = userDetails[0];

    // var user = await Users.findOne({ email: req.body.email });

    if (!user) {
      return res.status(404).json({
        success: false,
        error: { code: 404, message: constantObj.user.INVALID_CRED },
      });
    }

    if (user && user.status != "active") {
      return res.status(404).json({
        success: false,
        error: { code: 404, message: constantObj.user.USERNAME_INACTIVE },
      });
    }
    if (user.isVerified == "N") {
      return res.status(404).json({
        success: false,
        error: { code: 404, message: constantObj.user.USERNAME_VERIFIED },
      });
    }
    // if (!bcrypt.compareSync(req.body.password, user.password)) {
      // return res.status(404).json({
      //   success: false,
      //   error: { code: 404, message: constantObj.user.INVALID_CRED },
      // });
    // } else {
      /**Genreating access token for the user */
      var token = jwt.sign(
        { user_id: user.id, firstName: user.firstName },
        { issuer: "Jcsoftware", subject: user.email, audience: "L3Time" }
      );
      const refreshToken = jwt.sign(
        { user_id: user.id },
        { issuer: "refresh", subject: "user", audience: "L3Time" }
      );

      user.access_token = token;
      user.refresh_token = refreshToken;
      query = {};
      query.firstName = user.firstName;
      query.lastName = user.lastName;
      query.fullName = user.fullName;
      query.email = user.email;
      query.role = user.role;
      query.login = new Date();

      if (user) {
        var activitie = await Activities.create(query);
      }
      return res.status(200).json({
        success: true,
        code: 200,
        message: constantObj.user.SUCCESSFULLY_LOGGEDIN,
        data: user,
      });
    // }
  },

  /**
   *
   * @param {*} req
   * @param {*} res
   * @returns
   * @description : Used to Get listing of the users
   * @createdAt : 02/09/2021
   * @createdBy : Amit Kumar
   */

  getAllUsers: async (req, res) => {
    try {
      var search = req.param("search");
      var role = req.param("role");
      var isDeleted = req.param("isDeleted");
      var page = req.param("page");
      var teamLeader = req.param("tl");
      var status = req.param("status");

      // var date1 = new Date();
      // var date2 = new Date("2024-07-05");

      // const result = date2 - date1;
      // console.log(result, "=========result");
      // var days = result / (1000 * 60 * 60 * 24);
      // console.log(days, "=========days");

      if (!page) {
        page = 1;
      }
      var count = parseInt(req.param("count"));
      if (!count) {
        count = 10;
      }
      var skipNo = (page - 1) * count;
      var query = {};
      if (search) {
        query.$or = [
          { fullName: { $regex: search, $options: "i" } },
          { email: { $regex: search, $options: "i" } },
        ];
      }
      query.role = { $ne: "admin" }; // admin user will not be in listing
      if (role) {
        query.role = role;
      }

      if (isDeleted) {
        if (isDeleted === "true") {
          isDeleted = true;
        } else {
          isDeleted = false;
        }
        query.isDeleted = isDeleted;
      }

      if (teamLeader) {
        if (teamLeader === "true") {
          teamLeader = true;
        } else {
          teamLeader = false;
        }
        query.isTL = teamLeader;
      }
      if (status) {
        query.status = status;
      }

      db.collection("users")
        .aggregate([
          {
            $lookup: {
              from: "users",
              localField: "deletedBy",
              foreignField: "_id",
              as: "deletedBy",
            },
          },
          {
            $unwind: {
              path: "$deletedBy",
              preserveNullAndEmptyArrays: true,
            },
          },
          {
            $project: {
              id: "$_id",
              employeeId: "$employeeId",
              role: "$role",
              isDeleted: "$isDeleted",
              fullName: "$fullName",
              email: "$email",
              aadharCardno: "$aadharCardno",
              passportNo: "$passportNo",
              votercardNo: "$votercardNo",
              pancardNo: "$pancardNo",
              aadharCardimages: "$aadharCardimages",
              passportImages: "$passportImages",
              votercardImages: "$votercardImages",
              documents: "$documents",
              gmail: "$gmail",
              secondaryEmail: "$secondaryEmail",
              designation: "$designation",
              mobileNo: "$mobileNo",
              dob: "$dob",
              salary:"$salary",
              image: "$image",
              documentImages: "$documentImages",
              fatherName: "$fatherName",
              motherName: "$motherName",
              primaryAddress: "$primaryAddress",
              secondaryAddress: "$secondaryAddress",
              contactNumber: "$contactNumber",
              facebookURL: "$facebookURL",
              instagramURL: "$instagramURL",
              linkedinURL: "$linkedinURL",
              twitterURL: "$twitterURL",
              dateOfJoining: "$dateOfJoining",
              dateOfResignation: "$dateOfResignation",
              dateOfRelieving: "$dateOfRelieving",
              skill: "$skill",
              checkInstatus: "$checkInstatus",
              maritalStatus: "$maritalStatus",
              spouseName: "$spouseName",
              status: "$status",
              isProjectAssign: "$isProjectAssign",
              isTL: "$isTL",
              releave: "$releave",
              releaveDocument: "$releaveDocument",
              tlTechnology: "$tlTechnology",
              employee_code: "$employee_code",
              createdAt: "$createdAt",
              deletedBy: "$deletedBy",
              deletedAt: "$deletedAt",
            },
          },
          {
            $match: query,
          },
        ])
        .toArray((err, totalResult) => {
          if (err) {
            return res.status(400).json({
              success: false,
              error: { message: "" + err },
            });
          }
          db.collection("users")
            .aggregate([
              {
                $lookup: {
                  from: "users",
                  localField: "deletedBy",
                  foreignField: "_id",
                  as: "deletedBy",
                },
              },
              {
                $unwind: {
                  path: "$deletedBy",
                  preserveNullAndEmptyArrays: true,
                },
              },
              {
                $project: {
                  id: "$_id",
                  employeeId: "$employeeId",
                  role: "$role",
                  isDeleted: "$isDeleted",
                  fullName: "$fullName",
                  email: "$email",
                  aadharCardno: "$aadharCardno",
                  passportNo: "$passportNo",
                  votercardNo: "$votercardNo",
                  pancardNo: "$pancardNo",
                  aadharCardimages: "$aadharCardimages",
                  passportImages: "$passportImages",
                  votercardImages: "$votercardImages",
                  documents: "$documents",
                  gmail: "$gmail",
                  secondaryEmail: "$secondaryEmail",
                  designation: "$designation",
                  mobileNo: "$mobileNo",
                  dob: "$dob",
                  image: "$image",
                  documentImages: "$documentImages",
                  fatherName: "$fatherName",
                  motherName: "$motherName",
                  primaryAddress: "$primaryAddress",
                  secondaryAddress: "$secondaryAddress",
                  contactNumber: "$contactNumber",
                  facebookURL: "$facebookURL",
                  instagramURL: "$instagramURL",
                  linkedinURL: "$linkedinURL",
                  twitterURL: "$twitterURL",
                  dateOfJoining: "$dateOfJoining",
                  dateOfResignation: "$dateOfResignation",
                  dateOfRelieving: "$dateOfRelieving",
                  maritalStatus: "$maritalStatus",
                  spouseName: "$spouseName",
                  status: "$status",
                  checkInstatus: "$checkInstatus",
                  skill: "$skill",
                  isProjectAssign: "$isProjectAssign",
                  isTL: "$isTL",
                  releave: "$releave",
                  releaveDocument: "$releaveDocument",
                  tlTechnology: "$tlTechnology",
                  employee_code: "$employee_code",
                  createdAt: "$createdAt",
                  deletedBy: "$deletedBy",
                  deletedAt: "$deletedAt",
                },
              },
              {
                $match: query,
              },
              {
                $sort: {
                  createdAt: -1,
                },
              },

              {
                $skip: Number(skipNo),
              },
              {
                $limit: Number(count),
              },
            ])
            .toArray(async (err, result) => {
              // for await (let itemss of result) {
              //   var employee_name = itemss.fullName;
              //   var doj_date = new Date(itemss.dateOfJoining);
              //   var employee_doj = itemss.dateOfJoining;
              //   var doj_year = employee_doj.substring(0, 4);
              //   var doj_month = employee_doj.substring(5, 7);
              //   var doj_day = employee_doj.substring(8, 10);
              //   var appraisal_year = Number(doj_year) + 1;
              //   var appraisal_date = `${appraisal_year}` + "-" + `${doj_month}` + "-" + `${doj_day}`;
              //   var user_appraisal_date = new Date(appraisal_date);
              //   var current_date = new Date();

              //   const result = user_appraisal_date - doj_date;
              //   //console.log(result, "=========result");
              //   var days = result / (1000 * 60 * 60 * 24);
              //   //console.log(days, "=========days");
              //   //var diff = current_year - doj_year
              //   //if(diff == 0)
              //   const result2 = user_appraisal_date - current_date;
              //   console.log(result2, "=========result2");
              //   var dayss = result2 / (1000 * 60 * 60 * 24);
              //   console.log(dayss, "=========dayss");

              //   console.log(employee_name, "===========employee_name");
              //   console.log(employee_doj, "===========employee_doj");
              //   console.log(appraisal_date, "===========appraisal_date");
              //   console.log(doj_date, "=========doj_date");
              //   console.log(user_appraisal_date, "=========user_appraisal_date");

              //   // console.log(result, "=========result");
              //   // console.log(days, "===============days")
              //   console.log("_________________________________________");
              //   itemss.dayss = dayss;

              // }

              if (err) {
                return res.status(400).json({
                  success: false,
                  error: { message: "" + err },
                });
              } else {
                return res.status(200).json({
                  success: true,
                  code: 200,
                  data: result,
                  total: totalResult.length,
                });
              }
            });
        });
    } catch (error) {
      return res.status(400).json({
        success: false,
        code: 400,
        error: error,
      });
    }
  },

  /**
   *
   * @param {*} req
   * @param {*} res
   * @returns
   * @description : Used to Get listing of the admin HR
   * @createdAt : 05/10/2021
   * @createdBy : Rohit Kumar
   */
  getMailingList: async (req, res) => {
    try {
      var search = req.param("search");
      var role = req.param("role");
      let isTL = req.param("isTL");
      var isDeleted = req.param("isDeleted");
      var page = req.param("page");
      if (!page) {
        page = 1;
      }
      var count = parseInt(req.param("count"));
      if (!count) {
        count = 10;
      }
      var skipNo = (page - 1) * count;
      var query = {};
      if (search) {
        query.$or = [
          { fullName: { $regex: search, $options: "i" } },
          { email: { $regex: search, $options: "i" } },
        ];
      }

      if (role) {
        query.role = role;
      }

      query.$or = [];
      // query.$and.push({ role: { $in: ["admin", "subadmin"] } });
      query.$or.push({ role: { $in: ["admin", "subadmin"] } });
      query.$or.push({ isTL: true });

      if (isDeleted) {
        if (isDeleted === "true") {
          isDeleted = true;
        } else {
          isDeleted = false;
        }
        query.isDeleted = isDeleted;
      }

      db.collection("users")
        .aggregate([
          {
            $lookup: {
              from: "users",
              localField: "deletedBy",
              foreignField: "_id",
              as: "deletedBy",
            },
          },
          {
            $unwind: {
              path: "$deletedBy",
              preserveNullAndEmptyArrays: true,
            },
          },
          {
            $project: {
              id: "$_id",
              employeeId: "$employeeId",
              role: "$role",
              isTL: "$isTL",
              isDeleted: "$isDeleted",
              fullName: "$fullName",
              email: "$email",
              secondaryEmail: "$secondaryEmail",
              designation: "$designation",
              mobileNo: "$mobileNo",
              dob: "$dob",
              image: "$image",
              documentImages: "$documentImages",
              fatherName: "$fatherName",
              motherName: "$motherName",
              primaryAddress: "$primaryAddress",
              secondaryAddress: "$secondaryAddress",
              contactNumber: "$contactNumber",
              facebookURL: "$facebookURL",
              instagramURL: "$instagramURL",
              linkedinURL: "$linkedinURL",
              dateOfJoining: "$dateOfJoining",
              dateOfResignation: "$dateOfResignation",
              dateOfRelieving: "$dateOfRelieving",
              status: "$status",
              createdAt: "$createdAt",
              deletedBy: "$deletedBy",
              deletedAt: "$deletedAt",
            },
          },
          {
            $match: query,
          },
        ])
        .toArray((err, totalResult) => {
          if (err) {
            return res.status(400).json({
              success: false,
              error: { message: "" + err },
            });
          }
          db.collection("users")
            .aggregate([
              {
                $lookup: {
                  from: "users",
                  localField: "deletedBy",
                  foreignField: "_id",
                  as: "deletedBy",
                },
              },
              {
                $unwind: {
                  path: "$deletedBy",
                  preserveNullAndEmptyArrays: true,
                },
              },
              {
                $project: {
                  id: "$_id",
                  employeeId: "$employeeId",
                  role: "$role",
                  isTL: "$isTL",

                  isDeleted: "$isDeleted",
                  fullName: "$fullName",
                  email: "$email",
                  secondaryEmail: "$secondaryEmail",
                  designation: "$designation",
                  mobileNo: "$mobileNo",
                  dob: "$dob",
                  image: "$image",
                  documentImages: "$documentImages",
                  fatherName: "$fatherName",
                  motherName: "$motherName",
                  primaryAddress: "$primaryAddress",
                  secondaryAddress: "$secondaryAddress",
                  contactNumber: "$contactNumber",
                  facebookURL: "$facebookURL",
                  instagramURL: "$instagramURL",
                  linkedinURL: "$linkedinURL",
                  dateOfJoining: "$dateOfJoining",
                  dateOfResignation: "$dateOfResignation",
                  dateOfRelieving: "$dateOfRelieving",
                  status: "$status",
                  createdAt: "$createdAt",
                  deletedBy: "$deletedBy",
                  deletedAt: "$deletedAt",
                },
              },
              {
                $match: query,
              },
              {
                $sort: {
                  createdAt: -1,
                },
              },

              {
                $skip: Number(skipNo),
              },
              {
                $limit: Number(count),
              },
            ])
            .toArray((err, result) => {
              if (err) {
                return res.status(400).json({
                  success: false,
                  error: { message: "" + err },
                });
              } else {
                return res.status(200).json({
                  success: true,
                  code: 200,
                  data: result,
                  total: totalResult.length,
                });
              }
            });
        });
    } catch (error) {
      console.log(error, "=================");
      return res.status(400).json({
        success: false,
        code: 400,
        error: error,
      });
    }
  },

  /**
   *
   * @param {*} req
   * @param {*} res
   * @returns
   * @description : Used to Get listing of the project assign user
   * @createdAt : 22/10/2021
   * @createdBy : Rohit Kumar
   */
  getProjectAssignUser: async (req, res) => {
    try {
      var search = req.param("search");
      var projectId = req.param("projectId");
      var isDeleted = req.param("isDeleted");
      var page = parseInt(req.param("page"));

      if (!page) {
        page = 1;
      }
      var count = parseInt(req.param("count"));
      if (!count) {
        count = 10;
      }
      var skipNo = (page - 1) * count;

      var query = {};

      if (search) {
        query.$or = [
          { fullName: { $regex: search, $options: "i" } },
          { email: { $regex: search, $options: "i" } },
        ];
      }

      let projects = await Projects.findOne({ id: projectId });

      var memberArr = projects.members;

      var userArray = [];
      async.each(
        memberArr,
        function (memberdata, callback) {
          var UserId = memberdata;
          if (UserId) {
            userArray.push(ObjectId(UserId));
          }
          callback();
        },
        function (err) {}
      );
      query._id = { $nin: userArray };
      query.isDeleted = false;

      db.collection("users")
        .aggregate([
          {
            $lookup: {
              from: "users",
              localField: "deletedBy",
              foreignField: "_id",
              as: "deletedBy",
            },
          },
          {
            $unwind: {
              path: "$deletedBy",
              preserveNullAndEmptyArrays: true,
            },
          },
          {
            $project: {
              id: "$_id",
              employeeId: "$employeeId",
              role: "$role",
              isDeleted: "$isDeleted",
              fullName: "$fullName",
              email: "$email",
              aadharCardno: "$aadharCardno",
              passportNo: "$passportNo",
              secondaryEmail: "$secondaryEmail",
              designation: "$designation",
              mobileNo: "$mobileNo",
              dob: "$dob",
              image: "$image",
              documentImages: "$documentImages",
              fatherName: "$fatherName",
              motherName: "$motherName",
              primaryAddress: "$primaryAddress",
              secondaryAddress: "$secondaryAddress",
              contactNumber: "$contactNumber",
              facebookURL: "$facebookURL",
              instagramURL: "$instagramURL",
              linkedinURL: "$linkedinURL",
              dateOfJoining: "$dateOfJoining",
              dateOfResignation: "$dateOfResignation",
              dateOfRelieving: "$dateOfRelieving",
              status: "$status",
              createdAt: "$createdAt",
              deletedBy: "$deletedBy",
              deletedAt: "$deletedAt",
            },
          },
          {
            $match: query,
          },
        ])
        .toArray((err, totalResult) => {
          if (err) {
            return res.status(400).json({
              success: false,
              error: { message: "" + err },
            });
          }
          db.collection("users")
            .aggregate([
              {
                $lookup: {
                  from: "users",
                  localField: "deletedBy",
                  foreignField: "_id",
                  as: "deletedBy",
                },
              },
              {
                $unwind: {
                  path: "$deletedBy",
                  preserveNullAndEmptyArrays: true,
                },
              },
              {
                $project: {
                  id: "$_id",
                  employeeId: "$employeeId",
                  role: "$role",
                  isDeleted: "$isDeleted",
                  fullName: "$fullName",
                  email: "$email",
                  secondaryEmail: "$secondaryEmail",
                  designation: "$designation",
                  mobileNo: "$mobileNo",
                  dob: "$dob",
                  image: "$image",
                  documentImages: "$documentImages",
                  fatherName: "$fatherName",
                  motherName: "$motherName",
                  primaryAddress: "$primaryAddress",
                  secondaryAddress: "$secondaryAddress",
                  contactNumber: "$contactNumber",
                  facebookURL: "$facebookURL",
                  instagramURL: "$instagramURL",
                  linkedinURL: "$linkedinURL",
                  dateOfJoining: "$dateOfJoining",
                  dateOfResignation: "$dateOfResignation",
                  dateOfRelieving: "$dateOfRelieving",
                  status: "$status",
                  createdAt: "$createdAt",
                  deletedBy: "$deletedBy",
                  deletedAt: "$deletedAt",
                },
              },
              {
                $match: query,
              },
              {
                $sort: {
                  createdAt: -1,
                },
              },

              {
                $skip: Number(skipNo),
              },
              {
                $limit: Number(count),
              },
            ])
            .toArray((err, result) => {
              if (err) {
                return res.status(400).json({
                  success: false,
                  error: { message: "" + err },
                });
              } else {
                return res.status(200).json({
                  success: true,
                  code: 200,
                  data: result,
                  total: totalResult.length,
                });
              }
            });
        });
    } catch (error) {
      return res.status(400).json({
        success: false,
        code: 400,
        error: error,
      });
    }
  },

  /*
   *For Check Email Address Exit or not
   */
  checkEmail: async function (req, res) {
    var email = req.param("email");
    if (!email || typeof email == undefined) {
      return res.status(400).json({
        success: false,
        error: { code: 400, message: "Email is required" },
      });
    }
    Users.findOne({ email: email }).then((user) => {
      if (user) {
        return res.status(200).json({
          success: false,
          error: { code: 400, message: "Email already taken" },
        });
      } else {
        return res.status(200).json({
          success: true,
          code: 200,
          message: "you can use this email",
        });
      }
    });
  },

  editProfile: (req, res) => {
    API(UserService.editProfile, req, res);
  },

  addUser: function (req, res) {
    API(UserService.addUser, req, res);
  },

  forgotPassword: function (req, res) {
    API(UserService.webForgotPassword, req, res);
  },

  resetPassword: function (req, res) {
    API(UserService.resetPassword, req, res);
  },
  /**
   *
   * @param {*} req
   * @param {*} res
   * @param {*} next
   * @description : Used to get user profile data for current user
   * @createdAt : 01/09/2021
   * @createdBy : Amit Kumar
   */
  userProfileData: (req, res, next) => {
    let query = {};
    query.id = req.identity.id;
    Users.findOne(query).exec(async(err, userDetail) => {
      let numberOfAttendance = await Attendance.find({ userId: userDetail.id });
          let numberOfLeaves = await Leaves.find({userId:userDetail.id});

          userDetail.numberOfAttendance = numberOfAttendance.length;
          userDetail.numberOfLeaves = numberOfLeaves.length;
      if (err) {
        return res.status(400).json({
          success: false,
          error: { code: 400, message: "" + err },
        });
      } else {
        return res.status(200).json({
          success: true,
          code: 200,
          data: userDetail,
        });
      }
    });
  },

  userDetail: async (req, res, next) => {
    let query = {};
    query.id = req.param("id");
    Users.findOne(query).exec(async (err, userDetail) => {
      if (err) {
        return res.status(400).json({
          success: false,
          error: { code: 400, message: "" + err },
        });
      } else {
        if (userDetail.checkInstatus == "true") {
          let sortBy = "updatedAt desc";
          var dt = new Date();
          let attendanceData = await Attendance.find({ userId: userDetail.id })

          var attendancecurrentDate = attendanceData.attendanceDate;
          var checkIntime = attendanceData[0].checkIn;
          var startTime = checkIntime;
          var endTime = dt;

          var seconds = (endTime.getTime() - startTime.getTime()) / 1000;
          var lastSecond = seconds;
          if (seconds >= 3600) {
            var sec_num = parseInt(seconds, 10);
            var hours = Math.floor(sec_num / 3600);
            var minutes = Math.floor(sec_num / 60) % 60;
            var seconds = sec_num % 60;
            var Hourstotaltime =
              hours + "hr " + minutes + "m " + seconds + "s ";
          } else {
            if (seconds >= 60 && seconds < 3600) {
              var ndate = new Date(seconds * 1000).toISOString().substr(14, 5);
              var Newtimearray = ndate.split(":");
              var Hourstotaltime =
                Newtimearray[0] + "m " + Newtimearray[1] + "s";
            } else {
              var Hourstotaltime = Math.floor(seconds) + "s";
            }
          }

          userDetail.totalIntime = Hourstotaltime;

          
        }

          let numberOfAttendance = await Attendance.find({ userId: userDetail.id });
          let numberOfLeaves = await Leaves.find({fromEmail:userDetail.email,leave_status:"Accepted"});

          userDetail.numberOfAttendance = numberOfAttendance.length;
          userDetail.numberOfLeaves = numberOfLeaves.length;

         let leavesDetails = await services.calculateLeaveMatrics(userDetail.email,new Date())
          userDetail.leavesDetails = leavesDetails;
          
         return res.status(200).json({
          success: true,
          code: 200,
          data: userDetail,
        });
      }
    });
  },

  /**
   *
   * @param {*} req {id:""}
   * @param {*} res {data:contain user detail}
   * @description : Used to autoLogin the user
   *
   */

  autoLogin: (req, res) => {
    API(UserService.autoLogin, req, res);
  },

  verifyUser: (req, res) => {
    var id = req.param("id");
    Users.findOne({ id: id }).then((user) => {
      if (user) {
        Users.update({ id: id }, { isVerified: "Y" }).then((verified) => {
          return res.redirect(constant.FRONT_WEB_URL + "subscriptions");
        });
      } else {
        return res.redirect(constant.FRONT_WEB_URL);
      }
    });
  },

  verifyEmail: (req, res) => {
    var id = req.param("id");
    Users.findOne({ id: id }).then((user) => {
      if (user) {
        Users.update({ id: id }, { contact_information: "Yes" }).then(
          (verified) => {
            return res.redirect(
              constantObj.messages.FRONT_WEB_URL + "/auth/login"
            );
          }
        );
      } else {
        return res.redirect(constantObj.messages.FRONT_WEB_URL);
      }
    });
  },

  /**
   *
   * @param {id} req
   * @param {*} res
   * @returns
   * @description: Api Used to delete user profile
   * @createdAt : 10/09/2021
   * @createdBy Chandra Shekhar
   */

  profileDeleted: async (req, res) => {
    try {
      let userID = req.param("id");
      if (userID === "") {
        return res.status(404).json({
          success: false,
          message: constantObj.user.ID_REQUIRED,
        });
      }

      var userData = await Users.updateOne({
        id: userID,
      }).set({ isDeleted: true });

      if (userData) {
        return res.status(200).json({
          success: true,
          message: constantObj.user.DELETED_USER,
        });
      } else {
        return res.status(404).json({
          success: false,
          message: constantObj.user.NO_RESULT,
        });
      }
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },

  /**
   *
   * @param {*} req
   * @param {*} res
   * @returns
   * @description: Api Used to upload image
   * @createdAt : 07/09/2021
   * @createdBy Chandra Shekhar
   */

  uploadImage: async (req, res) => {
    var modelName = req.param("modelName");
    // define folders path
    var rootpath = process.cwd();
    var fullpath = rootpath + "/assets/images/" + modelName;
    var fullpaththumbnail =
      rootpath + "/assets/images/" + modelName + "/thumbnail";
    var fullpath200 =
      rootpath + "/assets/images/" + modelName + "/thumbnail/200";
    var fullpath300 =
      rootpath + "/assets/images/" + modelName + "/thumbnail/300";
    var fullpath500 =
      rootpath + "/assets/images/" + modelName + "/thumbnail/500";

    //Check image upload folder is exists or not. If not create all folders
    if (fs.existsSync(fullpath) == false) {
      fs.mkdirSync(fullpath);
      fs.mkdirSync(fullpaththumbnail);
      fs.mkdirSync(fullpath200);
      fs.mkdirSync(fullpath300);
      fs.mkdirSync(fullpath500);
    }

    try {
      req
        .file("file")
        .upload({ dirname: "../../assets/images" }, async (err, file) => {
          var responseData = {};
          file.forEach(async (element, index) => {
            var name = generateName();
            typeArr = element.type.split("/");
            fileExt = element.fd.split(".").pop();
            if (
              fileExt === "jpeg" ||
              fileExt === "JPEG" ||
              fileExt === "JPG" ||
              fileExt === "jpg" ||
              fileExt === "PNG" ||
              fileExt === "png"
            ) {
              fs.readFile(file[index].fd, async (err, data) => {
                if (err) {
                  return res.status(403).json({
                    success: false,
                    error: {
                      code: 403,
                      message: err,
                    },
                  });
                } else {
                  if (data) {
                    var path = file[index].fd;
                    fs.writeFile(
                      "assets/images/" + modelName + "/" + name + "." + fileExt,
                      data,
                      function (err, image) {
                        if (err) {
                          console.log(err);
                          return res.status(400).json({
                            success: false,
                            error: {
                              code: 400,
                              message: err,
                            },
                          });
                        }
                      }
                    );

                    // fullpath.push( name+'.'+fileExt)
                    // resImagePath.push('assets/images/'+modelName+'/'  + name+'.'+fileExt)
                    responseData.fullpath = name + "." + fileExt;
                    responseData.imagePath =
                      "images/" + modelName + "/" + name + "." + fileExt;
                    var thumbpath =
                      "assets/images/" +
                      modelName +
                      "/thumbnail/200/" +
                      name +
                      "." +
                      fileExt;
                    sharp(path)
                      .resize({ height: 200, width: 200 })
                      .toFile(thumbpath)
                      .then(function (newFileInfo) {})
                      .catch(function (err) {
                        console.log("Got Error", err);
                      });
                    var thumbpath1 =
                      "assets/images/" +
                      modelName +
                      "/thumbnail/300/" +
                      name +
                      "." +
                      fileExt;
                    var thumbpath2 =
                      "assets/images/" +
                      modelName +
                      "/thumbnail/500/" +
                      name +
                      "." +
                      fileExt;
                    sharp(path)
                      .resize({ height: 300, width: 300 })
                      .toFile(thumbpath1)
                      .then(function (newFileInfo) {})
                      .catch(function (err) {
                        console.log("Got Error", err);
                      });
                    sharp(path)
                      .resize({ height: 500, width: 500 })
                      .toFile(thumbpath2)
                      .then(function (newFileInfo) {})
                      .catch(function (err) {
                        console.log("Got Error");
                      });

                    if (index == file.length - 1) {
                      await new Promise((resolve) => setTimeout(resolve, 2000)); //Because file take times to write in .tmp folder
                      return res.json({
                        success: true,
                        code: 200,
                        data: responseData,
                      });
                    }
                  }
                }
              }); //end of loop
            } else {
              return res.json({
                success: false,
                error: {
                  code: 400,
                  message: constantObj.user.INVALID_IMAGE,
                },
              });
            }
          });
        });
    } catch (err) {
      return res
        .status(500)
        .json({ success: false, error: { code: 500, message: "" + err } });
    }
  },

  /**
   *
   * @param {*} req
   * @param {*} res
   * @returns
   * @description: Api Used to upload multipleimage
   * @createdAt : 17/09/2021
   * @createdBy Chandra Shekhar
   */

  uploadMultipleImage: async (req, res) => {
    var modelName = req.param("modelName");
    // define folders path
    var rootpath = process.cwd();
    var fullpath = rootpath + "/assets/images/" + modelName;
    var fullpaththumbnail =
      rootpath + "/assets/images/" + modelName + "/thumbnail";
    var fullpath200 =
      rootpath + "/assets/images/" + modelName + "/thumbnail/200";
    var fullpath300 =
      rootpath + "/assets/images/" + modelName + "/thumbnail/300";
    var fullpath500 =
      rootpath + "/assets/images/" + modelName + "/thumbnail/500";

    //Check image upload folder is exists or not. If not create all folders
    if (fs.existsSync(fullpath) == false) {
      fs.mkdirSync(fullpath);
      fs.mkdirSync(fullpaththumbnail);
      fs.mkdirSync(fullpath200);
      fs.mkdirSync(fullpath300);
      fs.mkdirSync(fullpath500);
    }

    try {
      req
        .file("file")
        .upload({ dirname: "../../assets/images" }, async (err, file) => {
          var responseData = {};
          var multipleimageArray = [];

          file.forEach(async (element, index) => {
            var name = generateName();
            typeArr = element.type.split("/");
            fileExt = element.fd.split(".").pop();

            fs.readFile(file[index].fd, async (err, data) => {
              if (err) {
                return res.status(403).json({
                  success: false,
                  error: {
                    code: 403,
                    message: err,
                  },
                });
              } else {
                if (data) {
                  var path = file[index].fd;

                  fs.writeFile(
                    "assets/images/" + modelName + "/" + name + "." + fileExt,
                    data,
                    function (err, image) {
                      if (err) {
                        return res.status(400).json({
                          success: false,
                          error: {
                            code: 400,
                            message: err,
                          },
                        });
                      }
                    }
                  );

                  // fullpath.push( name+'.'+fileExt)
                  // resImagePath.push('assets/images/'+modelName+'/'  + name+'.'+fileExt)

                  responseData.fullpath = name + "." + fileExt;
                  responseData.imagePath =
                    "images/" + modelName + "/" + name + "." + fileExt;

                  multipleimageArray.push(responseData);

                  var thumbpath =
                    "assets/images/" +
                    modelName +
                    "/thumbnail/200/" +
                    name +
                    "." +
                    fileExt;
                  sharp(path)
                    .resize({ height: 200, width: 200 })
                    .toFile(thumbpath)
                    .then(function (newFileInfo) {})
                    .catch(function (err) {
                      console.log("Got Error", err);
                    });
                  var thumbpath1 =
                    "assets/images/" +
                    modelName +
                    "/thumbnail/300/" +
                    name +
                    "." +
                    fileExt;
                  var thumbpath2 =
                    "assets/images/" +
                    modelName +
                    "/thumbnail/500/" +
                    name +
                    "." +
                    fileExt;
                  sharp(path)
                    .resize({ height: 300, width: 300 })
                    .toFile(thumbpath1)
                    .then(function (newFileInfo) {})
                    .catch(function (err) {
                      console.log("Got Error", err);
                    });
                  sharp(path)
                    .resize({ height: 500, width: 500 })
                    .toFile(thumbpath2)
                    .then(function (newFileInfo) {})
                    .catch(function (err) {});

                  if (index == file.length - 1) {
                    await new Promise((resolve) => setTimeout(resolve, 2000)); //Because file take times to write in .tmp folder
                    return res.json({
                      success: true,
                      code: 200,
                      data: multipleimageArray,
                    });
                  }
                }
              }
            }); //end of loop
          });
        });
    } catch (err) {
      console.log(err);
      return res
        .status(500)
        .json({ success: false, error: { code: 500, message: "" + err } });
    }
  },

  /**Activities listing */

  getActivitiesList: async (req, res) => {
    try {
      var search = req.param("search");
      var role = req.param("role");
      var page = req.param("page");
      if (!page) {
        page = 1;
      }
      var count = parseInt(req.param("count"));
      if (!count) {
        count = 10;
      }
      var skipNo = (page - 1) * count;
      var query = {};
      if (search) {
        query.$or = [
          { fullName: { $regex: search, $options: "i" } },
          { email: { $regex: search, $options: "i" } },
        ];
      }
      db.collection("activities")
        .aggregate([
          {
            $project: {
              id: "$_id",
              fullName: "$fullName",
              email: "$email",
              role: "$role",
              createdAt: "$createdAt",
            },
          },
          {
            $match: query,
          },
        ])
        .toArray((err, totalResult) => {
          if (err) {
            return res.status(400).json({
              success: false,
              error: { message: "" + err },
            });
          }
          db.collection("activities")
            .aggregate([
              {
                $project: {
                  id: "$_id",
                  fullName: "$fullName",
                  email: "$email",
                  role: "$role",
                  createdAt: "$createdAt",
                },
              },
              {
                $match: query,
              },
              {
                $sort: {
                  createdAt: -1,
                },
              },

              {
                $skip: Number(skipNo),
              },
              {
                $limit: Number(count),
              },
            ])
            .toArray((err, result) => {
              if (err) {
                return res.status(400).json({
                  success: false,
                  error: { message: "" + err },
                });
              } else {
                return res.status(200).json({
                  success: true,
                  code: 200,
                  data: result,
                  total: totalResult.length,
                });
              }
            });
        });
    } catch (error) {
      return res.status(400).json({
        success: false,
        code: 400,
        error: error,
      });
    }
  },

  /**get Today Login Users list */
  getTodayLoginUsers: async (req, res) => {
    try {
      var d = new Date();

      d.setUTCHours("00", "00", "00");

      query = {};
      query.createdAt = { ">=": d };

      var data = await Activities.find(query);
      if (data && data.length > 0) {
        return res.status(200).json({
          success: true,
          code: 200,
          data: data,
        });
      } else {
        return res.status(200).json({
          success: true,
          code: 200,
          message: constantObj.user.NO,
        });
      }
    } catch (err) {
      return res.status(400).json({ success: true, code: 400, error: err });
    }
  },

  /**Api for listing of developers  */
  getAllDevelopers: async (req, res) => {
    try {
      var search = req.param("search");
      var role = req.param("role");
      var isDeleted = req.param("isDeleted");
      var page = req.param("page");
      var teamLeader = req.param("tl");
      if (!page) {
        page = 1;
      }
      var count = parseInt(req.param("count"));
      if (!count) {
        count = 10;
      }
      var skipNo = (page - 1) * count;
      var query = {};
      if (search) {
        query.$or = [
          { fullName: { $regex: search, $options: "i" } },
          { email: { $regex: search, $options: "i" } },
        ];
      }
      // query.role = { $ne: "admin" }; // admin user will not be in listing
      query.role = {
        $nin: ["admin", "HR", "subadmin", "bde", "site_admin", "seo"],
      };
      if (role) {
        query.role = role;
      }

      if (isDeleted) {
        if (isDeleted === "true") {
          isDeleted = true;
        } else {
          isDeleted = false;
        }
        query.isDeleted = isDeleted;
      }

      if (teamLeader) {
        if (teamLeader === "true") {
          teamLeader = true;
        } else {
          teamLeader = false;
        }
        query.isTL = teamLeader;
      }

      db.collection("users")
        .aggregate([
          {
            $lookup: {
              from: "users",
              localField: "deletedBy",
              foreignField: "_id",
              as: "deletedBy",
            },
          },
          {
            $unwind: {
              path: "$deletedBy",
              preserveNullAndEmptyArrays: true,
            },
          },
          {
            $project: {
              id: "$_id",
              employeeId: "$employeeId",
              role: "$role",
              isDeleted: "$isDeleted",
              fullName: "$fullName",
              email: "$email",
              aadharCardno: "$aadharCardno",
              passportNo: "$passportNo",
              votercardNo: "$votercardNo",
              pancardNo: "$pancardNo",
              aadharCardimages: "$aadharCardimages",
              passportImages: "$passportImages",
              votercardImages: "$votercardImages",
              documents: "$documents",
              gmail: "$gmail",
              secondaryEmail: "$secondaryEmail",
              designation: "$designation",
              mobileNo: "$mobileNo",
              dob: "$dob",
              image: "$image",
              documentImages: "$documentImages",
              fatherName: "$fatherName",
              motherName: "$motherName",
              primaryAddress: "$primaryAddress",
              secondaryAddress: "$secondaryAddress",
              contactNumber: "$contactNumber",
              facebookURL: "$facebookURL",
              instagramURL: "$instagramURL",
              linkedinURL: "$linkedinURL",
              twitterURL: "$twitterURL",
              dateOfJoining: "$dateOfJoining",
              dateOfResignation: "$dateOfResignation",
              dateOfRelieving: "$dateOfRelieving",
              skill: "$skill",
              checkInstatus: "$checkInstatus",
              maritalStatus: "$maritalStatus",
              spouseName: "$spouseName",
              status: "$status",
              isProjectAssign: "$isProjectAssign",
              isTL: "$isTL",
              tlTechnology: "$tlTechnology",
              createdAt: "$createdAt",
              deletedBy: "$deletedBy",
              deletedAt: "$deletedAt",
            },
          },
          {
            $match: query,
          },
        ])
        .toArray((err, totalResult) => {
          if (err) {
            return res.status(400).json({
              success: false,
              error: { message: "" + err },
            });
          }
          db.collection("users")
            .aggregate([
              {
                $lookup: {
                  from: "users",
                  localField: "deletedBy",
                  foreignField: "_id",
                  as: "deletedBy",
                },
              },
              {
                $unwind: {
                  path: "$deletedBy",
                  preserveNullAndEmptyArrays: true,
                },
              },
              {
                $project: {
                  id: "$_id",
                  employeeId: "$employeeId",
                  role: "$role",
                  isDeleted: "$isDeleted",
                  fullName: "$fullName",
                  email: "$email",
                  aadharCardno: "$aadharCardno",
                  passportNo: "$passportNo",
                  votercardNo: "$votercardNo",
                  pancardNo: "$pancardNo",
                  aadharCardimages: "$aadharCardimages",
                  passportImages: "$passportImages",
                  votercardImages: "$votercardImages",
                  documents: "$documents",
                  gmail: "$gmail",
                  secondaryEmail: "$secondaryEmail",
                  designation: "$designation",
                  mobileNo: "$mobileNo",
                  dob: "$dob",
                  image: "$image",
                  documentImages: "$documentImages",
                  fatherName: "$fatherName",
                  motherName: "$motherName",
                  primaryAddress: "$primaryAddress",
                  secondaryAddress: "$secondaryAddress",
                  contactNumber: "$contactNumber",
                  facebookURL: "$facebookURL",
                  instagramURL: "$instagramURL",
                  linkedinURL: "$linkedinURL",
                  twitterURL: "$twitterURL",
                  dateOfJoining: "$dateOfJoining",
                  dateOfResignation: "$dateOfResignation",
                  dateOfRelieving: "$dateOfRelieving",
                  maritalStatus: "$maritalStatus",
                  spouseName: "$spouseName",
                  status: "$status",
                  checkInstatus: "$checkInstatus",
                  skill: "$skill",
                  isProjectAssign: "$isProjectAssign",
                  isTL: "$isTL",
                  tlTechnology: "$tlTechnology",
                  createdAt: "$createdAt",
                  deletedBy: "$deletedBy",
                  deletedAt: "$deletedAt",
                },
              },
              {
                $match: query,
              },
              {
                $sort: {
                  createdAt: -1,
                },
              },

              {
                $skip: Number(skipNo),
              },
              {
                $limit: Number(count),
              },
            ])
            .toArray((err, result) => {
              if (err) {
                return res.status(400).json({
                  success: false,
                  error: { message: "" + err },
                });
              } else {
                return res.status(200).json({
                  success: true,
                  code: 200,
                  data: result,
                  total: totalResult.length,
                });
              }
            });
        });
    } catch (error) {
      return res.status(400).json({
        success: false,
        code: 400,
        error: error,
      });
    }
  },
};

userVerifyLink = function (options) {
  var email = options.email;
  message = "";
  style = {
    header: `
      padding:30px 15px;
      text-align:center;
      background-color:#f2f2f2;
      `,
    body: `
      padding:15px;
      height: 230px;
      `,
    hTitle: `font-family: 'Raleway', sans-serif;
      font-size: 37px;
      height:auto;
      line-height: normal;
      font-weight: bold;
      background:none;
      padding:0;
      color:#333;
      `,
    maindiv: `
      width:600px;
      margin:auto;
      font-family: Lato, sans-serif;
      font-size: 14px;
      color: #333;
      line-height: 24px;
      font-weight: 300;
      border: 1px solid #eaeaea;
      `,
    textPrimary: `color:#3e3a6e;
      `,
    h5: `font-family: Raleway, sans-serif;
      font-size: 22px;
      background:none;
      padding:0;
      color:#333;
      height:auto;
      font-weight: bold;
      line-height:normal;
      `,
    m0: `margin:0;`,
    mb3: "margin-bottom:15px;",
    textCenter: `text-align:center;`,
    btn: `padding:10px 30px;
      font-weight:500;
      font-size:14px;
      line-height:normal;
      border:0;
      display:inline-block;
      text-decoration:none;
      `,
    btnPrimary: `
      background-color:#3e3a6e;
      color:#fff;
      `,
    footer: `
      padding:10px 15px;
      font-weight:500;
      color:#fff;
      text-align:center;
      background-color:#000;
      `,
  };

  message +=
    `<div class="container" style="` +
    style.maindiv +
    `">
  <div class="header" style="` +
    style.header +
    `text-align:center">
      <img src="http://198.251.65.146:4019/img/logo.png" width="150" style="margin-bottom:20px;" />
      <h2 style="` +
    style.hTitle +
    style.m0 +
    `">Welcome to JC Software Solution </h2>
  </div>
  <div class="body" style="` +
    style.body +
    `">
      <h5 style="` +
    style.h5 +
    style.m0 +
    style.mb3 +
    style.textCenter +
    `">Hello ` +
    options.fullName +
    `</h5>
      <p style="` +
    style.m0 +
    style.mb3 +
    style.textCenter +
    `margin-bottom:20px;font-weight: 600">You are one step away from verifying your account and joining the our community.
      Please verify your account by clicking the link below.</p>
      <div style="` +
    style.textCenter +
    `">
          <a style="text-decoration:none" href="` +
    constant.BACK_WEB_URL +
    "verifyUser?id=" +
    options.id +
    `"><span style="` +
    style.btn +
    style.btnPrimary +
    `">Verify Email</span></a>
      </div>
  </div>
  <div class="footer" style="` +
    style.footer +
    `">
  &copy 2023 JC Software Solution All rights reserved.
  </div>
</div>`;

  SmtpController.sendEmail(email, "Email Verification", message);
};

otpInEmail = function (options) {
  var email = options.email;
  var fullName = options.fullName;

  if (!fullName) {
    fullName = email;
  }
  message = "";
  style = {
    header: `
      padding:30px 15px;
      text-align:center;
      background-color:#f2f2f2;
      `,
    body: `
      padding:15px;
      height: 230px;
      `,
    hTitle: `font-family: 'Raleway', sans-serif;
      font-size: 37px;
      height:auto;
      line-height: normal;
      font-weight: bold;
      background:none;
      padding:0;
      color:#333;
      `,
    maindiv: `
      width:600px;
      margin:auto;
      font-family: Lato, sans-serif;
      font-size: 14px;
      color: #333;
      line-height: 24px;
      font-weight: 300;
      border: 1px solid #eaeaea;
      `,
    textPrimary: `color:#3e3a6e;
      `,
    h5: `font-family: Raleway, sans-serif;
      font-size: 22px;
      background:none;
      padding:0;
      color:#333;
      height:auto;
      font-weight: bold;
      line-height:normal;
      `,
    m0: `margin:0;`,
    mb3: "margin-bottom:15px;",
    textCenter: `text-align:center;`,
    btn: `padding:10px 30px;
      font-weight:500;
      font-size:14px;
      line-height:normal;
      border:0;
      display:inline-block;
      text-decoration:none;
      `,
    btnPrimary: `
      background-color:#3e3a6e;
      color:#fff;
      `,
    footer: `
      padding:10px 15px;
      font-weight:500;
      color:#fff;
      text-align:center;
      background-color:#000;
      `,
  };

  message +=
    `<div class="container" style="` +
    style.maindiv +
    `">
  <div class="header" style="` +
    style.header +
    `text-align:center">
      <img src="http://198.251.65.146:4019/img/logo.png" width="150" style="margin-bottom:20px;" />
      <h2 style="` +
    style.hTitle +
    style.m0 +
    `">Welcome to JC Software Solution </h2>
  </div>
  <div class="body" style="` +
    style.body +
    `">
      <h5 style="` +
    style.h5 +
    style.m0 +
    style.mb3 +
    style.textCenter +
    `">Hello ` +
    toTitleCase(fullName) +
    `</h5>
      <p style="` +
    style.m0 +
    style.mb3 +
    style.textCenter +
    `margin-bottom:20px;font-weight: 600">Your otp for login on vanmodsy is :   ` +
    options.otp +
    `    .
      </p>
     
  </div>
  <div class="footer" style="` +
    style.footer +
    `">
  &copy 2023 JC Software Solution All rights reserved.
  </div>
</div>`;

  SmtpController.sendEmail(email, "Login", message);
};

function toTitleCase(str) {
  return str.replace(/\w\S*/g, function (txt) {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  });
}
