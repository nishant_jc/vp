/**
 * TargetController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

var ObjectId = require("mongodb").ObjectID;
var constantObj = sails.config.constants;
const db = sails.getDatastore().manager;

module.exports = {
  
    addTarget: async (req, res) => {

        try {

            const data = req.body;

            if (!data.project_name) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Target.PROJECT_NAME_REQUIRED },
                });
            }

            if (!data.amount) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Target.AMOUNT_REQUIRED },
                });
            }

            if (!data.date) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Target.DATE_REQUIRED },
                });
            }

            data.addedBy = req.identity.id;

            const target_data = await Target.create(data).fetch();

            if (target_data) {
                return res.status(200).json({
                    success: true,
                    message: constantObj.Target.ADD_SUCCESS,
                    data: target_data,
                });
            } else {
                return res.status(400).json({
                    success: false,
                    message: constantObj.Target.ADD_FAILED
                });
            }

        } catch (error) {
            return res.status(400).json({
                success: false,
                error: { code: 400, message: error },
            });
        }

    },

    getSingleTarget: async (req, res) => {

        try {
            const id = req.param("id");

            if (!id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Target.ID_REQUIRED },
                });

            }

            const target_details = await Target.findOne({ id: id, isDeleted:false});

            if (target_details) {
                return res.status(200).json({
                    success: true,
                    message: constantObj.Target.FOUND,
                    data: target_details,
                });
            } else {
                return res.status(400).json({
                    success: false,
                    message: constantObj.Target.FAILED
                });
            }


        } catch (error) {
            return res.status(400).json({
                success: false,
                error: { code: 400, message: error },
            });
        }

    },

    getTargets: async (req, res) => {

        try {
            var search = req.param('search');
            var isDeleted = req.param('isDeleted');
            var page = req.param('page');
            var count = parseInt(req.param('count'));
            let sortBy = req.param("sortBy");
            let addedBy = req.param('addedBy');

            var date = new Date();
            var current_date = date.toISOString().substring(0, 10);

            var query = {};

            if (search) {
                query.$or = [
                    { event: { $regex: search, '$options': 'i' } },
                ]
            }
            let sortquery = {};
            if (sortBy) {
                let typeArr = [];
                typeArr = sortBy.split(" ");
                let sortType = typeArr[1];
                let field = typeArr[0];
                sortquery[field ? field : 'updatedAt'] = sortType ? (sortType == 'desc' ? -1 : 1) : -1;
            } else {
            sortquery = { updatedAt: -1 }
            }

            query.isDeleted = false;

            if (addedBy) {
                query.addedBy_id = ObjectId(addedBy);
            }

            const pipeline = [
                {
                    $project: {
                        project_name: "$project_name",
                        date: "$date",
                        amount: "$amount",
                        currency: "$currency",
                        description: "$description",
                        isDeleted: "$isDeleted",
                        addedBy: "$addedBy",
                        updatedBy: "$updatedBy",
                        createdAt: "$createdAt",
                        updatedAt: "$updatedAt",
                    }
                },
                {
                    $match: query
                },
                {
                    $sort: sortquery
                },
            ]
            db.collection('target').aggregate([...pipeline]).toArray((err, totalResult) => {
                if (page && count) {
                    var skipNo = (page - 1) * count;
                    pipeline.push(
                        {
                            $skip: Number(skipNo)
                        },
                        {
                            $limit: Number(count)
                        })
                }
                db.collection('target').aggregate([...pipeline]).toArray((err, result) => {
                    return res.status(200).json({
                        "success": true,
                        "data": result,
                        "total": totalResult.length,
                    });
                })
            })
        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { code: 400, message: "" + err }
            })
        }
    },

    updateTarget: async (req, res) => {

        try {
            const data = req.body;

            if (!data.id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Target.ID_REQUIRED },
                });

            }

            const find_target = await Target.findOne({ id: data.id, isDeleted: false });
            if (!find_target) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Target.ID_NOT_FOUND },
                });
            }

            data.updatedBy = req.identity.id;

            const updated_data = await Target.updateOne({ id: data.id, isDeleted: false }).set(data);

            if (updated_data) {
                return res.status(200).json({
                    success: true,
                    message: constantObj.Target.UPDATE_SUCCESS,
                    data: updated_data,
                });

            } else {
                return res.status(400).json({
                    success: false,
                    message: constantObj.Target.UPDATE_FAILED
                });
            }


        } catch (error) {
            return res.status(400).json({
                success: false,
                error: { code: 400, message: error },
            });
        }

    },
    
    deleteTarget: async (req, res) => {

        try {
            const id = req.param("id");
            var deleted_by = req.identity.id;

            if (!id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Target.ID_REQUIRED },
                });
            }

            const target_data = await Target.findOne({ id: id, isDeleted: false });

            if (!target_data) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Target.ID_NOT_FOUND },
                });
            }

            var deleted_at = new Date();

            const delete_target = await Target.updateOne({ id: id, isDeleted: false }).set({ isDeleted: true,deletedBy : deleted_by,deletedAt : deleted_at });
            if (delete_target) {
                return res.status(200).json({
                    success: true,
                    message: constantObj.Target.DELETE_SUCCESS
                });
            } else {
                return res.status(400).json({
                    success: false,
                    message: constantObj.Target.DELETE_FAILED
                });
            }


        } catch (error) {
            return res.status(400).json({
                success: false,
                error: { code: 400, message: error },
            });
        }
    }

};

