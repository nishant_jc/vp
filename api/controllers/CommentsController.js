/**
 * CommentsController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

var ObjectId = require("mongodb").ObjectID;
var constantObj = sails.config.constants;
const db = sails.getDatastore().manager;

module.exports = {
  
    addComment: async (req, res) => {

        try {

            const data = req.body;

            if (!data.knowledge_share_id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Comments.KNOWLEDGE_SHARE_ID_REQUIRED },
                });
            }

            data.addedBy = req.identity.id;

            const comment = await Comments.create(data).fetch();

            if (comment) {
                return res.status(200).json({
                    success: true,
                    message: constantObj.Comments.ADD_SUCCESS,
                    data: comment,
                });
            } else {
                return res.status(400).json({
                    success: false,
                    message: constantObj.Comments.ADD_FAILED
                });
            }

        } catch (error) {
            return res.status(400).json({
                success: false,
                error: { code: 400, message: error },
            });
        }

    },
    getSingleComment: async (req, res) => {

        try {
            const id = req.param("id");

            if (!id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Comments.ID_REQUIRED },
                });

            }

            const comment_details = await Comments.findOne({ id: id, isDeleted:false});

            if (comment_details) {
                return res.status(200).json({
                    success: true,
                    message: constantObj.Comments.FOUND,
                    data: comment_details,
                });
            } else {
                return res.status(400).json({
                    success: false,
                    message: constantObj.Comments.FAILED
                });
            }


        } catch (error) {
            return res.status(400).json({
                success: false,
                error: { code: 400, message: error },
            });
        }

    },

    comments: async (req, res) => {

        try {
            var search = req.param('search');
            var isDeleted = req.param('isDeleted');
            var page = req.param('page');
            var count = parseInt(req.param('count'));
            let sortBy = req.param("sortBy");
            let addedBy = req.param('addedBy');
            let knowledge_share_id = req.param('knowledge_share_id');

            // var date = new Date();
            // var current_date = date.toISOString().substring(0, 10);

            var query = {};

            // query.start_date = { '$gte': `${current_date}` };

            // query.$and = [
            //     {
            //     start_date: { '$gte': `${current_date}` },
            //     end_date: { '$lte': "2023-10-31" }
            //     }]
            if (search) {
                query.$or = [
                    { comment: { $regex: search, '$options': 'i' } },
                ]
            }
            let sortquery = {};
            if (sortBy) {
                let typeArr = [];
                typeArr = sortBy.split(" ");
                let sortType = typeArr[1];
                let field = typeArr[0];
                sortquery[field ? field : 'updatedAt'] = sortType ? (sortType == 'desc' ? -1 : 1) : -1;
            } else {
            sortquery = { updatedAt: 1 }
            }

            // if (isDeleted) {
            //     if (isDeleted === 'true') {
            //         isDeleted = true;
            //     } else {
            //         isDeleted = false;
            //     }
            query.isDeleted = false;
            // } else {
            //     query.isDeleted = false;
            // }
            if (addedBy) {
                query.addedBy_id = ObjectId(addedBy);
            }
            if (knowledge_share_id) {
                query.knowledge_share_id = ObjectId(knowledge_share_id);
            }

            const pipeline = [
                {
                    $lookup: {
                      from: "users",
                      localField: "addedBy",
                      foreignField: "_id",
                      as: "addedBy",
                    },
                  },
                {
                    $project: {
                        knowledge_share_id: "$knowledge_share_id",
                        comment: "$comment",
                        isDeleted: "$isDeleted",
                        addedBy: "$addedBy",
                        updatedBy: "$updatedBy",
                        createdAt: "$createdAt",
                        updatedAt: "$updatedAt",
                    }
                },
                {
                    $match: query
                },
                {
                    $sort: sortquery
                },
            ]
            db.collection('comments').aggregate([...pipeline]).toArray((err, totalResult) => {
                if (page && count) {
                    var skipNo = (page - 1) * count;
                    pipeline.push(
                        {
                            $skip: Number(skipNo)
                        },
                        {
                            $limit: Number(count)
                        })
                }
                db.collection('comments').aggregate([...pipeline]).toArray((err, result) => {
                    return res.status(200).json({
                        "success": true,
                        "data": result,
                        "total": totalResult.length,
                    });
                })
            })
        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { code: 400, message: "" + err }
            })
        }
    },
    updateComment: async (req, res) => {

        try {
            const data = req.body;

            if (!data.id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Comments.ID_REQUIRED },
                });

            }

            const find_comment = await Comments.findOne({ id: data.id, isDeleted: false });
            if (!find_comment) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Comments.ID_NOT_FOUND },
                });
            }

            data.updatedBy = req.identity.id;

            const updated_data = await Comments.updateOne({ id: data.id, isDeleted: false }).set(data);

            if (updated_data) {
                return res.status(200).json({
                    success: true,
                    message: constantObj.Comments.UPDATE_SUCCESS,
                    data: updated_data,
                });

            } else {
                return res.status(400).json({
                    success: false,
                    message: constantObj.Comments.UPDATE_FAILED
                });
            }


        } catch (error) {
            return res.status(400).json({
                success: false,
                error: { code: 400, message: error },
            });
        }

    },
    deleteComment: async (req, res) => {

        try {
            const id = req.param("id");
            var deleted_by = req.identity.id;

            if (!id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Comments.ID_REQUIRED },
                });
            }

            const comment_data = await Comments.findOne({ id: id, isDeleted: false });

            if (!comment_data) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Comments.ID_NOT_FOUND },
                });
            }

            var deleted_at = new Date();

            const delete_comment = await Comments.updateOne({ id: id, isDeleted: false }).set({ isDeleted: true,deletedBy : deleted_by,deletedAt : deleted_at });
            if (delete_comment) {
                return res.status(200).json({
                    success: true,
                    message: constantObj.Comments.DELETE_SUCCESS
                });
            } else {
                return res.status(400).json({
                    success: false,
                    message: constantObj.Comments.DELETE_FAILED
                });
            }


        } catch (error) {
            return res.status(400).json({
                success: false,
                error: { code: 400, message: error },
            });
        }
    }

};

