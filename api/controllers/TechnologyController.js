/**
 * TechnologyController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */


 var constantObj = sails.config.constants;

module.exports = {
    /**
   *
   * @param {*} req
   * @param {*} res
   * @description: Used to add Technology
   * @createdAt 25/10/2021
   * @createdBy : Rohit kumar
   */

   addTechnology: (req, res) => {
    API(TechnologyService.saveTechnology, req, res);
  },

  /**
   *
   * @param {*} req
   * @param {*} res
   * @description: Used to edit Service
   * @createdAt 22/10/2021
   * @createdBy : Rohit kumar
   */

  edit: (req, res) => {
    API(TechnologyService.updateTechnology, req, res);
  },


  /**
   *
   * @param {*} res
   * @returns
   * @description: Api Used to get Technology
   * @createdAt : 25/10/2021
   * @createdBy Rohit kumar
   */

   getAllTechnology: async (req, res, next)=> {
    var search = req.param("search");
    var sortBy = req.param("sortBy");
    var page = req.param("page");
    if (page == undefined) {
      page = 1;
    }
    var count = req.param("count");
    if (count == undefined) {
      count = 100;
    }
    var skipNo = (page - 1) * count;

    var query = {};
    if (sortBy) {
      sortBy = sortBy.toString();
    } else {
      sortBy = "createdAt desc";
    }

    query.isDeleted = false;
    await Technology.count(query)
      .exec( (err, total)=> {

        if (err) {
          return res.status(400).json({
            success: false,
            error: err,
          });
        } else {
          Technology.find(query)
            .sort(sortBy)
            .exec( (err, Technology)=> {
              if (err) {
                return res.status(400).json({ success: false, error: { code: 400, message: err } });
              } else {
                return res.json({
                  success: true,
                  code: 200,
                  data: Technology,
                  total: total,
                });
              }
            });
        }
      });
  },


  /**
   *
   * @param {id} req
   * @param {*} res
   * @returns
   * @description: Api Used to get single Technology by ID
   * @createdAt : 25/10/2021
   * @createdBy Rohit kumar
   */

   getSingleTechnology: async (req, res) => {
    try {
      let TechnologyID = req.param("id");
      if (TechnologyID == null) {
        return res.status(404).json({
          success: false,
          message: constantObj.Technology.ID_REQUIRED,
        });
      } else {
        var TechnologyData = await Technology.findOne({ id: TechnologyID,isDeleted:false });
        if (TechnologyData) {
          return res.status(200).json({
            success: true,
            message: constantObj.Technology.GET_DATA,
            data: TechnologyData,
          });
        } else {
          return res.status(404).json({
            success: false,
            message: constantObj.Technology.NO_RESULT,
          });
        }
      }
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },
  

  /**
   *
   * @param {id} req
   * @param {*} res
   * @returns
   * @description: Api Used to delete Technology by ID
   * @createdAt : 25/10/2021
   * @createdBy Rohit kumar
   */

   TechnologyDeletedByID: async (req, res) => {
    try {
       let deletedID = req.param("id");
       if (deletedID == undefined) {
        return res.status(404).json({
          success: false,
          message: constantObj.Technology.ID_REQUIRED,
        });
      }
      var deletedByUser = req.identity.id;
      var deletedData = await Technology.updateOne({
        id: deletedID,
      }).set({ isDeleted: true, deletedBy: deletedByUser });
        
        if (deletedData) {
          return res.status(200).json({
            success: true,
            message: constantObj.Technology.DELETED_TECHNOLOGY,
          });
        }
   
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },
  

};

