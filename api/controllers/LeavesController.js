/**
 * LeavesController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
var _request = require("request");
var ObjectId = require("mongodb").ObjectID;
var constantObj = sails.config.constants;
const db = sails.getDatastore().manager;
const moment = require('moment');

var bcrypt = require("bcrypt-nodejs");
var nodemailer = require("nodemailer");
var smtpTransport = require("nodemailer-smtp-transport");
const SmtpController = require("../controllers/SmtpController");
const Helper = require("../controllers/HelperController");


module.exports = {
  /**
   *
   * @param {*} req
   * @param {*} res
   * @description: Used to add leaves
   * @createdAt 16/09/2021
   * @createdBy : Chandra Shekhar
   */

  addLeaves: (req, res) => {
    API(LeavesService.saveLeaves, req, res);
  },

  /**
   *
   * @param {*} res
   * @returns
   * @description: Api Used to get leaves
   * @createdAt : 16/09/2021
   * @createdBy Chandra Shekhar
   */

  getLeaves: (req, res) => {
    try {
      var search = req.param("search");
      var listsortBy = req.param("sortBy");
      var page = req.param("page");
      var count = req.param("count");
      var memberId = req.param("id");
      var isDeleted = req.param("isDeleted");

      var email = req.param("email");

      if (page == undefined) {
        page = 1;
      }
      if (count == undefined) {
        count = 10;
      }
      var skipNo = (page - 1) * count;
      var query = {};
      //var sorting = "";
      // if (listsortBy) {
      //   sorting = listsortBy.toString();
      //   if (sorting == "asc") {
      //     sortBy = -1;
      //   } else {
      //     sortBy = 1;
      //   }
      // } else {
      //   sortBy = -1;
      // }

      let sortquery = {};

      if (listsortBy) {
        let typeArr = [];
        typeArr = listsortBy.split(" ");
        let sortType = typeArr[1];
        let field = typeArr[0]; 
        sortquery[field ? field : 'createdAt'] = sortType ? (sortType == 'desc' ? -1 : 1) : -1;
      } else {
        sortquery = { updatedAt: -1 };
      };

      if (search) {
        query.$or = [{ fromEmail: { $regex: search, $options: "i" } }];
      }
      if (memberId) {
        query.addedBy = ObjectId(memberId);
      }
      if (isDeleted) {
        if (isDeleted === "true") {
          isDeleted = true;
        } else {
          isDeleted = false;
        }
        query.isDeleted = isDeleted;
      }

      if (email) {
        query.fromEmail = email;
      }

      db.collection("leaves")
        .aggregate([
          {
            $project: {
              id: "$_id",
              employeeName: "$employeeName",
              fromEmail: "$fromEmail",
              toEmail: "$toEmail",
              subject: "$subject",
              description: "$description",
              leaveStartDate: "$leaveStartDate",
              leaveEndDate: "$leaveEndDate",
              leaveStarttime: "$leaveStarttime",
              leaveEndtime: "$leaveEndtime",
              leave_status: "$leave_status",
              approvedBy: "$approvedBy",
              status: "$status",
              createdAt: "$createdAt",
              deletedBy: "$deletedBy",
              addedBy: "$addedBy",
              updatedBy: "$updatedBy",
              isDeleted: "$isDeleted",
              deletedAt: "$deletedAt",
              updatedAt: "$updatedAt",
            },
          },
          {
            $match: query,
          },
        ])
        .toArray((err, totalResult) => {
          db.collection("leaves")
            .aggregate([
              {
                $project: {
                  id: "$_id",
                  employeeName: "$employeeName",
                  fromEmail: "$fromEmail",
                  toEmail: "$toEmail",
                  subject: "$subject",
                  description: "$description",
                  leaveStartDate: "$leaveStartDate",
                  leaveEndDate: "$leaveEndDate",
                  leaveStarttime: "$leaveStarttime",
                  leaveEndtime: "$leaveEndtime",
                  leave_status: "$leave_status",
                  approvedBy: "$approvedBy",
                  status: "$status",
                  createdAt: "$createdAt",
                  deletedBy: "$deletedBy",
                  addedBy: "$addedBy",
                  updatedBy: "$updatedBy",
                  isDeleted: "$isDeleted",
                  deletedAt: "$deletedAt",
                  updatedAt: "$updatedAt",
                },
              },
              {
                $match: query,
              },
              { $sort: sortquery },
              {
                $skip: skipNo,
              },
              {
                $limit: Number(count),
              },
            ])
            .toArray((err, result) => {
              if (err) {
                return res.status(400).json({
                  success: false,
                  error: { message: "" + err },
                });
              } else {
                return res.status(200).json({
                  success: true,
                  message: constantObj.leaves.GET_DATA,
                  data: result,
                  total: totalResult.length,
                });
              }
            });
        });
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },

  getLeaveEmployee: (req, res) => {
    try {
      var search = req.param("search");
      var listsortBy = req.param("sortBy");
      var page = req.param("page");
      var count = req.param("count");
      var memberId = req.param("id");
      var isDeleted = req.param("isDeleted");

      if (page == undefined) {
        page = 1;
      }
      if (count == undefined) {
        count = 10000;
      }
      var skipNo = (page - 1) * count;
      var query = {};
      var sorting = "";
      if (listsortBy) {
        sorting = listsortBy.toString();
        if (sorting == "asc") {
          sortBy = -1;
        } else {
          sortBy = 1;
        }
      } else {
        sortBy = -1;
      }

      if (search) {
        query.$or = [{ fromEmail: { $regex: search, $options: "i" } }];
      }
      if (memberId) {
        query.addedBy = ObjectId(memberId);
      }
      if (isDeleted) {
        if (isDeleted === "true") {
          isDeleted = true;
        } else {
          isDeleted = false;
        }
        query.isDeleted = isDeleted;
      }

      db.collection("leaves")
        .aggregate([
          {
            $match: query,
          },
          {
            $group: {
              _id: "$fromEmail",
              employeeName: { $first: "$employeeName" },

            }
          },
          { $sort: { updatedAt: sortBy } },
          {
            $skip: skipNo,
          },
          {
            $limit: Number(count),
          },
        ])
        .toArray((err, result) => {

          if (err) {
            return res.status(400).json({
              success: false,
              error: { message: "" + err },
            });
          } else {
            return res.status(200).json({
              success: true,
              message: constantObj.leaves.GET_DATA,
              data: result,
              // total: totalResult.length,
            });
          }
        });

    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },

  /**
   *
   * @param {*} req
   * @param {*} res
   * @returns
   * @description: Api Used to update status of the leaves
   * @createdAt : 05/10/2021
   * @createdBy Rohit Kumar
   */
  // updatestatus: async (req, res) => {
  //   try {
  //     let leaveId = req.param("id");
  //     let leaveStatus = req.param("leaveStatus");
  //     let fromemail = req.param("fromemail");
  //     let toemployeeemail = req.param("toemployeeemail");
  //     let tomanagementemail = req.param("tomanagementemail");

  //     let employee = await Users.findOne({ email: toemployeeemail, isDeleted: false });
  //     let employeeName = employee.fullName;

  //     // let doj = employee.dateOfJoining;
  //     // let doj_year = doj.substring(0, 4);
  //     // let doj_month = doj.substring(5, 7);
  //     // var leave_start_month;
  //     // var leave_policy_start_month;
  //     // var paid;
  //     // var deduction;

  //     if (leaveId == undefined) {
  //       return res.status(404).json({
  //         success: false,
  //         message: constantObj.leaves.ID_REQUIRED,
  //       });
  //     }
  //     var start_date;
  //     var end_date;
  //     var Difference_In_Days;
  //     var no_of_days;
  //     // var date = new Date();
  //     // const current_date = date.toISOString().substring(0, 10);
  //     // var year = date.toISOString().substring(0, 4);
  //     // var current_month = date.toISOString().substring(5, 7);

  //     if (leaveStatus == "Accepted") {
  //       var leaveData = await Leaves.findOne({ id: leaveId });
  //       start_date = leaveData.leaveStartDate;
  //       end_date = leaveData.leaveEndDate;
  //       var date1 = new Date(start_date);
  //       var date2 = new Date(end_date);
  //       var Difference_In_Time = date2.getTime() - date1.getTime();

  //       Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
  //       no_of_days = Difference_In_Days + 1;
  //       console.log(no_of_days, "=======no_of_days");



  //       // if (year == doj_year) {
  //       //   leave_start_month = Number(doj_month) + 3;
  //       //   console.log(leave_start_month, "==============leave_start_month");
  //       //   if (leave_start_month > 12) {
  //       //     leave_policy_start_month = leave_start_month - 12;
  //       //   } else {
  //       //     leave_policy_start_month = leave_start_month;
  //       //     console.log(leave_policy_start_month, "============leave_policy_start_month")
  //       //   }


  //       //   if (current_month == leave_policy_start_month) {
  //       //     paid = 0;
  //       //     deduction = no_of_days;
  //       //   } else if (current_month > leave_policy_start_month) {

  //       //     var qry = {};

  //       //     qry.and = [
  //       //       {
  //       //         leaveStartDate: { '>=': doj },
  //       //         leaveEndDate: { '<=': current_date }
  //       //       }]

  //       //       qry.fromEmail = user_email;
  //       //       qry.leave_status = "Accepted";
  //       //       qry.isDeleted = false;
  //       //       console.log(JSON.stringify(qry),"==========qry");

  //       //     const leave_data = await Leaves.find(qry);
  //       //     if(leave_data && leave_data.length >0){
  //       //       for await (let new_employee_paid_leave of leave_data){
  //       //         console.log(new_employee_paid_leave.paid,"=============paid")
  //       //         new_employee_paid_count += new_employee_paid_leave.paid;
  //       //       }
  //       //     }
  //       //     if(new_employee_paid_count <= (current_month - leave_start_month)){

  //       //     console.log(new_employee_paid_count,"================new_employee_paid_count")
  //       //     left_leaves = current_month - (leave_start_month + new_employee_paid_count);
  //       //     }
  //       //   } else {
  //       //     left_leaves = 0
  //       //   }
  //       // } else {

  //       //   var query = {};

  //       //   query.and = [
  //       //     {
  //       //       leaveStartDate: { '>=': `${year}` + "-01-01" },
  //       //       leaveEndDate: { '<=': `${year}` + "-12-31" }
  //       //     }]

  //       //     query.fromEmail = user_email;
  //       //     query.leave_status = "Accepted";
  //       //     query.isDeleted = false;
  //       //     console.log(JSON.stringify(query),"==========query");

  //       //   const leave_data = await Leaves.find(query);
  //       //   if(leave_data && leave_data.length >0){
  //       //     for await (let new_employee_paid_leave of leave_data){
  //       //       console.log(new_employee_paid_leave.paid,"=============paid")
  //       //       new_employee_paid_count += new_employee_paid_leave.paid;
  //       //     }
  //       //     if(new_employee_paid_count <= current_month){
  //       //       left_leaves = current_month - new_employee_paid_count;
  //       //     console.log(left_leaves,"=============left_leaves")
  //       //     }
  //       //   }else{
  //       //     left_leaves = current_month;
  //       //   }
  //       // }
  //     }
  //     var updatedleaveData = await Leaves.updateOne({
  //       id: leaveId,
  //     }).set({ leave_status: leaveStatus, no_of_days: no_of_days });
  //     //let toEmailArray = tomanagementemail.split(",");

  //     // sendLeaveEmailToManagement({
  //     //   toemail: toEmailArray,
  //     //   name: employeeName,
  //     //   status: leaveStatus
  //     // });

  //     // sendLeaveEmailToEmployee({
  //     //   toemail: toemployeeemail,
  //     //   name: employeeName,
  //     //   status: leaveStatus
  //     // });

  //     // return res.status(200).json({
  //     //   success: true,
  //     //   message: constantObj.leaves.UPDATED_LEAVE_STATUS,
  //     //   data: updatedleaveData,
  //     // });
  //   } catch (error) {
  //     console.log(error, "=================error")
  //     return res.status(400).json({
  //       success: false,
  //       error: { message: error },
  //     });
  //   }
  // },



  /**
  *
  * @param {*} res
  * @returns
  * @description: Api Used to get leaves count by status
  * @createdAt : 25/10/2021
  * @createdBy Rohit kumar
  */

  getLeavessstatuscount: async (req, res) => {
    try {
      let leaves = await Leaves.find({});
      let Accept = 0;
      let Reject = 0;
      let Cancel = 0;
      let Pending = 0;


      leaves.forEach((element) => {
        if (element.leave_status == "Accept") {
          Accept++;
        }
        if (element.leave_status == "Reject") {
          Reject++
        }
        if (element.leave_status == "Cancel") {
          Cancel++
        }
        if (element.leave_status == "Pending") {
          Pending++
        }
      });
      let result = {
        Accept: Accept,
        Reject: Reject,
        Cancel: Cancel,
        Pending: Pending,
      }
      return res.status(200).json({
        success: true,
        message: constantObj.leaves.GET_DATA,
        data: result,
        total: leaves.length,
      });


    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { code: 400, message: error },
      });
    }
  },

  /**
 *
 * @param {id} req
 * @param {*} res
 * @returns
 * @description: Api Used to get single post by ID
 * @createdAt : 19/11/2021
 * @createdBy Rohit kumar
 */

  getSingleLeave: async (req, res) => {
    try {
      let leaveID = req.param("id");
      if (leaveID == null) {
        return res.status(404).json({
          success: false,
          message: constantObj.leaves.ID_REQUIRED,
        });
      } else {
        var leaveData = await Leaves.findOne({ id: leaveID }).populate(
          "addedBy"
        );

        if (leaveData) {
          return res.status(200).json({
            success: true,
            message: constantObj.leaves.GET_DATA,
            data: leaveData,
          });
        } else {
          return res.status(404).json({
            success: false,
            message: constantObj.leaves.NO_RESULT,
          });
        }
      }
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },
  getLeavesStatus: async (req, res) => {

    try {

      const id = req.identity.id;
      var user_email = req.identity.email;
      var date = new Date();
      const current_date = date.toISOString().substring(0, 10);
      const year = date.toISOString().substring(0, 4);
      var current_month = date.toISOString().substring(5, 7);
      var left;
      var used;
      var new_employee_paid_count;
      var leave_data;
      var new_employee_paid_count = 0;

      const employee = await Users.findOne({ id: id, status: "active", isDeleted: false });

      var left;
      var used;
      var paid;
      var deduction;
      var new_employee_paid_count = 0;
      var new_employee_deduction_count = 0;
      var employee_doj = new Date(employee.dateOfJoining);
      const doj_date = new Date(employee_doj);
      doj_date.setMonth(doj_date.getMonth() + 3);
      var leave_policy_date = doj_date.toISOString().substring(0, 10);
      var leave_policy_start_date = leave_policy_date;
      var leave_policy_start_month = leave_policy_start_date.substring(5, 7);


      const start_date = moment().startOf('month').format('YYYY-MM-DD');
      const end_date = moment().endOf('month').format('YYYY-MM-DD');
      var leave_end_month = end_date.substring(5, 7);

      if (employee.isTrainee == true) {
        var qry = {};

        qry.and = [
          {
            leaveStartDate: { '>=': start_date },
            leaveEndDate: { '<=': end_date }
          }]

        qry.fromEmail = employee.email;
        qry.leave_status = "Accepted";
        qry.isDeleted = false;

        var leave_data = await Leaves.find(qry);
        if (leave_data && leave_data.length > 0) {
          for await (let new_employee_paid_leave of leave_data) {
            new_employee_paid_count += new_employee_paid_leave.paid;
            new_employee_deduction_count += new_employee_paid_leave.deduction;
          }
          left = (leave_end_month - (leave_policy_start_month - 1)) - new_employee_paid_count;
          used = new_employee_paid_count;
          deduction = new_employee_deduction_count;
          // if (new_employee_paid_count <= left) {
          //   //left = leave_end_month - new_employee_paid_count;
          //   if (left >= no_of_days) {
          //     paid = no_of_days;
          //     deduction = 0;
          //   } else {
          //     paid = left;
          //     deduction = no_of_days - left;
          //   }
          // if (new_employee_paid_count <= leave_end_month) {
          //   left = leave_end_month - new_employee_paid_count;
          //   used = new_employee_paid_count;
          //   deduction = new_employee_deduction_count;
          // } else {
          //   left = 0;
          //   used = new_employee_paid_count;
          //   deduction = new_employee_deduction_count;
          // }
        } else {
          left = (leave_end_month - (leave_policy_start_month - 1)) - new_employee_paid_count;
          used = 0;
          deduction = 0;
        }
      } else {
        var query = {};

        query.and = [
          {
            leaveStartDate: { '>=': start_date },
            leaveEndDate: { '<=': end_date }
          }]

        query.fromEmail = employee.email;
        query.leave_status = "Accepted";
        query.isDeleted = false;

        var leave_data = await Leaves.find(query);
        if (leave_data && leave_data.length > 0) {
          for await (let new_employee_paid_leave of leave_data) {
            new_employee_paid_count += new_employee_paid_leave.paid;
            new_employee_deduction_count += new_employee_paid_leave.deduction;
          }
          if (new_employee_paid_count <= leave_end_month) {
            left = leave_end_month - new_employee_paid_count;
            used = new_employee_paid_count;
            deduction = new_employee_deduction_count;
          } else {
            left = 0;
            used = new_employee_paid_count;
            deduction = new_employee_deduction_count;
          }
        } else {
          left = leave_end_month;
          used = 0;
          deduction = 0;
        }
      }
      // if (employee.isTrainee == true) {
      //   console.log("true");
      //   left = 0;
      //   used = 0;
      // } else {
      //   console.log("false");
      //   var query = {};

      //   query.and = [
      //     {
      //       leaveStartDate: { '>=': `${year}` + "-01-01" },
      //       leaveEndDate: { '<=': current_date }
      //     }]

      //   query.fromEmail = user_email;
      //   query.leave_status = "Accepted";
      //   query.isDeleted = false;
      //   console.log(JSON.stringify(query), "==========query");

      //   leave_data = await Leaves.find(query);
      //   if (leave_data && leave_data.length > 0) {
      //     for await (let new_employee_paid_leave of leave_data) {
      //       console.log(new_employee_paid_leave.paid, "=============paid")
      //       new_employee_paid_count += new_employee_paid_leave.paid;
      //     }
      //     if (new_employee_paid_count < current_month) {
      //       left = current_month - new_employee_paid_count;
      //       used = new_employee_paid_count;
      //     } else {
      //       left = 0;
      //       used = current_month;
      //     }
      //   } else {
      //     left = current_month;
      //     used = 0;
      //   }
      // }
      return res.status(200).json({
        success: true,
        message: "Details found successfully",
        left_leaves_status: left,
        used_leaves_status: used,
        deduction: deduction,
        leave_details: leave_data
      });

    } catch (error) {
      console.log(error,"=============error")
      return res.status(400).json({
        success: false,
        error: { code: 400, message: error },
      });
    }


  },
  // updatestatus: async (req, res) => {
  //   try {
  //     let leaveId = req.param("id");
  //     let leaveStatus = req.param("leaveStatus");
  //     let fromemail = req.param("fromemail");
  //     let toemployeeemail = req.param("toemployeeemail");
  //     let tomanagementemail = req.param("tomanagementemail");

  //     let employee = await Users.findOne({ email: toemployeeemail, status: "active", isDeleted: false });
  //     let employeeName = employee.fullName;

  //     if (leaveId == undefined) {
  //       return res.status(404).json({
  //         success: false,
  //         message: constantObj.leaves.ID_REQUIRED,
  //       });
  //     }
  //     var start_date;
  //     var end_date;
  //     var Difference_In_Days;
  //     var no_of_days;
  //     var paid;
  //     var deduction;
  //     var leaves;
  //     var left;
  //     var new_employee_paid_count = 0;
  //     var leave_end_month;
  //     var date = new Date();
  //     const current_date = date.toISOString().substring(0, 10);
  //     var year = date.toISOString().substring(0, 4);
  //     var current_month = date.toISOString().substring(5, 7);

  //     if (leaveStatus == "Accepted") {
  //       var leaveData = await Leaves.findOne({ id: leaveId });
  //       start_date = leaveData.leaveStartDate;
  //       end_date = leaveData.leaveEndDate;
  //       leave_end_month = end_date.substring(5, 7);
  //       var date1 = new Date(start_date);
  //       var date2 = new Date(end_date);
  //       var Difference_In_Time = date2.getTime() - date1.getTime();

  //       Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
  //       no_of_days = Difference_In_Days + 1;

  //       if (employee.isTrainee == true) {
  //         paid = 0;
  //         deduction = no_of_days;
  //       } else {
  //         var query = {};

  //         query.and = [
  //           {
  //             leaveStartDate: { '>=': `${year}` + "-01-01" },
  //             leaveEndDate: { '<=': end_date }
  //           }]

  //         query.fromEmail = toemployeeemail;
  //         query.leave_status = "Accepted";
  //         query.isDeleted = false;
  //         leaves = await Leaves.find(query);

  //         if (leaves && leaves.length > 0) {
  //           for await (let new_employee_paid_leave of leaves) {
  //             new_employee_paid_count += new_employee_paid_leave.paid;
  //           }
  //           if (new_employee_paid_count <= leave_end_month) {
  //             left = leave_end_month - new_employee_paid_count;
  //             if (left >= no_of_days) {
  //               paid = no_of_days;
  //               deduction = 0;
  //             } else {
  //               paid = left;
  //               deduction = no_of_days - left;
  //             }
  //           } else {
  //             paid = 0;
  //             deduction = no_of_days;
  //           }
  //         } else {
  //           left = leave_end_month;
  //           if (left >= no_of_days) {
  //             paid = no_of_days;
  //             deduction = 0;
  //           } else {
  //             paid = left;
  //             deduction = no_of_days - left;
  //           }
  //         }
  //       }
  //     }

  //     var updatedleaveData = await Leaves.updateOne({
  //       id: leaveId,
  //     }).set({ leave_status: leaveStatus, no_of_days: no_of_days, paid: paid, deduction: deduction });
  //     //let toEmailArray = tomanagementemail.split(",");

  //     // sendLeaveEmailToManagement({
  //     //   toemail: toEmailArray,
  //     //   name: employeeName,
  //     //   status: leaveStatus
  //     // });

  //     // sendLeaveEmailToEmployee({
  //     //   toemail: toemployeeemail,
  //     //   name: employeeName,
  //     //   status: leaveStatus
  //     // });

  //     return res.status(200).json({
  //       success: true,
  //       message: constantObj.leaves.UPDATED_LEAVE_STATUS,
  //       data: updatedleaveData,
  //     });
  //   } catch (error) {
  //     return res.status(400).json({
  //       success: false,
  //       error: { message: error },
  //     });
  //   }
  // },
  allEmployeeLeavesStatus: async (req, res) => {

    try {
      var date = new Date();
      var current_date = date.toISOString().substring(0, 10);
      var current_year = date.toISOString().substring(0, 4);
      var current_month = date.toISOString().substring(5, 7);
      const starting_date = moment().startOf('month').format('YYYY-MM-DD');
      const ending_date = moment().endOf('month').format('YYYY-MM-DD');

      var start_date = req.param("start_date") || starting_date;
      var end_date = req.param("end_date") || ending_date;
      var leave_end_month = end_date.substring(5, 7);

      var search = req.param('search');
      //var isDeleted = req.param('isDeleted');
      var page = req.param('page');
      var count = parseInt(req.param('count'));
      let sortBy = req.param("sortBy");
      //let addedBy = req.param('addedBy');
      let id = req.param('id');

      var query = {};
      if (search) {
        query.$or = [
          { email: { $regex: search, '$options': 'i' } },
          { fullName: { $regex: search, '$options': 'i' } }
        ]
      }

      if (id) {
          query._id = ObjectId(id);
      }

      query.role = { "$nin": ["admin", "subadmin"] };
      query.status = "active";
      query.isDeleted = false;
      // let sortquery = {};
      // if (sortBy) {
      //   let typeArr = [];
      //   typeArr = sortBy.split(" ");
      //   let sortType = typeArr[1];
      //   let field = typeArr[0];
      //   sortquery[field ? field : 'updatedAt'] = sortType ? (sortType == 'desc' ? -1 : 1) : -1;
      // } else {
      //   sortquery = { updatedAt: -1 }
      // }

      // if (addedBy) {
      //     query.addedBy_id = ObjectId(addedBy);
      // }
      //console.log(query, "=============query");

      const pipeline = [
        {
          $lookup: {
            from: 'leaves',
            localField: 'email',
            foreignField: 'fromEmail',
            as: 'leave_details'
          },
        },
        {
          $project: {
            fullName: "$fullName",
            email: "$email",
            status: "$status",
            isDeleted: "$isDeleted",
            role: "$role",
            isTrainee: "$isTrainee",
            dateOfJoining: "$dateOfJoining"
            //leaves_record: "$leave_details",
          }
        },
        {
          $match: query
        },
        // {
        //   $sort: sortquery
        // },
      ]
      //console.log(JSON.stringify(pipeline));
      //console.log(query, "=============query");
      db.collection('users').aggregate([...pipeline]).toArray((err, totalResult) => {
        //console.log(totalResult, "==============totalResult")
        if (err) {
          console.log(err)
          return res.status(400).json({
            success: false,
            error: { code: 400, message: "" + err }
          })
        }
        if (page && count) {
          var skipNo = (page - 1) * count;
          pipeline.push(
            {
              $skip: Number(skipNo)
            },
            {
              $limit: Number(count)
            })
        }
        db.collection('users').aggregate([...pipeline]).toArray(async (err, result) => {


          for await (let itemss of result) {
            var left;
            var used;
            var paid;
            var deduction;
            var new_employee_paid_count = 0;
            var new_employee_deduction_count = 0;
            var employee_doj = new Date(itemss.dateOfJoining);
            const doj_date = new Date(employee_doj);
            doj_date.setMonth(doj_date.getMonth() + 3);
            var leave_policy_date = doj_date.toISOString().substring(0, 10);
            var leave_policy_start_date = leave_policy_date;
            var leave_policy_start_month = leave_policy_start_date.substring(5, 7);


            if (itemss.isTrainee == true) {
              var qry = {};

              qry.and = [
                {
                  leaveStartDate: { '>=': start_date },
                  leaveEndDate: { '<=': end_date }
                }]

              qry.fromEmail = itemss.email;
              qry.leave_status = "Accepted";
              qry.isDeleted = false;

              var leave_data = await Leaves.find(qry);
              if (leave_data && leave_data.length > 0) {
                for await (let new_employee_paid_leave of leave_data) {
                  new_employee_paid_count += new_employee_paid_leave.paid;
                  new_employee_deduction_count += new_employee_paid_leave.deduction;
                }
                left = (leave_end_month - (leave_policy_start_month - 1)) - new_employee_paid_count;
                used = new_employee_paid_count;
                deduction = new_employee_deduction_count;
                // if (new_employee_paid_count <= left) {
                //   //left = leave_end_month - new_employee_paid_count;
                //   if (left >= no_of_days) {
                //     paid = no_of_days;
                //     deduction = 0;
                //   } else {
                //     paid = left;
                //     deduction = no_of_days - left;
                //   }
                // if (new_employee_paid_count <= leave_end_month) {
                //   left = leave_end_month - new_employee_paid_count;
                //   used = new_employee_paid_count;
                //   deduction = new_employee_deduction_count;
                // } else {
                //   left = 0;
                //   used = new_employee_paid_count;
                //   deduction = new_employee_deduction_count;
                // }
              } else {
                left = (leave_end_month - (leave_policy_start_month - 1)) - new_employee_paid_count;
                used = 0;
                deduction = 0;
              }
            } else {
              var query = {};

              query.and = [
                {
                  leaveStartDate: { '>=': start_date },
                  leaveEndDate: { '<=': end_date }
                }]

              query.fromEmail = itemss.email;
              query.leave_status = "Accepted";
              query.isDeleted = false;

              var leave_data = await Leaves.find(query);
              if (leave_data && leave_data.length > 0) {
                for await (let new_employee_paid_leave of leave_data) {
                  new_employee_paid_count += new_employee_paid_leave.paid;
                  new_employee_deduction_count += new_employee_paid_leave.deduction;
                }
                if (new_employee_paid_count <= leave_end_month) {
                  left = leave_end_month - new_employee_paid_count;
                  used = new_employee_paid_count;
                  deduction = new_employee_deduction_count;
                } else {
                  left = 0;
                  used = new_employee_paid_count;
                  deduction = new_employee_deduction_count;
                }
              } else {
                left = leave_end_month;
                used = 0;
                deduction = 0;
              }
            }
            itemss.left = left;
            itemss.used = used;
            itemss.deduction = deduction;
          }

          if (err) {
            console.log(err)
            return res.status(400).json({
              success: false,
              error: { code: 400, message: "" + err }
            })
          }
          return res.status(200).json({
            "success": true,
            "data": result,
            "total": totalResult.length,
          });
        })
      })
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }

  },
  singleEmployeeLeaves: async (req, res) => {

    try {
      var page = req.param('page') || 1;
      var count = parseInt(req.param('count')) || 10;
      let user_id = req.param("id");
      let start_date = req.param("start_date");
      let end_date = req.param("end_date");

      var query = {};

      query.$and = [
        {
          leaveStartDate: { '$gte': start_date },
          leaveEndDate: { '$lte': end_date }
        }]

      query.addedBy = ObjectId(user_id);
      query.leave_status = "Accepted";
      query.isDeleted = false;

      const pipeline = [
        {
          $project: {
            employeeName: "$employeeName",
            fromEmail: "$fromEmail",
            toEmail: "$toEmail",
            subject: "$subject",
            description: "$description",
            leave_status: "$leave_status",
            leaveStartDate: "$leaveStartDate",
            leaveEndDate: "$leaveEndDate",
            leaveStarttime: "$leaveStarttime",
            leaveStarttime: "$leaveStarttime",
            status: "$status",
            no_of_days: "$no_of_days",
            paid: "$paid",
            deduction: "$deduction",
            isDeleted: "$isDeleted",
            updatedBy: "$updatedBy",
            addedBy: "$addedBy",
            createdAt: "$createdAt",
            updatedAt: "$updatedAt",
            //leaves_record: "$leave_details",
          }
        },
        {
          $match: query
        },
        // {
        //   $sort: sortquery
        // },
      ]
      // console.log(JSON.stringify(pipeline));
      //console.log(JSON.stringify(query), "=============query");
      db.collection('leaves').aggregate([...pipeline]).toArray((err, totalResult) => {
        if (err) {
          console.log(err)
          return res.status(400).json({
            success: false,
            error: { code: 400, message: "" + err }
          })
        }
        if (page && count) {
          var skipNo = (page - 1) * count;
          pipeline.push(
            {
              $skip: Number(skipNo)
            },
            {
              $limit: Number(count)
            })
        }
        db.collection('leaves').aggregate([...pipeline]).toArray(async (err, result) => {

          if (err) {
            console.log(err)
            return res.status(400).json({
              success: false,
              error: { code: 400, message: "" + err }
            })
          }
          return res.status(200).json({
            "success": true,
            "data": result,
            "total": totalResult.length,
          });
        })
      })

    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },
  updatestatus: async (req, res) => {
    try {
      let leaveId = req.param("id");
      let leaveStatus = req.param("leaveStatus");
      let fromemail = req.param("fromemail");
      let toemployeeemail = req.param("toemployeeemail");
      let tomanagementemail = req.param("tomanagementemail");

      let employee = await Users.findOne({ email: toemployeeemail, status: "active", isDeleted: false });
      let employeeName = employee.fullName;
      let employee_doj = employee.dateOfJoining;

      if (leaveId == undefined) {
        return res.status(404).json({
          success: false,
          message: constantObj.leaves.ID_REQUIRED,
        });
      }
      var start_date;
      var end_date;
      var Difference_In_Days;
      var no_of_days;
      var paid;
      var deduction;
      var leaves;
      var left;
      var short_leaves;
      var new_employee_paid_count = 0;
      var new_employee_short_leave_count = 0;
      var leave_end_month;
      var next_month;
      var leave_end_year;
      var leave_policy_month;
      var leave_policy_date;
      var date = new Date();
      const current_date = date.toISOString().substring(0, 10);
      var year = date.toISOString().substring(0, 4);
      var current_year = date.toISOString().substring(0, 4);
      var current_month = date.toISOString().substring(5, 7);
      var doj_year = employee_doj.substring(0, 4);

      const doj_date = new Date(employee_doj);
      doj_date.setMonth(doj_date.getMonth() + 3);
      var leave_policy_date = doj_date.toISOString().substring(0, 10);
      var leave_policy_start_date = leave_policy_date;
      var leave_policy_start_month = leave_policy_start_date.substring(5, 7);

      if (leaveStatus == "Accepted") {
        var leaveData = await Leaves.findOne({ id: leaveId });
        start_date = leaveData.leaveStartDate;
        end_date = leaveData.leaveEndDate;
        leave_end_month = end_date.substring(5, 7);
        next_month = Number(leave_end_month) + 1;
        if (next_month > 12) {
          next_month = next_month - 12;
          year = Number(year) + 1;
        }

        leave_end_year = end_date.substring(0, 4);
        var date1 = new Date(start_date);
        var date2 = new Date(end_date);
        var Difference_In_Time = date2.getTime() - date1.getTime();

        Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
        no_of_days = Difference_In_Days + 1;

        // if (leaveData.subject == "half day Leave") {
        //   if (employee.isTrainee == true) {
        //     if (start_date > leave_policy_date) {
        //       var half_day_leave_query = {};

        //       half_day_leave_query.and = [
        //         {
        //           leaveStartDate: { '>=': `${leave_end_year}` + "-" + `${leave_end_month}` + "-01" },
        //           leaveEndDate: { '<': `${year}` + "-" + `${next_month}` + "-01" }
        //         }]

        //       half_day_leave_query.fromEmail = toemployeeemail;
        //       half_day_leave_query.leave_status = "Accepted";
        //       half_day_leave_query.isDeleted = false;
        //       leaves = await Leaves.find(half_day_leave_query);

        //       if (leaves && leaves.length > 0) {

        //         for await (let new_employee_paid_leave of leaves) {
        //           new_employee_paid_count += new_employee_paid_leave.paid;
        //         }
        //         left = (leave_end_month - (leave_policy_start_month - 1)) - new_employee_paid_count;
        //         if (left > 0) {
        //           paid = 0.50;
        //           deduction = 0;
        //         } else {
        //           paid = 0;
        //           deduction = 0.50;
        //         }
        //       } else {
        //         paid = 0.50;
        //         deduction = 0;
        //       }
        //     } else {
        //       paid = 0;
        //       deduction = 0.50;
        //     }
        //   } else { 
        //     var old_employee_half_day_leave_query = {};

        //     old_employee_half_day_leave_query.and = [
        //       {
        //         leaveStartDate: { '>=': `${leave_end_year}` + "-" + `${leave_end_month}` + "-01" },
        //         leaveEndDate: { '<': `${year}` + "-" + `${next_month}` + "-01" }
        //       }]

        //     old_employee_half_day_leave_query.fromEmail = toemployeeemail;
        //     old_employee_half_day_leave_query.leave_status = "Accepted";
        //     old_employee_half_day_leave_query.isDeleted = false;
        //     leaves = await Leaves.find(old_employee_half_day_leave_query);

        //     if (leaves && leaves.length > 0) {
        //       for await (let new_employee_paid_leave of leaves) {
        //         new_employee_paid_count += new_employee_paid_leave.paid;
        //       }
        //       if (new_employee_paid_count > 0) {
        //         paid = 0.50;
        //         deduction = 0;
        //       } else {
        //         paid = 0;
        //         deduction = 0.50;
        //       }
        //     } else {
        //       paid = 0.50;
        //       deduction = 0;
        //     }
        //   }
        // } else
        
         if (leaveData.subject == "short Leave") {
          if (employee.isTrainee == true) {
            if (start_date > leave_policy_date) {
              var short_leave_query = {};

              short_leave_query.subject = "short Leave";
              short_leave_query.and = [
                {
                  leaveStartDate: { '>=': `${leave_end_year}` + "-" + `${leave_end_month}` + "-01" },
                  leaveEndDate: { '<': `${year}` + "-" + `${next_month}` + "-01" }
                }]

              short_leave_query.fromEmail = toemployeeemail;
              short_leave_query.leave_status = "Accepted";
              short_leave_query.isDeleted = false;
              leaves = await Leaves.find(short_leave_query);

              if (leaves && leaves.length > 0) {
                for await (let new_employee_short_leave of leaves) {
                  new_employee_short_leave_count += new_employee_short_leave.short_leave_count;
                }
                left = (leave_end_month - (leave_policy_start_month - 1)) - new_employee_short_leave_count;
                if (new_employee_short_leave_count > 0) {
                  short_leaves = 1;
                  deduction = 0.25;
                }
              } else {
                short_leaves = 1;
                deduction = 0;
              }
            } else {
              short_leaves = 1;
              deduction = 0.25;
            }
          } else {
            var short_leave_old_employee_query = {};

            short_leave_old_employee_query.subject = "short Leave"
            short_leave_old_employee_query.and = [
              {
                leaveStartDate: { '>=': `${leave_end_year}` + "-" + `${leave_end_month}` + "-01" },
                leaveEndDate: { '<': `${year}` + "-" + `${next_month}` + "-01" }
              }]

            short_leave_old_employee_query.fromEmail = toemployeeemail;
            short_leave_old_employee_query.leave_status = "Accepted";
            short_leave_old_employee_query.isDeleted = false;
            leaves = await Leaves.find(query);

            if (leaves && leaves.length > 0) {
              for await (let old_employee_short_leave of leaves) {
                new_employee_short_leave_count += old_employee_short_leave.short_leave_count;
              }
              if (new_employee_short_leave_count > 0) {
                short_leaves = 1;
                deduction = 0.25;
              } else {
                short_leaves = 1;
                deduction = 0;
              }
            } else {
              short_leaves = 1;
              deduction = 0;
            }
          }
        } else {
          if (employee.isTrainee == true) {
            if (start_date > leave_policy_date && end_date > leave_policy_date || start_date < leave_policy_date && end_date > leave_policy_date) {
              var query = {};

              query.and = [
                {
                  leaveStartDate: { '>=': `${leave_end_year}` + "-01-01" },
                  leaveEndDate: { '<': `${year}` + "-" + `${next_month}` + "-01" }
                }]

              query.fromEmail = toemployeeemail;
              query.leave_status = "Accepted";
              query.isDeleted = false;
              leaves = await Leaves.find(query);

              if (leaves && leaves.length > 0) {
                for await (let new_employee_paid_leave of leaves) {
                  new_employee_paid_count += new_employee_paid_leave.paid;
                }
                left = (leave_end_month - (leave_policy_start_month - 1)) - new_employee_paid_count;
                if (new_employee_paid_count <= left) {
                  if (left >= no_of_days) {
                    paid = no_of_days;
                    deduction = 0;
                  } else {
                    paid = left;
                    deduction = no_of_days - left;
                  }
                } else {
                  paid = 0;
                  deduction = no_of_days;
                }
              } else {
                left = leave_end_month - (leave_policy_start_month - 1);
                if (left >= no_of_days) {
                  paid = no_of_days;
                  deduction = 0;
                } else {
                  paid = left;
                  deduction = no_of_days - left;
                }
              }
            } else {
              paid = 0;
              deduction = no_of_days;
            }
          } else {
            var query = {};

            query.and = [
              {
                leaveStartDate: { '>=': `${leave_end_year}` + "-01-01" },
                leaveEndDate: { '<': `${year}` + "-" + `${next_month}` + "-01" }
              }]

            query.fromEmail = toemployeeemail;
            query.leave_status = "Accepted";
            query.isDeleted = false;
            leaves = await Leaves.find(query);

            if (leaves && leaves.length > 0) {
              for await (let new_employee_paid_leave of leaves) {
                new_employee_paid_count += new_employee_paid_leave.paid;
              }
              if (new_employee_paid_count <= leave_end_month) {
                left = leave_end_month - new_employee_paid_count;
                if (left >= no_of_days) {
                  paid = no_of_days;
                  deduction = 0;
                } else {
                  paid = left;
                  deduction = no_of_days - left;
                }
              } else {
                paid = 0;
                deduction = no_of_days;
              }
            } else {
              left = leave_end_month;
              if (left >= no_of_days) {
                paid = no_of_days;
                deduction = 0;
              } else {
                paid = left;
                deduction = no_of_days - left;
              }
            }
          }
        }
      }
      const updated_by = req.identity.id;
      let approvedBy;

      if(leaveStatus == "Accepted"){
        approvedBy = req.identity.fullName;
    }
      var updatedleaveData = await Leaves.updateOne({
        id: leaveId,
      }).set({ leave_status: leaveStatus, no_of_days: no_of_days, paid: paid, deduction: deduction, short_leave_count: short_leaves,updatedBy:updated_by,approvedBy:approvedBy });
      let toEmailArray = tomanagementemail.split(",");

      sendLeaveEmailToManagement({
        toemail: toEmailArray,
        name: employeeName,
        status: leaveStatus,
      });

      sendLeaveEmailToEmployee({
        toemail: toemployeeemail,
        name: employeeName,
        status: leaveStatus
      });

      return res.status(200).json({
        success: true,
        message: constantObj.leaves.UPDATED_LEAVE_STATUS,
        data: updatedleaveData,
      });
    } catch (error) {
      console.log(error)
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },






























  //getLeavesStatus: async (req, res) => {

  //   try {

  //     var starting_date;
  //     var ending_date;
  //     var date = new Date();
  //     const year = date.toISOString().substring(0, 4);
  //     var current_month = date.toISOString().substring(5, 7);
  //     var user_email = req.identity.email;
  //     var leaves_count = 0;
  //     var left_leaves;
  //     var id = req.identity.id;

  //     const employee = await Users.findOne({ id: id, status: "active"});
  //     var employee_doj = employee.dateOfJoining;
  //     var doj_year = employee_doj.substring(0, 4);
  //     var doj_month = employee_doj.substring(5, 7);
  //     var leave_start_month;
  //     var leave_policy_start_month;
  //     console.log(employee_doj,"=============employee_doj");
  //     console.log(doj_year,"=============doj_year");
  //     console.log(doj_month,"=============doj_month");
  //     console.log(current_month,"=============current_month");

  //     if(year == doj_year){
  //       console.log("Checked the condition successfully");
  //        leave_start_month = Number(doj_month) + 3;
  //        console.log(leave_start_month,"==============leave_start_month");
  //        if(leave_start_month>12){
  //         leave_policy_start_month = leave_start_month - 12;
  //        }else{
  //         leave_policy_start_month = leave_start_month;
  //         console.log(leave_policy_start_month,"============leave_policy_start_month")
  //        }

  //        if(current_month == leave_policy_start_month){
  //         left_leaves = current_month - leave_start_month;
  //        }else if(current_month > leave_policy_start_month){
  //         left_leaves = current_month - leave_start_month;
  //         console.log(left_leaves,"===================left_leaves");
  //        }else{
  //         left_leaves = 0
  //        }
  //     }else {
  //       left_leaves = 12 - current_month;
  //     }


  //     if (current_month == 1) {
  //       starting_date = `${year}` + "-01-01";
  //       ending_date = `${year}` + "-01-31";

  //     }
  //     if (current_month == 2) {
  //       starting_date = `${year}` + "-02-01";
  //       ending_date = `${year}` + "-02-29";

  //     }
  //     if (current_month == 3) {
  //       starting_date = `${year}` + "-03-01";
  //       ending_date = `${year}` + "-03-31";

  //     }
  //     if (current_month == 4) {
  //       starting_date = `${year}` + "-04-01";
  //       ending_date = `${year}` + "-04-30";

  //     }
  //     if (current_month == 5) {
  //       starting_date = `${year}` + "-05-01";
  //       ending_date = `${year}` + "-05-31";

  //     }
  //     if (current_month == 6) {
  //       starting_date = `${year}` + "-06-01";
  //       ending_date = `${year}` + "-06-30";

  //     }
  //     if (current_month == 7) {
  //       starting_date = `${year}` + "-07-01";
  //       ending_date = `${year}` + "-07-31";

  //     }
  //     if (current_month == 8) {
  //       starting_date = `${year}` + "-08-01";
  //       ending_date = `${year}` + "-08-31";

  //     }
  //     if (current_month == 9) {
  //       starting_date = `${year}` + "-09-01";
  //       ending_date = `${year}` + "-09-30";

  //     }
  //     if (current_month == 10) {
  //       starting_date = `${year}` + "-10-01";
  //       ending_date = `${year}` + "-10-31";

  //     }
  //     if (current_month == 11) {
  //       starting_date = `${year}` + "-11-01";
  //       ending_date = `${year}` + "-11-30";

  //     }
  //     if (current_month == 12) {
  //       starting_date = `${year}` + "-12-01";
  //       ending_date = `${year}` + "-12-31";

  //     }

  //     var start_date = req.param('start_date') ? req.param('start_date') : starting_date;
  //     var end_date = req.param('end_date') ? req.param('end_date') : ending_date;

  //     //console.log(start_date, "=============start_date");
  //     //console.log(end_date, "=============end_date");

  //     var query = {};

  //     query.and = [
  //       {
  //         leaveStartDate: { '>=': start_date },
  //         leaveEndDate: { '<=': end_date }
  //       }]

  //     query.fromEmail = user_email;
  //     query.leave_status = "Accepted";
  //     query.isDeleted = false;

  //     //console.log(JSON.stringify(query), "======================query")

  //     var paid_leave;
  //     var deduction_leave;
  //     var left_leave_status;
  //     // const employee = await Users.findOne({ id: id });
  //     // var employee_total_leaves = employee.total_leaves;

  //     const leave = await Leaves.find(query);
  //     //console.log(leave, "======================leave");
  //     if (leave && leave.length > 0) {
  //       for await (let employee_leaves of leave) {
  //         var total_leaves = employee_leaves.no_of_days;
  //         console.log(total_leaves, "=========total_leaves");
  //         leaves_count += total_leaves;
  //       }
  //       if (leaves_count == 1) {
  //         paid_leave == leaves_count;
  //         deduction_leave = 0;
  //         left_leave_status = left_leaves;

  //       }
  //       if (leaves_count > 1) {
  //         if (left_leaves >= leaves_count) {
  //           left_leave_status = left_leaves - (leaves_count - 1);
  //           paid_leave = leaves_count;
  //           deduction_leave = 0;
  //           //  if(left_leave_status < 0){
  //           //   paid_leave = (leaves_count-1);
  //           //   deduction_leave = -(left_leave_status);
  //           // }
  //         } else {
  //           var status = (leaves_count - 1) - left_leaves;
  //           if (status >= 0) {
  //             left_leave_status = 0;
  //             paid_leave = left_leaves + 1;
  //             deduction_leave = status;
  //           }
  //           // if(status < 0){
  //           //   paid_leave = left_leaves + 1;
  //           //   deduction_leave = 0;
  //           // }
  //         }
  //       }

  //     // if(leaves_count== 1){
  //     //left_leaves = 12 - leaves_count;
  //     // }
  //     // if(leaves_count > 1){
  //     //   left_leaves = employee_total_leaves - leaves_count;
  //     // }

  //     // if (leave) {

  //       return res.status(200).json({
  //         success: true,
  //         message: "Details found successfully",
  //         paid_leave: paid_leave,
  //         deduction_leave: deduction_leave,
  //         left_leave_status: left_leave_status,
  //         //left_leaves:left_leaves,
  //         leave_details: leave
  //       });
  //     }
  //     else if(leave && leave.length == 0){
  //       return res.status(200).json({
  //         success: true,
  //         message:"Details found successfully",
  //         paid_leave:0,
  //         deduction_leave:0,
  //         left_leave_status:left_leaves,
  //         //left_leaves:left_leaves,
  //         leave_details:leave,
  //     });
  //     }else{
  //       return res.status(400).json({
  //         success: false,
  //         message:"Failed to find details.Maybe id is wrong"
  //     });
  //     }
  //   } catch (error) {
  //     console.log(error, "===================error");
  //     return res.status(400).json({
  //       success: false,
  //       error: { message: error },
  //     });
  //   }
  // }


  // getLeavesStatus: async (req, res) => {

  //   const user_email = req.identity.email;
  //   var date = new Date();
  //   const current_date = date.toISOString().substring(0, 10);
  //   //const year = date.toISOString().substring(0, 4);
  //   const year = 2023;
  //   var current_month = 12;
  //   //var current_month = date.toISOString().substring(5, 7);
  //   var id = req.identity.id;
  //   var left_leaves;

  //   const employee = await Users.findOne({ id: id, status: "active" });
  //   var employee_doj = employee.dateOfJoining;
  //   var doj_year = employee_doj.substring(0, 4);
  //   var doj_month = employee_doj.substring(5, 7);
  //   var leave_start_month;
  //   var leave_policy_start_month;
  //   console.log(employee_doj, "=============employee_doj");
  //   console.log(doj_year, "=============doj_year");
  //   console.log(doj_month, "=============doj_month");
  //   console.log(current_month, "=============current_month");
  //   var new_employee_paid_count = 0;

  //   if (year == doj_year) {
  //     console.log("Checked the condition successfully");
  //     leave_start_month = Number(doj_month) + 3;
  //     console.log(leave_start_month, "==============leave_start_month");
  //     if (leave_start_month > 12) {
  //       leave_policy_start_month = leave_start_month - 12;
  //     } else {
  //       leave_policy_start_month = leave_start_month;
  //       console.log(leave_policy_start_month, "============leave_policy_start_month")
  //     }
  //     if (current_month == leave_policy_start_month) {
  //       left_leaves = current_month - leave_start_month;
  //     } else if (current_month > leave_policy_start_month) {

  //       var qry = {};

  //       qry.and = [
  //         {
  //           leaveStartDate: { '>=': employee_doj },
  //           leaveEndDate: { '<=': "2023-09-31" }
  //         }]

  //         qry.fromEmail = user_email;
  //         qry.leave_status = "Accepted";
  //         qry.isDeleted = false;
  //         console.log(JSON.stringify(qry),"==========qry");

  //       const leave_data = await Leaves.find(qry);
  //       if(leave_data && leave_data.length >0){
  //         for await (let new_employee_paid_leave of leave_data){
  //           console.log(new_employee_paid_leave.paid,"=============paid")
  //           new_employee_paid_count += new_employee_paid_leave.paid;
  //         }
  //       }
  //       if(new_employee_paid_count <= (current_month - leave_start_month)){
  //       console.log(new_employee_paid_count,"================new_employee_paid_count")
  //       left_leaves = current_month - (leave_start_month + new_employee_paid_count);
  //       }
  //     } else {
  //       left_leaves = 0
  //     }
  //   } else {

  //     var query = {};

  //     query.and = [
  //       {
  //         leaveStartDate: { '>=': `${year}` + "-01-01" },
  //         leaveEndDate: { '<=': `${year}` + "-12-31" }
  //       }]

  //       query.fromEmail = user_email;
  //       query.leave_status = "Accepted";
  //       query.isDeleted = false;
  //       console.log(JSON.stringify(query),"==========query");

  //     const leave_data = await Leaves.find(query);
  //     if(leave_data && leave_data.length >0){
  //       for await (let new_employee_paid_leave of leave_data){
  //         console.log(new_employee_paid_leave.paid,"=============paid")
  //         new_employee_paid_count += new_employee_paid_leave.paid;
  //       }
  //       if(new_employee_paid_count <= current_month){
  //         left_leaves = current_month - new_employee_paid_count;
  //       console.log(left_leaves,"=============left_leaves")
  //       }
  //     }else{
  //       left_leaves = current_month;
  //     }
  //   }
  //   console.log(left_leaves, "===================left_leaves");
  //       return res.status(200).json({
  //         success: true,
  //         message: "Details found successfully",
  //         left_leave_status: left_leaves,
  //         used:new_employee_paid_count,
  //         //left_leaves:left_leaves,
  //         //leave_details: leave
  //       });
  // }




};




/** Leave status changes and Email send to management. */
var sendLeaveEmailToManagement = function (options) {
  let toemail = options.toemail;
  let employeeName = options.name;
  let status = options.status;

  message = "";
  style = {
    header: `
      padding:30px 15px;
      text-align:center;
      background-color:#f2f2f2;
      `,
    body: `
      padding:15px;
      height: 95px;
      `,
    hTitle: `font-family: 'Raleway', sans-serif;
      font-size: 37px;
      height:auto;
      line-height: normal;
      font-weight: bold;
      background:none;
      padding:0;
      color:#333;
      `,
    maindiv: `
      width:600px;
      margin:auto;
      font-family: Lato, sans-serif;
      font-size: 14px;
      color: #333;
      line-height: 24px;
      font-weight: 300;
      border: 1px solid #eaeaea;
      `,
    textPrimary: `color:#3e3a6e;
      `,
    h5: `font-family: Raleway, sans-serif;
      font-size: 22px;
      background:none;
      padding:0;
      color:#333;
      height:auto;
      font-weight: bold;
      line-height:normal;
      `,
    m0: `margin:0;`,
    mb3: "margin-bottom:15px;",
    textCenter: `text-align:center;`,
    textleft: `text-align:left;`,
    btn: `padding:10px 30px;
      font-weight:500;
      font-size:14px;
      line-height:normal;
      border:0;
      display:inline-block;
      text-decoration:none;
      `,
    btnPrimary: `
      background-color:#3e3a6e;
      color:#fff;
      `,
    footer: `
      padding:10px 15px;
      font-weight:500;
      color:#fff;
      text-align:center;
      background-color:#000;`,
  };

  message +=
    `<div class="container" style="` +
    style.maindiv +
    `">
  <div class="header" style="` +
    style.header +
    `text-align:center">
    <img src="https://mng.jcsoftwaresolution.in/assets/imgpsh_fullsize_anim.png" style="width: 107px;" />
  </div>
  <div class="body" style="` +
    style.body +
    `">
      <p style="` +
    style.m0 +
    style.mb3 +
    style.textCenter +
    `margin:35px 0px 20px">Leave application of <b>` + employeeName + `</b> is <b>` + status + `</b></p>
      </div>
      <div class="footer" style="` +
    style.footer +
    `">
      &copy 2024 JC Software Solution All rights reserved.
      </div>
  </div>`;

  //var to = ["chander@yopmail.com", "amit@yopmail.com"];
  SmtpController.sendLeaveEmailOnChange(
    toemail,
    "Response of leave application",
    message
  );
};



/** Leave status changes and Email send to Employee */
var sendLeaveEmailToEmployee = function (options) {
  let toemail = options.toemail;
  let name = options.name;
  let status = options.status


  message = "";
  style = {
    header: `
      padding:30px 15px;
      text-align:center;
      background-color:#f2f2f2;
      `,
    body: `
      padding:15px;
      height: 95px;
      `,
    hTitle: `font-family: 'Raleway', sans-serif;
      font-size: 30px;
      height:auto;
      line-height: normal;
      font-weight: bold;
      background:none;
      padding:0;
      color:#333;
      `,
    maindiv: `
      width:600px;
      margin:auto;
      font-family: Lato, sans-serif;
      font-size: 14px;
      color: #333;
      line-height: 24px;
      font-weight: 300;
      border: 1px solid #eaeaea;
      `,
    textPrimary: `color:#3e3a6e;
      `,
    h5: `font-family: Raleway, sans-serif;
      font-size: 22px;
      background:none;
      padding:0;
      color:#333;
      height:auto;
      font-weight: bold;
      line-height:normal;
      `,
    m0: `margin:0;`,
    mb3: "margin-bottom:15px;",
    textCenter: `text-align:center;`,
    textleft: `text-align:left;`,
    btn: `padding:10px 30px;
      font-weight:500;
      font-size:14px;
      line-height:normal;
      border:0;
      display:inline-block;
      text-decoration:none;
      `,
    btnPrimary: `
      background-color:#3e3a6e;
      color:#fff;
      `,
    footer: `
      padding:10px 15px;
      font-weight:500;
      color:#fff;
      text-align:center;
      background-color:#000;`,
  };

  message +=
    `<div class="container" style="` +
    style.maindiv +
    `">
  <div class="header" style="` +
    style.header +
    `text-align:center">
    <img src="https://mng.jcsoftwaresolution.in/assets/imgpsh_fullsize_anim.png" style="width: 107px;" />
      <h6 style="` +
    style.hTitle +
    style.m0 +
    `">Dear ` + name + `</h6>
  </div>
  <div class="body" style="` +
    style.body +
    `">
      <p style="` +
    style.m0 +
    style.mb3 +
    style.textCenter +
    `margin:35px 0px 20px">This Email is to inform you that your leave application is <b>` +
    status +
    `</b></p>
      </div>
      <div class="footer" style="` +
    style.footer +
    `">
      &copy 2023 JC Software Solution All rights reserved.
      </div>
  </div>`;

  //var to = ["chander@yopmail.com", "amit@yopmail.com"];
  SmtpController.sendLeaveEmailOnChange( 
    toemail,
    "Response of leave application",
    message
  );
};
