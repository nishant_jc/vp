/**
 * InterviewController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
// const constants = require('../../config/constants').constants;
const db = sails.getDatastore().manager;
var constantObj = sails.config.constants;
var constant = require('../../config/local.js');
const SmtpController = require("../controllers/SmtpController");


module.exports = {

    /**
       *
       * @description:
       * @createdAt : 21/06/2023
       * @createdBy :Vishal Tyagi
       */

    addInterview: async (req, res) => {
        const data = req.body;

        try {
            data.addedBy = req.identity.id
            let addInterview = await Interview.create(data).fetch();
            var find_employee = await Users.findOne({ id: data.employeeId, isDeleted: false });
            

            if (addInterview) {
                sendInterviewEmail({
                    toemail: find_employee.email,
                    fullName: find_employee.fullName,
                    wayOfInterview: data.wayOfInterview,
                    InterviewDateAndTime: data.InterviewDateAndTime,
                    attachment: data.resume
                });
                return res.status(200).json({
                    success: true,
                    code: 200,
                    data: addInterview,
                    message: constantObj.interview.ADD,
                });
            }
        } catch (err) {
          
            return res.status(400).json({
                success: false,
                error: { "code": 400, "message": "" + err }
            });
        }

    },

    interviewList: async (req, res) => {

        try {
            var search = req.param("search");
            var sortBy = req.param("sortBy");
            var page = req.param("page");
            var count = req.param("count");
            if (page == undefined) {
                page = 1;
            }
            if (count == undefined) {
                count = 1000;
            }
            var skipNo = (page - 1) * count;
            var query = {};

            if (sortBy) {
                sortBy = sortBy.toString();
            } else {
                sortBy = 1;
            }

            if (search) {
                query.$or = [{ name: { $regex: search, $options: "i" } }];
            }

            query.isDeleted = false;
            
            db.collection("interview")
                .aggregate([
                    {
                        $lookup: {
                            from: "users",
                            localField: "employeeId",
                            foreignField: "_id",
                            as: "employeeId",
                        },
                    },
                    {
                        $unwind: {
                            path: "$employeeId",
                            preserveNullAndEmptyArrays: true,
                        },
                    },

                    {
                        $project: {
                            id: "$_id",
                            wayOfInterview: "$wayOfInterview",
                            startTime: "$startTime",
                            InterviewDateAndTime: "$InterviewDateAndTime",
                            contactNumber: "$contactNumber",
                            resume: "$resume",
                            interviewLink: "$interviewLink",
                            employeeId: "$employeeId",
                            employeeId: "$employeeId",
                            addedBy: "$addedBy",
                            isDeleted: "$isDeleted",
                            createdAt: "$createdAt",
                        },
                    },
                    {
                        $match: query,
                    },
                ])
                .toArray((err, totalResult) => {
                    db.collection("interview")
                        .aggregate([

                            {
                                $lookup: {
                                    from: "users",
                                    localField: "employeeId",
                                    foreignField: "_id",
                                    as: "employeeId",
                                },
                            },
                            {
                                $unwind: {
                                    path: "$employeeId",
                                    preserveNullAndEmptyArrays: true,
                                },
                            },
                            {
                                $project: {
                                    id: "$_id",
                                    wayOfInterview: "$wayOfInterview",
                                    startTime: "$startTime",
                                    InterviewDateAndTime: "$InterviewDateAndTime",
                                    contactNumber: "$contactNumber",
                                    resume: "$resume",
                                    interviewLink: "$interviewLink",
                                    employeeId: "$employeeId",
                                    employeeId: "$employeeId",
                                    addedBy: "$addedBy",
                                    isDeleted: "$isDeleted",
                                    createdAt: "$createdAt",
                                },
                            },
                            {
                                $match: query,
                            },
                            { $sort: { createdAt: sortBy } },
                            {
                                $skip: skipNo,
                            },
                            {
                                $limit: Number(count),
                            },
                        ])
                        .toArray((err, result) => {
                            if (err) {
                                return res.status(400).json({
                                    success: false,
                                    error: { message: "" + err },
                                });
                            } else {
                                return res.status(200).json({
                                    success: true,
                                    code: 200,
                                    data: result,
                                    total: totalResult.length,
                                });
                            }
                        });
                });
        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { "code": 400, "message": "" + err }
            });
        }
    },

    getInterviewById: async (req, res) => {
        try {
            const id = req.param("id");
            if (!id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.interview.ID_MISSING },
                });
            } else {
                let find = await Interview.findOne({ id: id, }).populate('employeeId');

                if (!find) {
                    return res.status(404).json({
                        success: false,
                        error: { code: 404, message: constantObj.interview.NOT_FOUND },
                    });
                } else {
                    return res.status(200).json({
                        success: true,
                        code: 200,
                        data: find,
                    });
                }
            }

        } catch (err) {
            
            return res.status(400).json({
                success: false,
                error: { "code": 400, "message": "" + err }
            });
        }
    },

    editInterview: async (req, res) => {
        try {
            var data = req.body;
            const id = data.id;

            if (!id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.interview.ID_MISSING },
                });
            } else {
                var find = await Interview.findOne({ id: id, isDeleted: false });
                if (!find) {
                    return res.status(404).json({
                        success: false,
                        error: { code: 404, message: constantObj.interview.NOT_FOUND },
                    });
                } else {
                    data.updatedBy = req.identity.id
                    delete data.id;
                    let updated = await Interview.updateOne({ id: id }, data);

                    return res.status(200).json({
                        success: true,
                        code: 200,
                        message: constantObj.interview.UPDATED,
                    });
                }
            }

        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { "code": 400, "message": "" + err }
            });
        }
    },

    deleteInterview: async (req, res) => {
        try {
            const id = req.param("id");

            if (!id) {
                return res.status(400).json({
                    success: false,
                    error: { code: 400, message: constantObj.interview.ID_MISSING },
                });
            } else {
                var find = await Interview.findOne({ id: id, isDeleted: false });
                if (!find) {
                    return res.status(400).json({
                        success: false,
                        error: { code: 400, message: constantObj.interview.NOT_FOUND },
                    });
                } else {

                    let updated = await Interview.updateOne({ id: id }, { isDeleted: true });

                    return res.status(200).json({
                        success: true,
                        code: 200,
                        message: constantObj.interview.DELETE,
                    });
                }
            }

        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { "code": 400, "message": "" + err }
            });
        }
    }


};

    /**
       *
       * @description:
       * @createdAt : 28/06/2023
       * @createdBy :Shivani
       */

sendInterviewEmail = function (options) {

    var email = options.toemail;
    var resume = options.attachment;
    var now = new Date(options.InterviewDateAndTime);
    var date = now.toLocaleString();
    var wayOfInterview = options.wayOfInterview.replace(/_/g, ' ');

    message = '';
    style = {
        header: `
        padding:15px 15px;
        text-align:center;
        background-color:#f2f2f2;
        `,
        body: `
        padding:20px;
        `,
        hTitle: `font-family: 'Raleway', sans-serif;
        font-size: 37px;
        height:auto;
        line-height: normal;
        font-weight: bold;
        background:none;
        padding:0;
        color:#333;
        `,
        maindiv: `
        width:600px;
        margin:auto;
        font-family: Lato, sans-serif;
        font-size: 14px;
        color: #333;
        line-height: 24px;
        font-weight: 300;
        border: 1px solid #eaeaea;
        `,
        textPrimary: `color:#3e3a6e;
        `,
        h5: `font-family: Raleway, sans-serif;
        font-size: 22px;
        background:none;
        padding:0;
        color:#333;
        height:auto;
        font-weight: bold;
        line-height:normal;
        `,
        m0: `margin:0;`,
        mb3: 'margin-bottom:15px;',
        textCenter: `text-align:center;`,
        btn: `padding:10px 30px;
        font-weight:500;
        font-size:14px;
        line-height:normal;
        border:0;
        display:inline-block;
        text-decoration:none;
        `,
        btnPrimary: `
        background-color:#3e3a6e;
        color:#fff;
        `,
        footer: `
        padding:10px 15px;
        font-weight:500;
        color:#fff;
        text-align:center;
        background-color:#000;
        `
    }

    message += `<div class="container" style="` + style.maindiv + `">
    <div class="header" style="`+ style.header + `text-align:center">
    <img src="https://mng.jcsoftwaresolution.in/assets/img/logoImg.png" style="width: 107px;" />
        <h4 style="`+ style.hTitle + style.m0 + `">Interview invitation</h4>
    </div>
    <div class="body" style="`+ style.body + `">
        <h6 style="`+ style.h5 + style.m0 + style.mb3 + style.textCenter + `">Dear ` + options.fullName + `</h6>
        <p style="`+ style.m0 + style.mb3 + style.textCenter + `margin-bottom:20px;font-weight: 100">I am writing to invite you for taking interview. The interview will be conducted  at ` + date + `. The way of interview is ` + wayOfInterview + `.Please download the resume attachment. </p>
    </div>
    <div class="footer" style="`+ style.footer + `">
    &copy 2023 JC Software Solution All rights reserved.;
    </div>
  </div>`



    SmtpController.sendInvitationEmail(email, 'Invitation for taking interview', message, resume)
};

