/**
 * HelperController
 *
 * @description :: Server-side logic for managing Smtp
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */


 module.exports = {
    /**
     * 
     * @param {*} str 
     * @returns Title case string
     * @description: Used to convert string to titleCase
     * @createdAt : 01/09/2021
     */
    toTitleCase(str) {
        return str.replace(
          /\w\S*/g,
          function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
          }
        );
    }
};
