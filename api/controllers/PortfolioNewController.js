/**
 * PortfolioNewController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const { constants } = require("../../config/constants");
const db = sails.getDatastore().manager;


var constantObj = sails.config.constants;

exports.addPortfolio = async (req, res) => {
    try {
        let data = req.body;
        data.addedBy = req.identity.id;
        data.updatedBy = req.identity.id;

        let createPortfolio = await PortfolioNew.create(data).fetch();
        if (createPortfolio) {
            return res.status(200).json({
                success: true,
                message: constants.Portfolio.PORTFOLIO_SAVED
            });
        }

    }

    catch (err) {
        return res.status(400).json({
            success: false,
            error: { message: err },
        });
    }
};

exports.findSinglePortfolio = async (req, res) => {
    try {
        let id = req.param('id');
        if (!id) {
            throw constants.Portfolio.ID_REQUIRED;
        }

        let get_Portfolio = await PortfolioNew.findOne({ id: id });

        if (get_Portfolio) {
            if (get_Portfolio.addedBy) {
                let get_added_by_details = await Users.findOne({ id: get_Portfolio.addedBy });
                if (get_added_by_details) {
                    get_Portfolio.addedBy_name = get_added_by_details.fullName;
                }
            }
            if (get_Portfolio.updatedBy) {
                let get_updated_by_details = await Users.findOne({ id: get_Portfolio.updatedBy });
                if (get_updated_by_details) {
                    get_Portfolio.updatedBy = get_updated_by_details.fullName;
                }
            }
            return res.status(200).json({
                success: true,
                message: constants.Portfolio.GET_DATA,
                data: get_Portfolio
            });
        }
        throw constants.Portfolio.INVALID_ID;
    }
    catch (err) {
        return res.status(400).json({
            success: false,
            error: { message: err },
        });
    }
};

exports.editPortfolio = async (req, res) => {
    try {
        let portfolioData = req.body
        let id = req.body.id;
        if (!id) {
            throw constants.Portfolio.ID_REQUIRED;
        }
        portfolioData.updatedBy = req.identity.id;
        let updatePortfolioData = await PortfolioNew.updateOne({ id: id }, portfolioData);
        if (updatePortfolioData) {
            return res.status(200).json({
                success: true,
                message: constants.Portfolio.UPDATED_PORTFOLIO,
                data: updatePortfolioData
            });
        }
        throw constants.Portfolio.INVALID_ID;

    } catch (err) {
        return res.status(400).json({
            success: false,
            error: { message: err },
        });
    }
};

exports.getAllPortfolio = async (req, res) => {
    try {
        
        let search = req.param('search');
        let page = req.param('page');
        let count = req.param('count');

        if (!page) {
            page = 1;
        }
        if (!count) {
            count = 10;
        }
        let skipNo = (page - 1) * count;
        let query = {};
        query.$and = [];

        if (search) {
            query.$or = [
                { name: { $regex: search, '$options': 'i' } },
            ]
        }

        query.$and.push({ isDeleted: false })

        db.collection("portfolionew").aggregate([
            {
                $project: {
                    id: "$_id",
                    domain: "$domain",
                    name: "$name",
                    description: "$description",
                    image: "$image",
                    start_date: "$start_date",
                    end_date: "$end_date",
                    platform: "$platform",
                    technology: "$technology",
                    createdBy: "$createdBy",
                    isDeleted: "$isDeleted",
                    deletedAt: "$deletedAt",
                    addedBy: "$addedBy",
                    updatedBy: "$updatedBy",
                    updatedAt: "$updatedAt",
                    createdAt: "$createdAt",
                },
            },
            {
                $match: query,
            },
            {
                $sort: { createdAt: -1 },
            },
        ]).toArray((err, totalResult) => {
            if (err) {
                return res.status(400).json({
                    success: false,
                    error: { message: err },
                });
            }
            db.collection("portfolionew").aggregate([
                {
                    $project: {
                        id: "$_id",
                        domain: "$domain",
                        name: "$name",
                        description: "$description",
                        image: "$image",
                        start_date: "$start_date",
                        end_date: "$end_date",
                        platform: "$platform",
                        technology: "$technology",
                        createdBy: "$createdBy",
                        isDeleted: "$isDeleted",
                        deletedAt: "$deletedAt",
                        addedBy: "$addedBy",
                        updatedBy: "$updatedBy",
                        updatedAt: "$updatedAt",
                        createdAt: "$createdAt",
                    },
                },
                {
                    $match: query,
                },
                {
                    $sort: { createdAt: -1 },
                },
                {
                    $skip: skipNo,
                },
                {
                    $limit: Number(count),
                },
            ]).toArray((err, result) => {
                if (err) {
                    return res.status(400).json({
                        success: false,
                        error: { message: err },
                    });
                } else {

                    let resData = {
                        total_count: totalResult.length,
                        data: result
                    }

                    if (!req.param('page') && !req.param('count')) {
                        resData = {
                            total_count: totalResult.length,
                            data: result
                        }
                        return res.status(200).json({
                            success: true,
                            message: constants.Portfolio.ALL_PORTFOLIO,
                            data: resData
                        });
                    }

                    return res.status(200).json({
                        success: true,
                        message: constants.Portfolio.ALL_PORTFOLIO,
                        data: resData
                    });
                }
            });
        })
    }
    catch (err) {
        return res.status(400).json({
            success: false,
            error: { message: err },
        });
    }
};

exports.deletePortfolio = async (req, res) => {
    try {
        let id = req.param('id');
        if (!id) {
            throw constants.Portfolio.ID_REQUIRED;
        }

        let delete_portfolio_record = await PortfolioNew.updateOne({ id: id }, { 'isDeleted': true });
        if (delete_portfolio_record) {
            return res.status(200).json({
                success: true,
                message: constants.Portfolio.DELETED_PORTFOLIO,
                data: delete_portfolio_record
            });
        }

        throw constants.Portfolio.INVALID_ID;
    }

    catch (err) {
        return res.status(400).json({
            success: false,
            error: { message: err },
        });
    }
};

