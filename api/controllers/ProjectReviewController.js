/**
 * ProjectReviewController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const constants = require('../../config/constants').constants;
const db = sails.getDatastore().manager;
var constantObj = sails.config.constants;
var ObjectId = require("mongodb").ObjectID;
const SmtpController = require("../controllers/SmtpController");

module.exports = {

    /**
  *
  * @description: Api Used to add ProjectReview
  * @createdAt : 30/05/2023
  * @createdBy :Vishal Tyagi
  */

    addProjectReview: async (req, res) => {
        const data = req.body;
        // if (!data.employeeId || typeof data.employeeId == undefined) {
        //     return res.status(404).json({
        //         success: false,
        //         error: { code: 404, message: constantObj.projectReview.REQUIRED },
        //     });
        // }
        try {
            const query = {};
            query.employeeId = data.employeeId;
            query.projectId = data.projectId;
            query.isDeleted = false;
            query.reviewType = data.reviewType

            var existed = await ProjectReview.findOne(query);
            if (existed) {
                return res.status(400).json({
                    success: false,
                    error: { code: 400, message: constantObj.projectReview.EXIST },
                });
            } else {
                let addProjectReview = await ProjectReview.create(data).fetch();
                if (addProjectReview) {

                    const projectDetail = await Projects.findOne({ id: data.projectId, isDeleted: false });
                    if (projectDetail) {
                        var projectName = projectDetail.name;
                    }

                    const employeeDetails = await Users.findOne({ id: data.employeeId, isDeleted: false });
                    var employee = employeeDetails.name;

                    sendReviewEmail({
                        toemail: employeeDetails.email,
                        reviewType: data.reviewType,
                        project: projectDetail.name
                    });



                    return res.status(200).json({
                        success: true,
                        code: 200,
                        data: addProjectReview,
                        message: constantObj.projectReview.ADD,
                    });
                }
            }

        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { "code": 400, "message": "" + err }
            });
        }

    },
    /**
    *
    * @description: Api Used to Project Review Listing
    * @createdAt : 30/05/2023
    * @createdBy :Vishal Tyagi
    */

    projectReviewList: async (req, res) => {

        try {
            var search = req.param("search");
            var sortBy = req.param("sortBy");
            var page = req.param("page");
            var count = req.param("count");
            var employeeId = req.param('employeeId');
            if (page == undefined) {
                page = 1;
            }
            if (count == undefined) {
                count = 1000;
            }
            var skipNo = (page - 1) * count;
            var query = {};

            if (sortBy) {
                sorting = sortBy.toString();
                if (sorting == "asc") {
                    sortBy = -1;
                } else {
                    sortBy = 1;
                }
            } else {
                sortBy = -1;
            }


            if (search) {
                query.$or = [
                    { employee_name: { $regex: search, '$options': 'i' } },
                    { reviewType: { $regex: search, '$options': 'i' } },
                ]
            }
            query.isDeleted = false;

            if (employeeId) {
                query.employee_id = ObjectId(employeeId);
            }
            db.collection("projectreview")
                .aggregate([
                    {
                        $lookup: {
                            from: "users",
                            localField: "employeeId",
                            foreignField: "_id",
                            as: "employeeId",
                        },
                    },
                    {
                        $unwind: {
                            path: "$employeeId",
                            preserveNullAndEmptyArrays: true,
                        },
                    },
                    {
                        $lookup: {
                            from: "projects",
                            localField: "projectId",
                            foreignField: "_id",
                            as: "projectId",
                        },
                    },
                    {
                        $unwind: {
                            path: "$projectId",
                            preserveNullAndEmptyArrays: true,
                        },
                    },

                    {
                        $project: {
                            id: "$_id",
                            reviewType: "$reviewType",
                            projectId: "$projectId",
                            // employee_id:"$projectId",
                            employeeId: "$employeeId",
                            employee_id: "$employeeId._id",
                            employee_name: "$employeeId.fullName",
                            startTime: "$startTime",
                            endTime: "$endTime",
                            isDeleted: "$isDeleted",
                            createdAt: "$createdAt",
                        },
                    },
                    {
                        $match: query,
                    },
                ])
                .toArray((err, totalResult) => {
                    db.collection("projectreview")
                        .aggregate([
                            {
                                $lookup: {
                                    from: "users",
                                    localField: "employeeId",
                                    foreignField: "_id",
                                    as: "employeeId",
                                },
                            },
                            {
                                $unwind: {
                                    path: "$employeeId",
                                    preserveNullAndEmptyArrays: true,
                                },
                            },
                            {
                                $lookup: {
                                    from: "projects",
                                    localField: "projectId",
                                    foreignField: "_id",
                                    as: "projectId",
                                },
                            },
                            {
                                $unwind: {
                                    path: "$projectId",
                                    preserveNullAndEmptyArrays: true,
                                },
                            },
                            {
                                $project: {
                                    id: "$_id",
                                    reviewType: "$reviewType",
                                    projectId: "$projectId",
                                    employeeId: "$employeeId",
                                    employee_id: "$employeeId._id",
                                    employee_name: "$employeeId.fullName",
                                    // employee_id:"$projectId",
                                    startTime: "$startTime",
                                    endTime: "$endTime",
                                    isDeleted: "$isDeleted",
                                    createdAt: "$createdAt",
                                },
                            },
                            {
                                $match: query,
                            },
                            {
                                $sort: {
                                    createdAt: -1
                                }
                            },
                            {
                                $skip: skipNo,
                            },
                            {
                                $limit: Number(count),
                            },
                        ])
                        .toArray((err, result) => {
                            if (err) {
                                return res.status(400).json({
                                    success: false,
                                    error: { message: "" + err },
                                });
                            } else {
                                return res.status(200).json({
                                    success: true,
                                    code: 200,
                                    data: result,
                                    total: totalResult.length,
                                });
                            }
                        });
                });
        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { "code": 400, "message": "" + err }
            });
        }
    },
    /**
    *
    * @description: Api Used to Project Review edit
    * @createdAt : 30/05/2023
    * @createdBy :Vishal Tyagi
    */

    editProjectReview: async (req, res) => {
        try {
            var data = req.body;
            const id = data.id;
            data.updatedBy = req.identity.id

            if (!id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.projectReview.ID_MISSING },
                });
            } else {
                data.updatedBy = req.identity.id
                var find = await ProjectReview.findOne({ id: id, isDeleted: false });

                if (!find) {
                    return res.status(404).json({
                        success: false,
                        error: { code: 404, message: constantObj.projectReview.NOT_FOUND },
                    });
                } else {
                    delete data.id;
                    let updated = await ProjectReview.updateOne({ id: id }, data);

                    return res.status(200).json({
                        success: true,
                        code: 200,
                        message: constantObj.projectReview.UPDATED,
                    });
                }
            }

        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { "code": 400, "message": "" + err }
            });
        }
    },

    getProjectReviewById: async (req, res) => {
        try {
            const id = req.param("id");
            if (!id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.projectReview.ID_MISSING },
                });
            } else {
                let find = await ProjectReview.findOne({ id: id, });

                if (!find) {
                    return res.status(404).json({
                        success: false,
                        error: { code: 404, message: constantObj.projectReview.NOT_FOUND },
                    });
                } else {
                    return res.status(200).json({
                        success: true,
                        code: 200,
                        data: find,
                    });
                }
            }

        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { "code": 400, "message": "" + err }
            });
        }
    },


    removeProjectReview: async (req, res) => {
        try {
            const id = req.param("id");
            if (!id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.projectReview.ID_MISSING },
                });
            } else {
                var find = await ProjectReview.findOne({ id: id });
                if (!find) {
                    return res.status(404).json({
                        success: false,
                        error: { code: 404, message: constantObj.projectReview.NOT_FOUND },
                    });
                } else {
                    let removed = await ProjectReview.updateOne({ id: id }, { isDeleted: true });
                    return res.status(200).json({
                        success: true,
                        code: 200,
                        message: constantObj.projectReview.DELETE,
                    });
                }
            }

        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { "code": 400, "message": "" + err }
            });
        }
    },


};

//Send Email to employee when admin assign them to review code or project.
var sendReviewEmail = function (options) {
    var toemail = options.toemail;
    var reviewType = options.reviewType;
    var project = options.project;
    (message = "");
    message += ` <div class="maindiv" style="align-items: center; box-shadow: 0px 0px 8px 0px #8080808a; height: auto; background-color: #fff; 
    width:600px;
    margin:auto;">

        <div style="border: 1px solid #ff8f26cc ;    margin:2rem auto; max-width: 600px; height: auto; background-color: #ff8f26cc ;  text-align: center;">

            <h3 style="font-size: 25px; color: #fff; text-transform: capitalize;">${reviewType}</h3>

        </div>
        <div style="text-align: center;">



            <div style="margin-bottom: 14px;">
                <img src="https://mng.jcsoftwaresolution.in/assets/imgpsh_fullsize_anim.png" style="width: 107px;" />
            </div>
            <div>
                <p style="font-size: 15px;">A <b>${reviewType}</b> have been assign to you for <b>${project}</b> project.</p>

            </div >
            <div style=" margin-top:50px">

    <p style="font-size: 15px; color: gray; padding-bottom: 15px; ">© 2023 JC Software Solution  All rights reserved.</p>
    </div>

        </div >
    </div > `

    //var to = ["chander@yopmail.com", "amit@yopmail.com"];
    SmtpController.sendComplaintEmailResponse(
        toemail,
        `${reviewType}`,
        message
    );
};

