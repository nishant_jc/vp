/**
 * AttendanceController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */


var _request = require("request");
var ObjectId = require("mongodb").ObjectID;
var constantObj = require("../../config/constants").constants;
const db = sails.getDatastore().manager;
// var constantObj = sails.config.constants;

var async = require("async");
const SmtpController = require("../controllers/SmtpController");
const excel = require('exceljs');


function containsObject(obj, list) {
  var i;
  for (i = 0; i < list.length; i++) {
    if (list[i].id == obj) {
      return true;
    }
  }

  return false;
}
  let static_date = "2024-05-03T10:12:33.054Z" //added these for testing should be removed in production
  let end_static_date = "2024-05-03T20:12:33.054Z" //added these for testing should be removed in production

module.exports = {
  
  /**
   *
   * @param {*} req
   * @param {*} res
   * @description: Used to add checkin time to attendance sheet
   * @createdAt 21/09/2021
   * @createdBy : Chandra Shekhar
   */
  checkIn: async (req, res) => {
    var userID = req.identity.id;
    let {device} = req.body;
    
    if (!userID || typeof userID == undefined) {
      return res.status(404).json({
        success: false,
        error: { message: constantObj.attendance.USERID_REQUIRED },
      });
    }
    query = {};

    query.id = userID;
    query.status = "active";
    query.isDeleted = false;
    const userDetail = await Users.findOne(query);

    if (!userDetail) {
      return res.status(400).json({
        success: false,
        message: constantObj.attendance.NOT_FOUND
      });
    }
    var firstName = userDetail.firstName;
    var lastName = userDetail.lastName;
    var fullname = firstName + " " + lastName;
    var useremail = userDetail.email;
    var attendenceDate = new Date(static_date);
    var punchInDate = attendenceDate;

    const currentattendencedate = attendenceDate.toISOString().substring(0, 10);

    var today_date = new Date(static_date);
    today_date = today_date.toISOString().substring(0, 10);

    var dt = new Date(static_date);
    let new_query = {};
    new_query.userId = userDetail.id;
    new_query.checkOut = "";
    new_query.isDeleted = false;
    new_query.attendanceDate = today_date;

    let find_last_checkin = await Attendance.findOne(new_query);
    if (userDetail.checkInstatus == "true") {
      return res.status(200).json({
        success: true,
        alreadyCheckedIn: true,
        lastCheckInTime: find_last_checkin.checkIn,
        checkInId: find_last_checkin.id
      });
    } else {
      let savedattendance = await Attendance.create({
        checkIn: dt,
        checkOut: "",
        userId: userID,
        name: fullname,
        email: useremail,
        attendanceDate: currentattendencedate,
        punchInDate: punchInDate,
        totalseconds: "",
        totalTime: "",
        reason: "",
        check_in_device: device
      }).fetch();
      let updateddata = await Users.updateOne({
        id: userID,
      }).set({
        checkInstatus: "true",
      });
      return res.status(200).json({
        success: true,
        message: constantObj.attendance.CHECKIN_SAVED,
        data: savedattendance,
      });

    }
    // ActivityMail({
    //   type: "Check In",
    //   fullName: req.identity.fullName,
    //   date: dt,
    // });
    // let updateddata = await Users.updateOne({
    //   id: userID,
    // }).set({
    //   checkInstatus: "true",
    // });


  },

  /**
   *
   * @param {*} req
   * @param {*} res
   * @description: Used to add checkOut time to attendance sheet
   * @createdAt 21/09/2021
   * @createdBy : Chandra Shekhar
   */
  checkOut: async (req, res) => {
    try {
      var userID = req.identity.id;
      // console.log(userID, "=============userID");
      var attendanceId = req.param("attendanceId");
      var checkoutreason = req.param("reason");
      
      if(checkoutreason){
      var new_reason = checkoutreason.replace(/[Â,¤]/g, ' ');
      }


      if (!userID || typeof userID == undefined) {
        return res.status(404).json({
          success: false,
          error: { message: constantObj.attendance.USERID_REQUIRED },
        });
      }

      if (!attendanceId || typeof attendanceId == undefined) {
        return res.status(404).json({
          success: false,
          error: { message: constantObj.attendance.ATTENDANCE_ID_REQUIRED },
        });
      }
      var dt = new Date(end_static_date);
      let attendanceData = await Attendance.findOne({
        id: attendanceId,
      });

      if (!attendanceData) {
        return res.status(400).json({
          success: false,
          message: constantObj.attendance.FAILED
        });
      }


      var attendancecurrentDate = attendanceData.attendanceDate;
      var punchInCurrentDate = attendanceData.punchInDate;
      var checkIntime = attendanceData.checkIn;
      var startTime = checkIntime;
      var endTime = dt;

      var seconds = (endTime.getTime() - startTime.getTime()) / 1000;
      var lastSecond = seconds;
      if (seconds >= 3600) {
        var sec_num = parseInt(seconds, 10);
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor(sec_num / 60) % 60;
        var seconds = sec_num % 60;
        var Hourstotaltime = hours + "hr " + minutes + "m " + seconds + "s ";
      } else {
        if (seconds >= 60 && seconds < 3600) {
          var ndate = new Date(seconds * 1000).toISOString().substr(14, 5);
          var Newtimearray = ndate.split(":");
          var Hourstotaltime = Newtimearray[0] + "m " + Newtimearray[1] + "s";
        } else {
          var Hourstotaltime = Math.floor(seconds) + "s";
        }
      }

      let updateattendance = await Attendance.updateOne({
        id: attendanceId,
      }).set({
        checkOut: dt,
        totalseconds: Math.floor(lastSecond),
        totalTime: Hourstotaltime,
        reason: new_reason,
        check_out_device: req.body.device
      });

      var dt = new Date(static_date);
      let attendanceData1 = await Attendance.find({
        userId: userID,
        attendanceDate: attendancecurrentDate,

      });

      var seconds1 = 0;
      attendanceData1.forEach(function (value) {
        var getseconds = value.totalseconds;
        if (getseconds != "") {
          gettotaltime = parseInt(value.totalseconds);
          seconds1 = seconds1 + gettotaltime;
        }
      });
      if (seconds1 >= 3600) {
        var sec_num = parseInt(seconds1, 10);
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor(sec_num / 60) % 60;
        var seconds1 = sec_num % 60;
        var Hourstotaltime1 = hours + "hr " + minutes + "m " + seconds + "s ";
      } else {
        if (seconds1 >= 60 && seconds1 < 3600) {
          var ndate = new Date(seconds1 * 1000).toISOString().substr(14, 5);
          var Newtimearray = ndate.split(":");
          var Hourstotaltime1 = Newtimearray[0] + "m " + Newtimearray[1] + "s";
        } else {
          var Hourstotaltime1 = seconds1 + "s";
        }
      }

      let userdata = await Users.findOne({ id: userID });

      let timesheetdata = userdata.timeSheet;

      var atDate = attendancecurrentDate;

      //Find index of specific object using findIndex method.

      if (timesheetdata.length == 0) {
        timesheetdata.push({
          id: attendancecurrentDate,
          totaltime: Hourstotaltime1,
        });
      } else {
        objIndex = timesheetdata.findIndex((obj) => {
          let timedata = containsObject(attendancecurrentDate, timesheetdata)
          if (timedata == true) {
            obj.totaltime = Hourstotaltime1;
          }
          if (timedata == false) {
            timesheetdata.push({
              id: attendancecurrentDate,
              totaltime: Hourstotaltime1,
            });
          }
        });
      }

      /**Extra code start*/
      // var dt = new Date();
      // let attendanceData = await Attendance.findOne({
      //   id: attendanceId,
      // });
      /**Extra code end*/


      //*********************code for mobile device start****************************** */
      // if (attendanceData.check_in_device === "desktop") {
      //   console.log('in desktop');
      //   var attendancecurrentDate = attendanceData.attendanceDate;
      //   var punchInCurrentDate = attendanceData.punchInDate;
      //   var checkIntime = attendanceData.checkIn;
      //   var startTime = checkIntime;
      //   var endTime = dt;

      //   var seconds = (endTime.getTime() - startTime.getTime()) / 1000;
      //   var lastSecond = seconds;
      //   if (seconds >= 3600) {
      //     var sec_num = parseInt(seconds, 10);
      //     var hours = Math.floor(sec_num / 3600);
      //     var minutes = Math.floor(sec_num / 60) % 60;
      //     var seconds = sec_num % 60;
      //     var Hourstotaltime = hours + "hr " + minutes + "m " + seconds + "s ";
      //   } else {
      //     if (seconds >= 60 && seconds < 3600) {
      //       var ndate = new Date(seconds * 1000).toISOString().substr(14, 5);
      //       var Newtimearray = ndate.split(":");
      //       var Hourstotaltime = Newtimearray[0] + "m " + Newtimearray[1] + "s";
      //     } else {
      //       var Hourstotaltime = Math.floor(seconds) + "s";
      //     }
      //   }

      //   let updateattendance = await Attendance.updateOne({
      //     id: attendanceId,
      //   }).set({
      //     checkOut: dt,
      //     totalseconds: Math.floor(lastSecond),
      //     totalTime: Hourstotaltime,
      //     // desktop_device_time: desktop_device_time,
      //     reason: checkoutreason,
      //     check_out_device: req.body.device
      //   });

      //   var dt = new Date();
      //   let attendanceData1 = await Attendance.find({
      //     userId: userID,
      //     attendanceDate: attendancecurrentDate,

      //   });

      //   var seconds1 = 0;
      //   attendanceData1.forEach(function (value) {
      //     var getseconds = value.totalseconds;
      //     if (getseconds != "") {
      //       gettotaltime = parseInt(value.totalseconds);
      //       seconds1 = seconds1 + gettotaltime;
      //     }
      //   });
      //   if (seconds1 >= 3600) {
      //     var sec_num = parseInt(seconds1, 10);
      //     var hours = Math.floor(sec_num / 3600);
      //     var minutes = Math.floor(sec_num / 60) % 60;
      //     var seconds1 = sec_num % 60;
      //     var Hourstotaltime1 = hours + "hr " + minutes + "m " + seconds + "s ";
      //   } else {
      //     if (seconds1 >= 60 && seconds1 < 3600) {
      //       var ndate = new Date(seconds1 * 1000).toISOString().substr(14, 5);
      //       var Newtimearray = ndate.split(":");
      //       var Hourstotaltime1 = Newtimearray[0] + "m " + Newtimearray[1] + "s";
      //     } else {
      //       var Hourstotaltime1 = seconds1 + "s";
      //     }
      //   }

      //   let userdata = await Users.findOne({ id: userID });

      //   let timesheetdata = userdata.timeSheet;

      //   var atDate = attendancecurrentDate;

      //   //Find index of specific object using findIndex method.

      //   if (timesheetdata.length == 0) {
      //     timesheetdata.push({
      //       id: attendancecurrentDate,
      //       totaltime: Hourstotaltime1,
      //     });
      //   } else {
      //     objIndex = timesheetdata.findIndex((obj) => {
      //       let timedata = containsObject(attendancecurrentDate, timesheetdata)
      //       if (timedata == true) {
      //         obj.totaltime = Hourstotaltime1;
      //       }
      //       if (timedata == false) {
      //         timesheetdata.push({
      //           id: attendancecurrentDate,
      //           totaltime: Hourstotaltime1,
      //         });
      //       }
      //     });
      //   }


      //   var seconds2 = 0;
      //   attendanceData1.forEach(function (value) {
      //     var gettotalseconds = value.totalseconds;
      //     if (gettotalseconds != "") {
      //       gettotalTime = parseInt(value.totalseconds);
      //       seconds2 = seconds2 + gettotalTime;

      //     }
      //   });

      //   query = {};
      //   query.date = punchInCurrentDate;
      //   query.totaltime = Hourstotaltime1;
      //   query.addedBy = req.identity.id;
      //   query.attendanceDate = attendanceData.attendanceDate;
      //   query.totalseconds = seconds2;
      //   query.check_in_device = "desktop";
      //   query.desktop_device_time = Hourstotaltime1;


      //   let find_timesheet = await AttendenceTimeSheet.findOne({ addedBy: query.addedBy, attendanceDate: query.attendanceDate, check_in_device: "desktop" });
      //   if (find_timesheet) {
      //     let updatetimesheet = await AttendenceTimeSheet.updateOne({
      //       addedBy: userID,
      //       attendanceDate: query.attendanceDate,
      //       check_in_device: "desktop"
      //     }, {
      //       // date: query.date,
      //       totaltime: query.totaltime,
      //       totalseconds: query.totalseconds,
      //       check_in_device: "desktop",
      //       desktop_device_time: query.totaltime

      //     });

      //   } else {
      //     let timesheet = await AttendenceTimeSheet.create(query).fetch();

      //   }

      //   let updateddata = await Users.updateOne({
      //     id: userID,
      //   }).set({
      //     timeSheet: timesheetdata,
      //     checkInstatus: "false",
      //   });

      //   return res.status(200).json({
      //     success: true,
      //     message: constantObj.attendance.CHECKOUT_SAVED,
      //     data: updateattendance,
      //   });
      // }
      //*********************code for mobile device end****************************** */




      //*********************code for mobile device start****************************** */
      // if (attendanceData.check_in_device === "mobile") {
      //   console.log('in mobile');
      //   var attendancecurrentDate = attendanceData.attendanceDate;
      //   var punchInCurrentDate = attendanceData.punchInDate;
      //   var checkIntime = attendanceData.checkIn;
      //   var startTime = checkIntime;
      //   var endTime = dt;

      //   var seconds = (endTime.getTime() - startTime.getTime()) / 1000;
      //   var lastSecond = seconds;
      //   if (seconds >= 3600) {
      //     var sec_num = parseInt(seconds, 10);
      //     var hours = Math.floor(sec_num / 3600);
      //     var minutes = Math.floor(sec_num / 60) % 60;
      //     var seconds = sec_num % 60;
      //     var Hourstotaltime = hours + "hr " + minutes + "m " + seconds + "s ";
      //   } else {
      //     if (seconds >= 60 && seconds < 3600) {
      //       var ndate = new Date(seconds * 1000).toISOString().substr(14, 5);
      //       var Newtimearray = ndate.split(":");
      //       var Hourstotaltime = Newtimearray[0] + "m " + Newtimearray[1] + "s";
      //     } else {
      //       var Hourstotaltime = Math.floor(seconds) + "s";
      //     }
      //   }

      //   let updateattendance = await Attendance.updateOne({
      //     id: attendanceId,
      //   }).set({
      //     checkOut: dt,
      //     totalseconds: Math.floor(lastSecond),
      //     totalTime: Hourstotaltime,
      //     reason: checkoutreason,
      //     check_out_device: req.body.device
      //   });

      //   var dt = new Date();
      //   let attendanceData1 = await Attendance.find({
      //     userId: userID,
      //     attendanceDate: attendancecurrentDate,

      //   });

      //   var seconds1 = 0;
      //   attendanceData1.forEach(function (value) {
      //     var getseconds = value.totalseconds;
      //     if (getseconds != "") {
      //       gettotaltime = parseInt(value.totalseconds);
      //       seconds1 = seconds1 + gettotaltime;
      //     }
      //   });
      //   if (seconds1 >= 3600) {
      //     var sec_num = parseInt(seconds1, 10);
      //     var hours = Math.floor(sec_num / 3600);
      //     var minutes = Math.floor(sec_num / 60) % 60;
      //     var seconds1 = sec_num % 60;
      //     var Hourstotaltime1 = hours + "hr " + minutes + "m " + seconds + "s ";
      //   } else {
      //     if (seconds1 >= 60 && seconds1 < 3600) {
      //       var ndate = new Date(seconds1 * 1000).toISOString().substr(14, 5);
      //       var Newtimearray = ndate.split(":");
      //       var Hourstotaltime1 = Newtimearray[0] + "m " + Newtimearray[1] + "s";
      //     } else {
      //       var Hourstotaltime1 = seconds1 + "s";
      //     }
      //   }

      //   let userdata = await Users.findOne({ id: userID });

      //   let timesheetdata = userdata.timeSheet;

      //   var atDate = attendancecurrentDate;

      //   //Find index of specific object using findIndex method.

      //   if (timesheetdata.length == 0) {
      //     timesheetdata.push({
      //       id: attendancecurrentDate,
      //       totaltime: Hourstotaltime1,
      //     });
      //   } else {
      //     objIndex = timesheetdata.findIndex((obj) => {
      //       let timedata = containsObject(attendancecurrentDate, timesheetdata)
      //       if (timedata == true) {
      //         obj.totaltime = Hourstotaltime1;
      //       }
      //       if (timedata == false) {
      //         timesheetdata.push({
      //           id: attendancecurrentDate,
      //           totaltime: Hourstotaltime1,
      //         });
      //       }
      //     });
      //   }


      //   var seconds2 = 0;
      //   attendanceData1.forEach(function (value) {
      //     var gettotalseconds = value.totalseconds;
      //     if (gettotalseconds != "") {
      //       gettotalTime = parseInt(value.totalseconds);
      //       seconds2 = seconds2 + gettotalTime;

      //     }
      //   });

      //   query = {};
      //   query.date = punchInCurrentDate;
      //   query.totaltime = Hourstotaltime1;
      //   query.addedBy = req.identity.id;
      //   query.attendanceDate = attendanceData.attendanceDate;
      //   query.totalseconds = seconds2;
      //   query.check_in_device = "mobile";
      //   query.mobile_device_time = Hourstotaltime1;


      //   let find_timesheet = await AttendenceTimeSheet.findOne({ addedBy: query.addedBy, attendanceDate: query.attendanceDate, check_in_device: "mobile" });
      //   if (find_timesheet) {
      //     let updatetimesheet = await AttendenceTimeSheet.updateOne({
      //       addedBy: userID,
      //       attendanceDate: query.attendanceDate,
      //       check_in_device: "mobile",

      //     }, {
      //       // date: query.date,
      //       totaltime: query.totaltime,
      //       totalseconds: query.totalseconds,
      //       check_in_device: "mobile",
      //       mobile_device_time: query.mobile_device_time

      //     });

      //   } else {
      //     let timesheet = await AttendenceTimeSheet.create(query).fetch();
      //   }



      //   let updateddata = await Users.updateOne({
      //     id: userID,
      //   }).set({
      //     timeSheet: timesheetdata,
      //     checkInstatus: "false",
      //   });

      //   return res.status(200).json({
      //     success: true,
      //     message: constantObj.attendance.CHECKOUT_SAVED,
      //     data: updateattendance,
      //   });
      // }

      //*********************code for mobile device end****************************** */

      let updateddata = await Users.updateOne({
        id: userID,
      }).set({
        timeSheet: timesheetdata,
        checkInstatus: "false",
      });

      //---------------------------------Add total seconds in AttendenceTimeSheet Table----------------------


      var seconds2 = 0;
      attendanceData1.forEach(function (value) {
        var gettotalseconds = value.totalseconds;
        if (gettotalseconds != "") {
          gettotalTime = parseInt(value.totalseconds);
          seconds2 = seconds2 + gettotalTime;

        }
      });


      //timesheet table-------------start

      query = {};
      query.date = punchInCurrentDate;
      query.totaltime = Hourstotaltime1;
      query.addedBy = req.identity.id;
      query.attendanceDate = attendanceData.attendanceDate;
      query.totalseconds = seconds2;
      query.check_in_device = attendanceData.check_in_device

      // console.log(query, "=====================query");
      /**Old code--------------start */
      let find_timesheet = await AttendenceTimeSheet.findOne({ addedBy: query.addedBy, attendanceDate: query.attendanceDate });
      if (find_timesheet) {
        let updatetimesheet = await AttendenceTimeSheet.updateOne({
          addedBy: userID,
          attendanceDate: query.attendanceDate
        }, {
          // date: query.date,
          totaltime: query.totaltime,
          totalseconds: query.totalseconds
        });

      } else {
        let timesheet = await AttendenceTimeSheet.create(query).fetch();

      }
      /**Old code--------------end */
      // ----------------end

      // ActivityMail({
      //   type: "Check Out",
      //   fullName: req.identity.fullName,
      //   date: dt,
      // });
      return res.status(200).json({
        success: true,
        message: constantObj.attendance.CHECKOUT_SAVED,
        data: updateattendance,
      });
    } catch (err) {
      console.log(err, "================err");
    }
  },

  /**
   *
   * @param {*} req
   * @param {*} res
   * @description: Used to get list of employee checkin &  checkout time
   * @createdAt 21/09/2021
   * @createdBy : Chandra Shekhar
   */
  // getAllattendanceCheckinTime: (req, res) => {
  //   var search = req.param("search");
  //   var sortBy = req.param("sortBy");
  //   var page = req.param("page");
  //   var count = req.param("count");
  //   var id = req.param("id");
  //   var start_date = req.param("start_date");
  //   var end_date = req.param("end_date");


  //   if (page == undefined) {
  //     page = 1;
  //   }
  //   if (count == undefined) {
  //     count = 10;
  //   }
  //   var skipNo = (page - 1) * count;
  //   var query = {};

  //   if (sortBy) {
  //     sortBy = sortBy.toString();
  //   } else {
  //     sortBy = -1;
  //   }

  //   if (search) {
  //     query.$or = [
  //       { name: { $regex: search, $options: "i" } },
  //       { email: { $regex: search, $options: "i" } },
  //     ];
  //   }
  //   if (id) {
  //     query.userId = id;
  //   }
  //   if (start_date != "" && start_date != undefined && end_date != "" && end_date != undefined) {
  //     // console.log('edfcewsfc');
  //     var date = new Date(start_date);
  //     date.setDate(date.getDate());
  //     var Enddate = new Date(end_date);
  //     Enddate.setDate(Enddate.getDate() + 1);
  //     query.$and = [
  //       { punchInDate: { $gte: date } },
  //       { punchInDate: { $lte: Enddate } },
  //     ];
  //     // query.currentDate = { $gte: new Date(date), $lte: new Date(Enddate) };
  //   }

  //   query.checkOut = ""
  //   query.isDeleted = false;
  //   console.log(query, "=============qqqqqqqqqqqq");
  //   db.collection("attendance").aggregate([
  //     {
  //       $project: {
  //         id: "$_id",
  //         checkIn: "$checkIn",
  //         check0ut: "$checkOut",
  //         userId: "$userId",
  //         name: "$name",
  //         email: "$email",
  //         totalTime: "$totalTime",
  //         totalseconds: "$totalseconds",
  //         attendanceDate: "$attendanceDate",
  //         punchInDate: "$punchInDate",
  //         reason: "$reason",
  //         status: "$status",
  //         createdAt: "$createdAt",
  //         deletedBy: "$deletedBy",
  //         addedBy: "$addedBy",
  //         updatedBy: "$updatedBy",
  //         isDeleted: "$isDeleted",
  //         deletedAt: "$deletedAt",
  //         updatedAt: "$updatedAt",
  //       },
  //     },
  //     {
  //       $match: query,
  //     },
  //   ])
  //     .toArray((err, totalResult) => {
  //       console.log(totalResult, "===============total");

  //       db.collection("attendance").aggregate([
  //         {
  //           $project: {
  //             id: "$_id",
  //             checkIn: "$checkIn",
  //             checkOut: "$checkOut",
  //             userId: "$userId",
  //             name: "$name",
  //             email: "$email",
  //             totalTime: "$totalTime",
  //             totalseconds: "$totalseconds",
  //             attendanceDate: "$attendanceDate",
  //             punchInDate: "$punchInDate",
  //             reason: "$reason",
  //             status: "$status",
  //             createdAt: "$createdAt",
  //             deletedBy: "$deletedBy",
  //             addedBy: "$addedBy",
  //             updatedBy: "$updatedBy",
  //             isDeleted: "$isDeleted",
  //             deletedAt: "$deletedAt",
  //             updatedAt: "$updatedAt",
  //           },
  //         },
  //         {
  //           $match: query,
  //         },
  //         { $sort: { updatedAt: sortBy } },
  //         {
  //           $skip: skipNo,
  //         },
  //         {
  //           $limit: Number(count),
  //         },
  //       ])
  //         .toArray((err, result) => {
  //           if (err) {
  //             return res.status(400).json({
  //               success: false,
  //               error: { message: "" + err },
  //             });
  //           } else {
  //             return res.status(200).json({
  //               success: true,
  //               code: 200,
  //               data: result,
  //               total: totalResult.length,
  //             });
  //           }
  //         });
  //     });
  // },


  /**
   *
   * @param {*} req
   * @param {*} res
   * @description: Used to calculate total time of the attendance of present day
   * @createdAt 22/09/2021
   * @createdBy : Chandra Shekhar
   */
  totalCurrentTime: async (req, res) => {
    var userID = req.identity.id;
    var attendancetime = req.param("attendancedate");

    if (!userID || typeof userID == undefined) {
      return res.status(404).json({
        success: false,
        error: { message: constantObj.attendance.USERID_REQUIRED },
      });
    }

    var dt = new Date();
    let attendanceData = await Attendance.find({
      userId: userID,
      attendanceDate: attendancetime,
    });
    var seconds = 0;

    attendanceData.forEach(function (value) {
      var getseconds = value.totalseconds;
      if (getseconds != "") {
        gettotaltime = parseInt(value.totalseconds);
        seconds = seconds + gettotaltime;
      }
    });

    if (seconds >= 3600) {
      var sec_num = parseInt(seconds, 10);
      var hours = Math.floor(sec_num / 3600);
      var minutes = Math.floor(sec_num / 60) % 60;
      var seconds = sec_num % 60;
      var Hourstotaltime = hours + "hr " + minutes + "m " + seconds + "s ";
    } else {
      if (seconds >= 60 && seconds < 3600) {
        var ndate = new Date(seconds * 1000).toISOString().substr(14, 5);
        var Newtimearray = ndate.split(":");
        var Hourstotaltime = Newtimearray[0] + "m " + Newtimearray[1] + "s";
      } else {
        var Hourstotaltime = seconds + "s";
      }
    }

    return res.status(200).json({
      success: true,
      message: constantObj.attendance.TOTALTIME,
      data: Hourstotaltime,
    });
  },
  /**
   *
   * @param {*} req
   * @param {*} res
   * @description: Used to calculate all user time of the attendance of present day
   * @createdAt 24/09/2021
   * @createdBy : Chandra Shekhar
   */
  allUserCurrentTime: async (req, res) => {

    const db = sails.getDatastore().manager;
    var userId = req.param("userid");
    var start_date = req.param("start_date");
    var end_date = req.param("end_date");


    let query = {};
    // if (start_date != "" && start_date != undefined && end_date == "") {
    //   var date = new Date(start_date);
    //   console.log(date);
    //   date.setDate(date.getDate());

    //   // console.log(date,'startDate')
    //   var Enddate = new Date(start_date);
    //   console.log(Enddate);
    //   Enddate.setDate(Enddate.getDate() + 1);
    //   // console.log(Enddate ,'end date')
    //   // console.log(Enddate, "endDate");
    //   query.$and = [
    //     { currentDate: { $gte: date } },
    //     { currentDate: { $lte: Enddate } },
    //   ];
    // }
    if (start_date != "" && start_date != undefined && end_date != "" && end_date != undefined) {

      var date = new Date(start_date);
      date.setDate(date.getDate());
      var Enddate = new Date(end_date);
      Enddate.setDate(Enddate.getDate() + 1);
      query.$and = [
        { currentDate: { $gte: date } },
        { currentDate: { $lte: Enddate } },
      ];
      // query.currentDate = { $gte: new Date(date), $lte: new Date(Enddate) };
    }

    // console.log(query);
    // var utc = new Date().toJSON().slice(0, 10).replace(/-/g, '-');
    // var currentDate = "";
    // if (attendancedate) {
    //   currentDate = attendancedate;
    // } else {
    //   currentDate = utc;
    // }
    // let userattendancedata = [];
    // let totaltime = [];


    //   if (start && end) {
    //     startDate = new Date(start);
    //     startDate.setUTCHours(0, 0, 0, 0)      
    //     endDate = new Date(end);
    //     endDate.setUTCHours(23, 59, 0, 0)
    //     query.date = { $gte: startDate, $lte: endDate }
    // }
    // if(currentDate){
    //  query.attendanceDate=new Date(currentDate)
    // }


    // db.collection("users")                   //comment by shivani on 11 may 2023
    //   .aggregate([
    //     {
    //       $unwind: {
    //         path: "$timeSheet",
    //         preserveNullAndEmptyArrays: true,
    //       },
    //     },
    //     {
    //       $project: {
    //         id: "$_id",
    //         fullName: "$fullName",
    //         email: "$email",
    //         timeSheet: "$timeSheet",
    //       },
    //     },
    //     { $match: query },
    //   ])
    db.collection("attendencetimesheet")
      .aggregate([
        {
          $lookup: {
            from: 'users',
            localField: 'addedBy',
            foreignField: '_id',
            as: "addedBy_details"
          }
        },
        {
          $unwind: {
            path: '$addedBy_details',
            preserveNullAndEmptyArrays: true
          }
        },
        // {
        //   $lookup: {
        //     from: 'attendance',
        //     localField: 'addedBy',
        //     foreignField: 'userId',
        //     as: "attendance_details"
        //   }
        // },
        // {
        //   $unwind: {
        //     path: '$attendance_details',
        //     preserveNullAndEmptyArrays: true
        //   }
        // },
        {
          $project: {
            userid: "$_id",
            currentDate: "$date",
            attendancetime: "$totaltime",
            addedBy: "$addedBy",
            fullName: "$addedBy_details.fullName",
            email: "$addedBy_details.email",
            // addedBy_details:"$attendance_details"
          },
        },
        { $match: query },
      ])
      .toArray(async (err, totalResult) => {

        // async.each(totalResult, async function (userdata, callback) {
        //   var userID = userdata.id;
        //   var userEmail = userdata.email;
        //   var userFullName = userdata.fullName;
        //   var totaltimes = userdata.timeSheet;
        //   console.log(totaltimes);
        //   if (totaltimes && !totaltimes.length) {

        //     if (totaltimes.id == currentDate) {
        //       objtotaltime = totaltimes.totaltime;
        //       userattendancedata.push({
        //         userid: userID,
        //         fullName: userFullName,
        //         email: userEmail,
        //         attendancetime: objtotaltime,
        //         currentDate: currentDate,
        //       });
        //     }
        //   }
        // });

        // if (userattendancedata && !userattendancedata.length) {
        //   return res.status(200).json({
        //     success: true,
        //     message: constantObj.attendance.NO_RESULT,
        //   });
        // } else {
        return res.status(200).json({
          success: true,
          message: constantObj.attendance.DATA_FECTCH,
          data: totalResult,
        });
        // }
        //}, 3000);
      });
  },
  /**
   *
   * @param {*} req
   * @param {*} res
   * @description: Used to display single user attendance as per date
   * @createdAt 22/09/2021
   * @createdBy : Chandra Shekhar
   */
  singleUserAttendance: async (req, res) => {
    var start_date = req.param('start_date') ? req.param('start_date') : "";
    var end_date = req.param('end_date') ? req.param('end_date') : "";
    var userIDs = req.param("userID");
    if (userIDs) {
      userID = userIDs;
    } else {
      var userID = req.identity.id;
    }

    if (!userID || typeof userID == undefined) {
      return res.status(404).json({
        success: false,
        error: { message: constantObj.attendance.USERID_REQUIRED },
      });
    }

    var query = {}
    query.userId = userID;
    if (start_date != "" && end_date != "") {
      query.attendanceDate = {
        '>=': start_date,
        '<=': end_date
      };

    }

    let attendanceData = await Attendance.find(query).sort({ updatedAt: -1 });


    if (attendanceData.length !== 0) {
      return res.status(200).json({
        success: true,
        message: constantObj.attendance.CURRENT_ATTENDANCE,
        data: attendanceData,
      });
    } else {
      return res.status(200).json({
        success: true,
        message: constantObj.attendance.NO_CURRENT_ATTENDANCE,
      });
    }


  },

  /**
     *
     * @param {*} req
     * @param {*} res
     * @description: Used to display when employee is checkout
     * @createdAt 18/05/2023
     * @createdBy : shivani
     */


  // getAllattendanceCheckoutTime: (req, res) => {
  //   var search = req.param("search");
  //   var sortBy = req.param("sortBy");
  //   var page = req.param("page");
  //   var count = req.param("count");
  //   var id = req.param("id");
  //   var start_date = req.param("start_date");
  //   var end_date = req.param("end_date");


  //   if (page == undefined) {
  //     page = 1;
  //   }
  //   if (count == undefined) {
  //     count = 10;
  //   }
  //   var skipNo = (page - 1) * count;
  //   var query = {};

  //   if (sortBy) {
  //     sortBy = sortBy.toString();
  //   } else {
  //     sortBy = -1;
  //   }

  //   if (search) {
  //     query.$or = [
  //       { name: { $regex: search, $options: "i" } },
  //       { email: { $regex: search, $options: "i" } },
  //     ];
  //   }
  //   if (id) {
  //     query.userId = id;
  //   }
  //   query.checkOut = { $ne: "" };

  //   if (start_date != "" && start_date != undefined && end_date != "" && end_date != undefined) {
  //     // console.log('edfcewsfc');
  //     var date = new Date(start_date);
  //     date.setDate(date.getDate());
  //     var Enddate = new Date(end_date);
  //     Enddate.setDate(Enddate.getDate() + 1);
  //     query.$and = [
  //       { punchInDate: { $gte: date } },
  //       { punchInDate: { $lte: Enddate } },
  //     ];
  //     // query.currentDate = { $gte: new Date(date), $lte: new Date(Enddate) };
  //   }
  //   // query.checkOut != "";
  //   query.isDeleted = false;
  //   console.log(query, "=============qqqqqqqqqqqq");
  //   db.collection("attendance")
  //     .aggregate([
  //       {
  //         $project: {
  //           id: "$_id",
  //           checkIn: "$checkIn",
  //           check0ut: "$checkOut",
  //           userId: "$userId",
  //           name: "$name",
  //           email: "$email",
  //           totalTime: "$totalTime",
  //           totalseconds: "$totalseconds",
  //           attendanceDate: "$attendanceDate",
  //           punchInDate: "$punchInDate",
  //           reason: "$reason",
  //           status: "$status",
  //           createdAt: "$createdAt",
  //           deletedBy: "$deletedBy",
  //           addedBy: "$addedBy",
  //           updatedBy: "$updatedBy",
  //           isDeleted: "$isDeleted",
  //           deletedAt: "$deletedAt",
  //           updatedAt: "$updatedAt",
  //         },
  //       },
  //       {
  //         $match: query,
  //       },
  //     ])
  //     .toArray((err, totalResult) => {

  //       db.collection("attendance")
  //         .aggregate([
  //           {
  //             $project: {
  //               id: "$_id",
  //               checkIn: "$checkIn",
  //               checkOut: "$checkOut",
  //               userId: "$userId",
  //               name: "$name",
  //               email: "$email",
  //               totalTime: "$totalTime",
  //               totalseconds: "$totalseconds",
  //               attendanceDate: "$attendanceDate",
  //               punchInDate: "$punchInDate",
  //               reason: "$reason",
  //               status: "$status",
  //               createdAt: "$createdAt",
  //               deletedBy: "$deletedBy",
  //               addedBy: "$addedBy",
  //               updatedBy: "$updatedBy",
  //               isDeleted: "$isDeleted",
  //               deletedAt: "$deletedAt",
  //               updatedAt: "$updatedAt",
  //             },
  //           },
  //           {
  //             $match: query,
  //           },
  //           { $sort: { updatedAt: sortBy } },
  //           {
  //             $skip: skipNo,
  //           },
  //           {
  //             $limit: Number(count),
  //           },
  //         ])
  //         .toArray((err, result) => {
  //           if (err) {
  //             return res.status(400).json({
  //               success: false,
  //               error: { message: "" + err },
  //             });
  //           } else {
  //             return res.status(200).json({
  //               success: true,
  //               code: 200,
  //               data: result,
  //               total: totalResult.length,
  //             });
  //           }
  //         });
  //     });
  // },

  // getAllattendanceInExcelSheet: async (req, res) => {
  //   try {
  //     var search = req.param("search");
  //     var sortBy = req.param("sortBy");
  //     var page = req.param("page");
  //     var count = req.param("count");
  //     var id = req.param("id");
  //     var start_date = req.param("start_date");
  //     var end_date = req.param("end_date");
  //     let export_to_xls = req.param('export_to_xls');


  //     if (page == undefined) {
  //       page = 1;
  //     }
  //     if (count == undefined) {
  //       count = 10;
  //     }
  //     var skipNo = (page - 1) * count;
  //     var query = {};

  //     if (sortBy) {
  //       sortBy = sortBy.toString();
  //     } else {
  //       sortBy = -1;
  //     }

  //     if (search) {
  //       query.$or = [
  //         { name: { $regex: search, $options: "i" } },
  //         { email: { $regex: search, $options: "i" } },
  //       ];
  //     }
  //     if (id) {
  //       query.userId = id;
  //     }
  //     query.checkOut = { $ne: "" };
  //     query.isDeleted = false;
  //     console.log(query, "=============qqqqqqqqqqqq");
  //     db.collection("attendance")
  //       .aggregate([
  //         {
  //           $lookup: {
  //             from: "users",
  //             localField: "userId",
  //             foreignField: "_id",
  //             as: "user_details",
  //           },
  //         },
  //         {
  //           $unwind: {
  //             path: "$user_details",
  //             preserveNullAndEmptyArrays: true,
  //           },
  //         },
  //         {
  //           $project: {
  //             id: "$_id",
  //             checkIn: "$checkIn",
  //             check0ut: "$checkOut",
  //             userId: "$userId",
  //             name: "$name",
  //             email: "$email",
  //             totalTime: "$totalTime",
  //             totalseconds: "$totalseconds",
  //             attendanceDate: "$attendanceDate",
  //             punchInDate: "$punchInDate",
  //             reason: "$reason",
  //             status: "$status",
  //             createdAt: "$createdAt",
  //             deletedBy: "$deletedBy",
  //             addedBy: "$addedBy",
  //             updatedBy: "$updatedBy",
  //             isDeleted: "$isDeleted",
  //             deletedAt: "$deletedAt",
  //             updatedAt: "$updatedAt",
  //             user_name: "$user_details.fullName"
  //           },
  //         },
  //         {
  //           $match: query,
  //         },
  //       ])
  //       .toArray((err, totalResult) => {

  //         db.collection("attendance")
  //           .aggregate([
  //             {
  //               $lookup: {
  //                 from: "users",
  //                 localField: "userId",
  //                 foreignField: "_id",
  //                 as: "user_details",
  //               },
  //             },
  //             {
  //               $unwind: {
  //                 path: "$user_details",
  //                 preserveNullAndEmptyArrays: true,
  //               },
  //             },
  //             {
  //               $project: {
  //                 id: "$_id",
  //                 checkIn: "$checkIn",
  //                 checkOut: "$checkOut",
  //                 userId: "$userId",
  //                 name: "$name",
  //                 email: "$email",
  //                 totalTime: "$totalTime",
  //                 totalseconds: "$totalseconds",
  //                 attendanceDate: "$attendanceDate",
  //                 punchInDate: "$punchInDate",
  //                 reason: "$reason",
  //                 status: "$status",
  //                 createdAt: "$createdAt",
  //                 deletedBy: "$deletedBy",
  //                 addedBy: "$addedBy",
  //                 updatedBy: "$updatedBy",
  //                 isDeleted: "$isDeleted",
  //                 deletedAt: "$deletedAt",
  //                 updatedAt: "$updatedAt",
  //                 user_name: "$user_details.fullName"
  //               },
  //             },
  //             {
  //               $match: query,
  //             },
  //             { $sort: { updatedAt: sortBy } },
  //             {
  //               $skip: skipNo,
  //             },
  //             {
  //               $limit: Number(count),
  //             },
  //           ])
  //           .toArray(async (err, result) => {
  //             if (err) {
  //               return res.status(400).json({
  //                 success: false,
  //                 error: { message: "" + err },
  //               });
  //             }
  //             else {
  //               if (export_to_xls == "yes") {
  //                 if (totalResult && totalResult.length > 0) {
  //                   let workbook = new excel.Workbook();
  //                   let worksheet = workbook.addWorksheet("Daily Attendance");
  //                   worksheet.columns = [
  //                     { header: "Employee Name", key: "user_name", width: 15 },
  //                     { header: "Check In Time", key: "checkIn", width: 25 },
  //                     { header: "Check Out Time", key: "checkOut", width: 25 },
  //                     { header: "Total Time", key: "totalTime", width: 30 },
  //                     { header: "Client", key: "company_name", width: 20 },
  //                     { header: "Date", key: "punchInDate", width: 30 }
  //                   ];
  //                   try {
  //                     res.setHeader(
  //                       "Content-Type",
  //                       "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  //                     );
  //                     res.setHeader(
  //                       "Content-Disposition",
  //                       "attachment; filename=" + "Attendance.xlsx"
  //                     );

  //                     return workbook.xlsx.write(res).then(function () {
  //                       res.status(200).end();
  //                     });
  //                   } catch (err) {
  //                     return res.status(400).json({
  //                       success: false,
  //                       error: { message: err },
  //                     });
  //                   }

  //                 }

  //               } else {
  //                 // let resData = {
  //                 //   total_count: totalResult.length,
  //                 //   data: result
  //                 // }
  //                 return res.status(200).json({
  //                   success: true,
  //                   message: 'Get all Today Attendance',
  //                   total_count: totalResult.length,
  //                   data: result
  //                 });
  //               }
  //             }
  //           });
  //       });

  //   } catch (err) {
  //     return res.status(400).json({
  //       success: false,
  //       error: { message: err },
  //     });
  //   }
  // }
  getAllattendanceInExcelSheet: async (req, res) => {
    try {
      var search = req.param("search");
      var sortBy = req.param("sortBy");
      var page = req.param("page");
      var count = req.param("count");
      var id = req.param("id");
      var start_date = req.param("start_date");
      var end_date = req.param("end_date");
      let export_to_xls = req.param('export_to_xls');

      if (!page) {
        page = 1;
      }
      if (!count) {
        count = 10;
      }
      let skipNo = (page - 1) * count;
      let query = {};

      if (sortBy) {
        sortBy = sortBy.toString();
      } else {
        sortBy = -1;
      }

      if (search) {
        query.$or = [
          { name: { $regex: search, $options: "i" } },
          { email: { $regex: search, $options: "i" } },
        ];
      }
      if (id) {
        query.userId = id;
      }
      // query.checkOut = { $ne: "" };
      query.isDeleted = false;
      let currentDate = new Date();
      currentDate = currentDate.toISOString().substring(0, 10);

      query.attendanceDate = currentDate;

      db.collection("attendance").aggregate([
        // {
        //   $lookup: {
        //     from: "users",
        //     localField: "userId",
        //     foreignField: "_id",
        //     as: "user_details",
        //   },
        // },
        // {
        //   $unwind: {
        //     path: "$user_details",
        //     preserveNullAndEmptyArrays: true,
        //   },
        // },
        {
          $project: {
            id: "$_id",
            checkIn: "$checkIn",
            checkOut: "$checkOut",
            userId: "$userId",
            name: "$name",
            email: "$email",
            totalTime: "$totalTime",
            totalseconds: "$totalseconds",
            attendanceDate: "$attendanceDate",
            punchInDate: "$punchInDate",
            reason: "$reason",
            status: "$status",
            device: "$device",
            createdAt: "$createdAt",
            deletedBy: "$deletedBy",
            addedBy: "$addedBy",
            updatedBy: "$updatedBy",
            isDeleted: "$isDeleted",
            deletedAt: "$deletedAt",
            updatedAt: "$updatedAt",
            // user_name: "$user_details.fullName"
          },
        },
        {
          $match: query,
        },
        {
          $sort: { createdAt: -1 },
        },
      ]).toArray((err, totalResult) => {
        db.collection("attendance").aggregate([
          // {
          //   $lookup: {
          //     from: "users",
          //     localField: "userId",
          //     foreignField: "_id",
          //     as: "user_details",
          //   },
          // },
          // {
          //   $unwind: {
          //     path: "$user_details",
          //     preserveNullAndEmptyArrays: true,
          //   },
          // },
          {
            $project: {
              id: "$_id",
              checkIn: "$checkIn",
              checkOut: "$checkOut",
              userId: "$userId",
              name: "$name",
              email: "$email",
              totalTime: "$totalTime",
              totalseconds: "$totalseconds",
              attendanceDate: "$attendanceDate",
              punchInDate: "$punchInDate",
              reason: "$reason",
              status: "$status",
              device: "$device",
              createdAt: "$createdAt",
              deletedBy: "$deletedBy",
              addedBy: "$addedBy",
              updatedBy: "$updatedBy",
              isDeleted: "$isDeleted",
              deletedAt: "$deletedAt",
              updatedAt: "$updatedAt",
              // user_name: "$user_details.fullName"
            },
          },
          {
            $match: query,
          },
          {
            $sort: { createdAt: -1 },
          },
          {
            $skip: skipNo
          },
          {
            $limit: Number(count),
          },
        ]).toArray(async (err, result) => {
          if (err) {
            return res.status(400).json({
              success: false,
              error: { message: err },
            });
          }
          else {

            if (export_to_xls == "yes") {
              if (totalResult && totalResult.length > 0) {
                let workbook = new excel.Workbook();
                let worksheet = workbook.addWorksheet("Daily Attendance Records");

                worksheet.columns = [
                  // { header: "serial_number", key: "serial_number", width: 15 },
                  { header: "Employee Name", key: "name", width: 15 },
                  { header: "Check In Time", key: "checkIn", width: 25 },
                  { header: "Check Out Time", key: "checkOut", width: 25 },
                  { header: "Total Time", key: "totalTime", width: 30 },
                  { header: "Date", key: "punchInDate", width: 30 }
                ];
                // let counter
                for await (let values of totalResult) {

                  // id = counter;
                  // if (counter) {
                  //   values.serial_number = `${1 + counter}`;
                  // } else {
                  //   values.serial_number = `${1}`;
                  // }
                  worksheet.addRow(values);
                  // counter++;
                }


                try {
                  res.setHeader(
                    "Content-Type",
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                  );
                  res.setHeader(
                    "Content-Disposition",
                    "attachment; filename=" + "Attendance.xlsx"
                  );

                  return workbook.xlsx.write(res).then(function () {
                    res.status(200).end();
                  });
                } catch (err) {
                  return res.status(400).json({
                    success: false,
                    error: { message: err },
                  });
                }

              }

            } else {
              return res.status(200).json({
                success: true,
                message: 'Get all Today Attendance',
                total_count: totalResult.length,
                data: result
              });
            }
          }
        });
      })
    }

    catch (err) {
      return res.status(400).json({
        success: false,
        error: { message: err },
      });
    }
  },

  getAllattendanceCheckinTime: async (req, res) => {
    try {

      var search = req.param("search");
      var sortBy = req.param("sortBy");
      var page = req.param("page");
      var count = req.param("count");
      var id = req.param("id");
      var start_date = req.param("start_date");
      var end_date = req.param("end_date");

      if (!page) {
        page = 1;
      }
      if (!count) {
        count = 10;
      }
      let skipNo = (page - 1) * count;
      let query = {};
      // query.$and = [];

      if (search) {
        query.$or = [
          { name: { $regex: search, $options: "i" } },
          { email: { $regex: search, $options: "i" } },
        ];
      }

      if (id) {
        query.userId = id;
      }
      if (start_date && end_date) {
        var date = new Date(start_date);
        date.setDate(date.getDate());
        var Enddate = new Date(end_date);
        Enddate.setDate(Enddate.getDate() + 1);
        query.$and = [
          { punchInDate: { $gte: date } },
          { punchInDate: { $lte: Enddate } },
        ];
        // query.currentDate = { $gte: new Date(date), $lte: new Date(Enddate) };
      }

      query.checkOut = ""
      query.isDeleted = false;

      db.collection("attendance").aggregate([
        {
          $project: {
            id: "$_id",
            checkIn: "$checkIn",
            checkOut: "$checkOut",
            userId: "$userId",
            name: "$name",
            email: "$email",
            totalTime: "$totalTime",
            totalseconds: "$totalseconds",
            attendanceDate: "$attendanceDate",
            punchInDate: "$punchInDate",
            reason: "$reason",
            status: "$status",
            check_in_device: "$check_in_device",
            check_out_device: "$check_out_device",
            createdAt: "$createdAt",
            deletedBy: "$deletedBy",
            addedBy: "$addedBy",
            updatedBy: "$updatedBy",
            isDeleted: "$isDeleted",
            deletedAt: "$deletedAt",
            updatedAt: "$updatedAt",
          },
        },
        {
          $match: query,
        },
        {
          $sort: { createdAt: -1 },
        },
      ]).toArray((err, totalResult) => {
        // console.log(totalResult, "=========total");
        if (err) {
          return res.status(400).json({
            success: false,
            error: { message: err },
          });
        }
        db.collection("attendance").aggregate([
          {
            $project: {
              id: "$_id",
              checkIn: "$checkIn",
              checkOut: "$checkOut",
              userId: "$userId",
              name: "$name",
              email: "$email",
              totalTime: "$totalTime",
              totalseconds: "$totalseconds",
              attendanceDate: "$attendanceDate",
              punchInDate: "$punchInDate",
              reason: "$reason",
              status: "$status",
              check_in_device: "$check_in_device",
              check_out_device: "$check_out_device",
              createdAt: "$createdAt",
              deletedBy: "$deletedBy",
              addedBy: "$addedBy",
              updatedBy: "$updatedBy",
              isDeleted: "$isDeleted",
              deletedAt: "$deletedAt",
              updatedAt: "$updatedAt",
            },
          },
          {
            $match: query,
          },
          {
            $sort: { createdAt: -1 },
          },
          {
            $skip: skipNo,
          },
          {
            $limit: Number(count),
          },
        ]).toArray((err, result) => {

          if (err) {
            return res.status(400).json({
              success: false,
              error: { message: err },
            });
          } else {

            let resData = {
              total_count: totalResult.length,
              data: result
            }

            // if (!req.param('page') && !req.param('count')) {
            //   resData = {
            //     total_count: totalResult.length,
            //     data: result
            //   }
            //   return res.status(200).json({
            //     success: true,
            //     message: constantObj.attendance.ALL_CHEKIN_ATTENDANCE,
            //     data: resData
            //   });
            // }

            return res.status(200).json({
              success: true,
              message: constantObj.attendance.ALL_CHEKIN_ATTENDANCE,
              total_count: totalResult.length,
              data: result
            });
          }
        });
      })
    }
    catch (err) {
      // console.log(err, "==========err");
      return res.status(400).json({
        success: false,
        error: { message: err },
      });
    }
  },

  getAllattendanceCheckoutTime: async (req, res) => {
    try {

      var search = req.param("search");
      var sortBy = req.param("sortBy");
      var page = req.param("page");
      var count = req.param("count");
      var id = req.param("id");
      var start_date = req.param("start_date");
      var end_date = req.param("end_date");

      if (!page) {
        page = 1;
      }
      if (!count) {
        count = 10;
      }
      let skipNo = (page - 1) * count;
      let query = {};
      // query.$and = [];

      if (search) {
        query.$or = [
          { name: { $regex: search, $options: "i" } },
          { email: { $regex: search, $options: "i" } },
        ];
      }

      if (id) {
        query.userId = id;
      }
      if (start_date && end_date) {

        var date = new Date(start_date);
        date.setDate(date.getDate());
        var Enddate = new Date(end_date);
        Enddate.setDate(Enddate.getDate() + 1);
        query.$and = [
          { punchInDate: { $gte: date } },
          { punchInDate: { $lte: Enddate } },
        ];
        // query.currentDate = { $gte: new Date(date), $lte: new Date(Enddate) };
      }

      query.checkOut = { $ne: "" };
      query.isDeleted = false;
      // console.log(query, "=============qqqqqqqqqqqq")

      db.collection("attendance").aggregate([
        {
          $project: {
            id: "$_id",
            checkIn: "$checkIn",
            checkOut: "$checkOut",
            userId: "$userId",
            name: "$name",
            email: "$email",
            totalTime: "$totalTime",
            totalseconds: "$totalseconds",
            attendanceDate: "$attendanceDate",
            punchInDate: "$punchInDate",
            reason: "$reason",
            status: "$status",
            check_in_device: "$check_in_device",
            check_out_device: "$check_out_device",
            createdAt: "$createdAt",
            deletedBy: "$deletedBy",
            addedBy: "$addedBy",
            updatedBy: "$updatedBy",
            isDeleted: "$isDeleted",
            deletedAt: "$deletedAt",
            updatedAt: "$updatedAt",
          },
        },
        {
          $match: query,
        },
        {
          $sort: { createdAt: -1 },
        },
      ]).toArray((err, totalResult) => {

        if (err) {
          return res.status(400).json({
            success: false,
            error: { message: err },
          });
        }
        db.collection("attendance").aggregate([
          {
            $project: {
              id: "$_id",
              checkIn: "$checkIn",
              checkOut: "$checkOut",
              userId: "$userId",
              name: "$name",
              email: "$email",
              totalTime: "$totalTime",
              totalseconds: "$totalseconds",
              attendanceDate: "$attendanceDate",
              punchInDate: "$punchInDate",
              reason: "$reason",
              status: "$status",
              check_in_device: "$check_in_device",
              check_out_device: "$check_out_device",
              createdAt: "$createdAt",
              deletedBy: "$deletedBy",
              addedBy: "$addedBy",
              updatedBy: "$updatedBy",
              isDeleted: "$isDeleted",
              deletedAt: "$deletedAt",
              updatedAt: "$updatedAt",
            },
          },
          {
            $match: query,
          },
          {
            $sort: { createdAt: -1 },
          },
          {
            $skip: skipNo,
          },
          {
            $limit: Number(count),
          },
        ]).toArray((err, result) => {
          if (err) {
            return res.status(400).json({
              success: false,
              error: { message: err },
            });
          } else {

            // let resData = {
            //   total_count: totalResult.length,
            //   data: result
            // }

            // if (!req.param('page') && !req.param('count')) {
            //   resData = {
            //     total_count: totalResult.length,
            //     data: result
            //   }
            //   return res.status(200).json({
            //     success: true,
            //     message: constantObj.attendance.ALL_CHEKOUT_ATTENDANCE,
            //     data: resData
            //   });
            // }

            return res.status(200).json({
              success: true,
              message: constantObj.attendance.ALL_CHEKOUT_ATTENDANCE,
              total_count: totalResult.length,
              data: result
            });
          }
        });
      })
    }
    catch (err) {

      return res.status(400).json({
        success: false,
        error: { message: err },
      });
    }
  },

  absentEmployees: async (req, res) => {
    try {

      var search = req.param("search");
      var sortBy = req.param("sortBy");
      var page = req.param("page");
      var count = req.param("count");
      var id = req.param("id");
      var start_date = req.param("start_date");
      var end_date = req.param("end_date");

      if (!page) {
        page = 1;
      }
      if (!count) {
        count = 10;
      }
      let skipNo = (page - 1) * count;
      let query = {};
      // query.$and = [];

      if (search) {
        query.$or = [
          { name: { $regex: search, $options: "i" } },
          { email: { $regex: search, $options: "i" } },
        ];
      }

      if (id) {
        query.userId = id;
      }
      if (start_date && end_date) {
        var date = new Date(start_date);
        date.setDate(date.getDate());
        var Enddate = new Date(end_date);
        Enddate.setDate(Enddate.getDate() + 1);
        query.$and = [
          { punchInDate: { $gte: date } },
          { punchInDate: { $lte: Enddate } },
        ];
        // query.currentDate = { $gte: new Date(date), $lte: new Date(Enddate) };
      }

      query.checkOut = ""
      query.isDeleted = false;

      db.collection("attendance").aggregate([
        {
          $project: {
            checkIn: "$checkIn",
            checkOut: "$checkOut",
            userId: "$userId",
            name: "$name",
            email: "$email",
            totalTime: "$totalTime",
            totalseconds: "$totalseconds",
            attendanceDate: "$attendanceDate",
            punchInDate: "$punchInDate",
            reason: "$reason",
            status: "$status",
            check_in_device: "$check_in_device",
            check_out_device: "$check_out_device",
            createdAt: "$createdAt",
            deletedBy: "$deletedBy",
            addedBy: "$addedBy",
            updatedBy: "$updatedBy",
            isDeleted: "$isDeleted",
            deletedAt: "$deletedAt",
            updatedAt: "$updatedAt",
          },
        },
        {
          $match: query,
        },
        {
          $sort: { createdAt: -1 },
        },
      ]).toArray((err, totalResult) => {
        // console.log(totalResult, "=========total");
        if (err) {
          return res.status(400).json({
            success: false,
            error: { message: err },
          });
        }
        db.collection("attendance").aggregate([
          {
            $project: {
              checkIn: "$checkIn",
              checkOut: "$checkOut",
              userId: "$userId",
              name: "$name",
              email: "$email",
              totalTime: "$totalTime",
              totalseconds: "$totalseconds",
              attendanceDate: "$attendanceDate",
              punchInDate: "$punchInDate",
              reason: "$reason",
              status: "$status",
              check_in_device: "$check_in_device",
              check_out_device: "$check_out_device",
              createdAt: "$createdAt",
              deletedBy: "$deletedBy",
              addedBy: "$addedBy",
              updatedBy: "$updatedBy",
              isDeleted: "$isDeleted",
              deletedAt: "$deletedAt",
              updatedAt: "$updatedAt",
            },
          },
          {
            $match: query,
          },
          {
            $sort: { createdAt: -1 },
          },
          {
            $skip: skipNo,
          },
          {
            $limit: Number(count),
          },
        ]).toArray(async (err, result) => {

          if (err) {
            return res.status(400).json({
              success: false,
              error: { message: err },
            });
          } else {
            var array = [];
            var absent_employees_array = [];

            if (result && result.length > 0) {
              for await (let user of result) {
                var user_id = String(user.userId)
                array.push(user_id);
              }
            }

            var qry = {};
            qry.id = { 'nin': array };
            qry.role = { 'nin': ["admin", "subadmin"] };
            qry.status = "active";
            qry.isDeleted = false
            // console.log(JSON.stringify(qry), "================qry")

            let attendance_data = await Users.find(qry);
            if (attendance_data && attendance_data.length > 0) {
              for await (let users of attendance_data) {
                //let obj ={};
                absent_users = users.fullName;
                // obj['name'] = users.fullName;
                // obj['email'] = users.email;
                absent_employees_array.push(absent_users);
              }
            }


            let resData = {
              total_count: totalResult.length,
              data: result
            }

            return res.status(200).json({
              success: true,
              message: constantObj.attendance.ABSENT_EMPLOYEE_LIST_FOUND,
              total_count: absent_employees_array.length,
              data: absent_employees_array
            });
          }
        });
      })
    }
    catch (err) {
      return res.status(400).json({
        success: false,
        error: { message: err },
      });
    }

  }

  //   absentEmployees: async (req, res) => {

  //     try {
  //       var search = req.param('search');
  //       var isDeleted = req.param('isDeleted');
  //       var page = req.param('page');
  //       var count = parseInt(req.param('count'));
  //       let sortBy = req.param("sortBy");
  //       let addedBy = req.param('addedBy');
  //       let start_date = req.param('start_date');
  //       let end_date = req.param('end_date');


  //       var date = new Date();
  //       var current_date = date.toISOString().substring(0, 10);

  //       var query = {};
  //       query.isDeleted = false;
  //       if (search) {
  //         query.$or = [
  //           { email: { $regex: search, '$options': 'i' } },
  //         ]
  //       }
  //       let sortquery = {};
  //       sortquery = { updatedAt: 1 }

  //       if (addedBy) {
  //         query.addedBy_id = ObjectId(addedBy);
  //       }

  //       const pipeline = [
  //         {
  //           $project: {
  //             fullName: "$fullName",
  //             email: "$email",
  //             // end_date: "$end_date",
  //             // description: "$description",
  //             isDeleted: "$isDeleted",
  //             // addedBy: "$addedBy",
  //             // updatedBy: "$updatedBy",
  //             // createdAt: "$createdAt",
  //             updatedAt: "$updatedAt",
  //           }
  //         },
  //         {
  //           $match: query
  //         },
  //         {
  //           $sort: sortquery
  //         },
  //       ]
  //       db.collection('users').aggregate([...pipeline]).toArray((err, totalResult) => {
  //         if (page && count) {
  //           var skipNo = (page - 1) * count;
  //           pipeline.push(
  //             {
  //               $skip: Number(skipNo)
  //             },
  //             {
  //               $limit: Number(count)
  //             })
  //         }
  //         db.collection('users').aggregate([...pipeline]).toArray(async(err, result) => {


  // var qry ={};

  //           if (start_date && end_date) {
  //             var date = new Date(start_date);
  //             date.setDate(date.getDate());
  //             var Enddate = new Date(end_date);
  //             Enddate.setDate(Enddate.getDate() + 1);
  //             qry.and = [
  //               { punchInDate: { ">=": date } },
  //               { punchInDate: { "<=": Enddate } },
  //             ];
  //             // query.currentDate = { $gte: new Date(date), $lte: new Date(Enddate) };
  //           }

  //           qry.checkOut = ""
  //           qry.isDeleted = false;

  // const attendance_data = await Attendance.find(qry);
  // console.log(attendance_data,"=================attendance_data");





  //           return res.status(200).json({
  //             "success": true,
  //             "data": result,
  //             "total": totalResult.length,
  //           });
  //         })
  //       })
  //     } catch (err) {
  //       return res.status(400).json({
  //         success: false,
  //         error: { code: 400, message: "" + err }
  //       })
  //     }




  //   }





























}


ActivityMail = function (options) {
  const fullName = options.fullName;
  const date = new Date(options.date);
  const newDate = new Date(date.toString());
  let hours = newDate.getHours(); // gives the value in 24 hours format
  const AmOrPm = hours >= 12 ? "PM" : "AM";
  hours = hours % 12 || 12;
  const minutes = newDate.getMinutes();
  const time =
    "" +
    (hours < 10 ? `0${hours}` : hours) +
    ":" +
    (minutes < 10 ? `0${minutes}` : minutes) +
    " " +
    AmOrPm;
  message = "";
  style = {
    header: `
      padding:30px 15px;
      text-align:center;
      background-color:#f2f2f2;
      `,
    body: `
      padding:15px;time
      height: 230px;
      `,
    hTitle: `font-family: 'Raleway', sans-serif;
      font-size: 37px;
      height:auto;
      line-height: normal;
      font-weight: bold;
      background:none;
      padding:0;
      color:#333;
      `,
    maindiv: `
      width:600px;
      margin:auto;
      font-family: Lato, sans-serif;
      font-size: 14px;
      color: #333;
      line-height: 24px;
      font-weight: 300;
      border: 1px solid #eaeaea;
      `,
    textPrimary: `color:#3e3a6e;
      `,
    h5: `font-family: Raleway, sans-serif;
      font-size: 22px;
      background:none;
      padding:0;
      color:#333;
      height:auto;
      font-weight: bold;
      line-height:normal;
      `,
    m0: `margin:0;`,
    mb3: "margin-bottom:15px;",
    textCenter: `text-align:center;`,
    btn: `padding:10px 30px;
      font-weight:500;
      font-size:14px;
      line-height:normal;
      border:0;
      display:inline-block;
      text-decoration:none;
      `,
    btnPrimary: `
      background-color:#3e3a6e;
      color:#fff;
      `,
    footer: `
      padding:10px 15px;
      font-weight:500;
      color:#fff;
      text-align:center;
      background-color:#000;
      `,
  };

  message +=
    `<div class="container" style="` +
    style.maindiv +
    `">
  <div class="header" style="` +
    style.header +
    `text-align:center">
      <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAREAAADOCAMAAAAuVcbyAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAJcEhZcwAADsMAAA7DAcdvqGQAAACNUExURVJSVlVVV/+WNP+oV/z7+v3z6vX08/+PJv/////+/VxbXf/Hk+7s6//s2mRiZaaemf+QKvyhTOPh4YiGhv/48m5qanVzdH57e/jBj9vY1f3PpZiWl6upqP+aP/zYt/6zbtDNy6qinP3lzsjCv766uJSOiv7AiO3m4ryyrLCwsbmvp/i1d/qcRem5j/yc2l+2LUEAABRdSURBVHja7F2Ldqo6ECUasMlFjMpDtMeiSHGh5/9/7/LUPCYWrKelFqwVXX2xu2dmZ2YyGi/DIR7GAMGAyIDIgMiAyIDIgMiAyIDIgMiAyIDIgMiAyIDIgMiAyHAMiAyIDIgMiAyIDIgMiAyIDIgMiAyIDIgMiAyIDIgMiAzHgMiAyIDIgMiAyIDIgMiAyIDIHQe2rDSMovh4PGbHYxxHYWpb+HciQrAVxq+zwPccilD+URyUUs8PNlkcPhiW3iOC7fCY+DkUBkKGUXxCqDovgTGoEySH0CK/BBFsxzPfKa/cKPAw6sfmbpT4oByVXfQoUHqMCLHCmU/Lq66h0D7mhxNkIX5qRIh9WNKKC82Fl6eIP0E8a5CXRFbvESHT6XQP3KZT8gEex4A2Fy1cvmg4EltoEls9R8Qdj+Djzb3ND5+/fvW8ORVfQQUmEe45ImzClGPCJuMbiFhRQJFsGOA54ljTkMaZLXCPETHnE/jQI4IXG2pwsUWKL4i/etiveAerzxzpioh18Opgq79kjhyid6m/gCYL8iwcIemGCizgfQbPEiDw8A7Fu9+bfANHmB4RHAWI9yCSi+CMSXrVkFBBztHqMUeY+FG+AiLiHhxdcJVsR7QoQ9S05at0Zv18P2LtqhADRhORLJA8EQUbQpv0x/gRBiNibxCSbAMMKgZvUTBpyhtK7D5yZM4gq4EQsROqRg35eqVzzTc0iC7THnIEthrAj9gJAgGQ17wKBjdIZdzDki9HhMEcsRIqxFjNWhcpMVkkj/Q1aNM9R/BvESHufDIBIo2CiDWj4ppOUiQIcKyCPNGINpThn2E1MiL4lRqc9FIEKuxYNcsb4RtoTPrHEaaajeRHcEwh9ynoUNlz8MseWbfwMdhf9JIjSqjJEbn+70joIaQIU6Qs/VXVJodfwNRQ0tGV/PPo+7EeIfZScZnyQqX+RD0vWCabJFn6noME/aaFjh5I7zjCAFA4RPAMzglJ4tUJNocotS3LxRi7lh3Gs4AiULCIRuelpHexBjAcDpGYqmSQlToNXkMLk5fiVv3kqnCx86kSrWWHhDa4l35EMpurH0kDpAkZF9LQZWyT/IDKF4cA3fI9JbuifscaKfriHZIDq+w7g9htaKH+BmIfPQT7nssPTdwe6hE52lwQiRxh1aZmypxX+4O8/aLRu6p+rTMDUd9iDZNt5qJHiJWgW/khA/kR/sgxEuvoqMsajmsowf3jiGo15XWSGIEiorkeukxxi0iBI99QUm8cbZyQ9M2PaLIBxA4goXlNhCV2u0vBoQ+k3q5Jxh3uM0c4qyEHKjNEoPvGbI19Dgm0Cqp/lG/3iSNgtKkQMQNZTglqokN6g+DIA0rBDSwdfOv3xRpSiTONZC+jbqcUWM43ZKhL4IZtbo9iDdPm0KwlvIypnnphJ/VNrBlCatdAZ7P5Bs16rdeEFO5/qMLMsWuyJ/WB9ED9QSPSJz8i2s3Fj7jCEk9e6iWd6y3kkmYBiJeRHnGEAdGmQCT1DPivL6Vq2P231VaoVPqK29LtGUeYaDRlNuCApAIl30e16164JTimUFm4fOLZvfIjnNlcrcZK1NahywveXQU52wdrOmXiqK2f/ppYw4AcGll4hranjO7uqu3nC2lA61Wdnoe+cYSJDzkiB2RIBeyrGvEW9/2+iEpl4stp6zrF18Ua0cWOXTeRU6Mc3Td3tn+kHtJ1NCa4HxyZM9lqqqdjK/XAUFkZfXRnjxBeIt0RuKQfiMiFmoYjZkQNXeEJBXf2fhASZccsO8pHlr8W94cjUKZ1Mrd2CGkSo8YdtckrJqTIP+LqTop79RKYpv0GP2LNZbda29DJXMo12+vpPersYcfX+BH1WC08uLOqeC2wnxcRW+NH2Dak6oK3UquGMSPPzRFJo5Vno/0RqVW4BpqYkCdGBO4xOpszAypNVp2X3+lGvizW8NmAQo4UKWfDgLvJfPvlyf0IU8ucJ9vXbQPI1aX7OzgiONi/oYNUp1rDs8PPi0ihR5iaQmNsHVGwlbc8j1+eniMqT7aXLLza5XD3ouanxpoCnj/7DCkVzYYqzuLlF/gRqQvtnO70baue/cwcaWKNlCL5M91A28yqJ98bfL8h1hSgzM3EUPojGlwC84kRada+ktmw+XSJDF07XYKfniNKRY+N08CA4eiQ/vvRa1/ZdE5lShHoKyoev3Xl+4V+RDCbU5UhhvdB7J4cEZUgbMLeFw4kRiqEjr+BI/JCbx058B6JcrfDyy/wI1JTDdvHSNpHc3WsnVotf3CsEVhSSFbttu/vzRd9gR5hYAYt0W77Rp755ByRyhIlQCc7MLRtvYH79H5EiTXsv4Wj9p5+tuT70/SIsNrbH6huFx5CGfkFViOptPl0J26/5Bob8lAzdX8FR3iWnOylod2y6y225rP7EcVo1gsHmE5UcyYw3/fPzhGpYjM5N/U8cb9zDcrMHG97jQhJtUeLHg9Oj/BGkwB9381AiHg/WvcaEWvpeZ4D3me4q2dtiuAO0Adfc8RbrCcn0udKuBsYSldyfW/TCGTzmrXut0ozoMOyOV/ab5PxZ1yra2qPx3Rd4Q2Sm+67dCVzufgGllXTdKo0BZTdRdPz5Dy9HxC8mp/P+Ud+n18+VU/f3EcgQkimAyQ3edLOanhQ2Hl6oGpf72UDd7jNXfEnXKv1Vkygm9T9gOVCuxxRJ266/pRnjRGC2taLF1pUrMVcPCspEoD17zr22qf8r/97v5DXjhvLPTp+DCIX7aBi0qL4Zkt98blePVBwwln1kJVX9AlHsj9zzfficur9MX6E5MtUZMhDHOrWl4+jDd/PWi6Atwsf2t57cU3bguCj+zVa+f1gTzFbPybWEJKo0yyaC/i4V1uuYJ3MVwSr1coO7VO5OF7dazbkL1MYckncPUizXnbkKqOCikLCRwHYPAsK7byPHKSde2DQuCI9G9878K6Y9akxmz/TB/U8k1JPIWBQEPp4Jg6xz9yfxkbrNBDHqoqtrL69qr/wzmhD0jMDNmpUHsx8EEde3CWc2ak2BH4w5LLkCLtKkRlSh5ldf2h2kS+n+8yGrBnAkOrkjTyqLx5n8sQl/pJub8nNOcKlzk72kcLD3qqTXMEz1pjXnWoEnjVWPK4f1wUeOvDO4jre3BwT1PiRgikn8+AAs4quoezVHF8ixTv+ROxVCTJpL/taINJshEHgDkO0u+EFiXluwi5bNYAYmtnE3mI7YtcupLskvMKMS9hv60Za5UdiBI5cqm90dsNw7CbWnNf20VH7Zzg/UlCEj9N35Banc1iKlG7EehwiRF6ayQMd9TOEC46U/6G3fTqjwGZtjid+6UWucWnbPSWwYkr//eW0ddKlDUfy1Z6SDxVO/ci9GWtygkRLBM0z47TIYTpnPM3HJulMEY1gLWUwfiAi9fZzVVE1p8iZwTQheThl8//S9NUDBt4LJ0t7JV7P5L2j3eC/jGmMhnXQfK0QIUcENVvyNWwvA6eOmePT2i7mURmgE7qeOOH+LG2z6JhdJNs/yq7iiwWx1YMzz9xyDx5shxDyX4H3TMGmFWbF22moKSJhFi/K7DdFSJz3HeyG2GPZYBhfe38wIiSm0FwuCR0nyUKbGzuErTTMlg5freJx4dX80l4DDmDeIQS7JwbKd9Y1crWsTlgJAmeEyB+On+yyOIqKd2fKNoFHDQNyILJo9aJrXoNLQLJ5a5bg1QjoJG5Q6ZKVa1uvWXgQOeD3xijekanKvKmBSRqHVzfRHCopwuQZAzkk7UIEXo/gBd6k81q6LSLVCFVD989WVC1CCI7WykidYrS7qQkTLFd2bfhurc/aZGKRZ13/i3lo1kyZoyvPtRCnyarNzODyrswTrRmDlWau1N7NDy/HrUxGHJbEGdC8S1+50d6XB9DwbUVpcVsyVY5Au4yMYLE9q/bPDYHa3qYJ3p/Y5MbBWKcw3r7u20wcU2cOgcO4gffdAXhSpFjC/XyiX48UwuS0t24sJle6qbiXHfn2v0HkhUSemFNEyvy/D8aaA6MTkBdOx3o4WIPJFtb0OF2PGdNMGGsSrN0Scl16A3DsoBtvMgQ37Ep+Q044exHZn8HlmVjoGY1XW9MVPAp2p9sTELRlUE/Wv5vhi2PPAKIMULNUnO1lMIoInx+SPHIyDd1F5Tk650uCbfW+Yvv9dr0an0cMnsjHQ9RJ+XZFhINEXdQD24eQVOpReBOERfLTOukuTAKofAut0ehP8dZiZfUSQEEWaJ0rHR07akjka4bcQVEEnGp+PafLenWYjsXgy9SN9ez/9q5FO1UciiqElPASCY8LAeFWyiyuc///9+acJCAUrN7Wzqw1i1QRSaRmu8/OyXv2y4eLGSo3jebypx0dfzrGiOXZfn9zN4j16ysLOEuP/9r4prtp1s3m43CTWqG2GfLNiMAP+ubMtGJtgup+fR3qGXrJZNEYcqrCu3kOH0JhTi7j9G/sO0F/Bfvb4x12qztlvO+dAYvJp/0nul6yNJrw5WW1kTBcsaUV//0Toww+MzKPNdzZrY2J2S+l9cbmAMn7jbtoYbx8aDThHaNZEZYwvHyit/RzYxW9X8lH1d+VxseZyQRiuS0iLcJ7pU34cocS7xvQLp8ZdfHJ0ZssF8G62bzvLV/2Fzu8WVuIS7IkXC7MMctnuFbArE2OlB0S37IPFlsGIv9o8+bs10uaxWaAs4VSszMl5P09JSRjQ+n9QiW8b0xh/dfKl/8qIoz++nnAv8PPw/VEhcMhWY5fvLV/2dW2grcD3uSXuwx3q2wfMWJR0oSXlX/xQCfQHY7gLNRF0GNxdzfquze0ZDhRIXlZ2dNV94Ov19hueG0f1A9X7v/AeMXdPQk9fEsQv7tO8QIfP1x9/LEafqvn8jE+Z2+Gp9stKNJ9nSM2I4ughGQ4GR621pjrRXu8gG+IPVyAD8ZweC9R1xXu3osWk/9r5TE+Z2+G5HBHMqie/q6PCMldRIbj+JB7yLDJVjJ/XE7p+zx6h0ku7qZnjyT6ltL3fxw2RDZENkSejogXYfCYp6oiNKIML3kEOyxUnHLCozzHUxLpD8ieQG9yj4iQSDV5Ei/Pc0qUD0iG3g9Mi58eqjzEl8FSLe6vr6/TxmcLo1BzY5VKya+vqgdETX7EFx0dW3I0K/GtLyNCRODA3xvtE5zPT/rgTDhcSA6eTTMH4pwS8WgzOHPS0vNSTO+oZVVEKb9hiVecNPLSUuIh0kCmlffXa31RLohNRRAEpaqwMl+2Hhou5rQ7wtnRvebHNYyqqn2bnWSio5yAEteFHlojX/2q0NHVyZUpTsfi64jwoIRwtqMEZxjlAfdIBpe4I4iXphgHSFGxT0Xbl6nT07bke1GeVWexWqWpKYWTlT31AgHv8tThfdtzJ4vk/XNFvpRTu3Te4HqvaVC4ptG5r8x+NY3aLQrTMMfBAp3RdRfDtOxCeuuqL/TVUD6pb7hMIuIy33WrqnN9v7owFtfHr3PE5qluY+jhm9K3oLFJBtjArxlBJhTDabkTMuv0jERqh7lI7X6vF4pQWHgJHKM0kBu90zbIPNwYQY2up1lGaIp37vOxktlVFhytYyW79eKiGmdVdJWPRDnZheGPXlOn5hgwXxJLvjA7Nk208A6iCqNgT+DIMPmMZkF0dpDa8N3BEhwwAq4HDgSzGWqtk2tDCAJBdYpywKV0GnLFmHAnQK4gRwAR5NSk6UTmGzIzON+FzOoYc5KIjLyxjkejm3JEvhCJiG2BjZmPTDp6gCO5Vs/G4SnuhAocibwGvjxkQsX181VUWkfNqcidXug5OJHiCBy9hA/ooaFQnvVgidpqeidrpm1JwBEcuTVmJB5P4Se3/Br4UYQnqayoEkYhzWLKEYlIjIQrDPOh4fb3ObJHVcSfmojdHk2cZE6aBEELmdihZJZEzOceDRyBy7kWBW/kSDTIhEzgIQd7vD3NwARpHzgimiHCbH8khqaGPAmrqjLcGBAZlJXUlXWSXZr6E4ojgKL07S0zrNkzSt9BWW3coynwlPlwUea474VSVsLny9xpRDy0g0zZUzTqSD5BpAwiIBwhJUAiOQIJQYPzOUcmE4CZa4yIGMYFjakwtLJC2dSB5NTkHUfYwCv3sSGyj+uILGwVR/SlUVkXHIkUgGUEGWzmOqLOhpJdcgRluvf0zeh5IkqSEpDTEZFLpZ2SrupCKZNKR/DMDQv/tcZpNFcdsUeO2FPFeU5ZY/e7MguiCSJDJsD82yVHCN+BuxLsxKSsgaMsT3QBlACJOMo0hSI3zUbBbWYcuYoHs6Y6Usux80M+WWxiB2j44rIbHCnCpyBy5Qg4JF4TSIvPRmW8RunkZORIFHC0qSyJ5mVNP5gNKXct3D/Fj3jccTiJWqXg55mOsCLUQ6+IOw6OAfYo7wNKX0WBE/gsEI5mzNZ0RGLHnsQRbJIBiuNPJ3ZwmFgNxkn6cOlnRaIdOVIqr6SR/Jn4Ix4HE4HEUYnL0VCNeJTuMvhgiSbk5DOOMKs2XETG6oyxVRAQAVEpVD6xISjujtIxKUAtEArCvo0jAYfQk0aWNxEWupnmt5fKuBZdNAfUVvAkaAeOeEopVak65YgdcXBwhUgc9OoI1zfLsSgWu0xkjpjrCNC/No51V1dGbU1jLPOIpa9Z1/UlBqdUefBVF/vG3zU49EsdeQpHyizNsrSkJZdy2WaNLfRSQ1RAVJZJI2gElMipkArQoMsVcc39Hr0Nj/fSZZNpaQvQJfwsMz7cDO4Ayfo0UfUd7ZFpTsRFXVXH+jogjRUIzsk8wdM8msfaetUePnPrGOTGhBqML6s5pNOzl06m/xRlpSrAq36vxeIap8nked5Qj8VLZPyh6Xiwx2ILEpOJ8qgzFTP1ftnY1QWVV2vqcMYyBg4slgEqLbr1UZ5jkNHywvAZ9hSO/KeBPZzgeQt0bC1GGyIbIhsiGyIbIhsiGyIbIhsiGyIbIhsiGyIbIhsiGyJb2BDZENkQ2RDZEPne8A+kO8W9npQs3AAAAABJRU5ErkJggg==" width="150" style="margin-bottom:20px;" />
     
      <h3 style="` +
    style.hTitle +
    `"> Hello Management,</h3>
  </div>
  <div class="body" style="` +
    style.body +
    `">
      <p>Employee <u>` +
    toTitleCase(fullName) +
    `</u> has <strong>` +
    options.type.toLowerCase() +
    `</strong> at <strong>` +
    time +
    `</strong>.</p>
  </div>
  <div class="footer" style="` +
    style.footer +
    `">
  &copy 2023 JC Software Solution All rights reserved.
  </div>
</div>`;

  SmtpController.sendActivityMail(options.type, message);
};


function toTitleCase(str) {
  return str.replace(/\w\S*/g, function (txt) {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  });
}