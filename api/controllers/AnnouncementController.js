/**
 * AnnouncementController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

var constantObj = sails.config.constants;
var ObjectId = require("mongodb").ObjectID;
const db = sails.getDatastore().manager;

module.exports = {

    addAnnouncement: async (req, res) => {

        try {
            const date = new Date();
            const announcement_date = date.toISOString().substring(0, 10);
            const data = req.body;

            if (!data.subject) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Announcement.SUBJECT_REQUIRED },
                });
            }

            if (!data.description) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Announcement.DESCRIPTION_REQUIRED },
                });
            }

            data.date = announcement_date;
            data.addedBy = req.identity.id;

            const announcement_data = await Announcement.create(data).fetch();

            if (announcement_data) {
                return res.status(200).json({
                    success: true,
                    message: constantObj.Announcement.ADD_SUCCESS,
                    data: announcement_data,
                });
            } else {
                return res.status(400).json({
                    success: false,
                    message: constantObj.Announcement.ADD_FAILED
                });
            }

        } catch (error) {
            return res.status(400).json({
                success: false,
                error: { code: 400, message: error },
            });
        }
    },

    getSingleAnnouncement: async (req, res) => {

        try {

            const id = req.param("id");

            if (!id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Announcement.ID_REQUIRED },
                });
            }

            const announcement_details = await Announcement.findOne({ id: id, isDeleted: false });

            if (announcement_details) {
                return res.status(200).json({
                    success: true,
                    message: constantObj.Announcement.FOUND,
                    data: announcement_details,
                });

            } else {
                return res.status(400).json({
                    success: false,
                    message: constantObj.Announcement.FAILED
                });
            }

        } catch (error) {
            return res.status(400).json({
                success: false,
                error: { code: 400, message: error },
            });
        }

    },
    getAnnouncementListing: async (req, res) => {

        try {
            var search = req.param('search');
            var isDeleted = req.param('isDeleted');
            var page = req.param('page');
            var count = parseInt(req.param('count'));
            let sortBy = req.param("sortBy");
            let addedBy = req.param('addedBy');
            let start_date = req.param('start_date');
            let end_date = req.param('end_date');

            var date = new Date();
            var current_date = date.toISOString().substring(0, 10);

            var query = {};

            if (search) {
                query.$or = [
                    { subject: { $regex: search, '$options': 'i' } },
                ]
            }

            if (start_date && end_date) {
                query.$and = [
                    { date: { '$gte': start_date} },
                    { date: { '$lte': end_date} }
                ]
            }
            let sortquery = {};
            sortquery = { updatedAt: -1 }

            query.isDeleted = false;

            if (addedBy) {
                query.addedBy_id = ObjectId(addedBy);
            }

            const pipeline = [
                {
                    $project: {
                        subject: "$subject",
                        description: "$description",
                        date: "$date",
                        isDeleted: "$isDeleted",
                        addedBy: "$addedBy",
                        updatedBy: "$updatedBy",
                        createdAt: "$createdAt",
                        updatedAt: "$updatedAt",
                    }
                },
                {
                    $match: query
                },
                {
                    $sort: sortquery
                },
            ]
            db.collection('announcement').aggregate([...pipeline]).toArray((err, totalResult) => {
                if (page && count) {
                    var skipNo = (page - 1) * count;
                    pipeline.push(
                        {
                            $skip: Number(skipNo)
                        },
                        {
                            $limit: Number(count)
                        })
                }
                db.collection('announcement').aggregate([...pipeline]).toArray((err, result) => {
                    return res.status(200).json({
                        "success": true,
                        "data": result,
                        "total": totalResult.length,
                    });
                })
            })
        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { code: 400, message: "" + err }
            })
        }
    },













    updateAnnouncement: async (req, res) => {

        try {

            const data = req.body;

            if (!data.id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Announcement.ID_REQUIRED },
                });
            }

            const find_announcement = await Announcement.findOne({ id:data.id, isDeleted: false });

            if (!find_announcement) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Announcement.ID_NOT_FOUND },
                });
            }

            data.updatedBy = req.identity.id;

            const updated_data = await Announcement.updateOne({ id: data.id, isDeleted: false }).set(data);

            if (updated_data) {
                return res.status(200).json({
                    success: true,
                    message: constantObj.Announcement.UPDATE_SUCCESS,
                    data: updated_data,
                });
            } else {
                return res.status(400).json({
                    success: false,
                    message: constantObj.Announcement.UPDATE_FAILED
                });
            }
        } catch (error) {
            return res.status(400).json({
                success: false,
                error: { code: 400, message: error },
            });
        }

    },
    deleteAnnouncement: async (req, res) => {

        try {
            const id = req.param("id");
            var deleted_by = req.identity.id;

            if (!id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Announcement.ID_REQUIRED },
                });
            }

            const announcement_data = await Announcement.findOne({ id: id, isDeleted: false });

            if (!announcement_data) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Announcement.ID_NOT_FOUND },
                });
            }

            var deleted_at = new Date();

            const delete_announcement = await Announcement.updateOne({ id: id, isDeleted: false }).set({ isDeleted: true,deletedBy : deleted_by,deletedAt : deleted_at });
            if (delete_announcement) {
                return res.status(200).json({
                    success: true,
                    message: constantObj.Announcement.DELETE_SUCCESS
                });
            } else {
                return res.status(400).json({
                    success: false,
                    message: constantObj.Announcement.DELETE_FAILED
                });
            }


        } catch (error) {
            return res.status(400).json({
                success: false,
                error: { code: 400, message: error },
            });
        }
    }
};

