/**
 * InventoryController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const constants = require('../../config/constants').constants;
const db = sails.getDatastore().manager;

exports.addInventory = async (req, res) => {
    try {
        let data = req.body;
        data.addedBy = req.identity.id;
        data.updatedBy = req.identity.id;

        let save_inventory = await Inventory.createEach(data).fetch();
        if (save_inventory) {
            return res.status(200).json({
                success: true,
                message: constants.INVENTORY.INVENTORY_SAVE
            });
        }
    } catch (err) {
        return res.status(400).json({
            success: false,
            error: { message: err },
        });
    }
};


exports.findInventory = async (req, res) => {
    try {
        let id = req.param('id');
        if (!id) {
            throw constants.INVENTORY.ID_REQUIRED;
        }

        let get_Inventory = await Inventory.findOne({ id: id });

        if (get_Inventory) {
            if (get_Inventory.addedBy) {
                let get_added_by_details = await Users.findOne({ id: get_Inventory.addedBy });
                if (get_added_by_details) {
                    get_Inventory.addedBy_name = get_added_by_details.fullName;
                }
            }
            if (get_Inventory.updatedBy) {
                let get_updated_by_details = await Users.findOne({ id: get_Inventory.updatedBy });
                if (get_updated_by_details) {
                    get_Inventory.updatedBy = get_updated_by_details.fullName;
                }
            }
            if (get_Inventory.category_id) {
                let getcategory_details = await Category.findOne({ id: get_Inventory.category_id });
                if (getcategory_details) {
                    get_Inventory.category_name = getcategory_details.name;
                }
            }
            return res.status(200).json({
                success: true,
                message: constants.INVENTORY.GET_SINGLE_INVENTORY,
                data: get_Inventory
            });
        }
        throw constants.INVENTORY.INVALID_ID;
    }
    catch (err) {
        return res.status(400).json({
            success: false,
            error: { message: err },
        });
    }
};


exports.getAllInventories = async (req, res) => {
    try {

        let search = req.param('search');
        let page = req.param('page');
        let count = req.param('count');
        let status = req.param('status');


        if (!page) {
            page = 1;
        }
        if (!count) {
            count = 10;
        }
        let skipNo = (page - 1) * count;
        let query = {};
        query.$and = [];

        if (search) {
            query.$or = [
                { user_name: { $regex: search, '$options': 'i' } },
            ]
        }

        if (status) {
            query.$and.push({ status: status });
        }


        query.$and.push({ isDeleted: false })

        db.collection("inventory").aggregate([
            {
                $lookup: {
                    from: 'users',
                    localField: 'addedBy',
                    foreignField: '_id',
                    as: "addedBy_details"
                }
            },
            {
                $unwind: {
                    path: '$addedBy_details',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $lookup: {
                    from: 'category',
                    localField: 'category_id',
                    foreignField: '_id',
                    as: "category_details"
                }
            },
            {
                $unwind: {
                    path: '$category_details',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $lookup: {
                    from: 'users',
                    localField: 'updatedBy',
                    foreignField: '_id',
                    as: 'updatedBy_details'
                }
            },
            {
                $unwind: {
                    path: '$updatedBy_details',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $project: {
                    id: "$_id",
                    category_id: "$category_id",
                    code: "$code",
                    serial_number: "$serial_number",
                    replace_code: "$replace_code",
                    replace_serial_number: "$replace_serial_number",
                    isReplaced: "$isReplaced",
                    status: "$status",
                    image: "$image",
                    createdBy: "$createdBy",
                    isDeleted: "$isDeleted",
                    deletedAt: "$deletedAt",
                    addedBy: "$addedBy",
                    updatedBy: "$updatedBy",
                    updatedAt: "$updatedAt",
                    createdAt: "$createdAt",
                    InventoryImage: "$InventoryImage",
                    addedBy_name: "$addedBy_details.fullName",
                    category_name: "$category_details.name",
                    updatedBy_name: "$updatedBy_details.fullName"
                },
            },
            {
                $match: query,
            },
            {
                $sort: { createdAt: -1 },
            },
        ]).toArray((err, totalResult) => {
            if (err) {
                return res.status(400).json({
                    success: false,
                    error: { message: err },
                });
            }
            db.collection("inventory").aggregate([
                {
                    $lookup: {
                        from: 'users',
                        localField: 'addedBy',
                        foreignField: '_id',
                        as: "addedBy_details"
                    }
                },
                {
                    $unwind: {
                        path: '$addedBy_details',
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $lookup: {
                        from: 'category',
                        localField: 'category_id',
                        foreignField: '_id',
                        as: "category_details"
                    }
                },
                {
                    $unwind: {
                        path: '$category_details',
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $lookup: {
                        from: 'users',
                        localField: 'updatedBy',
                        foreignField: '_id',
                        as: 'updatedBy_details'
                    }
                },
                {
                    $unwind: {
                        path: '$updatedBy_details',
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $project: {
                        id: "$_id",
                        category_id: "$category_id",
                        code: "$code",
                        serial_number: "$serial_number",
                        replace_code: "$replace_code",
                        replace_serial_number: "$replace_serial_number",
                        isReplaced: "$isReplaced",
                        status: "$status",
                        image: "$image",
                        createdBy: "$createdBy",
                        isDeleted: "$isDeleted",
                        deletedAt: "$deletedAt",
                        addedBy: "$addedBy",
                        updatedBy: "$updatedBy",
                        updatedAt: "$updatedAt",
                        createdAt: "$createdAt",
                        InventoryImage: "$InventoryImage",
                        addedBy_name: "$addedBy_details.fullName",
                        category_name: "$category_details.name",
                        updatedBy_name: "$updatedBy_details.fullName"
                    },
                },
                {
                    $match: query,
                },
                {
                    $sort: { createdAt: -1 },
                },
                {
                    $skip: skipNo,
                },
                {
                    $limit: Number(count),
                },
            ]).toArray((err, result) => {
                if (err) {
                    return res.status(400).json({
                        success: false,
                        error: { message: err },
                    });
                } else {

                    let resData = {
                        total_count: totalResult.length,
                        data: result
                    }

                    if (!req.param('page') && !req.param('count')) {
                        resData = {
                            total_count: totalResult.length,
                            data: result
                        }
                        return res.status(200).json({
                            success: true,
                            message: constants.INVENTORY.GET_ALL,
                            data: result
                        });
                    }

                    return res.status(200).json({
                        success: true,
                        message: constants.INVENTORY.GET_ALL,
                        data: resData
                    });
                }
            });
        })
    }
    catch (err) {
        return res.status(400).json({
            success: false,
            error: { message: err },
        });
    }
};


exports.editInventory = async (req, res) => {
    try {
        let data = req.body
        let id = req.body.id;
        if (!id) {
            throw constants.INVENTORY.ID_REQUIRED;
        }
        data.updatedBy = req.identity.id;
        let update_inventory = await Inventory.updateOne({ id: id }, data);
        if (update_inventory) {
            return res.status(200).json({
                success: true,
                message: constants.INVENTORY.UPDATED_INVENTORY,
                data: update_inventory
            });
        }
        throw constants.INVENTORY.INVALID_ID;
    } catch (err) {
        return res.status(400).json({
            success: false,
            error: { message: err },
        });
    }
};


exports.deleteInventory = async (req, res) => {
    try {
        let id = req.param('id');
        if (!id) {
            throw constants.INVENTORY.ID_REQUIRED;
        }

        let delete_inventory_record = await Inventory.updateOne({ id: id }, { 'isDeleted': true });
        if (delete_inventory_record) {
            return res.status(200).json({
                success: true,
                message: constants.SYSTEM_MANAGEMENT.DELETED_SYSTEM,
                data: delete_inventory_record
            });
        }

        throw constants.INVENTORY.INVALID_ID;
    }

    catch (err) {
        return res.status(400).json({
            success: false,
            error: { message: err },
        });
    }
};

