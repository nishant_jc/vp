/**
 * PortfolioShareController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const { constants } = require("../../config/constants");

var randomString = function () {
    var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    var Stringlen = 6;
    var randomstring = "";
    for (var i = 0; i <= Stringlen; i++) {
        var rnum = Math.floor(Math.random() * characters.length);
        randomstring += characters.substring(rnum, rnum + 1);
    }
    return randomstring;
};


exports.savePortfolioShare = async (req, res) => {
    try {
        let data = req.body;
        req.body.addedBy = req.identity.id;
        var key = randomString().toLowerCase();
        data.key_send = key;

        let createData = await PortfolioShare.create(data).fetch();
        return res.status(200).json({
            success: true,
            message: constants.PORTFOLIO_SHARE.PORTFOLIO_SAVED,
            key: key,
        })

    } catch (error) {
        return res.status(500).json({
            success: false,
            error: { code: 500, message: "" + error }
        })
    }
};

exports.getPortfolioShare = async (req, res) => {
    try {
        let key = req.param('key');
        if (!key) {
            throw constants.PORTFOLIO_SHARE.KEY_REQUIRED;
        }

        let get_key_data = await PortfolioShare.findOne({ key_send: key });

        const current_date = new Date();

        let current_date_time_stamp = current_date.getTime();

        let get_end_date = get_key_data.end_date;

        let get_end_date_new = new Date(get_end_date);

        let end_date_time_stamp = get_end_date_new.getTime();

        if (end_date_time_stamp >= current_date_time_stamp) {

            return res.status(200).json({
                success: true,
                message: constants.PORTFOLIO_SHARE.PORTFOLIO_KEY_DATA,
                data: get_key_data,
                isExpired:false
                
            });

        } else {
            return res.status(400).json({
                success: false,
                isExpired:true
            });
        }
    }
    catch (err) {
        return res.status(400).json({
            success: false,
            error: { message: err },
        });
    }

}
