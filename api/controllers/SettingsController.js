/**
 * SettingsController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    addsettings: async (req, res) => {
        try {
            let data = req.body;
            data.addedBy = req.identity.id;
            data.updatedBy = req.identity.id;

            let save_setting_data = await Settings.create(data).fetch();
            if (save_setting_data) {
                return res.status(200).json({
                    success: true,
                    message: 'setting added successfully.',
                    data:save_setting_data
                });
            }
        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { message: err },
            });
        }
    },
    setting: (req, res) => {
        Settings.find().exec(function (err, data) {
            // Settings.find().exec(function (err, data) {
            if (err) {
                return res.status(400).json({ success: false, message: err })
            } else {
                if (data && data.length > 0) {
                    return res.status(200).json({
                        success: true,
                        data: data[0]
                    })
                } else {
                    return res.status(200).json({
                        success: true,
                        data: {}
                    })
                }

            }
        })
    },


    updateSetting: (req, res) => {
        var id = req.param('id')
        req.body.updatedBy = req.identity.id
        data = req.body
        Settings.update({ id: id }, data).then(updatedSetting => {
            return res.status(200).json({
                success: true,
                code: 200,
                data: updatedSetting,
                message: "Setting updated successfully"
            })
        })
    }

};

