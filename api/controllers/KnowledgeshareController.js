/**
 * KnowledgeshareController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

var _request = require("request");
var ObjectId = require("mongodb").ObjectID;
var constantObj = sails.config.constants;
const db = sails.getDatastore().manager;
module.exports = {
  /**
   *
   * @param {*} req
   * @param {*} res
   * @description: Used to add post by user
   * @createdAt 24/09/2021
   * @createdBy : Chandra Shekhar
   */

  addpost: (req, res) => {
    API(KnowledgeshareService.savePost, req, res);
  },

  /**
   *
   * @param {*} res
   * @returns
   * @description: Api Used to get post
   * @createdAt : 24/09/2021
   * @createdBy Chandra Shekhar
   */

  getPosts: (req, res) => {
    try {
      var search = req.param("search");
      var listsortBy = req.param("sortBy");
      var page = req.param("page");
      var count = req.param("count");
      var userid = req.param("userid");         

      if (page == undefined) {
        page = 1;
      }
      if (count == undefined) {
        count = 10;
      }
      var skipNo = (page - 1) * count;
      var query = {};
      var sorting = "";
      if (listsortBy) {
        sorting = listsortBy.toString();
        if (sorting == "asc") {
          sortBy = -1;
        } else {
          sortBy = 1;
        }
      } else {
        sortBy = -1;
      }

      if (search) {
        query.$or = [{ addedBy: { $regex: search, $options: "i" } }];
      }

        query.isDeleted = false;
        
        if (userid) {      
            query.postByUser = userid;
         }

  
      db.collection("knowledgeshare")
        .aggregate([
          {
            $lookup: {
              from: "users",
              localField: "addedBy",
              foreignField: "_id",
              as: "addedBy",
            },
          },

          {
            $project: {
              id: "$_id",
              name: "$name",
              description: "$description",
              image:"$image",
              status: "$status",
              postByUser: "$postByUser",
              createdAt: "$createdAt",
              deletedBy: "$deletedBy",
              addedBy: "$addedBy",
              updatedBy: "$updatedBy",
              isDeleted: "$isDeleted",
              deletedAt: "$deletedAt",
              updatedAt: "$updatedAt",
            },
          },
          {
            $match: query,
          },
        ])
        .toArray((err, totalResult) => {
          db.collection("knowledgeshare")
            .aggregate([
              {
                $lookup: {
                  from: "users",
                  localField: "addedBy",
                  foreignField: "_id",
                  as: "addedBy",
                },
              },
              {
                $project: {
                  id: "$_id",
                  name: "$name",
                  description: "$description",
                  image:"$image",
                  status: "$status",
                  postByUser: "$postByUser",
                  createdAt: "$createdAt",
                  deletedBy: "$deletedBy",
                  addedBy: "$addedBy",
                  updatedBy: "$updatedBy",
                  isDeleted: "$isDeleted",
                  deletedAt: "$deletedAt",
                  updatedAt: "$updatedAt",
                },
              },
              {
                $match: query,
              },
              { $sort: { updatedAt: sortBy } },
              {
                $skip: skipNo,
              },
              {
                $limit: Number(count),
              },
            ])
            .toArray((err, result) => {
              if (err) {
                return res.status(400).json({
                  success: false,
                  error: { message: "" + err },
                });
              } else {
                return res.status(200).json({
                  success: true,
                  message: constantObj.knowledgeshare.GET_DATA,
                  data: result,
                  total: totalResult.length,
                });
              }
            });
        });
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },

  /**
   *
   * @param {id} req
   * @param {*} res
   * @returns
   * @description: Api Used to get single post by ID
   * @createdAt : 24/09/2021
   * @createdBy Chandra Shekhar
   */

  getSinglePost: async (req, res) => {
    try {
      let postID = req.param("id");
      if (postID == null) {
        return res.status(404).json({
          success: false,
          message: constantObj.knowledgeshare.ID_REQUIRED,
        });
      } else {
        var postData = await Knowledgeshare.findOne({ id: postID }).populate(
          "addedBy"
        );
        if (postData) {
          return res.status(200).json({
            success: true,
            message: constantObj.knowledgeshare.GET_DATA,
            data: postData,
          });
        } else {
          return res.status(404).json({
            success: false,
            message: constantObj.knowledgeshare.NO_RESULT,
          });
        }
      }
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },

  /**
   *
   * @param {id} req
   * @param {*} res
   * @returns
   * @description: Api Used to update post data by ID
   * @createdAt : 24/09/2021
   * @createdBy Chandra Shekhar
   */

  updateSinglePost: async (req, res) => {
    try {
      let postId = req.param("id");
      if (postId == undefined) {
        return res.status(404).json({
          success: false,
          message: constantObj.knowledgeshare.ID_REQUIRED,
        });
      }

      let formpostData = req.body;
      var updatedpostData = await Knowledgeshare.updateOne({
        id: postId,
      }).set(formpostData);
      return res.status(200).json({
        code: 200,
        success: true,
        message: constantObj.knowledgeshare.UPDATED_POST,
        data: updatedpostData,
      });
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },
  /**
   *
   * @param {id} req
   * @param {*} res
   * @returns
   * @description: Api Used to delete post by ID
   * @createdAt : 24/09/2021
   * @createdBy Chandra Shekhar
   */

  postDeletedByID: async (req, res) => {
    try {
       let deletedID = req.param("id");
       if (deletedID == undefined) {
        return res.status(404).json({
          success: false,
          message: constantObj.knowledgeshare.ID_REQUIRED,
        });
      }
      var deletedByUser = req.identity.id;
      var deletedData = await Knowledgeshare.updateOne({
        id: deletedID,
      }).set({ isDeleted: true, deletedBy: deletedByUser });
        
        if (deletedData) {
          return res.status(200).json({
            success: true,
            message: constantObj.knowledgeshare.DELETED_POST,
          });
        }
   
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },
};

