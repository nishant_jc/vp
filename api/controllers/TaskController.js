/**
 * TaskController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

var constantObj = sails.config.constants;
const db = sails.getDatastore().manager;
var ObjectId = require('mongodb').ObjectID;


module.exports = {

    /**add task */
    assignTask: async (req, res) => {
        data = req.body;
        if (!data.taskName && typeof data.taskName == undefined) {
            return res.status(400).json({
                success: false,
                error: { message: constantObj.task.MISSING },
            });
        }
        try {
            for await (task of data) {
                // project
                // employee
                let obj = {};
                obj.taskName = task.taskName;
                obj.project = task.project;
                obj.employee = task.employee;
                obj.isDeleted = false;
                obj.assignDateAndTime = task.assignDateAndTime;

                let exist = await Task.findOne(obj);
                if (exist) {
                    return res.status(400).json({
                        success: false,
                        error: { message: constantObj.task.EXIST },
                    })
                }
            }
            // let exist = await Task.findOne({ taskName: data.taskName, isDeleted: false });
            // console.log(exist,"====already exist...........");
            // if (exist) {
            //     return res.status(400).json({
            //         success: false,
            //         error: { message: constantObj.task.EXIST },
            //     })
            // } else {
            date = new Date();
            // req.body["addedBy"] = req.identity.id

            let assignTask = await Task.createEach(data).fetch()

            return res.status(200).json({
                "success": true,
                "code": 200,
                "data": assignTask,
                "message": constantObj.task.ADD,
            });
            // }


        } catch (err) {
            res.status(404).json({
                success: false,
                error: { "code": 404, "message": err }
            });
        }

    },

    /**get task list */
    getTaskList: async (req, res) => {

        try {
            var search = req.param('search');

            var page = req.param('page');
            var sortBy = req.param('sortBy');
            var projectId = req.param('projectId');
            var employee_name = req.param('employee_name');
            var project_name = req.param('project_name');
            var employeeId = req.param('employeeId');
            let createdAt = req.param('createdAt');
            var addedBy = req.param('addedBy');
            var start_date = req.param("start_date");
            var end_date = req.param("end_date");

            if (!page) {
                page = 1
            }
            var count = parseInt(req.param('count'));
            if (!count) {
                count = 10
            }
            var skipNo = (page - 1) * count;
            var query = {}



            if (employee_name) {
                query.employee_name = employee_name
            }

            if (project_name) {
                query.project_name = project_name
            }

            if (search) {
                query.$or = [
                    { taskName: { $regex: search, '$options': 'i' } },

                ]
            }


            sortquery = {};
            if (sortBy) {
                var order = sortBy.split(" ");

                var field = order[0];
                var sortType = order[1];
            }
            sortquery[field ? field : 'createdAt'] = sortType ? (sortType == 'descending' ? -1 : 1) : -1;
            query.isDeleted = false

            if (projectId) {
                query.project_id = ObjectId(projectId)
            }
            if (employeeId) {
                query.employee_id = ObjectId(employeeId)
            }
            if (addedBy) {
                query.addedBy_id = ObjectId(addedBy)
            }

            if (start_date != "" && start_date != undefined && end_date != "" && end_date != undefined) {
                var date = new Date(start_date);
                date.setDate(date.getDate());
                var Enddate = new Date(end_date);
                Enddate.setDate(Enddate.getDate() + 1);
                query.$and = [
                    { createdAt: { $gte: date } },
                    { createdAt: { $lte: Enddate } },
                ];
            }

            db.collection('task').aggregate([
                {
                    $lookup: {
                        from: 'users',
                        localField: 'addedBy',
                        foreignField: '_id',
                        as: "addedBy"
                    }
                },
                {
                    $unwind: {
                        path: '$addedBy',
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $lookup: {
                        from: 'users',
                        localField: 'submitBy',
                        foreignField: '_id',
                        as: "submitBy"
                    }
                },
                {
                    $unwind: {
                        path: '$submitBy',
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $lookup: {
                        from: 'projects',
                        localField: 'project',
                        foreignField: '_id',
                        as: "project"
                    }
                },
                {
                    $unwind: {
                        path: '$projectName',
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $lookup: {
                        from: 'users',
                        localField: 'employee',
                        foreignField: '_id',
                        as: "employee"
                    }
                },
                {
                    $unwind: {
                        path: '$employee',
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $project: {
                        id: "$_id",
                        isDeleted: "$isDeleted",
                        taskName: "$taskName",
                        description: "$description",
                        assignDateAndTime: "$assignDateAndTime",
                        submitDateAndTime: "$submitDateAndTime",
                        expectedhours: "$expectedhours",
                        expectedminutes: "$expectedminutes",
                        Actual_hours: "$Actual_hours",
                        Actual_minutes: "$Actual_minutes",
                        // employee: "$employee",
                        employee_id: "$employee._id",
                        employee_name: "$employee.fullName",
                        Actual_time: "$Actual_time",
                        expected_time: "$expected_time",
                        reason: "$reason",
                        status: "$status",
                        // addedBy: "$addedBy",
                        addedBy_id: "$addedBy._id",
                        // project: "$project",
                        project_id: "$project._id",
                        project_name: "$project.name",
                        createdAt: "$createdAt",
                        updatedAt: "$updatedAt",
                        submitBy: "$submitBy",
                    }
                },
                {
                    $match: query
                },
            ]).toArray((err, totalResult) => {
                if (err) {
                    return res.status(400).json({
                        success: false,
                        error: { code: 400, message: "" + err }
                    })
                }

                db.collection('task').aggregate([
                    {
                        $lookup: {
                            from: 'users',
                            localField: 'addedBy',
                            foreignField: '_id',
                            as: "addedBy"
                        }
                    },
                    {
                        $unwind: {
                            path: '$addedBy',
                            preserveNullAndEmptyArrays: true
                        }
                    },
                    {
                        $lookup: {
                            from: 'users',
                            localField: 'employee',
                            foreignField: '_id',
                            as: "employee"
                        }
                    },
                    {
                        $unwind: {
                            path: '$employee',
                            preserveNullAndEmptyArrays: true
                        }
                    },
                    {
                        $lookup: {
                            from: 'projects',
                            localField: 'project',
                            foreignField: '_id',
                            as: "project"
                        }
                    },
                    {
                        $unwind: {
                            path: '$project',
                            preserveNullAndEmptyArrays: true
                        }
                    },
                    {
                        $lookup: {
                            from: 'users',
                            localField: 'submitBy',
                            foreignField: '_id',
                            as: "submitBy"
                        }
                    },
                    {
                        $unwind: {
                            path: '$submitBy',
                            preserveNullAndEmptyArrays: true
                        }
                    },
                    {
                        $project: {
                            id: "$_id",
                            isDeleted: "$isDeleted",
                            taskName: "$taskName",
                            description: "$description",
                            assignDateAndTime: "$assignDateAndTime",
                            submitDateAndTime: "$submitDateAndTime",
                            expectedhours: "$expectedhours",
                            expectedminutes: "$expectedminutes",
                            Actual_hours: "$Actual_hours",
                            Actual_minutes: "$Actual_minutes",
                            // employee: "$employee",
                            employee_id: "$employee._id",
                            employee_name: "$employee.fullName",
                            status: "$status",
                            Actual_time: "$Actual_time",
                            expected_time: "$expected_time",
                            reason: "$reason",
                            // addedBy: "$addedBy",
                            addedBy_id: "$addedBy._id",
                            // project: "$project",
                            project_id: "$project._id",
                            project_name: "$project.name",
                            createdAt: "$createdAt",
                            updatedAt: "$updatedAt",
                            submitBy: "$submitBy",
                        }
                    },
                    {
                        $addFields: {
                            sort_num: {
                                $cond: [{ $eq: ["$status", "inProgress"] }, 5, 0]
                            }
                        }
                    },
                    // isIn: ["done", "inProgress","pending","blocker","hold"],
                    {
                        $set: {
                            sort_num: {
                                $cond: [{ $eq: ["$status", "pending"] }, 4, 0]
                            }
                        }
                    },
                    {
                        $set: {
                            sort_num: {
                                $cond: [{ $eq: ["$status", "hold"] }, 3, 0]
                            }
                        }
                    },
                    {
                        $set: {
                            sort_num: {
                                $cond: [{ $eq: ["$status", "blocker"] }, 2, 0]
                            }
                        }
                    },
                    {
                        $set: {
                            sort_num: {
                                $cond: [{ $eq: ["$status", "done"] }, 1, 0]
                            }
                        }
                    },
                    {
                        $match: query
                    },
                    {
                        $sort: {
                            sort_num: 1,
                            createdAt: -1
                        }
                    },
                    {
                        $group: {
                            _id: { project_id: "$project_id", employee_id: "$employee_id" },
                            project_name: { $first: "$project_name" },
                            // _id: "$employee_id",
                            employee_name: { $first: "$employee_name" },
                            tasks: {
                                $push: {
                                    id: "$_id",
                                    taskName: "$taskName",
                                    employee_id: "$employee_id",
                                    employee_name: "$employee_name",
                                    description: "$description",
                                    assignDateAndTime: "$assignDateAndTime",
                                    submitDateAndTime: "$submitDateAndTime",
                                    expectedhours: "$expectedhours",
                                    expectedminutes: "$expectedminutes",
                                    Actual_hours: "$Actual_hours",
                                    Actual_minutes: "$Actual_minutes",
                                    status: "$status",
                                    Actual_time: "$Actual_time",
                                    expected_time: "$expected_time",
                                    reason: "$reason",
                                    addedBy_id: "$addedBy._id",
                                    // project: "$project",
                                    project_id: "$project._id",
                                    employee_id: "$employee._id",
                                    createdAt: "$createdAt",
                                    updatedAt: "$updatedAt",
                                    submitBy: "$submitBy",
                                }
                            }

                        }
                    },
                    {
                        $addFields: {
                            project_id: "$_id.project_id",
                            employee_id:"$_id.employee_id"
                        }
                    },
                    {
                        $set: {
                            "_id":0
                        }
                    },
                    {
                        $skip: Number(skipNo)
                    },
                    {
                        $limit: Number(count)
                    }
                ]).toArray((err, result) => {

                    if (err) {
                        return res.status(400).json({
                            success: false,
                            code: 400, message: "" + err
                        })
                    }

                    return res.status(200).json({
                        "success": true,
                        "data": result,
                        "total": totalResult.length,
                    });
                })
            })
        } catch (error) {
            return res.status(400).json({ "success": false, "code": 400, "error": "" + error });

        }
    },


    // /**All task new api */
    // getNewList: async (req, res) => {

    //     try {
    //         var search = req.param('search');

    //         var page = req.param('page');
    //         var sortBy = req.param('sortBy');
    //         var projectId = req.param('projectId');
    //         var employee_name = req.param('employee_name');
    //         var project_name = req.param('project_name');
    //         var employeeId = req.param('employeeId');
    //         let createdAt = req.param('createdAt');
    //         var addedBy = req.param('addedBy');
    //         var start_date = req.param("start_date");
    //         var end_date = req.param("end_date");

    //         if (!page) {
    //             page = 1
    //         }
    //         var count = parseInt(req.param('count'));
    //         if (!count) {
    //             count = 10
    //         }
    //         var skipNo = (page - 1) * count;
    //         var query = {}



    //         if (employee_name) {
    //             query.employee_name = employee_name
    //         }

    //         if (project_name) {
    //             query.project_name = project_name
    //         }

    //         if (search) {
    //             query.$or = [
    //                 { taskName: { $regex: search, '$options': 'i' } },

    //             ]
    //         }


    //         sortquery = {};
    //         if (sortBy) {
    //             var order = sortBy.split(" ");

    //             var field = order[0];
    //             var sortType = order[1];
    //         }
    //         sortquery[field ? field : 'createdAt'] = sortType ? (sortType == 'descending' ? -1 : 1) : -1;
    //         // console.log(sortquery, "============");
    //         query.isDeleted = false

    //         if (projectId) {
    //             query.project_id = ObjectId(projectId)
    //         }
    //         if (employeeId) {
    //             query.employee_id = ObjectId(employeeId)
    //         }
    //         if (addedBy) {
    //             query.addedBy_id = ObjectId(addedBy)
    //         }

    //         if (start_date != "" && start_date != undefined && end_date != "" && end_date != undefined) {
    //             // console.log('for range');
    //             var date = new Date(start_date);
    //             date.setDate(date.getDate());
    //             var Enddate = new Date(end_date);
    //             Enddate.setDate(Enddate.getDate() + 1);
    //             query.$and = [
    //                 { createdAt: { $gte: date } },
    //                 { createdAt: { $lte: Enddate } },
    //             ];
    //         }

    //         console.log(query, "---qery");
    //         db.collection('task').aggregate([
    //             {
    //                 $lookup: {
    //                     from: 'users',
    //                     localField: 'addedBy',
    //                     foreignField: '_id',
    //                     as: "addedBy"
    //                 }
    //             },
    //             {
    //                 $unwind: {
    //                     path: '$addedBy',
    //                     preserveNullAndEmptyArrays: true
    //                 }
    //             },
    //             {
    //                 $lookup: {
    //                     from: 'users',
    //                     localField: 'submitBy',
    //                     foreignField: '_id',
    //                     as: "submitBy"
    //                 }
    //             },
    //             {
    //                 $unwind: {
    //                     path: '$submitBy',
    //                     preserveNullAndEmptyArrays: true
    //                 }
    //             },
    //             {
    //                 $lookup: {
    //                     from: 'projects',
    //                     localField: 'project',
    //                     foreignField: '_id',
    //                     as: "project"
    //                 }
    //             },
    //             {
    //                 $unwind: {
    //                     path: '$projectName',
    //                     preserveNullAndEmptyArrays: true
    //                 }
    //             },
    //             {
    //                 $lookup: {
    //                     from: 'users',
    //                     localField: 'employee',
    //                     foreignField: '_id',
    //                     as: "employee"
    //                 }
    //             },
    //             {
    //                 $unwind: {
    //                     path: '$employee',
    //                     preserveNullAndEmptyArrays: true
    //                 }
    //             },
    //             {
    //                 $project: {
    //                     id: "$_id",
    //                     isDeleted: "$isDeleted",
    //                     taskName: "$taskName",
    //                     description: "$description",
    //                     assignDateAndTime: "$assignDateAndTime",
    //                     submitDateAndTime: "$submitDateAndTime",
    //                     expectedhours: "$expectedhours",
    //                     expectedminutes: "$expectedminutes",
    //                     Actual_hours: "$Actual_hours",
    //                     Actual_minutes: "$Actual_minutes",
    //                     // employee: "$employee",
    //                     employee_id: "$employee._id",
    //                     employee_name: "$employee.fullName",
    //                     Actual_time: "$Actual_time",
    //                     expected_time: "$expected_time",
    //                     reason: "$reason",
    //                     status: "$status",
    //                     // addedBy: "$addedBy",
    //                     addedBy_id: "$addedBy._id",
    //                     // project: "$project",
    //                     project_id: "$project._id",
    //                     project_name: "$project.name",
    //                     createdAt: "$createdAt",
    //                     updatedAt: "$updatedAt",
    //                     submitBy: "$submitBy",
    //                 }
    //             },
    //             {
    //                 $match: query
    //             },
    //         ]).toArray((err, totalResult) => {
    //             // var totalResult = dynamicSort(totalResult, [
    //             //     { status: 'asc' },
    //             //     { createdAt: 'asc' },
    //             // ]);
    //             // console.log(totalResult, "==========================totalResult");

    //             if (err) {
    //                 return res.status(400).json({
    //                     success: false,
    //                     error: { code: 400, message: "" + err }
    //                 })
    //             }

    //             db.collection('task').aggregate([
    //                 {
    //                     $lookup: {
    //                         from: 'users',
    //                         localField: 'addedBy',
    //                         foreignField: '_id',
    //                         as: "addedBy"
    //                     }
    //                 },
    //                 {
    //                     $unwind: {
    //                         path: '$addedBy',
    //                         preserveNullAndEmptyArrays: true
    //                     }
    //                 },
    //                 {
    //                     $lookup: {
    //                         from: 'users',
    //                         localField: 'employee',
    //                         foreignField: '_id',
    //                         as: "employee"
    //                     }
    //                 },
    //                 {
    //                     $unwind: {
    //                         path: '$employee',
    //                         preserveNullAndEmptyArrays: true
    //                     }
    //                 },
    //                 {
    //                     $lookup: {
    //                         from: 'projects',
    //                         localField: 'project',
    //                         foreignField: '_id',
    //                         as: "project"
    //                     }
    //                 },
    //                 {
    //                     $unwind: {
    //                         path: '$project',
    //                         preserveNullAndEmptyArrays: true
    //                     }
    //                 },
    //                 {
    //                     $lookup: {
    //                         from: 'users',
    //                         localField: 'submitBy',
    //                         foreignField: '_id',
    //                         as: "submitBy"
    //                     }
    //                 },
    //                 {
    //                     $unwind: {
    //                         path: '$submitBy',
    //                         preserveNullAndEmptyArrays: true
    //                     }
    //                 },
    //                 {
    //                     $project: {
    //                         id: "$_id",
    //                         isDeleted: "$isDeleted",
    //                         taskName: "$taskName",
    //                         description: "$description",
    //                         assignDateAndTime: "$assignDateAndTime",
    //                         submitDateAndTime: "$submitDateAndTime",
    //                         expectedhours: "$expectedhours",
    //                         expectedminutes: "$expectedminutes",
    //                         Actual_hours: "$Actual_hours",
    //                         Actual_minutes: "$Actual_minutes",
    //                         // employee: "$employee",
    //                         employee_id: "$employee._id",
    //                         employee_name: "$employee.fullName",
    //                         status: "$status",
    //                         Actual_time: "$Actual_time",
    //                         expected_time: "$expected_time",
    //                         reason: "$reason",
    //                         // addedBy: "$addedBy",
    //                         addedBy_id: "$addedBy._id",
    //                         // project: "$project",
    //                         project_id: "$project._id",
    //                         project_name: "$project.name",
    //                         createdAt: "$createdAt",
    //                         updatedAt: "$updatedAt",
    //                         submitBy: "$submitBy",
    //                     }
    //                 },
    //                 {
    //                     $match: query
    //                 },
    //                 {
    //                     $sort: {
    //                         createdAt: -1,
    //                         status: -1
    //                     }
    //                 },
    //                 {
    //                     $skip: Number(skipNo)
    //                 },
    //                 {
    //                     $limit: Number(count)
    //                 }
    //             ]).toArray((err, result) => {

    //                 if (err) {
    //                     return res.status(400).json({
    //                         success: false,
    //                         code: 400, message: "" + err
    //                     })
    //                 }

    //                 return res.status(200).json({
    //                     "success": true,
    //                     // "result":arr2,
    //                     "data": result,
    //                     "total": totalResult.length,
    //                 });
    //             })
    //         })
    //     } catch (error) {
    //         console.log(error, "=================")
    //         return res.status(400).json({ "success": false, "code": 400, "error": "" + error });

    //     }
    // },

    /**edit task */
    editTask: async (req, res) => {

        try {
            const id = req.param("id");
            if (!id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 400, message: constantObj.task.MISSING }
                })
            } else {
                req.body.updatedAt = new Date()
                const updateTask = await Task.update({ id: id }, req.body)

                return res.status(200).json({
                    success: true,
                    message: constantObj.task.UPDATED,
                })
            }
        } catch (err) {
            //(err)
            return res.status(400).json({ "success": false, "code": 400, "error": err, });
        }
    },

    /**get task detail */
    taskDetail: async (req, res) => {
        try {
            const id = req.param("id");
            if (!id) {
                return res.status(404).json({
                    success: false,
                    code: 400, message: constantObj.task.MISSING
                })
            } else {
                let taskDetail = await Task.findOne({ id: id }).populate('submitBy').populate('addedBy').populate('deletedBy').populate('employee')
                if (!taskDetail) {
                    return res.status(404).json({
                        success: false,
                        code: 400, message: constantObj.task.NOT_EXIST
                    })
                }
                return res.status(200).json({
                    success: true,
                    data: taskDetail
                })

            }
        } catch (err) {
            return res.status(404).json({ "success": false, "code": 404, "error": err, });
        }

    },

    /**remove task */
    removeTask: async (req, res) => {
        try {
            const id = req.param("id");
            if (!id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 400, message: constantObj.task.MISSING }
                })
            } else {
                req.body["deletedBy"] = req.identity.id
                const removeTask = await Task.update({ id: id }, { isDeleted: true });

                return res.status(200).json({
                    success: true,
                    message: constantObj.task.DELETED,
                })
            }
        } catch (err) {
            //(err)
            return res.status(400).json({ "success": false, "code": 400, "error": err, });
        }
    },

};

