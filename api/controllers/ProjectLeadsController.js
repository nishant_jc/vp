/**
 * ProjectLeadsController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const constantObj = require("../../config/constants");
const db = sails.getDatastore().manager;
exports.addProjectLeads = async (req, res) => {
    try {
        let data = req.body;
        req.body.addedBy = req.identity.id;
        req.body.updatedBy = req.identity.id;
        for (let items of data.bids) {
            items.addedBy = req.identity.id;
            items.updatedBy = req.identity.id;
            let createData = await ProjectLeads.create(items).fetch();
        }
        return res.status(200).json({
            success: true
        })
    } catch (error) {
        return res.status(500).json({
            success: false,
            error: { code: 500, message: constantObj.constants.PROJECT_LEADS.PROJECT_LEADS_ADDED + error }
        })
    }
}

exports.getProjectLeadsById = async (req, res) => {
    try {
        let id = req.param('id');
        if (!id) {
            throw constantObj.constants.PROJECT_LEADS.ID_REQUIRED;
        }
        let getData = await ProjectLeads.findOne({ id: id });
        if (getData) {
            return res.status(200).json({
                success: true,
                data: getData,
                message: constantObj.constants.PROJECT_LEADS.GET_DATA
            })
        }
        throw constantObj.constants.PROJECT_LEADS.INVALID_ID;
    } catch (error) {
        return res.status(500).json({
            success: false,
            error: { code: 500, message: "" + error }
        })
    }
}

exports.editProjectLeads = async (req, res) => {
    try {
        let data = req.body;
        let id = req.body.id;
        if (!id) {
            throw constantObj.constants.PROJECT_LEADS.ID_REQUIRED;
        }
        data.updatedBy = req.identity.id;
        let updateProjectLeadData = await ProjectLeads.updateOne({ id: id }, data);
        if (updateProjectLeadData) {
            return res.status(200).json({
                success: true,
                message: constantObj.constants.PROJECT_LEADS.UPDATED_PROJECT_LEADS
            })
        }
        throw constantObj.constants.PROJECT_LEADS.INVALID_ID;
    } catch (error) {
        return res.status(500).json({
            success: false,
            error: { code: 500, message: "" + error }
        })
    }
}

exports.getAllProjectLeads = async (req, res) => {
    try {
        let search = req.param('search');
        let page = req.param('page');
        let count = req.param('count');

        if (!page) {
            page = 1;
        }
        if (!count) {
            count = 10;
        }

        let skipNo = (page - 1) * count;
        let query = {};

        if (search) {
            query.$or = [
                { country: { $regex: search, '$options': 'i' } }
            ]
        }
        query.isDeleted = false;
        db.collection("projectleads").aggregate([
            {
                $project: {
                    id: "$_id",
                    title:"$title",
                    status:"$status",
                    upworkurl: "$upworkurl",
                    country: "$country",
                    technology: "$technology",
                    date: "$date",
                    isDeleted: "$isDeleted",
                    deletedBy: "$deletedBy",
                    updatedBy: "$updatedBy",
                    createdAt: "$createdAt",
                    updatedAt: "$updatedAt"
                }
            },
            {
                $match: query,
            },
        ]).toArray((err, totalResult) => {
            if (err) {
                return res.status(500).json({
                    success: false,
                    error: { code: 500, message: "" + err }
                })
            }
            db.collection("projectleads").aggregate([
                {
                    $project: {
                        id: "$_id",
                        title:"$title",
                        status:"$status",
                        upworkurl: "$upworkurl",
                        country: "$country",
                        technology: "$technology",
                        date: "$date",
                        isDeleted: "$isDeleted",
                        deletedBy: "$deletedBy",
                        updatedBy: "$updatedBy",
                        createdAt: "$createdAt",
                        updatedAt: "$updatedAt"
                    }
                },
                {
                    $match: query,
                },
                {
                    $sort: { createdAt: -1 }
                },
                {
                    $skip: skipNo
                },
                {
                    $limit: Number(count)
                }
            ]).toArray((err, result) => {
                if (err) {
                    return res.status(500).json({
                        success: false,
                        error: { code: 500, message: "" + err }
                    })
                } else {
                    let resData = {
                        total_count: totalResult.length,
                        data: result
                    }
                    if (!req.param('page') && !req.param('count')) {
                        resData = {
                            total_count: totalResult.length,
                            data: result
                        }
                        return res.status(200).json({
                            success: true,
                            data: resData
                        });
                    }
                    return res.status(200).json({
                        success: true,
                        data: resData,
                    });
                }
            });
        });
    } catch (error) {
        return res.status(500).json({
            success: false,
            error: { code: 500, message: "" + error }
        })
    }
}

exports.deleteProjectLeads = async (req, res) => {
    try {
        let id = req.param('id');
        if (!id) {
            throw constantObj.constants.PROJECT_LEADS.ID_REQUIRED;
        }
        let delete_Project_Record = await ProjectLeads.updateOne({ id: id }, { 'isDeleted': true });
        if (delete_Project_Record) {
            return res.status(200).json({
                success: true,
                message: constantObj.constants.PROJECT_LEADS.DELETED_DATA
            })
        }

        throw constantObj.constants.PROJECT_LEADS.INVALID_ID;

    }

    catch (error) {
        return res.status(500).json({
            success: false,
            error: { code: 500, message: "" + error }
        })
    }
}



