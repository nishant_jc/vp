/**
 * ServiceandtechnologyController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */


 var constantObj = sails.config.constants;

module.exports = {
    /**
   *
   * @param {*} req
   * @param {*} res
   * @description: Used to add Service
   * @createdAt 22/10/2021
   * @createdBy : Rohit kumar
   */

   addService: (req, res) => {
    API(ServiceandtechnologyService.saveServiceandtechnology, req, res);
  },

  /**
   *
   * @param {*} req
   * @param {*} res
   * @description: Used to edit Service
   * @createdAt 22/10/2021
   * @createdBy : Rohit kumar
   */

  edit: (req, res) => {
    API(ServiceandtechnologyService.updateServiceandtechnology, req, res);
  },


  /**
   *
   * @param {*} res
   * @returns
   * @description: Api Used to get Services
   * @createdAt : 22/10/2021
   * @createdBy Rohit kumar
   */

   getAllServices: async (req, res, next)=> {
    var search = req.param("search");
    var sortBy = req.param("sortBy");
    var page = req.param("page");
    if (page == undefined) {
      page = 1;
    }
    var count = req.param("count");
    if (count == undefined) {
      count = 100;
    }
    var skipNo = (page - 1) * count;

    var query = {};
    if (sortBy) {
      sortBy = sortBy.toString();
    } else {
      sortBy = "createdAt desc";
    }

    query.isDeleted = false;
    await Serviceandtechnology.count(query)
      .exec( (err, total)=> {

        if (err) {
          return res.status(400).json({
            success: false,
            error: err,
          });
        } else {
            Serviceandtechnology.find(query)
            .sort(sortBy)
            .exec( (err, Service)=> {
              if (err) {
                return res.status(400).json({ success: false, error: { code: 400, message: err } });
              } else {
                return res.json({
                  success: true,
                  code: 200,
                  data: Service,
                  total: total,
                });
              }
            });
        }
      });
  },


  /**
   *
   * @param {id} req
   * @param {*} res
   * @returns
   * @description: Api Used to get single Service by ID
   * @createdAt : 22/10/2021
   * @createdBy Rohit kumar
   */

   getSinglegetAllService: async (req, res) => {
    try {
      let ServiceID = req.param("id");
      if (ServiceID == null) {
        return res.status(404).json({
          success: false,
          message: constantObj.Serviceandtechnology.ID_REQUIRED,
        });
      } else {
        var ServiceData = await Serviceandtechnology.findOne({ id: ServiceID });
        if (ServiceData) {
          return res.status(200).json({
            success: true,
            message: constantObj.Serviceandtechnology.GET_DATA,
            data: ServiceData,
          });
        } else {
          return res.status(404).json({
            success: false,
            message: constantObj.Serviceandtechnology.NO_RESULT,
          });
        }
      }
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },
  

  /**
   *
   * @param {id} req
   * @param {*} res
   * @returns
   * @description: Api Used to delete Service by ID
   * @createdAt : 22/10/2021
   * @createdBy Rohit kumar
   */

   ServiceDeletedByID: async (req, res) => {
    try {
       let deletedID = req.param("id");
       if (deletedID == undefined) {
        return res.status(404).json({
          success: false,
          message: constantObj.Serviceandtechnology.ID_REQUIRED,
        });
      }
      var deletedByUser = req.identity.id;
      var deletedData = await Serviceandtechnology.updateOne({
        id: deletedID,
      }).set({ isDeleted: true, deletedBy: deletedByUser });
        
        if (deletedData) {
          return res.status(200).json({
            success: true,
            message: constantObj.Serviceandtechnology.DELETED_SERVICES,
          });
        }
   
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },
  

};

