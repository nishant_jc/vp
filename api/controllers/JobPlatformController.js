/**
 * JobPlatformController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
var _request = require("request");
var ObjectId = require("mongodb").ObjectID;
var constantObj = sails.config.constants;
const db = sails.getDatastore().manager;
module.exports = {
  /**
   *
   * @param {*} req
   * @param {*} res
   * @description: Used to add Platform
   * @createdAt 15/09/2021
   * @createdBy : Chandra Shekhar
   */
  addPlatform: (req, res) => {
    API(JobPlatformService.savePlatform, req, res);
  },

  /**
   *
   * @param {*} req
   * @param {*} res
   * @description: Used to get list of platform
   * @createdAt 15/09/2021
   * @createdBy : Chandra Shekhar
   */
  getAllPlatform: (req, res) => {
    var search = req.param("search");
    var sortBy = req.param("sortBy");
    var page = req.param("page");
    var count = req.param("count");

    if (page == undefined) {
      page = 1;
    }
    if (count == undefined) {
      count = 10;
    }
    var skipNo = (page - 1) * count;
    var query = {};

    if (sortBy) {
      sortBy = sortBy.toString();
    } else {
      sortBy = 1;
    }

    if (search) {
      query.$or = [{ name: { $regex: search, $options: "i" } }];
    }

    query.isDeleted = false;
    db.collection("jobplatform")
      .aggregate([
        {
          $project: {
            id: "$_id",
            name: "$name",
            status: "$status",
            deletedBy: "$deletedBy",
            addedBy: "$addedBy",
            updatedBy: "$updatedBy",
            isDeleted: "$isDeleted",
            deletedAt: "$deletedAt",
            updatedAt: "$updatedAt",
          },
        },
        {
          $match: query,
        },
      ])
      .toArray((err, totalResult) => {
        db.collection("jobplatform")
          .aggregate([
            {
              $project: {
                id: "$_id",
                name: "$name",
                status: "$status",
                deletedBy: "$deletedBy",
                addedBy: "$addedBy",
                updatedBy: "$updatedBy",
                isDeleted: "$isDeleted",
                deletedAt: "$deletedAt",
                updatedAt: "$updatedAt",
              },
            },
            {
              $match: query,
            },
            { $sort: { name: sortBy } },
            {
              $skip: skipNo,
            },
            {
              $limit: Number(count),
            },
          ])
          .toArray((err, result) => {
            if (err) {
              return res.status(400).json({
                success: false,
                error: { message: "" + err },
              });
            } else {
              return res.status(200).json({
                success: true,
                code: 200,
                data: result,
                total: totalResult.length,
              });
            }
          });
      });
  },

  /**
   *
   * @param {*} req
   * @param {*} res
   * @description : Used to edit & update the platform
   * @createdAt 15/09/2021
   * @createdBy : Chandra Shekhar
   */
  updatePlatform: async (req, res) => {
    try {
      let Id = req.param("id");
      if (!Id || typeof Id == undefined ) {
        return res.status(404).json({
          success: false,
          error: { message: constantObj.jobplatform.ID_REQUIRED },
        });
      }
        let platformData = req.body;        
        var updatedID = req.identity.id;
        req.body.updatedBy = updatedID;        
      var updatedPlatformData = await JobPlatform.updateOne({
        id: Id,
      }).set(platformData);
      return res.status(200).json({
        success: true,
        message: constantObj.jobplatform.UPDATED_PLATFORM,
        data: updatedPlatformData,
      });
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },

  /**
   *
   * @param {*} req
   * @param {*} res
   * @description: Used to get detail of platform
   * @createdAt 15/09/2021
   * @createdBy : Chandra Shekhar
   */
  getSinglePlatform: async (req, res) => {
      var id = req.param("id");
      if (!id || id == undefined) {
      return res.status(404).json({
        success: false,
        error: { message: constantObj.jobplatform.ID_REQUIRED },
      });
    } else {

      let checkPlatform = await JobPlatform.findOne({
        id: id,
      });
          
      if (checkPlatform == undefined) {
        return res.status(404).json({
          success: false,
          error: { message: constantObj.jobplatform.ID_NOT_FOUND },
        });
      } else {
        JobPlatform.findOne({ id: id }).then((platformData) => {
          return res.status(200).json({
            success: true,
            data: platformData,
          });
        });
      }
    }
  },

  /**
   *
   * @param {id} req
   * @param {*} res
   * @returns
   * @description: Api Used to delete job platform by ID
   * @createdAt : 15/09/2021
   * @createdBy Chandra Shekhar
   */

  jobPlatformDeletedByID: async (req, res) => {
    try {
        let deleteID = req.param("id");
        
      if (!deleteID || deleteID == undefined) {
        return res.status(404).json({
          success: false,
          error: { message: constantObj.jobplatform.ID_REQUIRED },
        });
      }
 
      let checkPlatformData = await JobPlatform.findOne({
        id: deleteID,
        isDeleted: false,
      });

      if (checkPlatformData == undefined) {
        return res.status(404).json({
          success: false,
          message: constantObj.jobplatform.ID_NOT_FOUND,
        });
      } else {
        var deletedByUser = req.identity.id;
        var deletedData = await JobPlatform.updateOne({
          id: deleteID,
        }).set({ isDeleted: true, deletedBy: deletedByUser });
          return res.status(200).json({
            success: true,
            message: constantObj.jobplatform.DELETED_PLATFORM,
          });
          
         
      }
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },
};

