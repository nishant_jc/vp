/**
 * AppraisalController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */


var _request = require("request");
var ObjectId = require("mongodb").ObjectID;
var constantObj = sails.config.constants;
const db = sails.getDatastore().manager;
var async = require("async");
const SmtpController = require("../controllers/SmtpController");

module.exports = {
  /**
*
* @param {*} req
* @param {*} res
* @description: Used to add Appraisal
* @createdAt 07/10/2021
* @createdBy : Rohit kumar
*/

  addAppraisal: (req, res) => {
    API(AppraisalService.saveAppraisal, req, res);
  },

  /**
  *
  * @param {*} req
  * @param {*} res
  * @returns
  * @description : Api use to edit Appraisal
  * @createdAt : 07/10/2021
  * @createdBy : Rohit kumar
  */

  edit: (req, res) => {
    API(AppraisalService.updateAppraisal, req, res);
  },

  /**
 *
 * @param {id} req
 * @param {*} res
 * @returns
 * @description: Api Used to delete appraisal by ID
 * @createdAt : 07/10/2021
 * @createdBy Rohit kumar
 */

  appraisalDeletedByID: async (req, res) => {
    try {
      let deletedID = req.param("id");
      if (deletedID == undefined) {
        return res.status(404).json({
          success: false,
          message: constantObj.Appraisal.ID_REQUIRED,
        });
      }
      var deletedByUser = req.identity.id;
      var deletedData = await Appraisal.updateOne({
        id: deletedID,
      }).set({ isDeleted: true, deletedBy: deletedByUser });

      if (deletedData) {
        return res.status(200).json({
          success: true,
          message: constantObj.Appraisal.DELETED_POST,
        });
      }

    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },
  /**
   *
   * @param {*} req
   * @param {*} res
   * @description : Listing of appraisal
   * @createdAt :07/10/2021
   * @createdBy : Rohit Kumar
   */
  getAllAppraisal: (req, res) => {
    var search = req.param("search");
    var sortBy = req.param("sortBy");
    var page = req.param("page");
    var count = req.param("count");
    var employee_id = req.param("employee_id");

    if (page == undefined) {
      page = 1;
    }
    if (count == undefined) {
      count = 10;
    }
    var skipNo = (page - 1) * count;
    var query = {};

    if (sortBy) {
      sortBy = sortBy.toString();
    } else {
      sortBy = "updatedAt desc";
    }

    if (search) {
      query.$or = [{ name: { $regex: search, $options: "i" } }];
    }

    if (employee_id) {
      query.employeeId = employee_id;
    }

    query.isDeleted = false;
    db.collection("appraisal")
      .aggregate([
        {
          $project: {
            id: "$_id",
            employeeId: "$employeeId",
            name: "$name",
            previousAmt: "$previousAmt",
            appraisalAmt: "$appraisalAmt",
            currentAmt: "$currentAmt",
            date: "$date",
            email: "$email",
            status: "$status",
            deletedBy: "$deletedBy",
            addedBy: "$addedBy",
            updatedBy: "$updatedBy",
            isDeleted: "$isDeleted",
            deletedAt: "$deletedAt",
            updatedAt: "$updatedAt",
          },
        },
        {
          $match: query,
        },
      ])
      .toArray((err, totalResult) => {
        db.collection("appraisal")
          .aggregate([
            {
              $project: {
                id: "$_id",
                employeeId: "$employeeId",
                name: "$name",
                previousAmt: "$previousAmt",
                appraisalAmt: "$appraisalAmt",
                currentAmt: "$currentAmt",
                date: "$date",
                email: "$email",
                status: "$status",
                deletedBy: "$deletedBy",
                addedBy: "$addedBy",
                updatedBy: "$updatedBy",
                isDeleted: "$isDeleted",
                deletedAt: "$deletedAt",
                updatedAt: "$updatedAt",
              },
            },
            {
              $match: query,
            },
            {
              $sort: {
                date: -1,
              },
            },
            {
              $skip: skipNo,
            },
            {
              $limit: Number(count),
            },
          ])
          .toArray((err, result) => {
            if (err) {
              return res.status(400).json({
                success: false,
                error: { message: "" + err },
              });
            } else {
              return res.status(200).json({
                success: true,
                code: 200,
                data: result,
                total: totalResult.length,
              });
            }
          });
      });
  },

  // /**
  //  *
  //  * @param {*} req
  //  * @param {*} res
  //  * @description: Used to display single user Appraisal
  //  * @createdAt 09/11/2021
  //  * @createdBy : Rohit kumar
  //  */
  singleUserAppraisal: async (req, res) => {
    // var userID = req.identity.id;
    let id = req.param('id');
    if (!id) {
      return res.status(400).json({
        success: false,
        message: constantObj.Appraisal.ID_REQUIRED
      });
    }

    // var sortby =-1
    let AppraisalData = await Appraisal.findOne({
      id: id,
    })
    // .sort([{ updatedAt: 'DESC' }]);
    if (AppraisalData) {
      if (AppraisalData.addedBy) {
        let get_added_by_details = await Users.findOne({ id: AppraisalData.addedBy });
        if (get_added_by_details) {
          AppraisalData.addedBy_name = get_added_by_details.fullName;
        }
      }
      if (AppraisalData.updatedBy) {
        let get_updated_by_details = await Users.findOne({ id: AppraisalData.updatedBy });
        if (get_updated_by_details) {
          AppraisalData.updatedBy = get_updated_by_details.fullName;
        }
      }
      return res.status(200).json({
        success: true,
        message: constantObj.Appraisal.GET_DATA,
        data: AppraisalData
      });
    }
    return res.status(400).json({
      success: false,
      message: constantObj.Appraisal.INVALID_ID
    });
    

    // if (AppraisalData.length !== 0) {
    //   return res.status(200).json({
    //     success: true,
    //     message: constantObj.Appraisal.GET_DATA,
    //     data: AppraisalData,
    //   });
    // } else {
    //   return res.status(200).json({
    //     success: true,
    //     message: constantObj.Appraisal.NO_RESULT,
    //   });
    // }


  },
};