/**
 * TodoListController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

var constantObj = sails.config.constants;

module.exports = {

    addTodoList: async (req, res) => {
        try {

            let data = req.body;

            const date = new Date();
            const new_date = date.toISOString().substring(0, 10);

            data.date = new_date;
            data.addedBy = req.identity.id;

            const add_todo = await TodoList.create(req.body).fetch();

            if (add_todo) {
                return res.status(200).json({
                    success: true,
                    message: constantObj.Todo.ADD_LIST_SUCCESS
                });
            }
        } catch (error) {
            return res.status(400).json({
                success: false,
                error: { message: error },
            });
        }

    },
    getAllTodoList: async (req, res) => {

        try {
            var search = req.param("search");
            var date = new Date();
            var current_date = date.toISOString().substring(0, 10);
            var current_year = date.toISOString().substring(0, 4);
            var current_month = date.toISOString().substring(5, 7);
        
            var starting_date = `${current_year}` + "-" + `${current_month}` + "-01";
            var ending_date = current_date;

            var start_date = req.param('start_date') ? req.param('start_date') : starting_date;
            var end_date = req.param('end_date') ? req.param('end_date') : ending_date;


            let total_pending = 0;
            let total_done = 0;
            let query = {};

            if (search) {
                query.$or = [{ task: { $regex: search, $options: "i" } }];
            }


            query.date = {
                '>=': start_date,
                '<=': end_date
            };

            query.isDeleted = "false";


            const all_todo = await TodoList.find(query);

            for (items of all_todo) {
                var status = items.status;
                if (status == "pending"){
                    total_pending += 1;
                }
                if (status == "done"){
                    total_done += 1;
                }
            }

            return res.status(200).json({
                success: true,
                message: constantObj.Todo.DETAILS_FOUND,
                data: all_todo,
                total_pending_task: total_pending,
                total_done_task: total_done
            });

        } catch (error) {
            return res.status(400).json({
                success: false,
                error: { message: error },
            });
        }

    },
    getSingleTodoList: async (req, res) => {

        try {

            var id = req.param('id');
            let query = {};

            if (!id) {
                return res.status(400).json({
                    success: false,
                    message: constantObj.Todo.REQUIRED
                });
            } else {
                query.id = id;
            }

            query.isDeleted = "false";

            const single_todo = await TodoList.findOne(query);
            if (single_todo) {
                return res.status(200).json({
                    success: true,
                    message: constantObj.Todo.DETAILS_FOUND,
                    data: single_todo
                });
            }

        } catch (error) {
            return res.status(400).json({
                success: false,
                error: { message: error },
            });
        }
    },
    updateTodoList: async (req, res) => {

        try {

            let data = req.body;
            var id = data.id;
            data.updatedBy = req.identity.id

            if (!id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.Todo.REQUIRED },
                });
            }

            const update_todo = await TodoList.updateOne({ id: id,isDeleted:false}).set(data);
            if (update_todo) {
                return res.status(200).json({
                    success: true,
                    message: constantObj.Todo.UPDATED,
                    data: update_todo
                });
            }

        } catch (error) {
            return res.status(400).json({
                success: false,
                error: { message: error },
            });
        }
    },
    deleteTodoList: async (req, res) => {

        try {

            let data = req.body;
            const id = req.param("id");
            data.deletedBy = req.identity.id;
            data.isDeleted = true

            const update_todo = await TodoList.updateOne({ id: id }).set(data);
            if (update_todo) {
                return res.status(200).json({
                    success: true,
                    message: constantObj.Todo.DELETED
                });
            }
        } catch (error) {
            return res.status(400).json({
                success: false,
                error: { message: error },
            });
        }

    }

};

