/**
 * SystemComplainController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const constantObj = require('../../config/constants').constants;
const db = sails.getDatastore().manager;
var ObjectId = require('mongodb').ObjectID;
const SmtpController = require("../controllers/SmtpController");



module.exports = {

    /**
      *
      * @description: Api Used to add Complain
      * @createdAt : 24/05/2023
      * @createdBy :Vishal Tyagi
      */

    addComplain: async (req, res) => {
        const data = req.body;
        // if (!data.deviceName || typeof data.deviceName == undefined) {
        //     return res.status(404).json({
        //         success: false,
        //         error: { code: 404, message: constantObj.complain.DeviceName_REQUIRED },
        //     });
        // }
        try {
            // const query = {};
            // query.employeeId = req.identity.id;
            // query.deviceName = data.deviceName;
            // query.status = "completed";

            // var existed = await SystemComplain.findOne(query);
            // if (existed) {
            //     return res.status(400).json({
            //         success: false,
            //         error: { code: 400, message: constantObj.complain.EXIST },
            //     });
            // } else {
            data.employeeId = req.identity.id;
            data.addedBy = req.identity.id;
            data.updatedBy = req.identity.id;
            let addComplain = await SystemComplain.create(data).fetch();
            if (addComplain) {
                sendComplainEmail({
                    toemail: "shivani@yopmail.com",
                    name: data.deviceName,
                    description: data.description
                });
                return res.status(200).json({
                    success: true,
                    code: 200,
                    data: addComplain,
                    message: constantObj.complain.ADD,
                });
            }
            // }

        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { "code": 400, "message": "" + err }
            });
        }

    },

    /**
    *
    * @description: Api Used to comaplain Listing
    * @createdAt : 24/05/2023
    * @createdBy :Vishal Tyagi
    */

    comaplainList: async (req, res) => {

        try {
            var search = req.param("search");
            var sortBy = req.param("sortBy");
            var page = req.param("page");
            var count = req.param("count");
            var employee_id = req.param('employee_id');

            if (page == undefined) {
                page = 1;
            }
            if (count == undefined) {
                count = 1000;
            }
            var skipNo = (page - 1) * count;
            var query = {};

            if (sortBy) {
                sortBy = sortBy.toString();
            } else {
                sortBy = 1;
            }

            if (search) {
                query.$or = [{ deviceName: { $regex: search, $options: "i" } }];
            }


            if (employee_id) {
                query.employeeId = ObjectId(employee_id)
            }

            query.isDeleted = false;
            db.collection("systemcomplain")
                .aggregate([
                    {
                        $lookup: {
                            from: "users",
                            localField: "employeeId",
                            foreignField: "_id",
                            as: "employee_details",
                        },
                    },
                    {
                        $unwind: {
                            path: "$employee_details",
                            preserveNullAndEmptyArrays: true,
                        },
                    },
                    {
                        $project: {
                            id: "$_id",
                            deviceName: "$deviceName",
                            date: "$date",
                            description: "$description",
                            status: "$status",
                            employeeId: "$employeeId",
                            employee_name: "$employee_details.fullName",
                            isDeleted: "$isDeleted",
                            deletedBy: "$deletedBy",
                            addedBy: "$addedBy",
                            updatedBy: "$updatedBy",
                            deletedAt: "$deletedAt",
                            updatedAt: "$updatedAt",
                            createdAt: "$createdAt",
                        },
                    },
                    {
                        $match: query,
                    },
                    {
                        $sort: { createdAt: -1 },
                    },
                ])
                .toArray((err, totalResult) => {
                    db.collection("systemcomplain")
                        .aggregate([
                            {
                                $lookup: {
                                    from: "users",
                                    localField: "employeeId",
                                    foreignField: "_id",
                                    as: "employee_details",
                                },
                            },
                            {
                                $unwind: {
                                    path: "$employee_details",
                                    preserveNullAndEmptyArrays: true,
                                },
                            },
                            {
                                $project: {
                                    id: "$_id",
                                    deviceName: "$deviceName",
                                    date: "$date",
                                    description: "$description",
                                    status: "$status",
                                    employeeId: "$employeeId",
                                    employee_name: "$employee_details.fullName",
                                    isDeleted: "$isDeleted",
                                    deletedBy: "$deletedBy",
                                    addedBy: "$addedBy",
                                    updatedBy: "$updatedBy",
                                    deletedAt: "$deletedAt",
                                    updatedAt: "$updatedAt",
                                    createdAt: "$createdAt",
                                },
                            },
                            {
                                $match: query,
                            },
                            {
                                $sort: { createdAt: -1 },
                            },
                            {
                                $skip: skipNo,
                            },
                            {
                                $limit: Number(count),
                            },
                        ])
                        .toArray((err, result) => {
                            if (err) {
                                return res.status(400).json({
                                    success: false,
                                    error: { message: "" + err },
                                });
                            } else {
                                return res.status(200).json({
                                    success: true,
                                    code: 200,
                                    data: result,
                                    total: totalResult.length,
                                });
                            }
                        });
                });
        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { "code": 400, "message": "" + err }
            });
        }
    },

    /**
    *
    * @description: Api Used to comaplain edit
    * @createdAt : 24/05/2023
    * @createdBy :Vishal Tyagi
    */

    editcomaplain: async (req, res) => {
        try {
            var data = req.body;
            const id = data.id;

            if (!id) {
                return res.status(404).json({
                    success: false,
                    error: { code: 404, message: constantObj.complain.ID_MISSING },
                });
            } else {
                data.updatedBy = req.identity.id
                var find_complain = await SystemComplain.findOne({ id: id, isDeleted: false });

                if (!find_complain) {
                    return res.status(404).json({
                        success: false,
                        error: { code: 404, message: constantObj.complain.NOT_FOUND },
                    });
                } else {
                    delete data.id;
                    let update_complain = await SystemComplain.updateOne({ id: id }, data);
                    var get_employee_email = await Users.findOne({ id: update_complain.employeeId, isDeleted: false });
                    if (update_complain.status == 'completed') {
                        sendComplainIssueSolveEmail({
                            toemail: "shivika@yopmail.com",
                            name: data.deviceName,
                            description: data.description
                        });
                    }
                    return res.status(200).json({
                        success: true,
                        code: 200,
                        message: constantObj.complain.UPDATED,
                    });
                }
            }

        } catch (err) {
            return res.status(400).json({
                success: false,
                error: { "code": 400, "message": "" + err }
            });
        }
    },

    /**
    *
    * @description: Api Used to get by id complain
    * @createdAt : 26/06/2023
    * @createdBy :Shivani Chauhan
    */

    findSingleComplain: async (req, res) => {
        try {
            let id = req.param('id');
            if (!id) {
                throw constantObj.complain.ID_REQUIRED;
            }

            let get_complain = await SystemComplain.findOne({ id: id });

            if (get_complain) {
                if (get_complain.addedBy) {
                    let get_added_by_details = await Users.findOne({ id: get_complain.addedBy });
                    if (get_added_by_details) {
                        get_complain.addedBy_name = get_added_by_details.fullName;
                    }
                }
                if (get_complain.updatedBy) {
                    let get_updated_by_details = await Users.findOne({ id: get_complain.updatedBy });
                    if (get_updated_by_details) {
                        get_complain.updatedBy = get_updated_by_details.fullName;
                    }
                }
                return res.status(200).json({
                    success: true,
                    message: constantObj.complain.GET_SINGLE_COMPLAIN,
                    data: get_complain
                });
            }
            throw constantObj.complain.INVALID_ID;
        }
        catch (err) {
            return res.status(400).json({
                success: false,
                error: { message: err },
            });
        }
    },

    /**
   *
   * @description: Api Used to get by id complain
   * @createdAt : 26/06/2023
   * @createdBy :Shivani Chauhan
   */

    deleteComplain: async (req, res) => {
        try {
            let id = req.param('id');
            if (!id) {
                throw constantObj.complain.ID_REQUIRED;
            }

            let delete_complaint = await SystemComplain.updateOne({ id: id }, { 'isDeleted': true });
            if (delete_complaint) {
                return res.status(200).json({
                    success: true,
                    message: constantObj.complain.DELETE,
                    data: delete_complaint
                });
            }

            throw constantObj.complain.INVALID_ID;
        }

        catch (err) {
            return res.status(400).json({
                success: false,
                error: { message: err },
            });
        }
    }

};

/** Email to  admin after generate system complain. */
// var sendComplainEmail = function (options) {
//     var toemail = options.toemail;
//     var devicename = options.name;
//     var description = options.description;
//     (message = "");
//     style = {
//         header: `
//           padding:30px 15px;
//           text-align:center;
//           background-color:#f2f2f2;
//           `,
//         body: `
//           padding:15px;
//           height: 140px;
//           `,
//         hTitle: `font-family: 'Raleway', sans-serif;
//           font-size: 37px;
//           height:auto;
//           line-height: normal;
//           font-weight: bold;
//           background:none;
//           padding:0;
//           color:#333;
//           `,
//         maindiv: `
//           width:600px;
//           margin:auto;
//           font-family: Lato, sans-serif;
//           font-size: 14px;
//           color: #333;
//           line-height: 24px;
//           font-weight: 300;
//           border: 1px solid #eaeaea;
//           `,
//         textPrimary: `color:#3e3a6e;
//           `,
//         h5: `font-family: Raleway, sans-serif;
//           font-size: 22px;
//           background:none;
//           padding:0;
//           color:#333;
//           height:auto;
//           font-weight: bold;
//           line-height:normal;
//           `,
//         m0: `margin:0;`,
//         mb3: "margin-bottom:15px;",
//         textCenter: `text-align:center;`,
//         textleft: `text-align:left;`,
//         btn: `padding:10px 30px;
//           font-weight:500;
//           font-size:14px;
//           line-height:normal;
//           border:0;
//           display:inline-block;
//           text-decoration:none;
//           `,
//         btnPrimary: `
//           background-color:#3e3a6e;
//           color:#fff;
//           `,
//         footer: `
//           padding:10px 15px;
//           font-weight:500;
//           color:#fff;
//           text-align:center;
//           background-color:#000;`,
//     };

//     message +=
//         `<div class="container" style="` +
//         style.maindiv +
//         `">
//       <div class="header" style="` +
//         style.header +
//         `text-align:center">
//     <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQMAAADDCAMAAACxkIT5AAAA5FBMVEX/////jyZSUlZUVFdTU1b/jyX/kCpPT1P//vyEfnv/pVf/5tDm4+H/s27/jR/8qV7/rmf66tyako5aWFr29fT7yaLx7+7++vX46t7/797Z1dL/8+X/1rbw7ep1cW/6q2f/xpD/n0b+uHi1s7N8entpZ2ignp//uH/Burff3t5samvEwsH/9+/Pzcz/8eH/3sL/iArwpWarqamJiImhmJP9lzX/olL/z6G6uLj/oEj+0ayTjIeGgX+loJyMiozOxb+0rKf6u4Pi5unqwZ3Xz8nq3M/23sv1u47z2cHk3daZiX7Hm3a5+pCfAAAXLElEQVR4nO1dC1vaytbGZJKJEoOaGCqXVMxFSUjphnKRItV99q71fP///3xzzwSCFsTK48kSRIYQM++s+6yZVCollVRSSSWVVFJJJZVUUkkllVRSSSWVVFJJJZVUUkkllVRSSSWVVFJJJZVUUkkllVTS/xoZhuM4LnkgMoz3vp4/TI5rdhvpje/ffcN0Nxj4Sdrotl3nva/sj5Dhmo1kcDb3QluFEALIyLbD4Pudn3Y/Og6O2ZjezWNbgVABigJUFT3QEygAE1TU0Iv81Py4MLiN6ZlnA4h6i7qPQFD5A5C3CAuEA1DD+eBjwmCYSeTZiPlxT1WMgaooKv6N32N2II34naLY8dm0u8co9Hq9JvqRiL59/lvmdBgiAaDjjnuNewwUzgr4HQcBNyAYgkFjX1EwLq+vr66XCDWcP2fbTH8eKkQCKN+risJ4gYqCQnkDw0OOwW0Qo7CfBtM4sSxtmSzNOl5/uS7iAQBV0mWVdl6Vukt+FPpk0ChESSJe8Nt/sGu/TfWaph2skrYWAyddhGK46ZOIvaIwrUDfUjgUgQphGcWep3vICsZJIQRrMWgPYhuw/ilivCWmYO8YKvihMK1AUAjv3D/bwd8gxAcFGKzjA6OxsAGBgHRL4aIAMk4ApNeAM4HCwOHqEthB90/38SUq4AN9LR84iacKgecDnPE+HXXefUWVWyhnkN/xjz/ey+eJ8IHOHvQVw1CIQW9gK5wHRBe5ImR8IUkGtYwKs5GCKxQY3v/xbj5LG+iD9neoct5mAHDPiA+ywuykqub1Af0GEO8Ge2UfivSBXoiBGUBVqHouCEo26IADwDQih4orBIlZgH23T/6SUTsokIUCnWgeipEGQtSZcqAtood80Lm9FAoScDlS7Ls94oR6oSys6gPzENLO8DEHQiEq3CoA2WFUuZvI5CHTk/gE0B7sj41cwUAv1AftQzb4XA/wgc8ZQYVbDAGCaMUxpJqJiAJCf2/EwSD6IBOCYn1gfqfGncYBwkUUvhE3/rR7JIQS8RRlC5xeACJ4QB9D78e+uIzFsrCkD9yIpgNEkiDTjSpgT/E51RMAqEIqeFylSpyADgga79brPGE+0HPd11f0gXNvs15n/RRjytMH5AMIoAIh5hNAE0ucd1h4rcjfUiPz/fotUzEf5GTBSGI2gKroM/cNWG9I/3EOMYwRhTZyhLAaVKnvoBBtwDMtKj+PEk73QyUQDPRVFCQMGgFXZ/QJZJ+Hdg8qdjyM7v0k+SdN02TqR/NYxTlGCg//niolGbCVCPYjobDqJ+pLGLTPqMxjkQeAZ8t49gzzPLSDKGmYrlMxyLSCYTiumfrfQ5ZVVBkWWXDNQAHRXhhIJgtLKkGSBeMmzI084NaRJkhwJDicdt3VAXXMNIoh0xs8lJQsJj5FmPzZ3hbTqk5c0geNIPOJgBQGM0igOr9prxFrw218syHjIGFAmStFWpThPriLQifqB5KfkGHgRnaWGJDGkYUBMByYz8m0m3iQGkOOgwi6iEm194ERhI+UUwiZbUy9zAZwUeaOMmoK0hdUu2F+w/nnpZQCPxWA8z3QCC/4SO0zmEsa8VCA9sVePMsElFwfy4McMnHfEoNg70GCkeuDfNzIZcGYhpxzRTJABAeKffZbg+hMEQhKjg9UzhkqPHt/H6GADyRZMBcSF0jZUtINGL0wEcPJubGhCKMEQ7Hke/j+zmKBPsj4wPhhM+FXeKTMkwUo6Pn9EXR8G/A0i5RZIjjA99eKsl2QhYFgYFJtkHGwpA5A8JtcgMmNaOpJsig85IbDDc7zNlTsHzBZSEOY92lEFlmB8UY83J0DkY8W5wF7IgwFfqLIrbt3EIj8sJhAo8Jg32z0b4wkBNIkhDxBCzc70xuQsZRTleyC0fAgl19eYcD1wcbqHEuDyLFJqQRkGb69t3UsjBt1DcdMjk8iYwCylCkLngAMN54sSj2oAB40kelqlpuE8XsrBMEHep4TEAbmEJLIVyQCFe7zAnC/sVVvR1KoxQMo8rf93grhmRxKGpMJBQCkpBFLKHpbXDaOPzMvgSVjMAow3X23NiKMwYooED5goiCSRiIHhiKdzdkAmYZA4SG4yuo0KCfA+533ajNaEy8gDNpIFCgESlZuQN2aeJupY/eMT8bxaJplIOC3nfdqM6rXVjPrJIdSaZBQQc4asTQggFulf4yBDYvJe2fDsDTfyKfatGMnAQqfMefpdEBMI4r1tvpXjW9nZ9/PCuju3THI954rxRP3G6SmUfg01FlEojDfTpE7bUwu+mmTp6B3TyHUlvxEIhjawUM7IKMuizAvpthGI+4zMX2wbBv184bNw+VsOpFMFuxJInSH5Ej6QJp61/splB1aRRQbAHi4dwVFrySqD/QlP1HrVO8gLTwUeS8+1Q7P9iEXvEtaU5d21P4Os+xhNp+OYRl8MHWQ2QUphaIfWCd/x0DJqk5ECgVXT3w0dUD1gb6kF5FZ+DsE+fIKLgvQ25cp852RxAeyo/CflIY0fD6BRfzYu9/SO9hnqmm61HdenthKIE+YiNJjUm2JVOK7uzS7JsQHq0kUZBp9KLKHInlA51j3Y654l7RqFxAeVufTHXcP+MQ7C/uBvT+1VLsiZ0UfHGAM2meQ1p3wEhyaA8Iz7dP3Tv/tnLhdWMKgifNoQEyrSJVV9s2Hw2A5r0z1wbgXQFE6xfN+FJRwb0rqdka8VjdP4zYpGxDVaFkuDcbvnf3bPTnLsTORhWMzpIm0rOyCKcgP6CIt60SKhfVg2gQBWRRoBgHuTWXl7qi+Ot+IwoWHrp2VnvFKaxouzJ/e+5J3TsxXzocL1iS1xUS7qKYijiL4/vFcZSdnF9hChq/VBKpieZZcOfIRXWXKB3qeE7SRSzMoQBHL+BTuM348VxnFTKvuwcGoPoe0QkDNqkcoJ3xAF4nHC5QTWB7BOna8rPJAKqDBlaUfEAOnwC5Yl3/TOJlVG2TluR/SRSqaY9G+Vn1uDnO1ZBiNw49nGpks5KZYDm6b36BkEYCoKUQU/dv8cMKwmks7sK4cD4qBFxXaRD3Yg5/V+ntf866J+weSKFizLgS80JqbRVZfHN60Ju9dOlNMhmMUkvMy2xr5uTacRLq9uIe8ploV27sQGGDwz/l18w/0aHNq3Kyhl+Ob1TyS9dDG6znF0iWWWqeYLP69rp3+gR5tTMbAtm1VevI/wfcXv+ss1+pq+q9/2MILUoCkAGkVElIHR53+1te5lrY9o0S+TVZQEleOLBGhS6YADF90bFf4wKq1SeWBSJ0AXrOvAhj/aI1Gs20v+XR2Xkxbn1GiNJYXyrAiIhLlvSgM9ZV84uwp5qJAz6XwNauKMv/3UtMet1WKs45OaKQv0dEOMGh4PLoVZcCUf+H9S2dfto1W7TSyWe4IACmvSl6jnyeWdf1pu6s0HsmUNvnBv8grMUTH250wR+0hu0ihxlg1MPRemgxwlmo09VnDE32miIppNiW86Y806/bndlfZu9aF7slxnvWw3QnzHYlUqg7EfgQ84HmxtBrPM0lTrmiQI5vnjAArxuMr2WDwNLHQ+M2285IuPq+4Y+yfTrY63xLhBcnMgPG8Dwt15i8wQl4WrNtqGvNAkSeSeFIZWYV/TyyM03Yewq+xtsQFImWz1fmWqBFDViykioJoWjfwUrxfPzrILkjTJ09DMdEuFiuwIgwl/qc1wtzc2c5DOB9JLqksDNqXrc63RGagZK6MIm3Eo4AXcuGOnE+0jk8HNgWTRQgZmKhh8enKItc82UYYnEe9aGE1otFOvG/jLlczJEW86MqflQbCB2zRglX7mcRAPpGiZjETDH9UR8x2bGMZqjVrhQt0errKTkLRHyFkwyc2ZGLxDny+wFrKqWrjXyletSEEQWxzQdvmnx4sMpTaqLXFRc9GRatF0MvXnahEZB0DNTd8XK9h1RbePAMC1we6ph31G3MlxwV8eyvSZieIDZjMnGzOvc0HfXmlBFfErVd0XCLj3uaLiRWQ0weYiZ8BoX6k0cJU/fOvxpzuCplbeydKUObtS5F61Db3bn+NLRkAPfvd2VUw3ggzjy5bXs4mzUN/bdxAdaKmjR5+poeK/C1FqFeCCWYDwczW0aamoXeZj9AlNjjeVVGHcSaKq1UJAqrToX23bmEy4gNds/Ta7NONB7Oda2i/gag/UeGifW1l125dbmgaKBvoywAg+jp7fe8Z/cO3alDE+rGstA6qi3+KWQHFTJY+fvyZRiGkiTM+00z1AouhYdjo5y5+VN1IGnqP+vLaev6qb3am56h+BnmJvUI9ZwkG1OANCne2rdc6R4/97jRQIZ1IEvERXb/C0mhw0Pts5Ti4drHBxRnERywSBV072mF9UyMU+a8s1FP5ShzECof3jVVeMM5bv7rJwibaUPReTDXylSuHf0+WFjhYVxtIw+lnbTltyc+j/Wd3EFQqZI6QLbgRRTRiPx8VkK1bnvI7l9Tdp3Qwx/skZxv4sMpMle0VjVvC9HRkLV/87ydXe4/rFCKKULaMxIvJjLPwhulDlQ+r6Mxh5KeNJ7PdbpvmU5rcR/NQgVSFCAwUVWE7IPLyi0Hv6uvyCB6MfheE+nlmUVacg4fdpqkT2olsC6NsfSqDAaDxtmPvcLhYLIbzQy+kHMAEnxSg8UxEpiAVuDAvlzU6cRd/DwSj1TlYmuCX3vzaKQQVhxRWihSKypfkSfklXEkCyZZeAO+Rz2dPqN7I0nECO3Iqr/FrtMIFhBMef0MxOq0jq/DbhA2Odz1bYR5CsbMb4Alxtlo3mygg8aDC1jEDKWGS676qsApVGKfVo+IeaKPj/ovJutntgSwJevZC2GDXM3dGiow8y3zQLjEkVMnUU7cJZHUFzAxkHhFbv0U9DWhPT6/0XH1/9k7Tx5PnEyoXjyOreBXp27AB3X6FmTXBBWqWCRGbMEiVZiwa4HygSGEHgcEefLrW1/dCG31ure+HMTvRVwRBmtYb7VgbUBAGNgAKZ3NRPsLqCNQsOcK8wGxCUWV1N7zyhjWpg0+Xa7U6zROPPs+KecHtH4+yYDHHS/Qv6/pN5i7dO15fzDvDU0s8BlYECICDkM2iKNJuoYDu6vYF+zeSWcjLBQbhYDR+aPWWBLtXvazpWsEiGUkSxj/fZh7fvRO7EcmZJa7ouCAo7KYh4iC276t4RxZ3k51f+x2JnZedHcIKmmXpnfHVpH/xpfrly0W12poc344OLE0r6L3ARNPfbAqbgCA6zHrM4z/BGkxJKFw8VMEaGVAwpEvYiItTOJY5brCsr18RGBYi9Hqw5BMssY+OvO3Pbzd76/ihIvUGFKSWeKlRxgVSzC3a4hsaEjjXuczHsjDovFnTtXzPc8O+IglHW87V/B4I01jNgSDtv5FJg8pzxlw5SnyAn9K9EpqfizaSWifmLxzAzero/E2LOYwfc1thLjJTikrmNLO8q6QwGVJi00v8Y59JRbnVo7XWUc+9PNPxPEto+sNb13I8fQsVydtRVUXsByrSpQBkOyNS08gn8FWoBvn0G4r/JZ0gOrOcF1mKKJ6RBO2vf98YAnzTlLkNufmjo86TQzTLAGSpYCLCnCMUXkbLMzMsJVrAAetJX/mDo6Lptbfwjlbo6S5Ws7oiaRtkFXDjKN6RGIunHBR7kazmdvqrIBR0eL0zkOeC2s7jhGJy0sgT/aNIKCyxxOtMgJI5DzxlYg+nheX5vz6vZAQL3MYCaFYE5OANuKDdWEPJN1vNgkY185YYBsJMqGLrZGDP7/FXn6rL9HMyWh82rKFiVLTR5c+V079OQzr33joK1m1Fs57UmHzzv0erRNyfXdDBeOXctddl2J2bw51T8H+12olMn8mvIvqr4PEXbSdP/ofcdpI/OaZXTri55q7pyTzN6AL9nJ5+Ib8L6BN6fCK/2eMTeeC/yZMdkGu7uDilP+zkqGHvql8/XFl2SSWV9L9EDlHmLjIQ+J3hugZuwbNqBrUZxPU12t0GOogdTdtMutdRm54Af5O8dxsNNlkv6qldl/4jEUpdYPpCMgz1i5yPYzTRJ3XSjqlJVGyPrX+5oN9oGuQoZA96F/QUr1waMg2hCuGhEYU41OkGi7ZvQ2B/NytuiO83jBdiGSmeVoRh5HZtfNOUkCzOGkQYCucMHwUGboTvBWC4i1CFwCN3CknYfZQcL8JnBiBgi7q+EI9phMx6b3akob+uRFboovZV025b9Upfw8fUMDzGjH7+ZYSrj4zWbbV59RV/en45qqKGye3W5fCU/DCa+v6PSjccOhV3ECcGbonsudEOA/RJYuKsexwlUxRNTp1kEMSDG8wz7twmG3+l/iLEt52NAhMXO9pnSeIPbbycc8puJuWEC8MdxoOBN6TRRK81OepMZs1K73JUu5y1rkYj3ouL2snk8gi9bR08TCYTMg3Tu+6Q16Z+STHo1/uT49HDpNq8raGvjK9faY99vrGlb08rqYeu3fdQywCaThjRT5KYXXvawLW9Czq6aQjZ1h4p3gnOwBiYHuGmiotwdBCLAcIPTjxEEA8QL2R3G7gaG2RC9ZHIQf9o1OQYPBqV6u2l0dLEXFx1bF32JAzIKohzUu15/rVvPHReW6tKeoylzg2C9Ayvzh+gFgNh4DIM3Eje/9OJhnRwIy8OXIZRyjDw7QY/yktRe0xun4P4oGLGi1xEeTXu4RQTnyToj67oH6e1xx7q9DnCoMpu0VI/H49J1cYSBrjnxrjW77y6bNuPB0mSNMhkWxzjjWv82E+nceA44Rx9krrdQL5THueDruf7bFPMhPOBEwz5kQ1vakwDf4HhwbLg+OE8ldJLV+M67o3w9I87dYbBVb+PeL/Ssh5brRaWgdPj68kYVzUWYIDURme8SVVLMQY26np4ZhDtRnjet+M4HqZIJ9peHM8baSxvZ+NE9D4ofthw4wVpSklHEQZuHPHDzCAypl63O0T84ODj3GkQR11xIoxB/fxIcPElKzw+rWn66Oi8V2npndvbW8QnRv+21Ty+6hVjYNQOXl+m6cfTNMWCXnHPIBliPxwkKQLDDYcN9ImDMJCOZ7LgLuamcxYSOJI4oRi0vQyDeeRMvQYyB14X8wFq6g68LNeKZSGHgdVkGHRGn/Hot/RJv9+vGkhv1qr1y9tqMQYVYhpei4HHhT0JA9Jb3sL1QSMY5GSBYJDGwcAfQh9fVMr1QU4WfANjgA4cdhm/ONNY3F0LY4A0fCYLIy4L19c1XG2G9QGh6nh8eX6sT+rFGEz0V4tCZhewTlwMu1mL0ImLYFUnDsIgCIZh4MgYoFbmATgD1EYwQNAOkU4kLpg5FDsgYFlAOpFX01QlndivXTUzDOqz0VGtVusgT+HtMRiECeoNGvJlDBCvR8Tzc5IuxwD1ptHtdn2y8CvlsoA4n95CzJ3iE1EMKokNF5Xpoov9pDwGvfPxJRGB6gnnaGwXzjuzusCgeXXcv6heTDot4+0wCAfT6TRFBhwpAxebtAyDAH2StJHnFM79JJlG4Q3HYOqRAXftqMJsI8HASD1sZqaLGN9Xj2GAlOwCoTucouaEK0ViGytfHm6vzlutx3En85Ee680rpCda2sNsNusb/THJlH2pPfSa+klrVn0DDBIv9rwwMgZk457u/A4pcKq53CD0PG+I/aJkiGwH0gA4YhggrJwB024RFoYU+8DGgCjULnIb0JE+/jhhMoS+gk4R4GZhHS9PsPwbzdlJp9O5fRBlOafH58hPPpoYfdzeeejNToifbExqn3qoBWmQVg1jwNors87rV7K4XcTUjW6lS67XMFGcYDLFRT8hb1xsIWh726SHsW9jo0qOYU1uVxwpTuSY+NlIzUy3NlmEgEKmVr+aJcPqFyg+qp8iN5pkjC9QdEThaVbrBs0h90jurMfW0Pf+4Fr6319Uu+ny22ePLzNzJZVUUkkllVRSSSWVVFJJJZVUUkkllVRSSX+a/h/jzSdnjQTBcgAAAABJRU5ErkJggg==" width="150" style="margin-bottom:20px;" />
//           <h3 style="` +
//         style.hTitle +
//         style.m0 +
//         `">System Complain</h3>
//       </div>
//       <div class="body" style="` +
//         style.body +
//         `">
//           <p style="` +
//         style.m0 +
//         style.mb3 +
//         style.textCenter +
//         `margin-bottom:20px"><b>Device Name: </b>` +
//         devicename +
//         `</p>

//         <p style="` +
//         style.m0 +
//         style.mb3 +
//         style.textCenter +
//         `margin-bottom:20px"><b>Problem in device: </b>` +
//         description +
//         `</p>

//           </div>
//           <div class="footer" style="` +
//         style.footer +
//         `">
//           &copy 2023 JC Software Solution All rights reserved.
//           </div>
//       </div>`;

//     //var to = ["chander@yopmail.com", "amit@yopmail.com"];
//     SmtpController.sendComplaintEmail(
//         toemail,
//         "New system complain from employee",
//         message
//     );
// };

var sendComplainEmail = function (options) {
    var toemail = options.toemail;
    var devicename = options.name;
    var description = options.description;
    (message = "");
    message += ` <div class="maindiv" style="align-items: center; box-shadow: 0px 0px 8px 0px #8080808a; height: auto; background-color: #fff; 
    width:600px;
    margin:auto;">

        <div style="border: 1px solid #ff8f26cc ;    margin:2rem auto; max-width: 600px; height: auto; background-color: #ff8f26cc ;  text-align: center;">

            <h3 style="font-size: 25px; color: #fff;">System Complain</h3>

        </div>
        <div style="text-align: center;">



            <div style="margin-bottom: 14px;">
                <img src="https://mng.jcsoftwaresolution.in/assets/imgpsh_fullsize_anim.png" style="width: 107px;" />
            </div>
            <div>
                <p style="font-size: 15px;">Device Name: <span> ${devicename} </span></p>
                <p style="font-size: 15px;">Problem in device: <span>${description}</span ></p >

            </div >
            <div style=" margin-top:50px">

    <p style="font-size: 15px; color: gray; padding-bottom: 15px; ">© 2023 JC Software Solution  All rights reserved.</p>
    </div>

        </div >
    </div > `

    //var to = ["chander@yopmail.com", "amit@yopmail.com"];
    SmtpController.sendComplaintEmail(
        toemail,
        "New system complain from employee",
        message
    );
};



/** Email to  user after system complain issue solved. */
// var sendComplainIssueSolveEmail = function (options) {
//     var toemail = options.toemail;
//     var devicename = options.name;
//     var description = options.description;
//     (message = "");
//     style = {
//         header: `
// padding: 30px 15px;
// text - align: center;
// background - color: #f2f2f2;
// `,
//         body: `
// padding: 15px;
// height: 140px;
// `,
//         hTitle: `font - family: 'Raleway', sans - serif;
// font - size: 37px;
// height: auto;
// line - height: normal;
// font - weight: bold;
// background: none;
// padding: 0;
// color:#333;
// `,
//         maindiv: `
// width: 600px;
// margin: auto;
// font - family: Lato, sans - serif;
// font - size: 14px;
// color: #333;
// line - height: 24px;
// font - weight: 300;
// border: 1px solid #eaeaea;
// `,
//         textPrimary: `color:#3e3a6e;
// `,
//         h5: `font - family: Raleway, sans - serif;
// font - size: 22px;
// background: none;
// padding: 0;
// color:#333;
// height: auto;
// font - weight: bold;
// line - height: normal;
// `,
//         m0: `margin: 0; `,
//         mb3: "margin-bottom:15px;",
//         textCenter: `text - align: center; `,
//         textleft: `text - align: left; `,
//         btn: `padding: 10px 30px;
// font - weight: 500;
// font - size: 14px;
// line - height: normal;
// border: 0;
// display: inline - block;
// text - decoration: none;
// `,
//         btnPrimary: `
// background - color:#3e3a6e;
// color: #fff;
// `,
//         footer: `
// padding: 10px 15px;
// font - weight: 500;
// color: #fff;
// text - align: center;
// background - color:#000; `,
//     };

//     message +=
//         `< div class="container" style = "` +
//         style.maindiv +
//         `">
//       <div class="header" style="` +
//         style.header +
//         `text-align:center">
//     <img src="https://mng.jcsoftwaresolution.in/assets/img/logoImg.png" width="100" style="margin-bottom:20px; />
//           <h3 style="` +
//         style.hTitle +
//         style.m0 +
//         `"></h3>
//       </div>
//       <div class="body" style="` +
//         style.body +
//         `">
//           <p style="` +
//         style.m0 +
//         style.mb3 +
//         style.textCenter +
//         `margin-bottom:20px"><b>Your system complain successfully solved.</b>` +
//         // devicename +
//         `</p>

//         <p style="` +
//         style.m0 +
//         style.mb3 +
//         style.textCenter +
//         `margin-bottom:20px"><b></b>` +
//         // description +
//         `</p>

//           </div>
//           <div class="footer" style="` +
//         style.footer +
//         `">
//           &copy 2023 JC Software Solution All rights reserved.
//           </div>
//       </div>`;

//     //var to = ["chander@yopmail.com", "amit@yopmail.com"];
//     SmtpController.sendComplaintEmail(
//         toemail,
//         "System issue solved",
//         message
//     );
// };

var sendComplainIssueSolveEmail = function (options) {
    var toemail = options.toemail;
    (message = "");
    message += ` <div class="maindiv" style="align-items: center; box-shadow: 0px 0px 8px 0px #8080808a; height: auto; background-color: #fff; 
    width:600px;
    margin:auto;">

        <div style="border: 1px solid #ff8f26cc ;    margin:2rem auto; max-width: 600px; height: auto; background-color: #ff8f26cc ;  text-align: center;">

            <h3 style="font-size: 25px; color: #fff;">System Complain</h3>

        </div>
        <div style="text-align: center;">

            <div style="margin-bottom: 14px;">
                <img src="https://mng.jcsoftwaresolution.in/assets/imgpsh_fullsize_anim.png" style="width: 107px;" />
            </div>
            <div>
                <p style="font-size: 15px;">Your system complain successfully solved.</p>

            </div >
            <div style=" margin-top:50px">

    <p style="font-size: 15px; color: gray; padding-bottom: 15px; ">© 2023 JC Software Solution  All rights reserved.</p>
    </div>

        </div >
    </div > `

    //var to = ["chander@yopmail.com", "amit@yopmail.com"];
    SmtpController.sendComplaintEmailResponse(
        toemail,
        "System issue solved",
        message
    );
};