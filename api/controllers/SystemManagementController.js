/**
 * SystemManagementController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const constants = require('../../config/constants').constants;
const db = sails.getDatastore().manager;

exports.addSystemData = async (req, res) => {
    try {
        let data = req.body;
        data.addedBy = req.identity.id;
        data.updatedBy = req.identity.id;

        let save_system_data = await SystemManagement.create(data).fetch();
        if (save_system_data) {
            return res.status(200).json({
                success: true,
                message: constants.SYSTEM_MANAGEMENT.SYSTEM_SAVE
            });
        }
    } catch (err) {
        return res.status(400).json({
            success: false,
            error: { message: err },
        });
    }
};


exports.findSingleSytem = async (req, res) => {
    try {
        let id = req.param('id');
        if (!id) {
            throw constants.SYSTEM_MANAGEMENT.ID_REQUIRED;
        }

        let get_System = await SystemManagement.findOne({ id: id });

        if (get_System) {
            if (get_System.addedBy) {
                let get_added_by_details = await Users.findOne({ id: get_System.addedBy });
                if (get_added_by_details) {
                    get_System.addedBy_name = get_added_by_details.fullName;
                }
            }
            if (get_System.updatedBy) {
                let get_updated_by_details = await Users.findOne({ id: get_System.updatedBy });
                if (get_updated_by_details) {
                    get_System.updatedBy = get_updated_by_details.fullName;
                }
            }
            return res.status(200).json({
                success: true,
                message: constants.SYSTEM_MANAGEMENT.GET_SINGLE_SYSTEM,
                data: get_System
            });
        }
        throw constants.SYSTEM_MANAGEMENT.INVALID_ID;
    }
    catch (err) {
        return res.status(400).json({
            success: false,
            error: { message: err },
        });
    }
};


exports.getAllSystems = async (req, res) => {
    try {

        let search = req.param('search');
        let page = req.param('page');
        let count = req.param('count');
        let status = req.param('status');


        if (!page) {
            page = 1;
        }
        if (!count) {
            count = 10;
        }
        let skipNo = (page - 1) * count;
        let query = {};
        query.$and = [];

        if (search) {
            query.$or = [
                { user_name: { $regex: search, '$options': 'i' } },
            ]
        }

        if (status) {
            query.$and.push({ status: status });
        }


        query.$and.push({ isDeleted: false })

        db.collection("systemmanagement").aggregate([
            {
                $project: {
                    id: "$_id",
                    description: "$description",
                    ram: "$ram",
                    drives: "$drives",
                    drives_size: "$drives_size",
                    operating_system: "$operating_system",
                    os_version: "$os_version",
                    os_type: "$os_type",
                    processor: "$processor",
                    cpu_serial_number: "$cpu_serial_number",
                    cpu_code: "$cpu_code",
                    screen_serial_number: "$screen_serial_number",
                    screen_code: "$screen_code",
                    mouse_serial_number: "$mouse_serial_number",
                    mouse_code: "$mouse_code",
                    keyboard_serial_number: "$keyboard_serial_number",
                    keyboard_code: "$keyboard_code",
                    ups_serial_number: "$ups_serial_number",
                    ups_code: "$ups_code",
                    user_name: "$user_name",
                    system_password: "$system_password",
                    seat_number: "$seat_number",
                    smps: "$smps",
                    battery: '$battery',
                    status: "$status",
                    ip: "$ip",
                    system: "$system",
                    laptopName: "$laptopName",
                    laptopId: '$laptopId',
                    isAssignable: "$isAssignable",
                    createdBy: "$createdBy",
                    isDeleted: "$isDeleted",
                    deletedAt: "$deletedAt",
                    addedBy: "$addedBy",
                    updatedBy: "$updatedBy",
                    updatedAt: "$updatedAt",
                    createdAt: "$createdAt",
                },
            },
            {
                $match: query,
            },
            {
                $sort: { createdAt: -1 },
            },
        ]).toArray((err, totalResult) => {
            if (err) {
                return res.status(400).json({
                    success: false,
                    error: { message: err },
                });
            }
            db.collection("systemmanagement").aggregate([
                {
                    $project: {
                        id: "$_id",
                        description: "$description",
                        ram: "$ram",
                        drives: "$drives",
                        drives_size: "$drives_size",
                        operating_system: "$operating_system",
                        os_version: "$os_version",
                        os_type: "$os_type",
                        processor: "$processor",
                        cpu_serial_number: "$cpu_serial_number",
                        cpu_code: "$cpu_code",
                        screen_serial_number: "$screen_serial_number",
                        screen_code: "$screen_code",
                        mouse_serial_number: "$mouse_serial_number",
                        mouse_code: "$mouse_code",
                        keyboard_serial_number: "$keyboard_serial_number",
                        keyboard_code: "$keyboard_code",
                        ups_serial_number: "$ups_serial_number",
                        ups_code: "$ups_code",
                        user_name: "$user_name",
                        system_password: "$system_password",
                        seat_number: "$seat_number",
                        smps: "$smps",
                        battery: '$battery',
                        status: "$status",
                        ip: "$ip",
                        system: "$system",
                        laptopName: "$laptopName",
                        laptopId: '$laptopId',
                        isAssignable: "$isAssignable",
                        createdBy: "$createdBy",
                        isDeleted: "$isDeleted",
                        deletedAt: "$deletedAt",
                        addedBy: "$addedBy",
                        updatedBy: "$updatedBy",
                        updatedAt: "$updatedAt",
                        createdAt: "$createdAt",
                    },
                },
                {
                    $match: query,
                },
                {
                    $sort: { createdAt: -1 },
                },
                {
                    $skip: skipNo,
                },
                {
                    $limit: Number(count),
                },
            ]).toArray((err, result) => {
                if (err) {
                    return res.status(400).json({
                        success: false,
                        error: { message: err },
                    });
                } else {

                    let resData = {
                        total_count: totalResult.length,
                        data: result
                    }

                    if (!req.param('page') && !req.param('count')) {
                        resData = {
                            total_count: totalResult.length,
                            data: result
                        }
                        return res.status(200).json({
                            success: true,
                            message: constants.SYSTEM_MANAGEMENT.GET_ALL,
                            data: resData
                        });
                    }

                    return res.status(200).json({
                        success: true,
                        message: constants.SYSTEM_MANAGEMENT.GET_ALL,
                        data: resData
                    });
                }
            });
        })
    }
    catch (err) {
        return res.status(400).json({
            success: false,
            error: { message: err },
        });
    }
};


exports.editSystemsRecord = async (req, res) => {
    try {
        let data = req.body
        let id = req.body.id;
        if (!id) {
            throw constants.SYSTEM_MANAGEMENT.ID_REQUIRED;
        }
        data.updatedBy = req.identity.id;
        let updateSystemRecord = await SystemManagement.updateOne({ id: id }, data);
        if (updateSystemRecord) {
            return res.status(200).json({
                success: true,
                message: constants.SYSTEM_MANAGEMENT.UPDATED_SYSTEM,
                data: updateSystemRecord
            });
        }
        throw constants.SYSTEM_MANAGEMENT.INVALID_ID;

    } catch (err) {
        return res.status(400).json({
            success: false,
            error: { message: err },
        });
    }
};


exports.deleteSystemRecord = async (req, res) => {
    try {
        let id = req.param('id');
        if (!id) {
            throw constants.SYSTEM_MANAGEMENT.ID_REQUIRED;
        }

        let delete_system_record = await SystemManagement.updateOne({ id: id }, { 'isDeleted': true });
        if (delete_system_record) {
            return res.status(200).json({
                success: true,
                message: constants.SYSTEM_MANAGEMENT.DELETED_SYSTEM,
                data: delete_system_record
            });
        }

        throw constants.SYSTEM_MANAGEMENT.INVALID_ID;
    }

    catch (err) {
        return res.status(400).json({
            success: false,
            error: { message: err },
        });
    }
};


