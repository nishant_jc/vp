/**
 * PortfolioController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

 var constantObj = sails.config.constants;

module.exports = {
    /**
   *
   * @param {*} req
   * @param {*} res
   * @description: Used to add Portfolio
   * @createdAt 25/10/2021
   * @createdBy : Rohit kumar
   */

   addPortfolio: (req, res) => {
    API(PortfolioService.savePortfolio, req, res);
  },

  /**
   *
   * @param {*} req
   * @param {*} res
   * @description: Used to edit Portfolio
   * @createdAt 25/10/2021
   * @createdBy : Rohit kumar
   */

  edit: (req, res) => {
    API(PortfolioService.updatePortfolio, req, res);
  },


  /**
   *
   * @param {*} res
   * @returns
   * @description: Api Used to get Portfolio
   * @createdAt : 25/10/2021
   * @createdBy Rohit kumar
   */

   getAllPortfolio: async (req, res, next)=> {
    var search = req.param("search");
    var sortBy = req.param("sortBy");
    var page = req.param("page");
    if (page == undefined) {
      page = 1;
    }
    var count = req.param("count");
    if (count == undefined) {
      count = 100;
    }
    var skipNo = (page - 1) * count;

    var query = {};
    if (sortBy) {
      sortBy = sortBy.toString();
    } else {
      sortBy = "createdAt desc";
    }

    query.isDeleted = false;
    await Portfolio.count(query)
      .exec( (err, total)=> {

        if (err) {module.exports = {
  

        };
        
          return res.status(400).json({
            success: false,
            error: err,
          });
        } else {
            Portfolio.find(query)
            .sort(sortBy)
            .exec( (err, Portfolio)=> {
              if (err) {
                return res.status(400).json({ success: false, error: { code: 400, message: err } });
              } else {
                return res.json({
                  success: true,
                  code: 200,
                  data: Portfolio,
                  total: total,
                });
              }
            });
        }
      });
  },


  /**
   *
   * @param {id} req
   * @param {*} res
   * @returns
   * @description: Api Used to get single Portfolio by ID
   * @createdAt : 25/10/2021
   * @createdBy Rohit kumar
   */

   getSinglePortfolio: async (req, res) => {
    try {
      let PortfolioID = req.param("id");
      if (PortfolioID == null) {
        return res.status(404).json({
          success: false,
          message: constantObj.Portfolio.ID_REQUIRED,
        });
      } else {
        var PortfolioData = await Portfolio.findOne({ id: PortfolioID,isDeleted:false });
        if (PortfolioData) {
          return res.status(200).json({
            success: true,
            message: constantObj.Portfolio.GET_DATA,
            data: PortfolioData,
          });
        } else {
          return res.status(404).json({
            success: false,
            message: constantObj.Portfolio.NO_RESULT,
          });
        }
      }
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },
  

  /**
   *
   * @param {id} req
   * @param {*} res
   * @returns
   * @description: Api Used to delete Portfolio by ID
   * @createdAt : 25/10/2021
   * @createdBy Rohit kumar
   */

   PortfolioDeletedByID: async (req, res) => {
    try {
       let deletedID = req.param("id");
       if (deletedID == undefined) {
        return res.status(404).json({
          success: false,
          message: constantObj.Portfolio.ID_REQUIRED,
        });
      }
      var deletedByUser = req.identity.id;
      var deletedData = await Portfolio.updateOne({
        id: deletedID,
      }).set({ isDeleted: true, deletedBy: deletedByUser });
        
        if (deletedData) {
          return res.status(200).json({
            success: true,
            message: constantObj.Portfolio.DELETED_PORTFOLIO,
          });
        }
   
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },
  

};
