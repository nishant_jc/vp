/**
 * TeamController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
var _request = require("request");
var ObjectId = require("mongodb").ObjectID;
var constantObj = sails.config.constants;
const db = sails.getDatastore().manager;
module.exports = {
  /**
   *
   * @param {*} req
   * @param {*} res
   * @description: Used to add team to team management
   * @createdAt 15/09/2021
   * @createdBy : Chandra Shekhar
   */
  addTeam: (req, res) => {
    API(TeamService.saveTeam, req, res);
  },

  /**
   *
   * @param {*} req
   * @param {*} res
   * @description: Used to get list of team using filters
   * @createdAt 15/09/2021
   * @createdBy : Chandra Shekhar
   */
  getAllTeam: (req, res, next) => {
    var search = req.param("search");
    var sortBy = req.param("sortBy");
    var page = req.param("page");
    var count = req.param("count");

    if (page == undefined) {
      page = 1;
    }
    if (count == undefined) {
      count = 10;
    }
    var skipNo = (page - 1) * count;
    var query = {};

    if (sortBy) {
      sortBy = sortBy.toString();
    } else {
      sortBy = 1;
    }

    if (search) {
      query.$or = [{ name: { $regex: search, $options: "i" } }];
    }

    query.isDeleted = false;
    db.collection("team")
      .aggregate([
        {
          $project: {
            id: "$_id",
            name: "$name",
            description: "$description",
            members: "$members",
            status: "$status",
            deletedBy: "$deletedBy",
            addedBy: "$addedBy",
            updatedBy: "$updatedBy",
            isDeleted: "$isDeleted",
            deletedAt: "$deletedAt",
            updatedAt: "$updatedAt",
          },
        },
        {
          $match: query,
        },
      ])
      .toArray((err, totalResult) => {
        db.collection("team")
          .aggregate([
            {
              $project: {
                id: "$_id",
                name: "$name",
                description: "$description",
                members: "$members",
                status: "$status",
                deletedBy: "$deletedBy",
                addedBy: "$addedBy",
                updatedBy: "$updatedBy",
                isDeleted: "$isDeleted",
                deletedAt: "$deletedAt",
                updatedAt: "$updatedAt",
              },
            },
            {
              $match: query,
            },
            { $sort: { name: sortBy } },
            {
              $skip: skipNo,
            },
            {
              $limit: Number(count),
            },
          ])
          .toArray((err, result) => {
            if (err) {
              return res.status(400).json({
                success: false,
                error: { message: "" + err },
              });
            } else {
              return res.status(200).json({
                success: true,
                code: 200,
                data: result,
                total: totalResult.length,
              });
            }
          });
      });
  },

  /**
   *
   * @param {*} req
   * @param {*} res
   * @description : Used to edit & update the team
   * @createdAt 15/09/2021
   * @createdBy : Chandra Shekhar
   */
  updateteam: async (req, res) => {
    try {
      let teamId = req.param("id");
      if (!teamId || typeof teamId == undefined || teamId == "") {
        return res.status(404).json({
          success: false,
          error: { message: constantObj.team.ID_REQUIRED },
        });
      }

      let checkTeam = await Team.findOne({
        id: teamId,
      });

      let teamData = req.body;
      let membersID = req.body.members;
      if (membersID != undefined) {
        let memberIdArray = checkTeam.members;
        let allmembersID = membersID.split(",");
        allmembersID.forEach((element) => {
          if (memberIdArray.indexOf(element) === -1) {
            memberIdArray.push(element);
          }
        });
        req.body.members = memberIdArray;
      }

      var updatedID = req.identity.id;
      req.body.updatedBy = updatedID;
      var updatedTeamData = await Team.updateOne({
        id: teamId,
      }).set(teamData);
      return res.status(200).json({
        success: true,
        message: constantObj.team.UPDATED_TEAM,
        data: updatedTeamData,
      });
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },

  /**
   *
   * @param {*} req
   * @param {*} res
   * @description: Used to get detail of team
   * @createdAt 15/09/2021
   * @createdBy : Chandra Shekhar
   */
  getSingleteam: async (req, res) => {
    var id = req.param("id");
    if (!id || id == undefined) {
      return res.status(404).json({
        success: false,
        error: { message: constantObj.team.ID_REQUIRED },
      });
    } else {
      let checkTeam = await Team.findOne({
        id: id,
      });

      if (checkTeam == undefined) {
        return res.status(404).json({
          success: false,
          error: { message: constantObj.team.TEAM_ID_NOT_FOUND },
        });
      } else {
        Team.findOne({ id: id }).then((teamData) => {
          return res.status(200).json({
            success: true,
            data: teamData,
          });
        });
      }
    }
  },

  /**
   *
   * @param {id} req
   * @param {*} res
   * @returns
   * @description: Api Used to delete team by teamID
   * @createdAt : 15/09/2021
   * @createdBy Chandra Shekhar
   */

  teamDeletedByID: async (req, res) => {
    try {
      let deletedTeamID = req.param("id");
      if (!deletedTeamID || deletedTeamID == undefined) {
        return res.status(404).json({
          success: false,
          error: { message: constantObj.team.ID_REQUIRED },
        });
      }

      let checkTeam = await Team.findOne({
        id: deletedTeamID,
        isDeleted: false,
      });

      if (checkTeam == undefined) {
        return res.status(404).json({
          success: false,
          message: constantObj.team.TEAM_ID_NOT_FOUND,
        });
      } else {
        var deletedByUser = req.identity.id;
        var deletedData = await Team.updateOne({
          id: deletedTeamID,
        }).set({ isDeleted: true, deletedBy: deletedByUser });

        if (deletedData) {
          return res.status(200).json({
            success: true,
            message: constantObj.team.DELETED_TEAM,
          });
        } else {
          return res.status(404).json({
            success: false,
            message: constantObj.team.NO_RESULT,
          });
        }
      }
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },

  /**
   *
   * @param {id,userid} req
   * @param {*} res
   * @returns
   * @description: Api Used to add multiple user to team
   * @createdAt : 15/09/2021
   * @createdBy Chandra Shekhar
   */

  addMultipleUserToTeam: async (req, res) => {
    try {
      let teamId = req.param("id");
      let userID = req.param("members");
      if (!teamId || typeof teamId == undefined) {
        return res.status(404).json({
          success: false,
          error: { code: 404, message: constantObj.team.ID_REQUIRED },
        });
      }
      if (!userID || typeof userID == undefined) {
        return res.status(404).json({
          success: false,
          error: { code: 404, message: constantObj.team.USER_ID_REQUIRED },
        });
      }

      let checkuser = await Team.findOne({
        id: teamId,
      });
       
      let usersArray = checkuser.members;        

      let allMembers = userID.split(",");
      let errorMessage = [];
      let successMessage = [];

      allMembers.forEach((element) => {
        if (usersArray.indexOf(element) === -1) {
          usersArray.push(element);
          successMessage.push(element + " member join the group successfully");
        } else {
          errorMessage.push(element + " is already exist in group");
        }
      });

      var usersUpdated = await Team.updateOne({
        id: teamId,
      }).set({ members: usersArray });

      return res.status(200).json({
        success: true,
        message: {
          join: successMessage,
          exist: errorMessage,
        },
      });
    } catch (error) {
      return res.status(400).json({
        success: false,
        error: { message: error },
      });
    }
  },
};

