/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {
  // "post /adminSignin": "UserController.adminSignin", // for admin and sub_admin
  "post /changePassword": "UserController.changePassword", //common for all current user
  "get /users": "UserController.getAllUsers",
  "put /editProfile": "UserController.editProfile",
  "post /forgotpassword": "UserController.forgotPassword",
  "put /resetPassword": "UserController.resetPassword",
  "get /user": "UserController.userProfileData", //for current user detail
  "get /userDetail": "UserController.userDetail", //user detail using Id
  "post /user/signin": "UserController.userSignin",
  "get /check/email": "UserController.checkEmail",
  "post /autoLogin": "UserController.autoLogin",
  "post /add/user": "UserController.addUser",
  "get /user/delete": "UserController.profileDeleted",
  "post /user/uploadProfileImage": "UserController.uploadImage",
  "post /user/uploadDocument": "UserController.uploadMultipleImage",
  "get /user/mailingList": "UserController.getMailingList",
  "get /user/projectassignlist": "UserController.getProjectAssignUser",
  "post /admin/signin": "UserController.admin_Signin",
  "get /getAllDevelopers": "UserController.getAllDevelopers",
  "post /user/register": "UserController.register",



  /**Roles and permission Routes */
  "post /role/add": "RolesController.addRoles",
  "put /role": "RolesController.edit",
  "get /role": "RolesController.getRole",
  "get /getAllRoles": "RolesController.getAllRoles",
  "get /permissions": "RolesController.permissionOfRole",

  /**Team Routes */
  "post /team/add": "TeamController.addTeam",
  "get /teams": "TeamController.getAllTeam",
  "put /team/update": "TeamController.updateteam",
  "get /team": "TeamController.getSingleteam",
  "get /team/delete": "TeamController.teamDeletedByID",
  "post /add/team/members": "TeamController.addMultipleUserToTeam",

  /**Job platform Routes */
  "post /platform/add": "JobPlatformController.addPlatform",
  "get /platform": "JobPlatformController.getSinglePlatform",
  "put /platform/update": "JobPlatformController.updatePlatform",
  "get /platform/delete": "JobPlatformController.jobPlatformDeletedByID",
  "get /platforms": "JobPlatformController.getAllPlatform",

  /**Category Routes */

  "post /category": "CategoryController.save",
  "put /category": "CategoryController.update",
  "get /allcategories": "CategoryController.getAllCategory",
  "get /category": "CategoryController.getSingleCategory",
  "get /subCategories": "CategoryController.getAllSubCategory",
  "get /mainCategories": "CategoryController.getAllMainCategories",
  "delete /category": "CategoryController.deleteCategory",

  /** Common controller routes */
  "put /update/status": "CommonController.statusUpdate",
  "put /delete": "CommonController.toDelete",
  "get /getgraphcount": "CommonController.getgraphcount",
  "post /multiple/documents": "CommonController.multipleDocument",


  /**Projects  Routes */
  "post /project/add": "ProjectsController.addproject",
  "get /projects": "ProjectsController.getProjects",
  "get /project": "ProjectsController.getSingleProject",
  "put /project/update": "ProjectsController.updateSingleProjects",
  "get /project/delete": "ProjectsController.projectDeletedByID",
  "get /project/member/remove": "ProjectsController.removeMemberFromProject",
  "post /project/uploadImage": "ProjectsController.uploadImage",
  "post /project/uploadDocument": "ProjectsController.uploadDocument",
  "get /project/add/member": "ProjectsController.addmemberToProject",
  "get /getprojectbyuserid": "ProjectsController.getProjectsByUserID",
  "get /getprojectstatuscount": "ProjectsController.getProjectsstatuscountByUserID",
  "get /getadminprojectstatuscount": "ProjectsController.getProjectsstatuscount",
  "get /getprojectdocuments": "ProjectsController.getprojectdocumentzip",

  /**Leaves  Routes */
  "post /leaves/add": "LeavesController.addLeaves",
  "get /allleaves": "LeavesController.getLeaves",
  "put /leaves/status/change": "LeavesController.updatestatus",
  "get /getadminleavesstatuscount": "LeavesController.getLeavessstatuscount",
  "get /getsingleleave": "LeavesController.getSingleLeave",
  "get /getLeaveEmployee": "LeavesController.getLeaveEmployee",
  "get /getleavesStatus": "LeavesController.getLeavesStatus",
  "get /allEmployeeLeavesStatus": "LeavesController.allEmployeeLeavesStatus",
  "get /singleEmployeeLeaves": "LeavesController.singleEmployeeLeaves",


  /**Attendance  Routes */
  "post /attendance/checkin": "AttendanceController.checkIn",
  "put /attendance/checkout": "AttendanceController.checkOut",
  "get /checkinattendances": "AttendanceController.getAllattendanceCheckinTime",
  "get /attendance/currentdaytotaltime": "AttendanceController.totalCurrentTime",
  "get /attendance/allusers": "AttendanceController.allUserCurrentTime",
  "get /attendance/single/user": "AttendanceController.singleUserAttendance",
  "get /checkoutattendances": "AttendanceController.getAllattendanceCheckoutTime",
  "get /chekoutdetails": "AttendanceController.getAllattendanceInExcelSheet",
  "get /absentemployees": "AttendanceController.absentEmployees",




  /**Resignation  Routes */
  "post /resign/add": "ResignationController.addResign",
  "get /allresign": "ResignationController.getResign",
  "put /resign/status/change": "ResignationController.updatestatus",
  "put /resign/status/cancel": "ResignationController.cancelstatus",
  "get /getsingleresign": "ResignationController.getSingleResignation",

  /**Knowledge share  Routes */
  "post /add/post": "KnowledgeshareController.addpost",
  "get /posts": "KnowledgeshareController.getPosts",
  "get /post": "KnowledgeshareController.getSinglePost",
  "put /post/update": "KnowledgeshareController.updateSinglePost",
  "put /post/delete": "KnowledgeshareController.postDeletedByID",

  /** task */
  "post /task": "TaskController.assignTask",
  "get /all/task": "TaskController.getTaskList",
  "put /update/task": "TaskController.editTask",
  "get /task": "TaskController.taskDetail",
  "delete /remove/task": "TaskController.removeTask",

  /** Analatics */

  "get /analatics": "AnalaticsController.getAnalatic",
  "get /total/employee": "AnalaticsController.getTotalEmployeeCount",
  "get /total/task": "AnalaticsController.getTotalTask",
  "get /newEmployee": "AnalaticsController.getNewEmployee",
  "get /all/projects": "AnalaticsController.getTotalProjects",
  "get /projects/status": "AnalaticsController.progressStatusProjectsCount",

  /** Activities */
  "get /activities": "UserController.getActivitiesList",
  "get /active": "UserController.getTodayLoginUsers",
  /**Appraisal  Routes */
  "post /add/appraisal": "AppraisalController.addAppraisal",
  "put /appraisal": "AppraisalController.edit",
  "put /appraisal/delete": "AppraisalController.appraisalDeletedByID",
  "get /getallappraisal": "AppraisalController.getAllappraisal",
  "get /appraisal/single/user": "AppraisalController.singleUserAppraisal",
  // "get /pending": "AppraisalController.getInProgressAppraisal",


  /**Contentmanagement Routes */
  "post /add/content": "ContentmanagementController.addContent",
  "put /content": "ContentmanagementController.edit",
  "get /getallcontents": "ContentmanagementController.getAllcontents",
  "get /getsinglecontents": "ContentmanagementController.getSingleContent",

  /**Testimonials Routes */
  "post /add/testimonial": "TestimonialsController.addTestimonial",
  "put /testimonial": "TestimonialsController.edit",
  "get /getalltestimonials": "TestimonialsController.getAllTestimonials",
  "get /getsingletestimonial": "TestimonialsController.getSingleTestimonial",
  "put /testimonial/delete": "TestimonialsController.testimonialDeletedByID",


  /**technology Routes */
  "post /add/technology": "TechnologyController.addTechnology",
  "put /technology": "TechnologyController.edit",
  "get /getalltechnology": "TechnologyController.getAllTechnology",
  "get /getsingletechnology": "TechnologyController.getSingleTechnology",
  "put /technology/delete": "TechnologyController.TechnologyDeletedByID",


  /**Service Routes */
  "post /add/service": "ServicesController.addService",
  "put /service": "ServicesController.edit",
  "get /getallservices": "ServicesController.getAllServices",
  "get /getsingleservice": "ServicesController.getSingleService",
  "put /service/delete": "ServicesController.ServiceDeletedByID",

  /**Weeklyplan Routes */
  "post /add/Weeklyplan": "WeeklyplanController.addWeeklyplan",
  "put /Weeklyplan": "WeeklyplanController.edit",
  "get /getallWeeklyplan": "WeeklyplanController.getAllWeeklyplan",
  "get /getsingleWeeklyplan": "WeeklyplanController.getSingleWeeklyplan",
  "put /Weeklyplan/delete": "WeeklyplanController.WeeklyplanDeletedByID",
  "get /getWeeklyplanbyprojectID": "WeeklyplanController.getWeeklyplanbyuserID",
  "get /getWeeklyplanjson": "WeeklyplanController.getSingleWeeklyplanjson",


  /**Portfolio Routes */
  // "post /add/Portfolio": "PortfolioController.addPortfolio",
  // "put /Portfolio": "PortfolioController.edit",
  // "get /getallPortfolio": "PortfolioController.getAllPortfolio",
  // "get /getsinglePortfolio": "PortfolioController.getSinglePortfolio",
  // "put /Portfolio/delete": "PortfolioController.PortfolioDeletedByID",

  /**ContactUs Routes */
  "post /add/ContactUs": "ContactUsController.addContactUs",
  "put /ContactUs": "ContactUsController.edit",
  "get /getallContactUs": "ContactUsController.getAllContactUs",
  "get /getContactUs": "ContactUsController.getContactUs",


  /**DailyTask Routes */
  "post /add/dailytask": "DailyTaskController.addDailyTask",
  "put /dailytask": "DailyTaskController.edit",
  "get /getalldailytask": "DailyTaskController.getAllDailyTask",
  "get /getsingledailytask": "DailyTaskController.getSingleDailyTask",
  "put /dailytask/delete": "DailyTaskController.DailyTaskDeletedByID",
  "get /getdailytaskbyusrid": "DailyTaskController.getDailyTaskbyuserID",

  /**DailyTask Routes */
  "post /add/hrexpenses": "HrExpensesController.addHrExpenses",
  "put /hrexpenses": "HrExpensesController.edit",
  "get /getallhrexpenses": "HrExpensesController.getAllHrExpenses",
  "get /getsinglehrexpenses": "HrExpensesController.getSingleHrExpenses",
  "put /hrexpenses/delete": "HrExpensesController.HrExpensesDeletedByID",

  /**ProjectLeads Routes */
  "post /lead": "ProjectLeadsController.addProjectLeads",
  "get /lead": "ProjectLeadsController.getProjectLeadsById",
  "put /lead": "ProjectLeadsController.editProjectLeads",
  "get /leads": "ProjectLeadsController.getAllProjectLeads",
  "delete /lead": "ProjectLeadsController.deleteProjectLeads",

  /**PortfolioNew Routes */

  "post /portfolio": "PortfolioNewController.addPortfolio",
  "get /portfolio": "PortfolioNewController.findSinglePortfolio",
  "get /all/portfolio": "PortfolioNewController.getAllPortfolio",
  "put /portfolio": "PortfolioNewController.editPortfolio",
  "delete /portfolio": "PortfolioNewController.deletePortfolio",

  /**PortfolioShare Routes */

  "post /portfolioShare": "PortfolioShareController.savePortfolioShare",
  "get /portfolioShare": "PortfolioShareController.getPortfolioShare",

  /**System management */

  "post /system": "SystemManagementController.addSystemData",
  "get /system": "SystemManagementController.findSingleSytem",
  "get /systems": "SystemManagementController.getAllSystems",
  "put /system": "SystemManagementController.editSystemsRecord",
  "delete /system": "SystemManagementController.deleteSystemRecord",

  /**Inventory */

  "post /inventory": "InventoryController.addInventory",
  "get /inventory": "InventoryController.findInventory",
  "get /inventories": "InventoryController.getAllInventories",
  "put /inventory": "InventoryController.editInventory",
  "delete /inventory": "InventoryController.deleteInventory",

  /**Settings Routes */
  // "post /setting": "SettingsController.addsettings",
  // "get /settings": "SettingsController.setting",
  // "put /setting": "SettingsController.updateSetting",


  // /**LaptopAssignController */

  // "post /laptop-assign": "LaptopAssignController.assignLaptop",
  // "get /laptop-list": "LaptopAssignController.assignLaptopList",
  // "put /laptop": "LaptopAssignController.editAssignLaptop",
  // "get /laptop": "LaptopAssignController.detailAssignLaptop",
  // "delete /laptop": "LaptopAssignController.removeAssignLaptop",


  /**ProjectReviewController */

  "post /project-review": "ProjectReviewController.addProjectReview",
  "get /project-reviews": "ProjectReviewController.projectReviewList",
  "put /project-review": "ProjectReviewController.editProjectReview",
  "get /project-review": "ProjectReviewController.getProjectReviewById",
  "delete /project-review": "ProjectReviewController.removeProjectReview",

  //ReviewTaskController

  "post /task_review": "ReviewTaskController.addTaskReview",
  "get /task_reviews": "ReviewTaskController.taskReviewList",
  "put /task_review": "ReviewTaskController.editTaskReview",
  "get /task_reviews/detail": "ReviewTaskController.getTaskReviewById",
  "delete /task_reviews": "ReviewTaskController.removeTaskReview",

  //InterviewController

  "post /interview": "InterviewController.addInterview",
  "get /interview/list": "InterviewController.interviewList",
  "put /interview": "InterviewController.editInterview",
  "get /interview": "InterviewController.getInterviewById",
  "delete /interview": "InterviewController.deleteInterview",

  /**SystemComplainController */

  "post /complain": "SystemComplainController.addComplain",
  "get /complain": "SystemComplainController.findSingleComplain",
  "get /complains": "SystemComplainController.comaplainList",
  "put /complain": "SystemComplainController.editcomaplain",
  "delete /complain": "SystemComplainController.deleteComplain",

  /**HrwalletController */
  "post /add/moneyto/Hrwallet": "HrwalletController.addMoneyToHrWallet",
  "get /get/Hrwallet/details": "HrwalletController.getHrWalletDetails",
  "get /get/AdvanceMoney/details": "HrwalletController.getAdvanceMoneyDetails",

  /**TodoListController */
  "post /add/todo/list": "TodoListController.addTodoList",
  "get /all/todo/list": "TodoListController.getAllTodoList",
  "get /single/todo/list": "TodoListController.getSingleTodoList",
  "put /update/todo/list": "TodoListController.updateTodoList",
  "delete /delete/todo/list": "TodoListController.deleteTodoList",

  /**UpcomingEventsController */
  "get /workAnniversary": "UpcomingEventsController.workAnniversary",
  "get /birthdays": "UpcomingEventsController.birthdays",
  "get /upcomingappraisals": "UpcomingEventsController.upcomingAppraisals",

  /**HolidaysController */
  "post /holiday": "HolidaysController.addHoliday",
  "get /holiday": "HolidaysController.getSingleHoliday",
  "get /holidaylist": "HolidaysController.Holidaylist",
  "put /holiday": "HolidaysController.updateHoliday",
  "delete /holiday": "HolidaysController.deleteHoliday",

/**AnnouncementController */
"post /announcement": "AnnouncementController.addAnnouncement",
"get /announcement": "AnnouncementController.getSingleAnnouncement",
"get /announcementListing": "AnnouncementController.getAnnouncementListing",
"put /announcement": "AnnouncementController.updateAnnouncement",
"delete /announcement": "AnnouncementController.deleteAnnouncement",


/**DeviceController */

"post /device": "DeviceController.addDevice",
"get /device": "DeviceController.getSingleDevice",
"get /deviceList": "DeviceController.deviceList",
"put /device": "DeviceController.updateDevice",
"delete /device": "DeviceController.deleteDevice",

// /**DeviceController */
// "post /notification": "NotificationsController.addNotification",
// "get /notifications": "NotificationsController.Notifications",



/**DeviceAssignController */

"post /assign/device": "DeviceAssignController.assignDevice",
"get /all/assigned/devices": "DeviceAssignController.assignDeviceList",
"put /edit/assigned/device": "DeviceAssignController.editAssignDevice",
"get /get/assign/device": "DeviceAssignController.detailAssignDevice",
"delete /remove/assigned/device": "DeviceAssignController.removeAssignDevice",

/**CommentsController */
"post /comment": "CommentsController.addComment",
"get /comment": "CommentsController.getSingleComment",
"get /comments": "CommentsController.comments",
"put /comment": "CommentsController.updateComment",
"delete /comment": "CommentsController.deleteComment",

/**SalaryController */
"post /salary": "SalaryController.addSalary",
"get /salary": "SalaryController.getSingleSalary",
"get /salaries": "SalaryController.Salarylist",
"put /salary": "SalaryController.updateSalary",
"delete /salary": "SalaryController.deleteSalary",
"get /payableSalaryList": "SalaryController.payableSalaryList",

/**TargetController */
"post /target": "TargetController.addTarget",
"get /target": "TargetController.getSingleTarget",
"get /targets": "TargetController.getTargets",
"put /target": "TargetController.updateTarget",
"delete /target": "TargetController.deleteTarget",

/**NewUserController */
"get /newUser": "NewUserController.newUser", 

/**TasktrackingController */
'get /getalltask': 'TaskTrackingController.getalltask'

};



