/**
 * Seed Function
 * (sails.config.bootstrap)
 *
 * A function that runs just before your Sails app gets lifted.
 * > Need more flexibility?  You can also create a hook.
 *
 * For more information on seeding your app with fake data, check out:
 * https://sailsjs.com/config/bootstrap
 */
const cron = require('node-cron');
const excel = require('exceljs');
const SmtpController = require("../api/controllers/SmtpController");
var constant = require('../config/local');
const { log } = require('grunt');
const process = require('process');
process.env.TZ = 'Asia/Kolkata';

function containsObject(obj, list) {
  var i;
  for (i = 0; i < list.length; i++) {
    if (list[i].id == obj) {
      return true;
    }
  }

  return false;
}

function getDayName(date = new Date(), locale = 'en-US') {
  return date.toLocaleDateString(locale, { weekday: 'long' });
}


/**For this format dd-mm-yyyy */
function taskDate(dateMilli) {
  var d = (new Date(dateMilli) + '').split(' ');
  d[2] = d[2] + ',';

  //console.log([d[0], d[1], d[2], d[3]].join(' '), "==========================dateMilli");
  return [d[0], d[1], d[2], d[3]].join(' ');
}

module.exports.bootstrap = async function (cb) {

  cron.schedule('59 23 * * *', async () => {
    try {
      const db = sails.getDatastore().manager;

      //var todayDate = "2023-09-15";
      var todayDate = new Date().toISOString().slice(0, 10);
      let ans = getDayName()
      //let ans = "Friday";

      // var todayDate = new Date("2023-07-15T11:12:22.258Z").toISOString().slice(0, 10);

      var datemilli = todayDate;
      var newFormat = taskDate(datemilli)
      var array = [];
      var absent_users;
      // console.log(newFormat, "newFormat");
      // const result = moment().format('DD/MM/YYYY');
      // let attendanceData1 = await Attendance.find({
      //   attendanceDate: todayDate,
      // });
      let query = {};
      // query.device = "desktop"
      query.isDeleted = false;
      // if (todayDate) {
      //   query.attendanceDate = {
      //     '===': todayDate,
      //   };

      // }


      if (todayDate) {
        // console.log(todayDate, "==============todayDate");
        // var date = new Date(todayDate);
        // date.setDate(date.getDate());
        query.$and = [
          { attendanceDate: { $eq: todayDate } }
        ];
        // query.currentDate = { $gte: new Date(date), $lte: new Date(Enddate) };
      }


      db.collection('attendance').aggregate([
        {
          $project: {
            id: "$_id",
            checkIn: "$checkIn",
            checkOut: "$checkOut",
            userId: "$userId",
            name: "$name",
            email: "$email",
            totalTime: "$totalTime",
            totalseconds: "$totalseconds",
            attendanceDate: "$attendanceDate",
            punchInDate: "$punchInDate",
            reason: "$reason",
            status: "$status",
            check_in_device: "$check_in_device",
            check_out_device: "$check_out_device",
            createdAt: "$createdAt",
            deletedBy: "$deletedBy",
            addedBy: "$addedBy",
            updatedBy: "$updatedBy",
            isDeleted: "$isDeleted",
            deletedAt: "$deletedAt",
            updatedAt: "$updatedAt",
          }
        },
        {
          $match: query,
        },
      ]).toArray((err, totalResult) => {
        if (err) {
          return res.status(400).json({
            success: false,
            error: { code: 400, message: "" + err }
          })
        }
        db.collection('attendance').aggregate([
          {
            $project: {
              id: "$_id",
              checkIn: "$checkIn",
              checkOut: "$checkOut",
              userId: "$userId",
              name: "$name",
              email: "$email",
              totalTime: "$totalTime",
              totalseconds: "$totalseconds",
              attendanceDate: "$attendanceDate",
              punchInDate: "$punchInDate",
              reason: "$reason",
              check_in_device: "$check_in_device",
              check_out_device: "$check_out_device",
              status: "$status",
              createdAt: "$createdAt",
              deletedBy: "$deletedBy",
              addedBy: "$addedBy",
              updatedBy: "$updatedBy",
              isDeleted: "$isDeleted",
              deletedAt: "$deletedAt",
              updatedAt: "$updatedAt",
            }
          },
          {
            $match: query,
          },
          {
            $group: {
              _id: { userId: "$userId", check_in_device: "$check_in_device" },
              // _id: { userId: "$userId" },
              name: { $first: "$name" },
              // _id: "$employee_id",
              checkIn: { $first: "$checkIn" },
              checkOut: { $last: "$checkOut" },
              // totalTime: { $first: "$totalTime" },
              // totalTime:"$totalTime",
              Attendance: {
                $push: {
                  id: "$_id",
                  checkIn: "$checkIn",
                  checkOut: "$checkOut",
                  userId: "$userId",
                  name: "$name",
                  totalTime: "$totalTime",
                  totalseconds: "$totalseconds",
                  check_in_device: "$check_in_device",
                  check_out_device: "$check_out_device",
                  attendanceDate: "$attendanceDate",
                }
              }

            }
          },
          // {
          //   $sort: {
          //     createdAt: -1
          //   }
          // },
        ]).toArray(async (err, result) => {
          // console.log(JSON.stringify(result), "============result");
          if (result && result.length > 0) {
            let workbook = new excel.Workbook();
            let worksheet = workbook.addWorksheet("Daily Attendance Records");

            worksheet.columns = [
              { header: newFormat, key: todayDate, width: 15, color: { argb: 'ffffff' } },
              { header: "Employee Name", key: "name", width: 17 },
              { header: "Check In Time", key: "checkIn", width: 25 },
              { header: "Check Out Time", key: "checkOut", width: 25 },
              { header: "Total Time", key: "totalTime", width: 30 },
              { header: "Device", key: "check_in_device", width: 30 },
              { header: "Leaves", key: "Leaves", width: 30 },

            ];

            worksheet.addRow({ date: todayDate });
            worksheet.getCell('A1').border = {
              top: { style: 'thick', color: { argb: 'FF00FF00' } },
              left: { style: 'thick', color: { argb: 'FF00FF00' } },
              bottom: { style: 'thick', color: { argb: 'FF00FF00' } },
              right: { style: 'thick', color: { argb: 'FF00FF00' } },
            };
            worksheet.getCell('A1').fill = {
              type: 'pattern',
              pattern: 'darkTrellis',
              fgColor: { argb: 'FF00FF00' },
              bgColor: { argb: 'FF00FF00' }
            };

            for await (let values of result) {

              //----------------In Time
              let checkIndate = values.checkIn;
              if (checkIndate) {
                let checkIntime = checkIndate.toLocaleTimeString('en-IN', { timeStyle: 'medium' });
                values.checkIn = checkIntime;
              }


              //--------------Out Time
              let checkOutdate = values.checkOut;
              if (checkOutdate) {
                let checkOutTime = checkOutdate.toLocaleTimeString('en-IN', { timeStyle: 'medium' });

                values.checkOut = checkOutTime;
                // console.log(values.checkOut);
                // values.totalTime=items.totalTime;
              }

              //-----------------Device type
              values.check_in_device = values._id.check_in_device

              // for await (items of values.Attendance) {
              //   // console.log(items, "===============items");
              //   if (items.check_in_device === "desktop") {
              //     items.totalseconds = Number(items.totalseconds)
              //     seconds1 = seconds1 + items.totalseconds

              //     if (seconds1 >= 3600) {
              //       var sec_num = parseInt(seconds1, 10);
              //       var hours = Math.floor(sec_num / 3600);
              //       var minutes = Math.floor(sec_num / 60) % 60;
              //       // var seconds1 = sec_num % 60;
              //       var Hourstotaltime1 = hours + "hr " + minutes + "m " + seconds + "s ";
              //       console.log(Hourstotaltime1, "----------------------desktop   if");

              //     } else {
              //       if (seconds1 >= 60 && seconds1 < 3600) {
              //         var ndate = new Date(seconds1 * 1000).toISOString().substr(14, 5);
              //         var Newtimearray = ndate.split(":");
              //         var Hourstotaltime1 = Newtimearray[0] + "m " + Newtimearray[1] + "s";
              //       } else {
              //         var Hourstotaltime1 = seconds1 + "s";
              //       }
              //     }
              //     console.log(Hourstotaltime1, "---------------------desktop");
              //     // items.totalTime = Hourstotaltime1
              //     // items.totalTime = Hourstotaltime1

              //     values.time = items.totalTime

              //     console.log(items, "======items");

              //   }



              //   if (items.check_in_device === "mobile") {
              //     items.totalseconds = Number(items.totalseconds)
              //     seconds2 = seconds2 + items.totalseconds

              //     if (seconds2 >= 3600) {
              //       var sec_num = parseInt(seconds2, 10);
              //       var hours = Math.floor(sec_num / 3600);
              //       var minutes = Math.floor(sec_num / 60) % 60;
              //       // var seconds2 = sec_num % 60;
              //       var Hourstotaltime1 = hours + "hr " + minutes + "m " + seconds + "s ";
              //       console.log(Hourstotaltime1, "----------------------mobile   if");

              //     } else {
              //       if (seconds2 >= 60 && seconds2 < 3600) {
              //         var ndate = new Date(seconds2 * 1000).toISOString().substr(14, 5);
              //         var Newtimearray = ndate.split(":");
              //         var Hourstotaltime1 = Newtimearray[0] + "m " + Newtimearray[1] + "s";
              //       } else {
              //         var Hourstotaltime1 = seconds2 + "s";
              //       }
              //     }
              //     console.log(Hourstotaltime1, "----------------------mobile");
              //     items.totalTime = Hourstotaltime1
              //     values.time = items.totalTime
              //     console.log(items, "======items");
              //   }
              // }
              // console.log(values, "===============values");



              let id = values._id.userId;
              id = id.toString();

              // let attendanceData2 = await AttendenceTimeSheet.findOne({
              //   attendanceDate: todayDate,
              //   addedBy: id
              // });


              let attendance_desktop_data = await Attendance.find({
                attendanceDate: todayDate,
                userId: id,
                check_in_device: "desktop"
              });
              if (attendance_desktop_data && attendance_desktop_data.length > 0) {
                // console.log(attendance_desktop_data, "================attendance_desktop_data");
                let counter1 = 0
                for await (let desktop of attendance_desktop_data) {
                  array.push(desktop.userId);
                  var name = desktop.name;
                  var user_id = desktop.userId;

                  desktop.totalseconds = Number(desktop.totalseconds)
                  counter1 = counter1 + desktop.totalseconds
                  if (values.check_in_device == "desktop") {
                    // console.log(counter1,"counter1");
                    // if (counter1 < 28800) {
                    //   values.Leaves = "Short Leave"
                    // }
                    if (counter1 >= 3600) {
                      var sec_num = parseInt(counter1, 10);
                      var hours = Math.floor(sec_num / 3600);
                      var minutes = Math.floor(sec_num / 60) % 60;
                      var seconds = sec_num % 60;
                      var Hourstotaltime1 = hours + "hr " + minutes + "m " + seconds + "s ";
                      // console.log(Hourstotaltime1, "----------------------desktop   if");

                    } else {
                      if (counter1 >= 60 && counter1 < 3600) {
                        var ndate = new Date(counter1 * 1000).toISOString().substr(14, 5);
                        var Newtimearray = ndate.split(":");
                        var Hourstotaltime1 = Newtimearray[0] + "m " + Newtimearray[1] + "s";
                      } else {
                        var Hourstotaltime1 = counter1 + "s";
                      }
                    }
                    values.totalTime = Hourstotaltime1
                    //    if (counter1 < 28800) {
                    //   values.Leaves = "Short Leave"
                    // }
                  }
                }
                // console.log(counter1, "counter1");
                if ((user_id == "6465d610fd6729ad6e4b1946") && counter1 < 18000) {
                  values.Leaves = "Short Leave"
                }

                if ((user_id !== "6465d610fd6729ad6e4b1946") && counter1 < 28800) {
                  values.Leaves = "Short Leave"
                }


              }
              //console.log(array, "=====================array");


              let attendance_mobile_data = await Attendance.find({
                attendanceDate: todayDate,
                userId: id,
                check_in_device: "mobile"
              });
              // console.log(attendance_mobile_data,"===================attendance_mobile_data");
              if (attendance_mobile_data && attendance_mobile_data.length > 0) {
                let counter2 = 0
                for await (let desktop of attendance_mobile_data) {
                  array.push(desktop.userId);
                  desktop.totalseconds = Number(desktop.totalseconds)
                  counter2 = counter2 + desktop.totalseconds
                  if (values.check_in_device == "mobile") {
                    // console.log(counter2, "counter2counter2counter2counter2");
                    // if (counter2 < 28800) {
                    values.Leaves = "Short Leave"
                    // }
                    if (counter2 >= 3600) {
                      var sec_num = parseInt(counter2, 10);
                      var hours = Math.floor(sec_num / 3600);
                      var minutes = Math.floor(sec_num / 60) % 60;
                      var seconds = sec_num % 60;
                      var Hourstotaltime1 = hours + "hr " + minutes + "m " + seconds + "s ";
                      // console.log(Hourstotaltime1, "----------------------desktop   if");

                    } else {
                      if (counter2 >= 60 && counter2 < 3600) {
                        var ndate = new Date(counter2 * 1000).toISOString().substr(14, 5);
                        var Newtimearray = ndate.split(":");
                        var Hourstotaltime1 = Newtimearray[0] + "m " + Newtimearray[1] + "s";
                      } else {
                        var Hourstotaltime1 = counter2 + "s";
                      }
                    }
                    values.totalTime = Hourstotaltime1
                  }
                }
                // console.log(counter2,"counter2");

                // if (counter2 < 28800) {
                //   values.Leaves = "Short Leave"
                // }
              }



              // if (attendanceData2) {
              //   // values.totalTime = attendanceData2.totaltime
              //   attendanceData2.totalseconds = Number(attendanceData2.totalseconds)
              //   if (attendanceData2.totalseconds && attendanceData2.totalseconds <= 28800) {
              //     values.Leaves = "Short Leave";
              //   }
              // }


              // let startTime = checkIndate;
              // let endTime = checkOutdate
              // if (startTime && endTime) {
              //   var seconds = (endTime.getTime() - startTime.getTime()) / 1000;
              //   var lastSecond = seconds;
              //   if (seconds >= 3600) {
              //     var sec_num = parseInt(seconds, 10);
              //     var hours = Math.floor(sec_num / 3600);
              //     var minutes = Math.floor(sec_num / 60) % 60;
              //     var seconds = sec_num % 60;
              //     var Hourstotaltime = hours;
              //     console.log('Hours----------', Hourstotaltime);

              //     if (Hourstotaltime) {
              //       if (Hourstotaltime < 8) {
              //         values.Leaves = "Short Leave"
              //       }
              //     } else {
              //       Hourstotaltime = 0
              //       if (Hourstotaltime == 0) {
              //         values.Leaves = "Short Leave"
              //       }
              //     }
              //     // console.log(typeof Hourstotaltime, "========Hourstotaltime===", Hourstotaltime);
              //   } else {
              //     if (seconds >= 60 && seconds < 3600) {
              //       var ndate = new Date(seconds * 1000).toISOString().substr(14, 5);
              //       var Newtimearray = ndate.split(":");
              //       var Hourstotaltime = Newtimearray[0] + "m " + Newtimearray[1] + "s";
              //     } else {
              //       var Hourstotaltime = Math.floor(seconds) + "s";
              //     }
              //   }
              // }

              // console.log(values, "================values");
              // id = counter;
              // if (counter) {
              //   values.serial_number = `${1 + counter}`;
              // } else {
              //   values.serial_number = `${1}`;
              // }

              worksheet.addRow(values);
            }

            let uniqueItems = [...new Set(array)];
            var len = uniqueItems.length;
            var cell = len + 4;
            worksheet.addRow();

            worksheet.addRow({ name: "Absentees" });
            worksheet.getCell('B' + `${cell}`).border = {
              top: { style: 'thick', color: { argb: '00FF0000' } },
              left: { style: 'thick', color: { argb: '00FF0000' } },
              bottom: { style: 'thick', color: { argb: '00FF0000' } },
              right: { style: 'thick', color: { argb: '00FF0000' } },
            };
            worksheet.getCell('B' + `${cell}`).fill = {
              type: 'pattern',
              pattern: 'darkTrellis',
              fgColor: { argb: '00FF0000' },
              bgColor: { argb: '00FF0000' }
            };

            let attendance_data = await Users.find({ id: { 'nin': array }, role: { 'nin': ["admin", "subadmin"] }, status: "active", isDeleted: false });
            if (attendance_data && attendance_data.length > 0) {
              for await (let users of attendance_data) {
                absent_users = users.fullName;
                worksheet.addRow({ name: absent_users });
              }
            }


            try {
              // res.setHeader(
              //   "Content-Type",
              //   "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
              // );
              // res.setHeader(
              //   "Content-Disposition",
              //   "attachment; filename=" + "Attendance.xlsx"
              // );

              var rootpath = process.cwd();
              var downloadFileName = 'Attendance.xlsx';

              var folder = rootpath + "/assets/documents/documents/" + downloadFileName;
              console.log(folder, "============folder");

              workbook.xlsx.writeFile(folder).then(function () {
                console.log("Excel sheet successfully saved....");
              });

              if (ans != "Saturday" && "Sunday") {
                console.log("Mail sent successfully")
                SendEmailToAdmin({
                  toemail: ["nishant@jcsoftwaresolution.com", "shivani@yopmail.com"],
                  attachment: folder
                });
              } else {
                console.log("Weekend");
              }
            } catch (err) {
              console.log(err);
            }
            // }
          } else {
            console.log('No attendance data found');
          }
        })
      })

      /**NEW Method---------------------- */


      /**ENding */

      // if (attendanceData1 && attendanceData1.length > 0) {
      //   let workbook = new excel.Workbook();
      //   let worksheet = workbook.addWorksheet("Daily Attendance Records");


      //   worksheet.columns = [
      //     { header: newFormat, key: todayDate, width: 15, color: { argb: 'ffffff' } },
      //     // { header: "serial_number", key: "serial_number", width: 15 },
      //     { header: "Employee Name", key: "name", width: 17 },
      //     { header: "Check In Time", key: "checkIn", width: 25 },
      //     { header: "Check Out Time", key: "checkOut", width: 25 },
      //     { header: "Total Time", key: "totalTime", width: 30 },
      //     { header: "Leaves", key: "Leaves", width: 30 },

      //   ];

      //   worksheet.addRow({ date: todayDate });
      //   worksheet.getCell('A1').border = {
      //     top: { style: 'thick', color: { argb: 'FF00FF00' } },
      //     left: { style: 'thick', color: { argb: 'FF00FF00' } },
      //     bottom: { style: 'thick', color: { argb: 'FF00FF00' } },
      //     right: { style: 'thick', color: { argb: 'FF00FF00' } },
      //   };
      //   worksheet.getCell('A1').fill = {
      //     type: 'pattern',
      //     pattern: 'darkTrellis',
      //     fgColor: { argb: 'FF00FF00' },
      //     bgColor: { argb: 'FF00FF00' }
      //   };
      //   // let counter
      //   // worksheet.addRow({ date: todayDate });


      //   for await (let values of attendanceData1) {
      //     //----------------In Time
      //     let checkIndate = values.checkIn;
      //     console.log(checkIndate, values.name, "==================check in date fort repeated");
      //     if (checkIndate) {
      //       // console.log(checkIndate, "================checkIndate");
      //       let checkIntime = checkIndate.toLocaleTimeString('en-IN', { timeStyle: 'medium' });
      //       // console.log(checkIntime, "===============checkIntime");
      //       values.checkIn = checkIntime;
      //     }

      //     //--------------Out Time
      //     let checkOutdate = values.checkOut;
      //     // console.log(checkOutdate, "================checkOutdate");
      //     if (checkOutdate) {
      //       let checkOutTime = checkOutdate.toLocaleTimeString('en-IN', { timeStyle: 'medium' });
      //       // console.log(checkOutTime, "===============checkOutTime");
      //       values.checkOut = checkOutTime;
      //     }
      //     // console.log(values.userId, "==================values.userId");


      //     let attendanceData2 = await AttendenceTimeSheet.findOne({
      //       attendanceDate: todayDate,
      //       addedBy: values.userId
      //     });


      //     // console.log(attendanceData2, "===================attendanceData1")
      //     if (attendanceData2) {
      //       // console.log(attendanceData2, "===================attendanceData2");
      //       values.totalTime = attendanceData2.totaltime

      //     }
      //     // if(values.name)


      //     // let Leaves
      //     // if (values.totalTime > "8") {
      //     //   console.log("Good");
      //     //   // worksheet.columns = [{ key: "Short Leave" }]
      //     //   // worksheet.addRow({Leaves: "Short Leave"})
      //     //   // key.Leaves = "Short Leave";
      //     // } else {
      //     //   console.log(values, "===short leaves");
      //     // }



      //     let startTime = checkIndate;
      //     let endTime = checkOutdate
      //     if (startTime && endTime) {
      //       var seconds = (endTime.getTime() - startTime.getTime()) / 1000;
      //       var lastSecond = seconds;
      //       if (seconds >= 3600) {
      //         var sec_num = parseInt(seconds, 10);
      //         var hours = Math.floor(sec_num / 3600);
      //         var minutes = Math.floor(sec_num / 60) % 60;
      //         var seconds = sec_num % 60;
      //         var Hourstotaltime = hours;
      //         console.log('Hours----------', Hourstotaltime);

      //         if (Hourstotaltime) {
      //           if (Hourstotaltime < 8) {
      //             values.Leaves = "Short Leave"
      //           }
      //         } else {
      //           Hourstotaltime = 0
      //           if (Hourstotaltime == 0) {
      //             values.Leaves = "Short Leave"
      //           }
      //         }
      //         // console.log(typeof Hourstotaltime, "========Hourstotaltime===", Hourstotaltime);
      //       } else {
      //         if (seconds >= 60 && seconds < 3600) {
      //           var ndate = new Date(seconds * 1000).toISOString().substr(14, 5);
      //           var Newtimearray = ndate.split(":");
      //           var Hourstotaltime = Newtimearray[0] + "m " + Newtimearray[1] + "s";
      //         } else {
      //           var Hourstotaltime = Math.floor(seconds) + "s";
      //         }
      //       }
      //     }

      //     // console.log(values, "================values");
      //     // id = counter;
      //     // if (counter) {
      //     //   values.serial_number = `${1 + counter}`;
      //     // } else {
      //     //   values.serial_number = `${1}`;
      //     // }
      //     worksheet.addRow(values);
      //     // counter++;
      //   }



      //   // console.log(worksheet);
      //   try {
      //     // res.setHeader(
      //     //   "Content-Type",
      //     //   "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      //     // );
      //     // res.setHeader(
      //     //   "Content-Disposition",
      //     //   "attachment; filename=" + "Attendance.xlsx"
      //     // );

      //     var rootpath = process.cwd();
      //     var downloadFileName = 'Attendance.xlsx';
      //     var folder = rootpath + "/assets/documents/documents/" + downloadFileName;

      //     // console.log(folder, "==========folder");
      //     workbook.xlsx.writeFile(folder).then(function () {
      //       // res.status(200).end();
      //       console.log("Excel sheet successfully saved....");
      //     });

      //     SendEmailToAdmin({
      //       toemail: ["shivani@yopmail.com", "shivika@yopmail.com"],
      //       attachment: folder
      //     });
      //   } catch (err) {
      //     console.log(err);
      //   }

      //   // }

      // } else {
      //   console.log('No attendance data found');
      // }

    } catch (error) {
      console.log(error);
    }
  });






  (() => cron.schedule('50 23 * * *', async () => {
    var query = {};
    query.checkInstatus = true;
    query.role = { in: ["developer", "qa", "bde", "seo", "designer", "HR"] }
    // console.log(query, "query");
    // query.email = "test@jcsoftwaresolution.com"
    let users = await Users.find(query);
    // console.log(users, "users");
    if (users) {
      for (i = 0; i < users.length; i++) {
        let sortBy = "updatedAt desc"
        var dt = new Date();
        // console.log(users[i].id, "---------------------vusers[i].id");
        let attendanceData = await Attendance.find({ userId: users[i].id }).sort(sortBy).limit(1);
        // console.log(attendanceData,"=============attendanceData");
        var attendancecurrentDate = attendanceData[0].attendanceDate;
        // console.log(attendancecurrentDate,"=============attendancecurrentDate");

        var checkIntime = attendanceData[0].checkIn;
        // console.log(checkIntime,"=============checkIntime");

        var startTime = checkIntime;
        // console.log(startTime,"=============startTime");

        var endTime = dt;
        // console.log(endTime,"=============endTime");


        var seconds = (endTime.getTime() - startTime.getTime()) / 1000;
        // console.log(seconds,"=============seconds");

        var lastSecond = seconds;
        if (seconds >= 3600) {
          var sec_num = parseInt(seconds, 10);
          var hours = Math.floor(sec_num / 3600);
          var minutes = Math.floor(sec_num / 60) % 60;
          var seconds = sec_num % 60;
          var Hourstotaltime = hours + "hr " + minutes + "m " + seconds + "s ";
        } else {
          if (seconds >= 60 && seconds < 3600) {
            var ndate = new Date(seconds * 1000).toISOString().substr(14, 5);
            var Newtimearray = ndate.split(":");
            var Hourstotaltime = Newtimearray[0] + "m " + Newtimearray[1] + "s";
          } else {
            var Hourstotaltime = Math.floor(seconds) + "s";
          }
        }

        let updateattendance = await Attendance.updateOne({
          id: attendanceData[0].id,
        }).set({
          checkOut: dt,
          totalseconds: Math.floor(lastSecond),
          totalTime: Hourstotaltime,
          reason: "Auto Punchout",
        });

        // console.log(updateattendance,"updateattendance");

        var dt = new Date();
        let attendanceData1 = await Attendance.find({
          userId: attendanceData[0].userId,
          attendanceDate: attendanceData[0].attendanceDate,
        });
        var seconds1 = 0;
        attendanceData1.forEach(function (value) {
          var getseconds = value.totalseconds;
          if (getseconds != "") {
            gettotaltime = parseInt(value.totalseconds);
            seconds1 = seconds1 + gettotaltime;
          }
        });
        if (seconds1 >= 3600) {
          var sec_num = parseInt(seconds1, 10);
          var hours = Math.floor(sec_num / 3600);
          var minutes = Math.floor(sec_num / 60) % 60;
          var seconds1 = sec_num % 60;
          var Hourstotaltime1 = hours + "hr " + minutes + "m " + seconds + "s ";
        } else {
          if (seconds1 >= 60 && seconds1 < 3600) {
            var ndate = new Date(seconds1 * 1000).toISOString().substr(14, 5);
            var Newtimearray = ndate.split(":");
            var Hourstotaltime1 = Newtimearray[0] + "m " + Newtimearray[1] + "s";
          } else {
            var Hourstotaltime1 = seconds1 + "s";
          }
        }

        let userdata = await Users.findOne({ id: users[i].id });
        // console.log(userdata,"userdata");

        let timesheetdata = userdata.timeSheet;
        var atDate = attendancecurrentDate;

        //Find index of specific object using findIndex method.

        if (timesheetdata.length == 0) {
          timesheetdata.push({
            id: attendancecurrentDate,
            totaltime: Hourstotaltime1,
          });
        } else {
          objIndex = timesheetdata.findIndex((obj) => {
            let timedata = containsObject(attendancecurrentDate, timesheetdata)
            if (timedata == true) {
              obj.totaltime = Hourstotaltime1;
            }
            if (timedata == false) {
              timesheetdata.push({
                id: attendancecurrentDate,
                totaltime: Hourstotaltime1,
              });
            }
          });
        }
        let updateddata = await Users.updateOne({
          id: users[i].id,
        }).set({
          // timeSheet: timesheetdata,
          checkInstatus: "false",
        });
        // console.log(updateddata,"=================updateddata");


        query = {};
        query.date = dt;
        query.totaltime = Hourstotaltime1;
        query.addedBy = users[i].id;
        query.attendanceDate = attendancecurrentDate;
        query.totalseconds = seconds1;
        query.check_in_device = attendanceData.check_in_device

        // console.log(query, "=====================query");
        /**Old code--------------start */
        let find_timesheet = await AttendenceTimeSheet.findOne({ addedBy: users[i].id, attendanceDate: query.attendanceDate });
        // console.log(find_timesheet,"*************************find_timesheet");
        if (find_timesheet) {
          let updatetimesheet = await AttendenceTimeSheet.updateOne({
            addedBy: users[i].id,
            attendanceDate: query.attendanceDate
          }, {
            // date: query.date,
            totaltime: query.totaltime,
            totalseconds: query.totalseconds
          });

          // console.log(updatetimesheet,"===================updatetimesheet");


        } else {
          let timesheet = await AttendenceTimeSheet.create(query).fetch();
          // console.log(timesheet,"===================timesheet");

        }

        // ActivityMail({
        //   type: "Check Out",
        //   fullName: users[i].fullName,
        //   date: dt,
        // });

      }
    }
  }))(null, true, 'IST');

  cb();

  /**Seeding SMTP Detail into db */

  if (await Smtp.count() == 0) {
    var smtp = await Smtp.create({
      "service": "Gmail",
      "host": "smtp.gmail.com",
      "port": 587,
      "debug": true,
      "sendmail": true,
      "requiresAuth": true,
      "domains": [
        "gmail.com",
        "googlemail.com"
      ],
      "user": "amitkjcsoftwaresolution@gmail.com",
      "pass": "iqzipnrnnuvgglvv"
    })
  }



};

SendEmailToAdmin = function (options) {
  var toemail = options.toemail;
  var attachment = options.attachment;
  (message = "");
  message += ` <div class="maindiv" style="align-items: center; box-shadow: 0px 0px 8px 0px #8080808a; height: auto; background-color: #fff; 
  width:600px;
  margin:auto;">

      <div style="border: 1px solid #ff8f26cc ;    margin:2rem auto; max-width: 600px; height: auto; background-color: #ff8f26cc ;  text-align: center;">

          <h3 style="font-size: 25px; color: #fff;">Hello Admin</h3>

      </div>
      <div style="text-align: center;">

          <div style="margin-bottom: 14px;">
              <img src="${constant.LOGO}/assets/img/logoImg.png" style="width: 107px;" />
          </div>
          <div>
              <p style="font-size: 15px;"><b>Today attendance</b></p>
              <p style="font-size: 15px;">Please Download the below attachment to view.</p>

          </div >
          <div style=" margin-top:50px">

  <p style="font-size: 15px; color: gray; padding-bottom: 15px; ">© 2023 JC Software Solution  All rights reserved.</p>
  </div>

      </div >
  </div > `

  //var to = ["chander@yopmail.com", "amit@yopmail.com"];
  SmtpController.sendInvitationEmail(
    toemail,
    "Today Attendance",
    message,
    attachment
  );
};

cron.schedule('0 0 2 * *', async () => {

  try {
    const date = new Date();
    const lastMonthDate = new Date(date);
    lastMonthDate.setMonth(date.getMonth() - 1);

    // Get the first date of the last month
    const firstDateOfMonth = new Date(lastMonthDate.getFullYear(), lastMonthDate.getMonth(), 2);

    // Get the last date of the last month
    const lastDateOfMonth = new Date(lastMonthDate.getFullYear(), lastMonthDate.getMonth() + 1, 1);

    // Format the dates as YYYY-MM-DD strings
    const startDate = firstDateOfMonth.toISOString().substring(0, 10);
    const endDate = lastDateOfMonth.toISOString().substring(0, 10);

    const current_date = lastMonthDate.toISOString().substring(0, 10);

    const currentDate = new Date(current_date);

    // Set the date to the first day of the month
    currentDate.setDate(1);

    const daysInMonth = [];
    while (currentDate.getMonth() === lastMonthDate.getMonth()) {
      const formattedDate = currentDate.toISOString().substring(0, 10);
      daysInMonth.push(formattedDate);
      currentDate.setDate(currentDate.getDate() + 1);
    }

    const weekendDates = daysInMonth.filter(dateString => {
      const parts = dateString.split('/');
      const year = parseInt(parts[2], 10);
      const month = parseInt(parts[1], 10) - 1; // Months are zero-indexed
      const day = parseInt(parts[0], 10);

      const date = new Date(year, month, day);

      // Check if it's Saturday or Sunday (0 = Sunday, 6 = Saturday)
      return date.getDay() === 0 || date.getDay() === 6;
    });

    let workbook;
    let worksheet;

    if (daysInMonth.length > 0) {
      workbook = new excel.Workbook();
      worksheet = workbook.addWorksheet("Monthly Attendance Records");

      // Add dates as headers
      worksheet.columns = [
        { header: "Employee Name", key: "name", width: 17 },
        ...daysInMonth.map((formattedDate, index) => ({
          header: formattedDate,
          key: `attendance_${index + 1}`,
          width: 15,
          style: { numFmt: 'DD/MM/YYYY' },
        })),
      ];
    }

    let employees = await Users.find({ role: { 'nin': ["admin", "subadmin"] }, status: "active", isDeleted: false });
    if (employees && employees.length > 0) {
      for await (let emp of employees) {
        var emp_id = emp.id;
        emp_name = emp.fullName;
        worksheet.addRow({ name: emp_name });

        let query = {};
        query.addedBy = emp_id;
        query.attendanceDate = {
          '>=': startDate,
          '<=': endDate
        }
        query.isDeleted = false;

        const attendance = await AttendenceTimeSheet.find(query);

        if (attendance && attendance.length > 0) {
          // Create an object to store attendance data for each day of the month
          const attendanceMap = {};

          // Initialize the attendanceMap with 'A' for each day of the month
          daysInMonth.forEach(formattedDate => {
            attendanceMap[formattedDate] = 'AB';
          });

          // Update the attendanceMap with actual attendance data
          attendance.forEach(attendance_data => {
            const attendance_date = attendance_data.attendanceDate;
            attendanceMap[attendance_date] = 'P';
          });

          // Ensure that rows exist before setting cell values
          if (!worksheet.lastRow) {
            worksheet.addRow();
          }

          // Add attendance data to the worksheet
          daysInMonth.forEach((formattedDate, index) => {
            const columnKey = `attendance_${index + 1}`;
            if (!worksheet.lastRow.getCell(columnKey)) {
              worksheet.lastRow.getCell(columnKey);
            }
            worksheet.lastRow.getCell(columnKey).value = attendanceMap[formattedDate];
          });
        } else {
          // If there's no attendance data, fill all columns with 'A'
          daysInMonth.forEach((formattedDate, index) => {
            const columnKey = `attendance_${index + 1}`;
            if (!worksheet.lastRow.getCell(columnKey)) {
              worksheet.lastRow.getCell(columnKey);
            }
            worksheet.lastRow.getCell(columnKey).value = 'AB';
          });
        }
        const leaves_query = {
          addedBy: emp_id,
          leave_status: "Accepted",
          leaveStartDate: { ">=": startDate },
          leaveEndDate: { "<=": endDate },
          isDeleted: false
        };

        const leaves = await Leaves.find(leaves_query);

        if (leaves && leaves.length > 0) {
          // Create a map to store leave dates for the employee
          const leaveDatesMap = {};
          leaves.forEach(leave_data => {
            const start_date = leave_data.leaveStartDate;
            const end_date = leave_data.leaveEndDate;
            const datesRange = getDatesInRange(start_date, end_date);
            datesRange.forEach(date => {
              leaveDatesMap[date] = true; // Mark leave dates as true
            });
          });

          // Iterate through each day of the month and update attendance data
          daysInMonth.forEach((formattedDate, index) => {
            const columnKey = `attendance_${index + 1}`;
            const currentDate = formattedDate;

            // Check if the current date is a leave date for the employee
            if (leaveDatesMap[currentDate]) {
              if (!worksheet.lastRow.getCell(columnKey)) {
                worksheet.lastRow.getCell(columnKey);
              }
              worksheet.lastRow.getCell(columnKey).value = 'LEAVE'; // Mark 'LEAVE' for leave
            }
          });
        }

        // Function to get dates range between start date and end date
        function getDatesInRange(startDate, endDate) {
          const datesRange = [];
          const currentDate = new Date(startDate);
          while (currentDate <= new Date(endDate)) {
            datesRange.push(currentDate.toISOString().substring(0, 10));
            currentDate.setDate(currentDate.getDate() + 1);
          }
          return datesRange;
        }
      }
    }

    // Iterate through each day of the month
    daysInMonth.forEach((formattedDate, index) => {
      const columnKey = `attendance_${index + 1}`;
      const currentDate = new Date(formattedDate);

      // Check if the current day is Saturday (6) or Sunday (0)
      if (currentDate.getDay() === 6 || currentDate.getDay() === 0) {
        // Apply formatting to the column (optional)
        worksheet.getColumn(columnKey).eachCell({ includeEmpty: true }, (cell, rowNumber) => {
          if (rowNumber > 1) { // Skip the header row
            // Set the cell value to "Weekend"
            cell.value = "Weekend";
            // Apply formatting to the cell
            cell.fill = {
              type: 'pattern',
              pattern: 'solid',
              fgColor: { argb: 'FFA500' } // Orange color for weekends
            };
          }
        });
      }
    });
    const holiday_query = {
      start_date: { ">=": startDate },
      end_date: { "<=": endDate },
      isDeleted: false
    };
    //console.log(holiday_query,"==============holiday_query")

    const holidays = await Holidays.find(holiday_query);

    if (holidays && holidays.length > 0) {
      for await (let holiday_data of holidays) {
        const holiday_start_date = holiday_data.start_date;
        const holiday_end_date = holiday_data.end_date;

        const startIndex = daysInMonth.findIndex(date => date === holiday_start_date);
        const endIndex = daysInMonth.findIndex(date => date === holiday_end_date);

        // If both start date and end date are found in daysInMonth array
        if (startIndex !== -1 && endIndex !== -1) {
          // Iterate through the range and update the corresponding cell value in the worksheet
          for (let i = startIndex; i <= endIndex; i++) {
            const columnKey = `attendance_${i + 1}`;
            worksheet.getColumn(columnKey).eachCell({ includeEmpty: true }, (cell, rowNumber) => {
              if (rowNumber > 1) { // Skip the header row
                cell.value = "Holiday";
                cell.fill = {
                  type: 'pattern',
                  pattern: 'solid',
                  fgColor: { argb: 'FFA500' }
                };
              }
            });
          }
        }

      }
    }

    var rootpath = process.cwd();
    var downloadFileName = 'Monthlyattendance.xlsx';

    var folder = rootpath + "/assets/documents/documents/" + downloadFileName;
    console.log(folder, "============folder");

    workbook.xlsx.writeFile(folder).then(function () {
      console.log("Excel sheet successfully saved....");
    });

    console.log("Mail sent successfully")
    SendAttendanceEmailToAdmin({
      toemail: ["kawal@yopmail.com","nishant@jcsoftwaresolution.com"],
      attachment: folder
    });



  }
  catch (err) {
    console.log(err, "=============err")
    return res.status(400).json({
      success: false,
      error: { message: err },
    });
  }
});

cron.schedule('0 0 1 * *', async () => {

  try {

    const db = sails.getDatastore().manager;
    let excelFilePath;

    var date = new Date();
      const current_date = date.toISOString().substring(0, 10);
      var current_year = date.toISOString().substring(0, 4);
      var current_month = date.toISOString().substring(5, 7);

        const pipeline = [
            {
                $lookup: {
                    from: "users",
                    localField: "employeeId",
                    foreignField: "_id",
                    as: "employee_details",
                },
            },
            {
                $unwind: {
                    path: "$employee_details",
                    preserveNullAndEmptyArrays: true,
                },
            },
            {
                $project: {
                    employeeId: "$employeeId",
                    employee_name: "$employee_details.fullName",
                    salary: "$salary",
                    account_no: "$account_no",
                    ifsc: "$ifsc",
                    bank_name: "$bank_name",
                    professional_tax: "$professional_tax",
                    //employee_details: "$employee_details",
                    isDeleted: "$isDeleted",
                    addedBy: "$addedBy",
                    updatedBy: "$updatedBy",
                    createdAt: "$createdAt",
                    updatedAt: "$updatedAt",
                }
            },
        ]
        //db.collection('salary').aggregate([...pipeline]).toArray((err, totalResult) => {
            db.collection('salary').aggregate([...pipeline]).toArray(async (err, result) => {

                if (result && result.length > 0) {

                    let salary;
                    let employee_id;
                    let professional_tax;
                    // let leave = 0;
                    // let deduction = 0;
                    // let paid = 0;
                    let per_day_pay;
                    let deduction_amount;
                    let total_salary;


                    for await (let employee of result) {

                        employee_id = employee.employeeId;
                        salary = employee.salary;
                        professional_tax = employee.professional_tax;
                        // console.log(employee_id, "============employee_id")
                        // console.log(salary, "============salary")

                        per_day_pay = salary / 30;
                        //console.log(per_day_pay, "============per_day_pay")

                        let leaves_query = {};

                        leaves_query.and = [
                            {
                                leaveStartDate: { '>=': `${current_year}` + "-" + `${current_month}` + "-01" },
                                leaveEndDate: { '<': `${current_year}` + "-" + `${current_month}` + "-31" }
                            }]

                        leaves_query.addedBy = String(employee_id);
                        leaves_query.leave_status = "Accepted";
                        leaves_query.isDeleted = false;


                        let leave = 0;
                        let deduction = 0;
                        let paid = 0;
                        let working_days;

                        const leaves_data = await Leaves.find(leaves_query);
                        if (leaves_data) {

                            for await (let leave_details of leaves_data) {
                                leave = leave + leave_details.no_of_days;
                                deduction = deduction + leave_details.deduction;
                                paid = paid + leave_details.paid;
                            }

                        }
                        working_days = 30 - deduction;
                        deduction_amount = (per_day_pay * deduction) + Number(professional_tax);
                        total_salary = salary - deduction_amount;

                        employee.leave = leave;
                        employee.deduction = deduction;
                        employee.paid = paid;
                        employee.deduction_amount = deduction_amount;
                        employee.total_salary = total_salary;
                        employee.working_days = working_days;

                    }
                }
                excelFilePath = await generatePayableSalaryExcel(result);
            })
        // })

    // Send the Excel file path in the response
} catch (err) {
    return res.status(400).json({
        success: false,
        error: { code: 400, message: "" + err }
    });
}
})

const generatePayableSalaryExcel = async (result) => {
  const workbook = new excel.Workbook();
  const worksheet = workbook.addWorksheet('Payable Salary List');

  // Define column headers
  worksheet.columns = [
      { header: 'Employee Name', key: 'employee_name', width: 20 },
      { header: 'Leave', key: 'leave', width: 15 },
      { header: 'A/C', key: 'account_no', width: 15 },
      { header: 'IFSC', key: 'ifsc', width: 15 },
      { header: 'Bank Name', key: 'bank_name', width: 20 },
      { header: 'Deduction', key: 'deduction', width: 15 },
      { header: 'Salary', key: 'salary', width: 15 },
      { header: 'Working Days', key: 'working_days', width: 15 },
      { header: 'Net Amount', key: 'net_amount', width: 15 },
      { header: 'Professional Tax', key: 'professional_tax', width: 15 },
      { header: 'Grand Total', key: 'total_salary', width: 15 },
  ];

  // Make the header row bold
  worksheet.getRow(1).font = { bold: true };

  // Add data rows
  result.forEach(employee => {
      worksheet.addRow({
          employee_name: employee.employee_name,
          leave: employee.leave,
          account_no: employee.account_no,
          ifsc: employee.ifsc,
          bank_name: employee.bank_name,
          deduction: employee.deduction,
          salary: employee.salary,
          working_days: employee.working_days,
          net_amount: employee.total_salary,
          professional_tax: employee.professional_tax,
          total_salary: employee.total_salary
      });
  });


  var rootpath = process.cwd();
  var downloadFileName = 'payable_salary_list.xlsx';

  var folder = rootpath + "/assets/documents/documents/" + downloadFileName;
  console.log(folder, "============folder");

  await workbook.xlsx.writeFile(folder).then(function () {
    console.log("Excel sheet successfully saved....");
  });

  console.log(folder,"=============filePath")

  SendSalaryEmail({
      toemail: ["kawal@yopmail.com","nishant@jcsoftwaresolution.com","hr@jcsoftwaresolution.com"],
      attachment: folder
    });
  return folder;
};

SendAttendanceEmailToAdmin = function (options) {
  var toemail = options.toemail;
  var attachment = options.attachment;
  (message = "");
  message += ` <div class="maindiv" style="align-items: center; box-shadow: 0px 0px 8px 0px #8080808a; height: auto; background-color: #fff; 
  width:600px;
  margin:auto;">

      <div style="border: 1px solid #ff8f26cc ;    margin:2rem auto; max-width: 600px; height: auto; background-color: #ff8f26cc ;  text-align: center;">

          <h3 style="font-size: 25px; color: #fff;">Hello Admin</h3>

      </div>
      <div style="text-align: center;">

          <div style="margin-bottom: 14px;">
              <img src="${constant.LOGO}/assets/img/logoImg.png" style="width: 107px;" />
          </div>
          <div>
              <p style="font-size: 15px;"><b>Today attendance</b></p>
              <p style="font-size: 15px;">Please Download the below attachment to view.</p>

          </div >
          <div style=" margin-top:50px">

  <p style="font-size: 15px; color: gray; padding-bottom: 15px; ">© 2023 JC Software Solution  All rights reserved.</p>
  </div>

      </div >
  </div > `

  //var to = ["mailto:chander@yopmail.com", "mailto:amit@yopmail.com"];
  SmtpController.sendInvitationEmail(
    toemail,
    "Monthly Attendance",
    message,
    attachment
  );
};

SendSalaryEmail = function (options) {
  var toemail = options.toemail;
  var attachment = options.attachment;
  (message = "");
  message += ` <div class="maindiv" style="align-items: center; box-shadow: 0px 0px 8px 0px #8080808a; height: auto; background-color: #fff; 
  width:600px;
  margin:auto;">

      <div style="border: 1px solid #ff8f26cc ;    margin:2rem auto; max-width: 600px; height: auto; background-color: #ff8f26cc ;  text-align: center;">

          <h3 style="font-size: 25px; color: #fff;">Hello Admin</h3>

      </div>
      <div style="text-align: center;">

          <div style="margin-bottom: 14px;">
              <img src="${constant.LOGO}/assets/img/logoImg.png" style="width: 107px;" />
          </div>
          <div>
              <p style="font-size: 15px;"><b>Today attendance</b></p>
              <p style="font-size: 15px;">Please Download the below attachment to view.</p>

          </div >
          <div style=" margin-top:50px">

  <p style="font-size: 15px; color: gray; padding-bottom: 15px; ">© 2023 JC Software Solution  All rights reserved.</p>
  </div>

      </div >
  </div > `

  //var to = ["mailto:chander@yopmail.com", "mailto:amit@yopmail.com"];
  SmtpController.sendInvitationEmail(
    toemail,
    "Monthly Attendance",
    message,
    attachment
  );
};
