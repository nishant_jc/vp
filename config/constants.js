

module.exports.constants = {
  user: {
    //Registration and Login
    USERNAME_REQUIRED: "Email is required",
    FIRSTNAME_REQUIRED: "Firstname is required",
    LASTNAME_REQUIRED: "Lastname is required",
    PHONE_REQUIRED: "Phone number is required",
    EMAIL_REQUIRED: "Email is required",
    EMAIL_EXIST: "Email-Id already exists.",
    WRONG_EMAIL: "Email-Id does not exists",
    PASSWORD_REQUIRED: "Password is required",
    UNVERIFIED: "You have not verified your account. Please verify",
    USERNAME_NOT_APPROVED: "You have not approved by the admin",
    SUCCESSFULLY_REGISTERED: "Successfully registered",
    USER_SUCCESSFULLY_REGISTERED: "Successfully registered.",
    SUCCESSFULLY_LOGGEDIN: "Successfully logged in",
    WRONG_USERNAME: "Username does not exists",
    WRONG_PASSWORD: "Password is wrong!",
    CURRENT_PASSWORD: "Current Password is wrong!",
    INVALID_USER: "Invalid User. Your email does not exist to our system.",
    ALREADY_VERIFIED:
      "You have already verified your email. Please login to website.",
    ERROR_MAIL: "There is some error to send mail to your email id.",
    LINK_MAIL: "Link for reset password has been sent to your email id.",
    PASSWORD_CHANGED: "Password has been changed",
    ROLE_REQUIRED: "Role is required.",
    USERNAME_ALREADY: "Username is already exits.",
    USERNAME_INACTIVE: "User is inactive",
    CONPASSWORD_REQUIRED: "Confirm Password is required",
    CURRENTPASSWORD_REQUIRED: "Current Password is required",
    CONFIRM_PASSWORD_NOTMATCH: "Confirm Password is not Match",
    ID_REQUIRED: "id Is Required",
    UPDATED_USER: "User updated successfully.",
    USERNAME_VERIFIED: "User is not verified.",
    INVALID_CRED: "Invalid login credentials.",
    OTP_SENT: "OTP sent on email successfully.",
    ERROR_OCCUR: "Some error occur.Please try again.",
    INVALID_OTP: "OTP is not valid.",
    DELETED_USER: "User profile deleted successfully",
    NO_RESULT: "No user found.",
    NO:"No user login",
    INVALID_IMAGE: "please select valid image to upload",
    INVALID_EMAIL:"Invalid email."
  },

  Roles: {
    ROLE_ALREADY_EXIST: "Role already exists.",
    SAVED_ROLE: "Role saved successfully.",
    UPDATED_ROLE: "Role updated successfully.",
    ID_REQUIRED: "Role id required.",
    ROLE_REQUIRED: "Role name is required",
    ROLES_SUCCESSFULLY_CREATED: "Role added successfully",
  },

  team: {
    TEAM_ALREADY_EXIST: "Team already exists.",
    TEAM_SAVED: "Team saved successfully.",
    UPDATED_TEAM: "Team updated successfully.",
    ID_REQUIRED: "Team id required.",
    TEAM_NAME_REQUIRED: "Team name is required.",
    TEAM_SUCCESSFULLY_CREATED: "Team added successfully.",
    TEAM_ID_NOT_FOUND: "Team not found.",
    DELETED_TEAM: "Team deleted Successfully.",
    NO_RESULT: "No result found.",
    USER_ID_REQUIRED: "User id required.",
    TEAM_NOT_EXIST: "Team not exist in project.",
    REMOVED_TEAM_SUCCESS: "Team remove successfully from project.",
  },
  jobplatform: {
    PLATFORM_NAME_REQUIRED: "Name is required.",
    PLATFORM_ALREADY_EXIST: "Name is already exist.",
    PLATFORM_SAVED: "Job platform added successfully.",
    NO_RESULT: "No job platform found.",
    DELETED_PLATFORM: "Job platform deleted successfully.",
    ID_NOT_FOUND: "ID not found.",
    ID_REQUIRED: "Platform id required.",
    UPDATED_PLATFORM: "Platform updated successfully.",
  },
  category: {
    NAME_REQUIRED: "Category name required.",
    CATEGORY_ALREADY_EXIST: "Category already exist.",
    CATEGORY_SAVED: "Category saved successfully.",
    UPDATED_CATEGORY: "Category updated successfully.",
    ISSUE_IN_UPDATE: "Issue in category update.",
    ID_REQUIRED: "Category id required.",
    DELETED: "Category deleted successfully.",
  },

  projects: {
    PROJECT_NAME_REQUIRED: "Project name is required.",
    PROJECT_ALREADY_EXIST: "Project already exist.",
    PROJECT_SAVED: "Project added successfully.",
    GET_DATA: "Project data fetch successfully.",
    ID_REQUIRED: "Project id required.",
    NO_RESULT: "No project found.",
    UPDATED_PROJECT: "Project is updated successfully.",
    DELETED_PROJECT: "Project deleted successfully.",
    REMOVE_MEMBER: "Member remove from project successfully.",
    USER_ID_REQUIRED: "Member id required.",
    MEMBER_NOT_EXIST: "Member not exist in project.",
    REMOVED_MEMBER_SUCCESS: "Member remove successfully from project.",
    ASSIGN_MEMBER_PROJECT: "member is assign to project successfully.",
    MODEL_NAME_REQUIRED:"Model name require."
  },
  leaves: {
    LEAVES_SAVED: "Leaves added successfully.",
    GET_DATA: "leaves data fetch successfully.",
    ID_REQUIRED: "Leave id required.",
    UPDATED_LEAVE_STATUS: "Leave status updated successfully.",
    CURRENT_USER_LEAVES: "Current user leaves fetch  successfully.",
    NO_RESULT: "No leave found.",
  },
  attendance: {
    USERID_REQUIRED: "User Need to be login.",
    CHECKINTIME_REQUIRED: "CheckIn time required.",
    CHECKIN_SAVED: "CheckIn time saved successfully.",
    CHECKOUT_SAVED: "CheckOut time saved successfully.",
    ATTENDANCE_ID_REQUIRED: "Attendance Id required.",
    TOTALTIME: "Today's total time.",
    NO_RESULT: "No attendance found.",
    DATA_FECTCH: "Data fetch successfully.",
    CURRENT_ATTENDANCE: "Current day attendance fetch successfully.",
    NO_CURRENT_ATTENDANCE: "No attendance found",
    ALL_CHEKIN_ATTENDANCE:"All checkin employees data fetch successfully.",
    ALL_CHEKOUT_ATTENDANCE:"All checkout employees data fetch successfully.",
    ABSENT_EMPLOYEE_LIST_FOUND:"All absent employees data fetched successfully.",
    NOT_FOUND:"User not found",
    FAILED:"Failed to checkout.Maybe attendanceId is wrong"

  },
  resign: {
    ID_REQUIRED: "Resignation id required.",
    UPDATED_RESIGN_STATUS: "Resignation status updated successfully.",
    RESIGN_SAVED: "Resignation saved successfully.",
    CANCEL_RESIGN_STATUS: "Your resignation is cancelled successfully.",
    GET_DATA: "Resignation data fetch successfully",
    CHECK_RESIGN_STATUS:"You are allready applied",
    NO_RESULT: "No resignation found.",
  },
  knowledgeshare: {
    POST_ALREADY_EXIST: "Post already exist.",
    POST_SAVED: "Post Added successfully.",
    GET_DATA: "Post data fetch successfully.",
    ID_REQUIRED: "Post id required",
    NO_RESULT: "No post found",
    DELETED_POST: "Post deleted successfully.",
    UPDATED_POST: "Post updated successfully.",
  },
  task: {
    "MISSING": "Payload is missing.",
    "EXIST": "Task already exist.",
    "ADD": "Task added successfully.",
    "UPDATED": "Task updated successfully.",
    "DELETED": "Task removed successfully.",
    "NOT_EXIST": "Task not exist.",
  },
  analatics:{
    NOTEXIST:"Employee not Exist.",
    SUCCESS:"Count Employee successfully."
  },


  Appraisal:{
    APPRAISAL_SAVED: "Appraisal added successfully.",
    UPDATED_APPRAISAL: "Appraisal updated successfully.",
    GET_DATA: "Appraisal data fetch successfully.",
    NO_RESULT: "No Appraisal found",
    ID_REQUIRED: "Appraisal id required",
    INVALID_ID:"Invalid id.",
    DELETED_POST: "Appraisal deleted successfully.",
  },
  Contentmanagement:{
    CONTENT_SAVED: "Content saved successfully.",
    CHECK_CONTENT_STATUS:"This Content allready exist",
    UPDATED_CONTENT:"Content updated successfully.",
    GET_DATA: "Content data fetch successfully.",
    ID_REQUIRED: "Content id required.",
    NO_RESULT: "No Content found.",
  },

  Testimonials:{
    CONTENT_SAVED: "Testimonial saved successfully.",
    CHECK_CONTENT_STATUS:"This Testimonial allready exist",
    UPDATED_CONTENT:"Testimonial updated successfully.",
    GET_DATA: "Testimonial data fetch successfully.",
    ID_REQUIRED: "Testimonial id required.",
    NO_RESULT: "No Testimonial found.",
    DELETED_TESTIMONIALS:" Testimonial deleted successfully"
  },

  Service:{
    SERVICE_SAVED: "Service saved successfully.",
    CHECK_SERVICE_STATUS:"This Service allready exist",
    UPDATED_SERVICE:"Service updated successfully.",
    GET_DATA: "Service data fetch successfully.",
    ID_REQUIRED: "Service id required.",
    NO_RESULT: "No Service found.",
    DELETED_SERVICE:" Service deleted successfully"
  },

  Technology:{
    TECHNOLOGY_SAVED: "Technology saved successfully.",
    CHECK_TECHNOLOGY_STATUS:"This Technology allready exist",
    UPDATED_TECHNOLOGY:"Technology updated successfully.",
    GET_DATA: "Technology data fetch successfully.",
    ID_REQUIRED: "Technology id required.",
    NO_RESULT: "No Technology found.",
    DELETED_TECHNOLOGY:" Technology deleted successfully"
  },

  Weeklyplan:{
    WEEKLYPLAN_SAVED: "Weeklyplan saved successfully.",
    CHECK_WEEKLYPLAN_STATUS:"This Weeklyplan allready exist",
    UPDATED_WEEKLYPLAN:"Weeklyplan updated successfully.",
    GET_DATA: "Weeklyplan data fetch successfully.",
    ID_REQUIRED: "Weeklyplan id required.",
    NO_RESULT: "No Weeklyplan found.",
    DELETED_WEEKLYPLAN:" Weeklyplan deleted successfully"
  },

  Portfolio:{
    PORTFOLIO_SAVED: "Portfolio saved successfully.",
    CHECK_PORTFOLIO_STATUS:"This Portfolio allready exist",
    UPDATED_PORTFOLIO:"Portfolio updated successfully.",
    GET_DATA: "Portfolio data fetch successfully.",
    ID_REQUIRED: "Portfolio id required.",
    NO_RESULT: "No Portfolio found.",
    INVALID_ID:"Invalid Id.",
    ALL_PORTFOLIO:"All portfolio found successfully.",
    DELETED_PORTFOLIO:" Portfolio deleted successfully"
  },

  ContactUs:{
    CONTACTUS_SAVED: "ContactUs saved successfully.",
    CHECK_CONTACTUS_STATUS:"This ContactUs allready exist",
    UPDATED_CONTACTUS:"ContactUs updated successfully.",
    GET_DATA: "ContactUs data fetch successfully.",
    ID_REQUIRED: "ContactUs id required.",
    NO_RESULT: "No ContactUs found.",
  },

  DailyTask:{
    DAILYTASK_SAVED: "DailyTask saved successfully.",
    CHECK_DAILYTASK_STATUS:"This DailyTask allready exist",
    UPDATED_DAILYTASK:"DailyTask updated successfully.",
    GET_DATA: "DailyTask data fetch successfully.",
    ID_REQUIRED: "DailyTask id required.",
    NO_RESULT: "No DailyTask found.",
    DELETED_DAILYTASK:" DailyTask deleted successfully"
  },

  HrExpenses:{
    EXPENSES_SAVED: "HrExpenses saved successfully.",
    CHECK_EXPENSES_STATUS:"This HrExpenses allready exist",
    UPDATED_EXPENSES:"HrExpenses updated successfully.",
    GET_DATA: "HrExpenses data fetch successfully.",
    ID_REQUIRED: "HrExpenses id required.",
    NO_RESULT: "No HrExpenses found.",
    DELETED_EXPENSES:" HrExpenses deleted successfully"
  },

  PROJECT_LEADS:{
    PROJECT_LEADS_ADDED:"Project leads data added successfully.",
    ID_REQUIRED: "id required.",
    INVALID_ID:"Invalid id.",
    GET_DATA:"Project leads Data found succesfully.",
    UPDATED_PROJECT_LEADS:"Project leads updated successfully.",
    GET_ALL_DATA:"All Project leads found successfully.",
    DELETED_DATA:" Project leads deleted successfully"
  },

  PORTFOLIO_SHARE:{
    PORTFOLIO_SAVED:"Portfolio saved successfully.",
    PORTFOLIO_KEY_DATA:"Portfolio data fetch successfully.",
    KEY_REQUIRED:"Key required.",
    INVALID_KEY:"Invalid key.",
    EXPIRY_TIME:"Time is Expire."
    
  },
  SYSTEM_MANAGEMENT:{
    SYSTEM_SAVE:"System added successfully.",
    ID_REQUIRED:"Id required.",
    INVALID_ID:"Invalid id.",
    GET_SINGLE_SYSTEM:"Fetch system data successfully.",
    GET_ALL:"Fetch all system data successfully.",
    UPDATED_SYSTEM:"System updated successfully.",
    DELETED_SYSTEM:"System deleted successfully"
  },
  INVENTORY:{
    INVENTORY_SAVE:"Inventory added successfully.",
    ID_REQUIRED:"Id required.",
    INVALID_ID:"Invalid id.",
    GET_SINGLE_INVENTORY:"Fetch Inventory data successfully.",
    GET_ALL:"Fetch all Inventory data successfully.",
    UPDATED_INVENTORY:"Inventory updated successfully.",
    DELETED_INVENTORY:"Inventory deleted successfully"
  },
  // assignLaptop:{
  //   LAPTOP_REQUIRED:"Laptop  field is require.",
  //   EXIST:"Laptop already assign.",
  //   ASSIGN:"Laptop assigned successfully.",
  //   ID_MISSING:"Id is missing.",
  //   NOT_FOUND:"Record not found.",
  //   UPDATED:"Laptop updated successfully.",
  //   DETAIL:"Laptop Detail successfully get.",
  //   DELETED:"Laptop successfully removed",

  // },
  projectReview:{
    EXIST:"Project review already exist.",
    ADD:"Project review added successfully.",
    ID_MISSING:"Project review id missing.",
    NOT_FOUND:"Project review not found.",
    UPDATED:"Project review updated successfully.",
    DELETE:"Project review deleted successfully.",
  },
  reviewTask:{
    ADD:"Review added successfully",
    EXIST:"Project review already exist.",
    ID_MISSING:"Task review id missing.",
    NOT_FOUND:"Task review not found.",
    UPDATED:"Project review updated successfully.",
    DELETE:"Project review deleted successfully.",

  },
  interview:{
    ADD:"Interview added successfully",
    EXIST:"Interview already exist.",
    ID_MISSING:"Interview id missing.",
    NOT_FOUND:"Interview not found.",
    UPDATED:"Interview updated successfully.",
    DELETE:"Interview deleted successfully.",

  },
  complain:{
    ADD:"Complain added successfully",
    EXIST:"Complain already exist.",
    ID_MISSING:"Complain id missing.",
    NOT_FOUND:"Complain not found.",
    GET_SINGLE_COMPLAIN:"Fetch Complain successfully.",
    UPDATED:"Complain updated successfully.",
    DELETE:"Complain deleted successfully.",
    ID_REQUIRED:"Id required.",
    INVALID_ID:"Invalid id.",
  },
  HrWallet:{
    ADD_MONEY_SUCCESS: "Money added Successfully",
    DETAILS_FOUND: "Details found Successfully"
  },
  Todo:{
    DETAILS_FOUND: "Details found Successfully",
    ADD_LIST_SUCCESS: "Todo list added successfully",
    REQUIRED: "Id is Required",
    UPDATED:"Todo list updated successfully",
    DELETED:"Todo list deleted successfully"
  },
  Holidays:{
    EVENT_REQUIRED: "Event is required",
    START_DATE_REQUIRED: "start_date is required",
    END_DATE_REQUIRED : "end_date is required",
    ADD_SUCCESS : "Holiday details added successfully",
    ADD_FAILED : "Failed to add holiday data.Something went wrong",
    ID_REQUIRED : "id is required",
    FOUND : "Details found successfully",
    FAILED : "Failed to find details.Maybe id is wrong",
    ID_NOT_FOUND : "id not found",
    UPDATE_SUCCESS : "Holiday details updated successfully",
    UPDATE_FAILED : "Failed to update data.Maybe id is wrong",
    DELETE_SUCCESS : "Holiday data deleted successfully",
    DELETE_FAILED : "Failed to delete holiday data.Maybe id is wrong"
  },
  Announcement:{
    SUBJECT_REQUIRED: "Subject is required",
    DESCRIPTION_REQUIRED: "Description is required",
    ADD_SUCCESS : "Announcement details added successfully",
    ADD_FAILED : "Failed to add announcement details.Something went wrong",
    ID_REQUIRED : "id is required",
    FOUND : "Details found successfully",
    FAILED : "Failed to find details.Maybe id is wrong",
    ID_NOT_FOUND : "id not found",
    UPDATE_SUCCESS : "Announcement details updated successfully",
    UPDATE_FAILED : "Failed to update data.Maybe id is wrong",
    DELETE_SUCCESS : "Announcement data deleted successfully",
    DELETE_FAILED : "Failed to delete Announcement data.Maybe id is wrong"
  },
  Device:{
    DEVICE_NAME_REQUIRED: "Device name is required",
    DEVICE_TYPE_REQUIRED: "Device type is required",
    OS_TYPE_REQUIRED: "OS type is required",
    SERIAL_NO_REQUIRED: "Serial no is required",
    DEVICE_NAME_ALREADY_EXIST: "Failed to add device.This device name already exist",
    SERIAL_NO_ALREADY_EXIST: "Failed to add device.This serial_no already exist",
    ALREADY_EXIST: "Failed to add device.This device already exist",
    // START_DATE_REQUIRED: "start_date is required",
    // END_DATE_REQUIRED : "end_date is required",
    ADD_SUCCESS : "Device details added successfully",
    ADD_FAILED : "Failed to add device data.Something went wrong",
    ID_REQUIRED : "id is required",
    FOUND : "Details found successfully",
    FAILED : "Failed to find details.Maybe id is wrong",
    ID_NOT_FOUND : "id not found",
    UPDATE_SUCCESS : "Device details updated successfully",
    UPDATE_FAILED : "Failed to update data.Maybe id is wrong",
    DELETE_SUCCESS : "Device data deleted successfully",
    DELETE_FAILED : "Failed to delete device data.Maybe id is wrong"
  },
  Notification:{
    NOTIFICATION_TYPE_REQUIRED: "notification_type is required",
    NOTIFICATION_REQUIRED: "notification is required",
    DATE_REQUIRED : "date is required",
    ADD_SUCCESS : "Notification details added successfully",
    ADD_FAILED : "Failed to add notification data.Something went wrong",
    // ID_REQUIRED : "id is required",
    // FOUND : "Details found successfully",
    // FAILED : "Failed to find details.Maybe id is wrong",
    // ID_NOT_FOUND : "id not found",
    // UPDATE_SUCCESS : "Holiday details updated successfully",
    // UPDATE_FAILED : "Failed to update data.Maybe id is wrong",
    // DELETE_SUCCESS : "Holiday data deleted successfully",
    // DELETE_FAILED : "Failed to delete holiday data.Maybe id is wrong"
  },

  assignDevice:{
    DEVICE_NAME_REQUIRED:"Device name field is required.",
    SERIAL_NO_REQUIRED:"Serial no. field is required.",
    DEVICE_TYPE_REQUIRED:"Device type field is required.",
    EXIST:"Device already assign.",
    ASSIGN:"Device assigned successfully.",
    ID_MISSING:"Id is missing.",
    NOT_FOUND:"Record not found.",
    UPDATED:"Device updated successfully.",
    DETAIL:"Device Detail successfully get.",
    DELETED:"Device successfully removed",

  },

  Comments:{
    KNOWLEDGE_SHARE_ID_REQUIRED : "knowledge_share_id is required",
    ADD_SUCCESS : "Comment added successfully",
    ADD_FAILED : "Failed to add comment.Something went wrong",
    ID_REQUIRED : "Id is required",
    FOUND : "Details found successfully",
    FAILED : "Failed to find details.Maybe id is wrong",
    ID_NOT_FOUND : "id not found",
    UPDATE_SUCCESS : "Comment updated successfully",
    UPDATE_FAILED : "Failed to update data.Maybe id is wrong",
    DELETE_SUCCESS : "Comment deleted successfully",
    DELETE_FAILED : "Failed to delete comment.Maybe id is wrong"
  },

  Salary:{
    SALARY_REQUIRED: "Salary is required",
    EMPLOYEE_ID_REQUIRED: "employeeId is required",
    ALREADY_EXIST: "salary already exist",
    ADD_SUCCESS : "Salary details added successfully",
    ADD_FAILED : "Failed to add salary data.Something went wrong",
    ID_REQUIRED : "id is required",
    FOUND : "Details found successfully",
    FAILED : "Failed to find details.Maybe id is wrong",
    ID_NOT_FOUND : "id not found",
    CANNOT_UPDATE_ID : "employeeId cannot be updated",
    UPDATE_SUCCESS : "Salary details updated successfully",
    UPDATE_FAILED : "Failed to update data.Maybe id is wrong",
    DELETE_SUCCESS : "Salary data deleted successfully",
    DELETE_FAILED : "Failed to delete salary data.Maybe id is wrong"
  },

  Target:{
    PROJECT_NAME_REQUIRED: "Project name is required",
    AMOUNT_REQUIRED: "Amount is required",
    DATE_REQUIRED : "Date is required",
    ADD_SUCCESS : "Target details added successfully",
    ADD_FAILED : "Failed to add target data.Something went wrong",
    ID_REQUIRED : "id is required",
    FOUND : "Details found successfully",
    FAILED : "Failed to find details.Maybe id is wrong",
    ID_NOT_FOUND : "id not found",
    UPDATE_SUCCESS : "Target details updated successfully",
    UPDATE_FAILED : "Failed to update data.Maybe id is wrong",
    DELETE_SUCCESS : "Target data deleted successfully",
    DELETE_FAILED : "Failed to delete target data.Maybe id is wrong"
  },

};
