/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {
  /***************************************************************************
   *                                                                          *
   * Default policy for all controllers and actions, unless overridden.       *
   * (`true` allows public access)                                            *
   *                                                                          *
   ***************************************************************************/
  "*": "isAuthorized",
  UserController: {
    // adminSignin: true,
    register: true,
    forgotPassword: true,
    resetPassword: true,
    userSignin: true,
    verifyUser: true,
    verifyEmail: true,
    checkEmail: true,
    autoLogin: true,
    verifiyOtp: true,
    admin_Signin:true,
  },

  RolesController: {
    getAllRoles: true,
  },
  LeavesController: {
    //updatestatus: true,
  },
  ResignationController: {
    updatestatus: true,
  },
  ContentmanagementController: {
    getAllcontents: true,
  },
  ServicesController: {
    getAllServices: true,
  },
  ContactUsController: {
    addContactUs: true,
    getAllContactUs: true,
    edit: true,
    getContactUs:true
  },
  TestimonialsController: {
    getAllTestimonials: true,
  },
  ProjectsController: {
    getprojectdocumentzip: true,
  },
  PortfolioShareController: {
    getPortfolioShare: true,
  },
  PortfolioNewController: {
    findSinglePortfolio: true,
  }
};
